<%-- 
    Document   : ManageAirplanes
    Created on : 02-Nov-2016, 23:54:11
    Author     : Jelle
--%>

<%@page import="Models.Domain.Message"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.AirplaneVM"%>
<%AirplaneVM VM = (AirplaneVM) request.getAttribute("VM"); %>
<%@page import="java.util.List"%>
<%@page import="Models.Domain.Airplane"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<% List<Airplane> Airplanes = VM.getAirplaneList(); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap();%>

<jsp:include page="../Includes/NavBarAirline.jsp"/>
<div class="col-md-10">
    <div class="well">
        <h2><%=varMap.get("Manage Airplanes")%></h2>
        <% for (Message m : VM.getListMessage()) {
                out.println("<p class='bg-" + m.getBootstrapStatus() + " message'>" + m.getMessage() + "</p>");
            }
        %> 
        <form action="AirplaneOverview" method="post">
            <div class="well">
                <label><%= varMap.get("planeCode")%>:</label>
                <input type="text" name="search"/>
                <!--<button type="submit" name="submit" class="btn btn-default" value="search"><%= varMap.get("Search")%></button>-->
                <button type="submit" name="submit" class="btn btn-default glyphicon glyphicon-search" value="search"
                        data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Search")%>"></button>
                <button type="submit" name="submit" class="btn btn-default" value="Create"><%=varMap.get("Create")%></button>
                <button type="submit" name="submit" class="btn btn-default" value="Cancel"><%=varMap.get("Cancel")%></button>
            </div>
            <table class="table table-striped">
                <tr>
                    <th><%=varMap.get("Code")%></th>
                    <th><%=varMap.get("Owner")%></th>
                    <th><%=varMap.get("Status")%></th>
                    <th><%=varMap.get("Capacity")%></th>
                    <th><%=varMap.get("Action")%></th>
                </tr>
                <% for (Airplane a : Airplanes) {%>
                <tr>
                    <td><%=a.getCode()%> </td>
                    <td><%=a.getOwner().getName()%></td>
                    <td><%=a.getStatus()%> </td>
                    <td><%=a.getCapacity()%> </td>
                    <td>
                        <!--<button type="submit" name="submit" class="btn btn-default" value="edi<%=a.getID()%>"><%=varMap.get("Edit")%></button>-->
                        <button type="submit" name="submit" class="btn btn-default glyphicon glyphicon-pencil" value="edi<%=a.getID()%>"
                                data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Edit")%>"></button>
                        <!--<button type="submit" onclick="clicked(event)" name="submit" class="btn btn-default glyphicon glyphicon-remove" value="del<%=a.getID()%>"><%=varMap.get("Delete")%></button>-->
                        <button type="submit" onclick="clicked(event)" name="submit" class="btn btn-default glyphicon glyphicon-remove" value="del<%=a.getID()%>"
                                data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Delete")%>"></button>
                        <!--<button type="submit" name="submit" class="btn btn-default" value="det<%=a.getCode()%>"><%=varMap.get("Details")%></button>-->
                        <button type="submit" name="submit" class="btn btn-default glyphicon glyphicon-eye-open" value="det<%=a.getCode()%>"
                                data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Details")%>"></button>
                        <!--<a href="AirplaneHangar?ID=<%=a.getID()%>" class="btn btn-default"><%=varMap.get("ToHangar")%></a>-->
                        <a href="AirplaneHangar?ID=<%=a.getID()%>" class="btn btn-default glyphicon glyphicon-wrench"
                           data-toggle="tooltip" data-placement="left" title="<%= varMap.get("ToHangar")%>"></a>
                    </td>      
                </tr>
                <%}%>      
            </table>
            </br>
            

        </form>
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
<script>
function clicked(e)
{
    if(!window.confirm("<%=varMap.get("ConfirmDelete")%>")){e.preventDefault()};
}

</script> 