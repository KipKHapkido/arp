<%-- 
    Document   : AirplaneHangar
    Created on : 26-Nov-2016, 23:37:14
    Author     : Jelle
--%>

<%@page import="Models.Domain.Hangar"%>
<%@page import="Models.Domain.Airplane"%>
<%@page import="Models.ViewModel.AirplaneVM"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>

<% AirplaneVM AVM = (AirplaneVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) AVM.getVarMap(); %>
<jsp:include page="../Includes/NavBarAirline.jsp"/>
<div class="col-md-8">     
    <div class="well">
        <h2><%=varMap.get("ToHangar")%></h2>
        <% Airplane plane = AVM.getAirplane();%>
        <form method="Post" Action="AirplaneHangar">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td><%= varMap.get("Code")%></td>
                    <td><%= varMap.get("Hangar")%></td>
                </tr>
            </thead>
            <tr>
                <td><%= plane.getCode().toString()%></td>
                <td>
                    <select class="form-control" name="hangar">
                        <option value="" disabled selected hidden><%=varMap.get("PHhangar")%></option>
                        <% for (Hangar h : AVM.getHangarList()) {%>
                        <option class="form-control" value="<%=h.getID()%>"
                                <% if (plane.getHangarID() == h.getID()) {%>selected="selected"<%}%>
                                ><%=h.getName()%></option>
                        <% }%>
                    </select> 
                </td>
            </tr>
        </table>
        <input type="hidden" name="id" value="<%= AVM.getAirplane().getID()%>"/>

        
            <button type="submit" name="submit" class="btn btn-default" value="save"><%=varMap.get("Save")%></button>
            <button type="submit" name="submit" class="btn btn-default" value="cancel"><%=varMap.get("Cancel")%></button>
        </form>
    </div>
</div>

<jsp:include page="../Includes/Footer.jsp"/>