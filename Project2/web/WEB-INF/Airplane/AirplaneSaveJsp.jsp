<%-- 
    Document   : UpdateAirplaneJsp
    Created on : 04-Nov-2016, 01:46:31
    Author     : Jelle
--%>


<%@page import="Models.Domain.AirplaneState"%>
<%@page import="Models.Domain.Message"%>
<%@page import="java.util.Map"%>
<%@page import="Models.Domain.Owner"%>
<%@page import="Models.ViewModel.AirplaneVM"%>
<%@page import="Models.Domain.Airplane"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->

<% AirplaneVM vm = (AirplaneVM) request.getAttribute("VM");%>
<% Map<String, String> varMap = (Map<String, String>) vm.getVarMap();%>
<jsp:include page="../Includes/NavBarAirline.jsp"/>
<div class="col-md-10">
    <div class="well col-lg-6">
        <h2><%=varMap.get("Update airplane")%></h2>
        <% for (Message m : vm.getListMessage()) {
             out.println("<p class='bg-"+ m.getBootstrapStatus() + " message'>" + m.getMessage() + "</p>");
            }
        %> 
            <form action="AirplaneSave" method="post" class="form-horizontal">
                <input type="hidden" name="id" value="<%= vm.getAirplane().getID()%>"/>
                <label class="control-label"><%=varMap.get("Code")%></label>
                <input class="form-control" type="text" name="code" value="<%= vm.getAirplane().getCode()%>" placeholder="<%=varMap.get("PHcode")%>"/>  </br>            
                <label class="control-label"><%=varMap.get("Owner")%></label>
                <select class="form-control" name="owner">
                    <option value="" disabled selected hidden><%=varMap.get("PHowner")%></option>
                    <% for (Owner o : vm.getOwnersList()) {%>
                    <option class="form-control" value="<%=o.getID()%>"
                            <% if (o.getID() == vm.getAirplane().getOwnerID()) {%>selected="selected"<%}%>
                            ><%=o.getName()%></option>
                    <% }%>
                </select> </br>
                <label class="control-label"><%=varMap.get("Status")%></label>
                <select class="form-control" name="state">
                    <option value="" disabled selected hidden><%=varMap.get("PHstatus")%></option>
                    <% for (AirplaneState as : vm.getStateList()) {%>
                    <% if (as.getID()!= 2) { %>
                        <option class="form-control" value="<%=as.getID()%>"
                            <% if (as.getID() == vm.getAirplane().getAirplaneStateID()) {%>selected="selected"<%}%>
                            ><%=as.getState()%></option>
                    <%} }%>
                </select> </br>
                <label class="control-label"><%=varMap.get("Capacity")%></label>
                <input class="form-control" type="text" name="capacity" value="<%= vm.getAirplane().getCapacity()%>" placeholder="<%=varMap.get("PHcapacity")%>"/>  </br>

                <button type="submit" name="submit" class="btn btn-default" value="0"><%=varMap.get("Save")%></button>
                <button type="submit" name="submit" class="btn btn-default" value="-1"><%=varMap.get("Cancel")%></button>
            </form>
        
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
