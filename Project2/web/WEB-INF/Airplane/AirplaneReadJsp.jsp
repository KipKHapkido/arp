<%-- 
    Document   : Airplane
    Created on : 12-Nov-2016, 10:39:43
    Author     : Dries
--%>

<%@page import="Models.Domain.Flight"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<%@page import="Models.ViewModel.AirplaneVM"%>
<%@page import="Models.Domain.Airplane"%>
<%@page import="Models.Domain.Airport"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->

<% AirplaneVM AVM = (AirplaneVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) AVM.getVarMap();%>
<jsp:include page="../Includes/NavBarAirline.jsp"/>
<div class="col-md-10">
    <div class="well">

        <h2><%= varMap.get("PlaneDetails")%></h2>

        <%
            Airplane plane = AVM.getAirplane();
            List<Airplane> planes = new ArrayList<>();
            List<Flight> flightOverview = AVM.getFlightOverview();
            
            int previous = AVM.getPrevious();
            int next = AVM.getNext();
            
            if (plane != null) {
                planes.add(plane);
            }

            if (planes.isEmpty()) {%>
        <%= varMap.get("Unknown")%>
        <% } else {%>
        <table class="table table-striped">
            <thead>
                <tr>
                    <td><%= varMap.get("Code")%></td>
                    <td><%= varMap.get("Capacity")%></td>
                    <td><%= varMap.get("Owner")%></td>
                    <td><%= varMap.get("Builder")%></td>
                    <td><%= varMap.get("Type")%></td>
                    <td><%= varMap.get("BuildYear")%></td>
                </tr>
            </thead>
            <% for (Airplane a : planes) {%>
            <tr>
                <td><%= a.getCode().toString()%></td>
                <td><%= a.getCapacity()%></td>
                <td><%= a.getOwner().getName()%></td>
                <td><%= a.getBuilder()%></td>
                <td><%= a.getType()%></td>
                <td><%= a.getYear()%></td>
            </tr>
            <% } %>
        </table>
        <% }
        %>

        <% if (plane.getHangar() != null) {%>
        <h2><%= varMap.get("Status")%></h2>

        <table class="table table-striped">
            <thead>
            <th><%= varMap.get("State") %></th>
                <th><%= varMap.get("HangarCode") %></th>
                <th><%= varMap.get("Airport") %></th>
            </thead>
            <tr>
                <td><%= plane.getAirplaneState() %></td>
                <td><%= plane.getHangar().getName() %></td>
                <td><%= plane.getHangar().getAirportObj().getName() %></td>
            </tr>
        </table>

        <% }%>

        <h2><%= varMap.get("Flights")%></h2>

        <% if (flightOverview != null && flightOverview.size() > 0) {%>
        <form action="AirplaneOverview" method="post">
            <label>
                <!-- previous flights message -->
                <%= varMap.get("FlightsPreviousLabel") %>
                <% if (0 < previous && previous < 5){ %>
                    <% String outs  = varMap.get("All").substring(0, 1).toUpperCase() + varMap.get("All").substring(1); %>
                    <%= varMap.get("FlightsPrevious").replace("@",outs) %>
                <% } else if(previous < 1) { %>
                    <%= varMap.get("FlightsNoPrevious") %>
                <% } else { %>
                    <%= varMap.get("FlightsPrevious").replace("@", previous + "") %>
                <% } %>
                </br>
                <!-- upcoming flights message -->
                <%= varMap.get("FlightsNextLabel") %>
                <% if(previous < flightOverview.size()){ %>
                    <%= varMap.get("FlightsNext1") %>
                    <% int max = flightOverview.size()-previous; %>
                        <% if(previous < 1){ %>
                            <input type="number" class="form-group-sm" name="next" value="<%= next %>" min="1" max="<%= max %>" />
                        <% }else{ %>
                            <input type="number" class="form-group-sm" name="next" value="<%= next %>" min="0" max="<%= max %>" />
                        <% } %>
                        <% if(flightOverview.size() - previous <= next+1) { %>
                            <%= "/" + (flightOverview.size()-previous) %>
                        <% } %>
                    <%= varMap.get("FlightsNext2") %>
                <% }else{ %>
                    <%= varMap.get("FlightsNoNext") %>
                <% } %>
            </label>
            </br>
            <button type="submit" name="submit" class="btn btn-default" value="det<%=AVM.getAirplane().getCode()%>"><%=varMap.get("Refresh")%></button>
        </form>

        <br>

        <table class="table table-striped">
            <thead>
            <th><%= varMap.get("FlightNR")%></th>
            <th><%= varMap.get("DepartDate")%></th>
            <th><%= varMap.get("Departure")%></th>
            <th><%= varMap.get("ArrivalDate")%></th>
            <th><%= varMap.get("ArrivalTime")%></th>
            <th><%= varMap.get("Departure")%></th>
            <th><%= varMap.get("Destination")%></th>
            <th><%= varMap.get("FlightState")%></th>
            </thead>
            <% 
                for (int i = 0; i < (previous + next) && i < flightOverview.size(); i++) {
                    Flight f = flightOverview.get(i);
                    Airport origin = f.getDepartAirport();
                    Airport destination = f.getDestinationAirport();
            %>
            <tr>
                <td>
                    <% if (f.getFlightNR() != null) {%>
                    <%= f.getFlightNR()%>
                    <%} else {%>
                    <%= varMap.get("Unknown")%>
                    <%}%>
                </td>
                <td>
                    <% if (f.getDepartDate() != null) {%>
                    <%= f.getDepartDate().toString()%>
                    <%} else {%>
                    <%= varMap.get("Unknown")%>
                    <%}%>
                </td>
                <td>
                    <% if (f.getDepartFrom() != null) {
                            if (f.getDepartFrom().getTimecol() != null) {%>
                    <%= f.getDepartFrom().getTimecol().toString()%>
                    <%  } else {%>
                    <%= varMap.get("Unknown")%>
                    <%  }
                    } else {%>
                    <%= varMap.get("Unknown")%>
                    <%}%>
                </td>
                <td>
                    <% if (f.getArrivalDate() != null) {%>
                    <%= f.getArrivalDate().toString()%>
                    <%} else {%>
                    <%= varMap.get("Unknown")%>
                    <%}%>
                </td>
                <td>
                    <% if (f.getArrivalTime() != null) {
                            if (f.getArrivalTime().getTimecol() != null) {%>
                    <%= f.getArrivalTime().getTimecol().toString()%>
                    <%  } else {%>
                    <%= varMap.get("Unknown")%>
                    <%  }
                    } else {%>
                    <%= varMap.get("Unknown")%>
                    <%}%>
                </td>
                <td>
                    <% if (origin != null) {%>
                    <%= origin.getName()%>
                    <%} else {%>
                    <%= varMap.get("Unknown")%>
                    <%}%>
                </td>
                <td>
                    <% if (destination != null) {%>
                    <%= destination.getName()%>
                    <%} else {%>
                    <%= varMap.get("Unknown")%>
                    <%}%>
                </td>
                <td>
                    <% if (f.getFlightstate() != null) {%>
                    <%= f.getFlightstate().getState()%>
                    <%} else {%>
                    <%= varMap.get("Unknown")%>
                    <%}%>
                </td>
            </tr>
            <% } %>
        </table>
        <% } else {%>
        <%= varMap.get("NoFlights")%>
        <% }%>

    </div>
</div>
<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
