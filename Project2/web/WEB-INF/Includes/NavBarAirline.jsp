<%-- 
    Document   : NavBarAirline
    Created on : 26-Nov-2016, 16:34:00
    Author     : Jelle
--%>

<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>

<% BaseVM VM = (BaseVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>
<div class="col-md-2">  
<div class="dropdown">    
        <a href="#" class="list-group-item" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><%=varMap.get("Passenger")%><span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="PassengerRead"><%=varMap.get("Searchandadjust")%></a></li>
                <li role="separator" class="divider"></li>
                <li><a href="CreatePassenger"><%=varMap.get("New")%></a></li>                       
            </ul> 
    </div>
    <div class="dropdown">  
        <a href="#" class="list-group-item" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><%=varMap.get("Flight")%><span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="SearchFlight"><%=varMap.get("Search")%></a></li>
                <li><a href="UpcomingFlight"><%=varMap.get("Upcoming")%></a></li>
                <li><a href="FlightReport"><%=varMap.get("Report")%></a></li>
                <li role="separator" class="divider"></li>
                <li><a href="CreateFlight"><%=varMap.get("New")%></a></li>
                <li><a href="SearchFlight"><%=varMap.get("Update")%></a></li>
            </ul>                     
    </div>
    <a href="AirplaneOverview" class="list-group-item"><%=varMap.get("Manage Airplanes")%></a> 
</div>