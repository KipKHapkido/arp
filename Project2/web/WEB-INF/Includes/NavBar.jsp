 
<%@page import="Logic.Helpers.UserObject"%>
<%@page import="java.util.Map"%>
<%@page import="Models.Datamodels.Language"%>
<%@page import="Models.ViewModel.BaseVM"%>
<%@page import="java.util.List"%>
<%@page import="Models.Datamodels.Navigation"%>
<%-- 
    Document   : NavBar
    Created on : 23-okt-2016, 14:04:12
    Author     : Sven Vervloet
--%>
<% BaseVM VM = (BaseVM) request.getAttribute("VM"); %>
<% UserObject userObject = (UserObject)session.getAttribute("userObject"); %>
<% List<Navigation> navBarLijst = (List<Navigation>) VM.getListNav(); %>
<% List<Language> ListLang = (List<Language>) VM.getListLang(); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>

<div class="container-fluid">
    <div class="row">
        <div class="loginInfo">
        <% if(userObject != null){ %>
            <label><%= userObject.getUsername() %> - <%= userObject.getFunctionDescription() %></label>
        <% } %>
        </div>
        <div class="languageBox">
            <% if (ListLang != null) { %>
            <form action="Language" method="post" class="form-inline">
                <div class="form-group">
                    <label for="language"><%= varMap.get("language") %></label>
                    <select name="language" class="form-control">
                    <% for (Language lang : ListLang) {%>
                    <option value="<%=lang.getID()%>" <% if (lang.getID() == VM.getLanguage()) { %>selected<% }%>><%=lang.getLname()%></option>
                    <% } %>
                    </select>
                </div>
                    <button type="submit" value="submit" class="btn btn-default"><%=varMap.get("SubmitLanguage")%></button>
            </form>
            <% } %>
        </div>
    </div>
</div>


<nav class="navbar navbar-default">
     <% if(userObject != null){ %>
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="Home">ARP 2016-2017</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <% for (Navigation nav : navBarLijst) {%>
                <li><a href="<%=nav.getUrl()%>"><%=nav.getName()%></a></li>
                    <% }%>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Account <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><form style="text-align:center" action="Logout" method="post"><button type="submit" class="btn btn-default"><%= varMap.get("LogOut") %></button></form></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
      <% } %>
</nav>

<div class="container-fluid">
    <div class="row-fluid">
