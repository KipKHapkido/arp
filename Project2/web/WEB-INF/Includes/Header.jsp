<%-- 
    Document   : Header
    Created on : 23-okt-2016, 14:03:16
    Author     : Sven Vervloet
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ARP 2016-2017 - ${pageName}</title>
    
    <link href="/Project2/Resources/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="/Project2/Resources/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="/Project2/Resources/css/main.css" rel="stylesheet" type="text/css"/>
    
    <script src="/Project2/Resources/js/jquery.js" type="text/javascript"></script>
    
</head>

<body>