<%-- 
    Document   : Messages
    Created on : 20-nov-2016, 20:22:05
    Author     : Sven Vervloet
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="Models.Domain.Message"%>
<%@page import="java.util.List"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% BaseVM VM = (BaseVM)request.getAttribute("VM"); %>

<% if (VM.getListMessage() != null) {
        for (Message m : VM.getListMessage()) {

            out.println("<div class='alert alert-" + m.getBootstrapStatus() + " message'>" + m.getMessage() + "</div>");

        }
    }
%> 