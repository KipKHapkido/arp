<%-- 
    Document   : Footer
    Created on : 23-okt-2016, 14:07:54
    Author     : Sven Vervloet
--%>

</div>
    </div>
    <div class="clearfix container-fluid">
        <div class="row-fluid">
            <div class="col-lg-12">
                &copy; ARP 2016-2017
            </div>
        </div>
    </div>
    
</body>
<script src="/Project2/Resources/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="/Project2/Resources/js/bootstrap.js" type="text/javascript"></script>
<script src="/Project2/Resources/js/jquery-ui.min.js" type="text/javascript"></script>
<script>
    setTimeout(function () {
        $(".message").slideUp("normal").promise().done(function () {
            $('.message').remove();
        });
    }, 4000);
    
     $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
    
</script>
</html>
