<%-- 
    Document   : NavBarAirport
    Created on : 26-Nov-2016, 17:50:10
    Author     : Jelle
--%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>

<% BaseVM VM = (BaseVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>
<div class="col-md-2">
    <div class="list-group">
        <a href="RunwayOverview" class="list-group-item"><%=varMap.get("Manage runway")%></a>        
        <a href="HangarOverview" class="list-group-item"><%=varMap.get("Manage hangar")%></a>
        
        <div class="dropdown">    
        <a href="#" class="list-group-item" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><%=varMap.get("manage_flights")%><span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="NewFlights"><%=varMap.get("Approve Flights")%></a></li>
                <li role="separator" class="divider"></li>
                <li><a href="IncomingFlights"><%=varMap.get("Incoming Flights")%></a></li>                    
            </ul> 
    </div>
    </div>
</div>