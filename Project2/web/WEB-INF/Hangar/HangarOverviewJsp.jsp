<%-- 
    Document   : HangarOverviewJsp
    Created on : 15-Nov-2016, 08:47:12
    Author     : Jelle
--%>

<%@page import="Models.ViewModel.RunwayVM"%>
<%@page import="Models.ViewModel.HangarVM"%>
<%@page import="Models.Domain.Hangar"%>
<%@page import="java.util.List"%>
<%@page import="Models.Domain.Message"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<% List<Hangar> hangars = (List<Hangar>) request.getAttribute("Hangars");%>
<% HangarVM VM = (HangarVM)request.getAttribute("VM"); %>
<% Map<String,String> varMap = (Map<String,String>)VM.getVarMap();%>
<jsp:include page="../Includes/NavBarAirport.jsp"/>
<div class="col-md-10">
    <div class="well">
        <h2><%=varMap.get("Manage hangar")%></h2>
        <% for (Message m : VM.getListMessage()) {
             out.println("<p class=\"bg-"+ m.getBootstrapStatus() + " message\">" + m.getMessage() + "</p>");
            }
        %> 
        <form action="HangarOverview" method="post">
        <table class="table table-striped">
            <tr>
                <th><%=varMap.get("Name")%></th>                
                <th><%=varMap.get("Airport")%></th>
                 <th><%=varMap.get("Capacity")%></th>
                <th><%=varMap.get("Action")%></th>
            </tr>
        <% for(Hangar h : hangars){%>
         <div class="form-group">
        <tr>
            <td><%=h.getName()%> </td>
            <td><%=h.getAirportObj().getName()%></td>
            <td><%=h.getCapacity()%></td>
            
           
            <td>
                <button type="submit" name="submit" class="btn btn-default glyphicon glyphicon-pencil" value="upd<%=h.getID()%>"
                        data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Edit")%>"></button>                
                <button type="submit" onclick="clicked(event)" name="submit" class="btn btn-default glyphicon glyphicon-remove" value="del<%=h.getID()%>"
                        data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Delete")%>"></button>
            </td>      
        </tr>
        <%}%> 
         </div>
       </table>
       
        <button type="submit" name="submit" class="btn btn-default" value="Create"><%=varMap.get("Create")%></button>
        <button type="submit" name="submit" class="btn btn-default confirm" value="Cancel"><%=varMap.get("Cancel")%></button>
        
        </form>
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
<script>
function clicked(e)
{
    if(!window.confirm("<%=varMap.get("DelAsk")%>")){e.preventDefault()};
}
</script> 
<!--function clicked(e)
{
    boolean proceed = $.confirm({        
    text:"<%=varMap.get("DelAsk")%>",
    confirm: function() {
        return true;
    },
    cancel: function() {
        return false;
    },
    confirmButton: "Yes I am",
    cancelButton: "No"});
    if(!proceed){e.preventDefault()};
}-->