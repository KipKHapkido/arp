<%-- 
    Document   : HangarSaveJsp.jsp
    Created on : 20-Nov-2016, 11:00:24
    Author     : Erik Michielsen
--%>

<%@page import="Models.Domain.Airport"%>
<%@page import="Models.Domain.Message"%>
<%@page import="java.util.*"%>
<%@page import="Models.ViewModel.RunwayVM"%>
<%@page import="Models.ViewModel.HangarVM"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<% HangarVM vm = (HangarVM) request.getAttribute("VM");%>
<% Map<String, String> varMap = (Map<String, String>) vm.getVarMap();%>
<jsp:include page="../Includes/NavBarAirport.jsp"/>
<div class="col-md-10">
    <div class="well col-lg-6">
        <h2><%=varMap.get("Update hangar")%></h2>
        <% for (Message m : vm.getListMessage()) {
             out.println("<p class='bg-"+ m.getBootstrapStatus() + " message'>" + m.getMessage() + "</p>");
            }
        %> 
            <form action="HangarSave" method="post" class="form-horizontal">
                
                <input type="hidden" name="id" value="<%= vm.getHangar().getID()%>"/>
                
                <div class="form-group">
                <label class="control-label"><%=varMap.get("Name")%></label>
                <input class="form-control" type="text" name="name" value="<%= vm.getHangar().getName()%>" placeholder="<%=varMap.get("PHname")%>"/>
                </div>
                 <div class="form-group">
                <label class="control-label"><%=varMap.get("Capacity")%></label>
                <input class="form-control" type="text" name="capacity" value="<%= vm.getHangar().getCapacity()%>" placeholder="<%=varMap.get("PHcapacity")%>"/> 
                 </div>
                  <div class="form-group">
                <label class="control-label"><%=varMap.get("Airport")%></label>
                <select class="form-control" name="airport">    
                <option value="" disabled selected hidden><%=varMap.get("PHairport")%></option>
                    <% for (Airport a : vm.getAirportList()) {%>
                    <option class="form-control" value="<%=a.getID()%>"
                            <% if (a.getID() == vm.getHangar().getAirportID()) {%>selected="selected"<%}%>
                            ><%=a.getName()%></option>
                    <% }%>
                </select>
                  </div>
                <button type="submit" name="submit" class="btn btn-default" value="0"><%=varMap.get("Save")%></button>
                <button type="submit" name="submit" class="btn btn-default" value="-1"><%=varMap.get("Cancel")%></button>
            </form>
        
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>

