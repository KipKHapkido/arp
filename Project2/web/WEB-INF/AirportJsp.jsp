<%-- 
    Document   : AirportJsp
    Created on : 25-okt-2016, 20:57:06
    Author     : Jelle Raus
--%>

<%@page import="java.util.List"%>
<%@page import="Models.Domain.Message"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.AirportVM"%>
<% AirportVM VM = (AirportVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String,String>) VM.getVarMap(); %>
<jsp:include page="Includes/Header.jsp"/>
<jsp:include page="Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<jsp:include page="Includes/NavBarAirport.jsp"/>
<div class="col-md-10">
   <div>
    <div class="well">
        <div class="text-center">
        <p><%=varMap.get("Airport")%></p>
        <img src="/Project2/Resources/images/AirportPic.png" class="img-rounded" alt="Airplane">
        
        </div>
    </div>
</div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="Includes/Footer.jsp"/>
