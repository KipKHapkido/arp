<%-- 
    Document   : HangarOverviewJsp.jsp
    Created on : 20-Nov-2016, 11:00:24
    Author     : Erik Michielsen
--%>
<%@page import="Models.ViewModel.RunwayVM"%>
<%@page import="Models.ViewModel.HangarVM"%>
<%@page import="Models.Domain.Airport"%>
<%@page import="Models.Domain.Message"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.RunwayVM"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<% RunwayVM vm = (RunwayVM) request.getAttribute("VM");%>
<% Map<String, String> varMap = (Map<String, String>) vm.getVarMap();%>
<jsp:include page="../Includes/NavBarAirport.jsp"/>
<div class="col-md-10">
    <div class="well col-lg-6">
        <h2><%=varMap.get("Update runway")%></h2>
        <% for (Message m : vm.getListMessage()) {
             out.println("<p class='bg-"+ m.getBootstrapStatus() + " message'>" + m.getMessage() + "</p>");
            }
        %> 
            <form action="RunwaySave" method="post" class="form-horizontal">
                <input type="hidden" name="id" value="<%= vm.getRunway().getID()%>"/>
                <label class="control-label"><%=varMap.get("Name")%></label>
                <input class="form-control" type="text" name="name" value="<%= vm.getRunway().getName()%>" placeholder="<%=varMap.get("PHname")%>"/>  </br>               
                <label class="control-label"><%=varMap.get("Length")%></label>
                <input class="form-control" type="text" name="length" value="<%= vm.getRunway().getLength()%>" placeholder="<%=varMap.get("PHlength")%>"/>  </br>
                <label class="control-label"><%=varMap.get("Airport")%></label>
                <select class="form-control" name="airport">    
                <option value="" disabled selected hidden><%=varMap.get("PHairport")%></option>
                    <% for (Airport a : vm.getAirportList()) {%>
                    <option class="form-control" value="<%=a.getID()%>"
                            <% if (a.getID() == vm.getRunway().getAirportID()) {%>selected="selected"<%}%>
                            ><%=a.getName()%></option>
                    <% }%>
                </select> </br>
                <button type="submit" name="submit" class="btn btn-default" value="0"><%=varMap.get("Save")%></button>
                <button type="submit" name="submit" class="btn btn-default" value="-1"><%=varMap.get("Cancel")%></button>
            </form>
        
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>

