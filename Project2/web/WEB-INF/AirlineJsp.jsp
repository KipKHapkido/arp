<%-- 
    Document   : Template:OneViewToRuleThemAll
    Created on : 25-okt-2016, 20:30:00
    Author     : Sven Vervloet
--%>

<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.AirlineVM"%>

<% AirlineVM VM = (AirlineVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String,String>) VM.getVarMap(); %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="Includes/Header.jsp"/>
<jsp:include page="Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<jsp:include page="/WEB-INF/Includes/NavBarAirline.jsp"/>

<div class="col-md-10">
    <div>
    <div class="well">
        <div class="text-center">
        <p><%=varMap.get("Airline")%></p>
        <img src="/Project2/Resources/images/AirlinePic.png" class="img-rounded" alt="Airplane">
        
        </div>
    </div>
</div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="Includes/Footer.jsp"/>






