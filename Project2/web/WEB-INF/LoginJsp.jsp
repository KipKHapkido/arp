<%-- 
    Document   : Template:OneViewToRuleThemAll
    Created on : 25-okt-2016, 20:30:00
    Author     : Sven Vervloet
--%>

<%@page import="Models.Domain.Message"%>
<%@page import="java.util.List"%>
<%@page import="Models.ViewModel.LoginVM"%>
<%@page import="java.util.Map"%>
<% LoginVM VM = (LoginVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="Includes/Header.jsp"/>
<jsp:include page="Includes/NavBar.jsp"/>
<jsp:include page="Includes/Messages.jsp"/>
<div class="container">
    <div class="col-md-4 col-md-offset-4">
        <form action="Login" method="Post" class="form-signin">
            <h2 class="form-signin-heading"><%= varMap.get("SignIn")%></h2>
            <label for="inputUsername" class="sr-only"><%= varMap.get("Username")%></label>
            <input type="text" name="username" id="inputUsername" class="form-control" placeholder="<%= varMap.get("Username")%>" required autofocus value="<%= VM.getUsername()%>">
            <label for="inputPassword" class="sr-only"><%= varMap.get("Password")%></label>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="<%= varMap.get("Password")%>" required value="<%= VM.getPassword() %>">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember_me" value="remember-me"> <%= varMap.get("RememberMe")%>
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit"><%= varMap.get("SignInButton")%></button>
        </form>

    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="Includes/Footer.jsp"/>







