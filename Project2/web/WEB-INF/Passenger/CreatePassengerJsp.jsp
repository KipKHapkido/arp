<%-- 
    Document   : CreatePassengerJsp
    Created on : 02-Nov-2016, 18:11:52
    Author     : Ruben Veris
--%>

<%@page import="Models.Domain.Message"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.CreatePassengerVM"%>
<%@page import="Models.ViewModel.UpdatePassengerVM"%>
<% CreatePassengerVM VM = (CreatePassengerVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap();%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<jsp:include page="/WEB-INF/Includes/NavBarAirline.jsp"/>

<div class="col-md-8">
    <div class="well"> 
        <h2 for="CreatePassenger"><%=varMap.get("CreatePass")%></h2></br>
        <div>
            <% List<Message> messageList = (List<Message>) VM.getListMessage(); %>
            <% for (Message m : messageList){out.println("<p class='bg-" + m.getBootstrapStatus() + " message'>" + m.getMessage() + "</p>");} %>
        </div>
        <form action="CreatePassenger" method="post" >
            <div class="row">
                <div class="col-md-6">
                    <label for="passenger_firstName"><%=varMap.get("First Name")%></label>                    
                    <input type="text" class="form-control" id="passenger_firstName" name="passenger_firstName" placeholder="<%=varMap.get("First Name")%>"/></br>
                </div>
                <div class="col-md-6">
                    <label for="passenger_surName"><%=varMap.get("Last Name")%></label>
                    <input type="text" class="form-control" id="passenger_lastName" name="passenger_surName" placeholder="<%=varMap.get("Last Name")%>"/></br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="passenger_birthday"><%=varMap.get("Birthday")%></label>
                    <div class="input-group date" >
                        <input type='text' class="form-control form-date" id="passenger_birthday" name="passenger_birthday" placeholder="<%=varMap.get("Birthday")%>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        <script type="text/javascript">
                            $(function () {
                                $('#passenger_birthday').datepicker({
                                    changeYear: true,
                                    changeMonth: true,
                                    changeDay: true,
                                    yearRange: "c-100:+0"
                                }).attr('readonly', 'readonly');
                            });
                        </script>
                    </div></br>
                </div>
            </div>
            <div class="row">                
                <div class="col-md-6">
                    <label for="passenger_address"><%=varMap.get("Street")%></label>
                    <input type="text" class="form-control" id="passenger_street" name="passenger_street" placeholder="<%=varMap.get("Street")%>"/></br>
                </div>
                <div class="col-md-6">
                    <label for="passenger_address"><%=varMap.get("Number")%></label>
                    <input type="text" class="form-control" id="passenger_number" name="passenger_number" placeholder="<%=varMap.get("Number")%>"/></br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="passenger_address"><%=varMap.get("Location")%></label>
                    <input type="text" class="form-control" id="passenger_location" name="passenger_location" placeholder="<%=varMap.get("Location")%>"/></br>
                </div>
                <div class="col-md-6">
                    <label for="passenger_address"><%=varMap.get("ZIP")%></label>
                    <input type="text" class="form-control" id="passenger_zip" name="passenger_zip" placeholder="<%=varMap.get("ZIP")%>"/></br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="passenger_addressId"><%=varMap.get("Country")%></label>
                    <input type="text" class="form-control" id="passenger_country" name="passenger_country" placeholder="<%=varMap.get("Country")%>"/></br>
                </div>
            </div>
            </br>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>

<jsp:include page="../Includes/Footer.jsp"/>