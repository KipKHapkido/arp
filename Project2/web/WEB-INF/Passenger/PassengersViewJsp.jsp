<%-- 
    Document   : PassengersViewJsp
    Created on : 8-nov-2016, 16:03:48
    Author     : Stijn
    Thanks to  : Kathleen
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="Models.Domain.Passenger"%>
<%@page import="Models.Domain.Address" %>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<jsp:include page="/WEB-INF/Includes/NavBarAirline.jsp"/>
<% ArrayList<Passenger> PassList = (ArrayList) request.getAttribute("PassList");%>
    
<%@page import="java.util.Map"%>
<!--anders extra vertalingen-->
<%@page import="Models.ViewModel.CreatePassengerVM"%>
<% CreatePassengerVM VM = (CreatePassengerVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap();%>


<div class="col-md-10">
    <div class="well">
        <h2><%=varMap.get("View passengers")%></h2>
       
            <table class="table">
                <tr class="">
                    <th>ID</th>
                    <th><%=varMap.get("First Name")%></th>
                    <th><%=varMap.get("Surname")%></th>
                    <th><%=varMap.get("Address")%></th>
                    <th><%=varMap.get("Birthday")%></th>
                    <th></th>
                </tr>
                 <form action="PassengerRead" method="post">
                <tr>
                    <th><input type="text" name="ID" placeholder="ID"/></th>
                    <th><input type="text" name="FirstName" placeholder="<%=varMap.get("First Name")%>"/></th>
                    <th><input type="text" name="Surname" placeholder="<%=varMap.get("Surname")%>"/></th>
                    <th></th>
                    <th></th>
                    <th><button type="submit" name="submit" class="btn btn-default glyphicon glyphicon-search" value="Search"                    
                        data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Search")%>"</button></th>
                </tr>
                </form>
                <% if (PassList.size() > 0) 
                {
                    for (Passenger a : PassList) {%>
                     <form action="PassengerRead" method="post">
                <tr>
                    <td><%=a.getID()%> </td>
                    <td><%=a.getFirstName()%> </td>
                    <td><%=a.getSurName()%></td>
                    <td><%=a.getPassAddress().getFullAddress()%></td>
                    <td><%=a.getBirthday()%> </td>
                    <td>
                        <button type="submit" name="submit" class="btn btn-default glyphicon glyphicon-pencil" value="<%=a.getID()%>"
                         data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Edit")%>"></button>
                        <button type="submit" name="submit" class="btn btn-default glyphicon glyphicon-new-window" value="SFlights"
                         data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Search flight")%>"></button>
                        <input type="hidden" name="PasId" value="<%=a.getID() %>" />
                        <form action="PassengerRead" method="post">
                            <input type="hidden" name="pid" value="<%=a.getID()%>" />
                            <button type="submit" name="submit" value="BookFlight" class="btn btn-default glyphicon glyphicon-plane"
                                    data-toggle="tooltip" data-placement="left" title="<%= varMap.get("BookFlight")%>"></button>
                        </form>
                    </td>      
                </tr>
                </form> 
                <%}
                }
                else
                {%>
                <tr>
                    <th><%=varMap.get("NoData")%></th>
                </tr>    
                <%}%>      
            </table>            
       
    </div>
</div>


<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
