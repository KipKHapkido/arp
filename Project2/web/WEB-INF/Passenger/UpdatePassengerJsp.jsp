<%-- 
    Document   : CreatePassengerJsp
    Created on : 02-Nov-2016, 18:11:52
    Author     : Ruben Veris
--%>

<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.CreatePassengerVM"%>
<%@page import="Models.ViewModel.UpdatePassengerVM"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% UpdatePassengerVM VM = (UpdatePassengerVM) request.getAttribute("VM");%>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap();%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<jsp:include page="/WEB-INF/Includes/NavBarAirline.jsp"/>

<div class="col-md-8">
    <div class="well">   
        <% String Id = (String)request.getAttribute("address_id"); %>
        <% String Street = (String)request.getAttribute("address_street"); %>
        <% String StreetNumber = (String)request.getAttribute("passenger_number"); %>
        <% String Town = (String)request.getAttribute("address_location"); %>
        <% String Zip_Code = (String)request.getAttribute("address_zipCode"); %>
        <% String Country = (String)request.getAttribute("address_country"); %>
        <h2 for="CreatePassenger"><%=request.getAttribute("Action")%></h2></br>
        <form action="<%=request.getAttribute("Action")%>" method="post" >
            <div class="row">
                <div class="col-md-6">
                    <label for="passenger_firstName"><%=varMap.get("First Name")%></label>
                    <input type="hidden" class="form-control" id="passenger_id" name="passenger_id" value="<%= VM.getId() %>"/>
                    <input type="text" class="form-control" id="passenger_firstName" name="passenger_firstName" value="<%= VM.getFirstName()%>" placeholder="<%=varMap.get("First Name")%>"/></br>
                </div>
                <div class="col-md-6">
                    <label for="passenger_surName"><%=varMap.get("Last Name")%></label>
                    <input type="text" class="form-control" id="passenger_lastName" name="passenger_surName" value="<%= VM.getSurName() %>" placeholder="<%=varMap.get("Last Name")%>"/></br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="passenger_birthday"><%=varMap.get("Birthday")%></label>
                    <div class="input-group date" >
                        <input type='text' class="form-control form-date" id="passenger_birthday" name="passenger_birthday" value="<%= VM.getBirthday() %>" placeholder="<%=varMap.get("Birthday")%>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        <script type="text/javascript">
                            $(function () {
                                $('#passenger_birthday').datepicker({
                                    changeYear: true,
                                    changeMonth: true,
                                    changeDay: true,
                                    numberOfMonths: 2,
                                    yearRange: "c-100:+0"
                                }).attr('readonly', 'readonly');
                            });
                        </script>
                    </div></br>
                </div>
            </div>
            <div class="row">
                    <input type="hidden" class="form-control" id="address_id" name="address_id" value="<%= Id %>"> 
                <div class="col-md-6">
                    <label for="passenger_address"><%=varMap.get("Street")%></label>
                    <input type="text" class="form-control" id="passenger_street" name="passenger_street" value="<%= Street %>" placeholder="<%=varMap.get("Street")%>"/></br>
                </div>
                <div class="col-md-6">
                    <label for="passenger_address"><%=varMap.get("Number")%></label>
                    <input type="text" class="form-control" id="passenger_number" name="passenger_number" value="<%= StreetNumber %>" placeholder="<%=varMap.get("Number")%>"/></br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="passenger_address"><%=varMap.get("Location")%></label>
                    <input type="text" class="form-control" id="passenger_location" name="passenger_location" value="<%= Town %>" placeholder="<%=varMap.get("Location")%>"/></br>
                </div>
                <div class="col-md-6">
                    <label for="passenger_address"><%=varMap.get("ZIP")%></label>
                    <input type="text" class="form-control" id="passenger_zip" name="passenger_zip" value="<%= Zip_Code %>" placeholder="<%=varMap.get("ZIP")%>"/></br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="passenger_addressId"><%=varMap.get("Country")%></label>
                    <input type="text" class="form-control" id="passenger_country" name="passenger_country" value="<%= Country %>" placeholder="<%=varMap.get("Country")%>"/></br>
                </div>
            </div>
            <!--<div class="row">
                <div class="col-md-6">
                    <label for="optClass"><%=varMap.get("Class")%></label></br>
                    <label class="radio-inline"><input type="radio" name="optClass">Business</label>
                    <label class="radio-inline"><input type="radio" name="optClass">Economy</label>
                </div>
            </div>--></br>
            <button type="submit" class="btn btn-default">Submit</button> 
        </form>
    </div>
</div>

<jsp:include page="../Includes/Footer.jsp"/>