<%-- 
    Document   : SearchFlightsByPassenger
    Created on : 3-dec-2016, 11:14:15
    Author     : Kristof
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="Models.Domain.Passenger"%>
<%@page import="Models.Domain.Flight" %>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<jsp:include page="/WEB-INF/Includes/NavBarAirline.jsp"/>

<% ArrayList<Flight> allFlights = (ArrayList) request.getAttribute("Flights");%>
<% Passenger tempPass = (Passenger) request.getAttribute("Passenger"); %>


    
<%@page import="java.util.Map"%>
<!--anders extra vertalingen-->
<%@page import="Models.ViewModel.CreatePassengerVM"%>
<% CreatePassengerVM VM = (CreatePassengerVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap();%>


<div class="col-md-10">
    <div class="well">
        <% int PassengerID = (int)request.getAttribute("passenger_id");%>
        <% String PassengerFirstname = (String)request.getAttribute("passenger_firstname");%>
        <% String PassengerLastname = (String)request.getAttribute("passenger_lastname");%>
        <h2><%=varMap.get("Linked Flights")%><%= PassengerFirstname%> <%= PassengerLastname%></h2>
        <form action="DeleteFlightByPassenger" method="post">
            <table class="table">
                <tr class="">
                    
                    <th><%=varMap.get("FlightNR")%></th>
                    <th><%=varMap.get("DepatureDate")%></th>
                    <th></th>
                </tr>
               
                <% if (allFlights.size() > 0) 
                {
                    for (Flight a : allFlights) {%>
                <tr>
                    <td><%=a.getFlightNR()%> </td>
                    <td><%=a.getDepartDate()%> </td>
                    
                    
                    <td>
                       
                        <button type="submit" onclick="clicked(event)" name="flight_id" class="btn btn-default glyphicon glyphicon-remove" value="<%=a.getID()%>"
                                data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Delete")%>"></button>
                       <input type='hidden' name='passenger_id' value="<%= PassengerID%>">
                    </td>      
                </tr>
                <%}
                }
                else
                {%>
                <tr>
                    <th>No data</th>
                </tr>    
                <%}%>      
            </table>            
        </form>
    </div>
</div>


<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
<script>
function clicked(e)
{
    if(!window.confirm("<%=varMap.get("ConfirmDelete")%>")){e.preventDefault()};
}
</script>
