<%-- 
    Document   : ManageAirplanes
    Created on : 02-Nov-2016, 23:54:11
    Author     : Jelle
--%>


<%@page import="java.util.List"%>
<%@page import="Models.Domain.Airplane"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="Includes/Header.jsp"/>
<jsp:include page="Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<% List<Airplane> Airplanes = (List<Airplane>)request.getAttribute("Airplanes"); %>
<div class="col-md-2">
    <div class="list-group">
        <a href="ManageAirplanes" class="list-group-item">Manage airplanes</a>        
        <a href="#" class="list-group-item">Second item</a>
        <a href="#" class="list-group-item">Third item</a>
    </div>
</div>
<div class="col-md-10">
    <div class="well">
        <h2>Manage airplanes</h2>
        <form action="ManageAirplanes" method="post">
        
        
        <table class="table">
            <tr class="">
                <th>ID</th>
                <th>Code</th>
                <th>Owner</th>
                <th>Status</th>
                <th>Capacity</th>
                <th>Action</th>
            </tr>
        <% for(Airplane a : Airplanes){%>
        <tr>
            <td><%=a.getID()%> </td>
            <td><%=a.getCode()%> </td>
            <td><%=a.getOwnerID()%></td>
            <td><%=a.getStatus()%> </td>
            <td><%=a.getCapacity()%> </td>
            <td>
                <button type="submit" name="submit" class="btn btn-default" value="<%=a.getID()%>">Edit</button>
                
            </td>      
        </tr>
        <%}%>      
       </table>
        </br>
        <input type="submit" class="btn btn-default" name="submit" value="Create"/>            
        <input type="submit" class="btn btn-default" name="submit" value="Cancel"/>
        </form>
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="Includes/Footer.jsp"/>
