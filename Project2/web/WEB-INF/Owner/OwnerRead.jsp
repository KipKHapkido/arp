<%-- 
    Document   : OwnerCreate
    Created on : 5-nov-2016, 14:28:30
    Author     : Kathleen
--%>

<%@page import="Models.ViewModel.OwnerDetailVM"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% OwnerDetailVM VM = (OwnerDetailVM) request.getAttribute("VM");%>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap();%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>

<!-- Ruimte voor eigen content -->

<div class="col-md-10 col-md-offset-2">
<div class="well">
    <div>
        <h3><%= varMap.get("Read")%></h3>
    </div>
    <div>
        <table class="table table-striped">
            <tr>
                <th><%= varMap.get("Field")%></th>
                <th><%= varMap.get("Value")%></th>
            </tr>
            <tr>
                <td>
                    <%= varMap.get("Name")%> 
                </td>
                <td>
                    <%= VM.getName()%>
                </td>
            </tr>
            <tr>
                <td>
                    <%= varMap.get("Country")%>
                </td>
                <td>
                    <%= VM.getCountry()%>
                </td>
            </tr>
            <tr>
                <td>
                    <%= varMap.get("Street")%>
                </td>
                <td>
                    <%= VM.getStreet()%>
                </td>
            </tr>
            <tr>
                <td>
                    <%= varMap.get("HouseNumber")%>
                </td>
                <td>
                    <%= VM.getNumber()%>
                </td>
            </tr>
            <tr>
                <td>
                    <%= varMap.get("ZipCode")%>
                </td>
                <td>
                    <%= VM.getZip_code()%>
                </td>
            </tr>
            <tr>
                <td>
                    <%= varMap.get("Town")%>
                </td>
                <td>
                    <%= VM.getTown()%>
                </td>
            </tr>
        </table>
    </div>      
                
     <a href="Owner" class="btn btn-default" value="Cancel"><%=varMap.get("Cancel")%></a>
</div>
</div>     
<jsp:include page="../Includes/Footer.jsp"/>
