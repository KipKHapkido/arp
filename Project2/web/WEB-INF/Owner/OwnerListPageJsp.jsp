<%-- 
    Document   : OwnerListPageJsp
    Created on : 5-nov-2016, 11:59:02
    Author     : Kathleen
--%>
<%@page import="Models.Domain.Owner"%>
<%@page import="Models.Domain.Message"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.OwnerVM"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% OwnerVM VM = (OwnerVM) request.getAttribute("VM"); %>
<% List<Owner> ownerList = (List<Owner>) VM.getownerList(); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap();%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<jsp:include page="../Includes/Messages.jsp"/>
<div class="col-md-10 col-md-offset-2">
<div class="well">
    <div class="row" >
        <div class="gridContent">
            <div class="well">

                <form class="form-search" Action="Owner" method="post">
                    <label><%= varMap.get("Owner")%>:</label>
                    <input type="text" name="search"/>
                    <!--<button type="submit" name="submit" class="btn btn-default" value="search"><%= varMap.get("Search")%></button>-->
                    <button type="submit" name="submit" class="btn btn-default glyphicon glyphicon-search" value="search"
                            data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Search")%>"></button>
                    <a href="OwnerCreate" class="btn btn-default" value="Create"><%= varMap.get("New")%></a>
                </form>

            </div>

            <table  class="table table-striped">
                <tr>
                    <th><%= varMap.get("Name")%></th>
                    <th><%= varMap.get("Address")%></th>
                    <th><%= varMap.get("Actions")%></th>                                                     
                </tr>
                <% for (Owner o : ownerList) {%>
                <tr>
                    <td >
                        <%=  o.getName()%>
                    </td>
                    <td >
                        <%=  o.getAddress().getFullAddress()%>
                    </td>
                    <td >

                        <form action="OwnerDelete" method="post">
                            <a href="OwnerUpdate?Id=<%=  o.getID()%>" class="btn btn-default glyphicon glyphicon-pencil"
                               data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Edit")%>"></a>
                            <button type="submit" class="btn btn-default glyphicon glyphicon-remove"
                                    data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Delete")%>"></button>
                            <input type='hidden' name='owner_Id' value="<%=  o.getID()%>">
                            <a href="OwnerRead?Id=<%=  o.getID()%>" class="btn btn-default glyphicon glyphicon-eye-open"
                               data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Details")%>"></a>
                        </form>            

                    </td>
                </tr>
                <%}%>               
            </table>
        </div>        
    </div>
</div>
    <!-- Einde Ruimte voor eigen content -->
    <jsp:include page="../Includes/Footer.jsp"/>



