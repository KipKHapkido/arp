<%-- 
    Document   : OwnerCreate
    Created on : 5-nov-2016, 14:28:30
    Author     : Kathleen
--%>

<%@page import="Models.ViewModel.OwnerDetailVM"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% OwnerDetailVM VM = (OwnerDetailVM) request.getAttribute("VM");%>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap();%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>

<!-- Ruimte voor eigen content -->

<div class="col-md-10 col-md-offset-2">
<div class="well">
    <div>
        <h3><%= varMap.get("Create")%></h3>
    </div>
    <div>
        <form action="OwnerCreate" method="post">
            <div class="form-group">
                <label for="owner_name"><%= varMap.get("Name")%></label>
                <input type="text" class="form-control" name="owner_name" placeholder="<%= varMap.get("Name")%>">
            </div>
            <div class="form-group">
                <label for="owner_country"><%= varMap.get("Country")%></label>
                <input type="text" class="form-control" name="owner_country" placeholder="<%= varMap.get("Country")%>">
            </div>
            <div class="form-group">
                <label for="owner_street"><%= varMap.get("Street")%></label>
                <input type="text" class="form-control" name="owner_street" placeholder="<%= varMap.get("Street")%>">
            </div>
            <div class="form-group">
                <label for="owner_housenumber"><%= varMap.get("HouseNumber")%></label>
                <input type="text" class="form-control" name="owner_number" placeholder="<%= varMap.get("HouseNumber")%>">
            </div>
            <div class="form-group">
                <label for="owner_zipcode"><%= varMap.get("ZipCode")%></label>
                <input type="text" class="form-control" name="owner_zipcode" placeholder="<%= varMap.get("ZipCode")%>">
            </div>
            <div class="form-group">
                <label for="owner_town"><%= varMap.get("Town")%></label>
                <input type="text" class="form-control" name="owner_town" placeholder="<%= varMap.get("Town")%>">
            </div>
            <div class="form-group">
                <label for="owner_extra">Extra</label>
                <input type="text" class="form-control" name="owner_extra" placeholder="Extra">
            </div>
            <a href="Owner" class="btn btn-default" value="Cancel"><%=varMap.get("Cancel")%></a>
            <button type="submit" class="btn btn-default"><%= varMap.get("Submit")%></button>
        </form> 
    </div>       
</div>
</div>
<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
