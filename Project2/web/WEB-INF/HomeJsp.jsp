<%-- 
    Document   : Homejsp
    Created on : 22-okt-2016, 9:28:36
    Author     : Sven Vervloet
--%>

<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% BaseVM VM = (BaseVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="Includes/Header.jsp"/>
<jsp:include page="Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->


<div>
    <div class="well">
        <div class="text-center">
        <p><%=varMap.get("Welcome")%></p>
        <img src="/Project2/Resources/images/HomePic.png" class="img-rounded" alt="Airplane">
        
        </div>
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="Includes/Footer.jsp"/>






