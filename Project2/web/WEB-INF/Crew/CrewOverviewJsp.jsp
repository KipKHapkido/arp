
<%@page import="Models.Domain.Crew"%>
<%@page import="java.util.Date"%>
<%@page import="Models.ViewModel.CrewVM"%>
<%@page import="Models.Domain.TimeClass"%>
<%@page import="Models.Domain.Flight"%>
<%@page import="Models.Domain.Airport"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% CrewVM VM = (CrewVM) request.getAttribute("VM");%>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>
<% Flight f = VM.getFlight(); %>
<% List<Crew> crewList = VM.getCrewList(); %>
<% List<Crew> listPilots = VM.getListPilots();%>
<% List<Crew> listStewards = VM.getListStewards();%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<jsp:include page="../Includes/NavBarAirline.jsp"/>
<style>
    #addCrew, #counter {
        margin-bottom: 20px;
    }
</style>
<!-- Ruimte voor eigen content -->
<div class="col-md-10">
    <div class="well">
        <H1><%=varMap.get("CrewForFlight")%> <%=f.getFlightNR()%> (<%=varMap.get("Depart")%>: <%=f.getDepartDate()%> <%=f.getDepartTime().getTimecol()%>)</H1>
    </div>

    <span data-target="#myModal">
        <button id="addCrew" 
                title="<%=varMap.get("addCrew")%>" 
                class="btn btn-default glyphicon glyphicon-plus-sign" 
                data-toggle="tooltip" data-placement="left"></button>
        <div>
    </span>
    <jsp:include page="../Includes/Messages.jsp"/>
    <% if ((crewList != null) && (crewList.size() > 0)) {
            int function = 0;
            int pilots = 0;
            int stewards = 0; %>
    <div id="resultDiv" class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th><%=varMap.get("Function")%></th>
                    <th><%=varMap.get("Surname")%></th>
                    <th><%=varMap.get("Firstname")%></th>  
                    <th><%=varMap.get("Remove")%></th>
                </tr>
            </thead>
            <tbody>
                <% for (Crew c : crewList) {
                        if (c.getFunction() != function) {%>
                <tr class='header active' id='header<%=c.getFunction()%>'>
                    <td colspan='4'><button id='btn-header<%=c.getFunction()%>' class='btn btn-default btn-xs glyphicon glyphicon-resize-full'></button>&nbsp;&nbsp;<strong><%=c.getDescription().getDescription()%></strong></td>
                </tr>
                <% function = c.getFunction();
                    }%>
                <tr>
                    <td>
                        <% if (c.getFunction()==3) { 
                                pilots++; 
                            } else {
                                stewards++;
                            }%>
                    </td>
                    <td><%=c.getSurname()%></td>
                    <td><%=c.getFirstname()%></td> 
                    <td>
                        <form method="post" name="deleteCrew" action="CrewOverview">
                            <input type="hidden" name="ID" value="<%=f.getID()%>"/>
                            <input type="hidden" name="EmployeeID" value="<%=c.getID()%>"/>
                            <button type="submit" name="Submit" 
                                    value="Delete" 
                                    title="<%=varMap.get("deleteCrew")%>"
                                    class="btn btn-default glyphicon glyphicon glyphicon-ban-circle"
                                    data-toggle="tooltip" data-placement="left"></button>
                        </form>

                    </td>
                </tr>
                <% } %>
            </tbody>

        </table>
                <div id='counter'><em>Pilots: <%=pilots%>&nbsp;-&nbsp;Stewards: <%=stewards%></em></div>
        <% }%>
        <a href="SearchFlight"><button class="btn btn-default"><%=varMap.get("Back")%></button></a>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><%=varMap.get("availableCrew")%></h4>
                </div>
                <div class="modal-body">
                    <form method="post" name="addCrew" action="CrewOverview">
                        <div class="form-group">
                            <input type="hidden" name="ID" value="<%=f.getID()%>"/>
                            <select name="EmployeeID" class="form-control">
                                <option value="0"><%=varMap.get("Choose")%></option>
                                <% if (!listPilots.isEmpty()) { %>
                                <optgroup label="Pilots">
                                    <% for (Crew c : listPilots) {%>
                                    <option value="<%=c.getID()%>"><%=c.getSurname()%>, <%=c.getFirstname()%></option>
                                    <%    } %>
                                </optgroup>
                                <% }%>
                                <% if (!listStewards.isEmpty()) { %>
                                <optgroup label="Stewards">
                                    <% for (Crew c : listStewards) {%>
                                    <option value="<%=c.getID()%>"><%=c.getSurname()%>, <%=c.getFirstname()%></option>
                                    <%    } %>
                                </optgroup>
                                <% }%>
                            </select>
                        </div>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%=varMap.get("Close")%></button>
                        <button type="submit" name="Submit" value="Add" class="btn btn-primary" title="<%=varMap.get("addCrew")%>" data-toggle="tooltip" data-placement="bottom"><%=varMap.get("Save")%></button>
                    </form>
                </div>

            </div>
        </div>
    </div>    

    <!-- Einde ruimte voor eigen content -->
    <jsp:include page="../Includes/Footer.jsp"/>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('tr.header').nextUntil('tr.header').slideToggle("slow");
        });

        $(document).on("click", "#addCrew", function () {
            $('#myModal').modal({
                show: 'true'
            });

        });

        function clicked(e)
        {
            if (!window.confirm("<%=varMap.get("CancelAsk")%>")) {
                e.preventDefault();
            }
            ;
        }

        $('#header3').click(function () {
            if ($("#btn-header3").hasClass("glyphicon-resize-full")) {
                $("#btn-header3").removeClass("glyphicon-resize-full").addClass("glyphicon-resize-small");
            } else if ($("#btn-header3").hasClass("glyphicon-resize-small")) {
                $("#btn-header3").removeClass("glyphicon-resize-small").addClass("glyphicon-resize-full");
            }

            $(this).nextUntil('tr.header').slideToggle("slow", function () {

            });
        });
        $('#header4').click(function () {
            if ($("#btn-header4").hasClass("glyphicon-resize-full")) {
                $("#btn-header4").removeClass("glyphicon-resize-full").addClass("glyphicon-resize-small");
            } else if ($("#btn-header4").hasClass("glyphicon-resize-small")) {
                $("#btn-header4").removeClass("glyphicon-resize-small").addClass("glyphicon-resize-full");
            }
            $(this).nextUntil('tr.header').slideToggle("slow", function () {

            });
        });
    </script>