<%-- 
    Document   : UpdateAirplaneJsp
    Created on : 04-Nov-2016, 01:46:31
    Author     : Jelle
--%>


<%@page import="Models.Domain.Airplane"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="Includes/Header.jsp"/>
<jsp:include page="Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<% Airplane airp = (Airplane)request.getAttribute("Airplane"); %>
<div class="col-md-2">
    <div class="list-group">
        <a href="ManageAirplanes" class="list-group-item">Manage Airplanes</a> 
        <a href="#" class="list-group-item">Second item</a>
        <a href="#" class="list-group-item">Third item</a>
    </div>
</div>
<div class="col-md-10">
    <div class="well">
        <h2>Update airplane</h2>
        <form action="SaveAirplane" method="post" class="form-horizontal">
            <input type="hidden" name="id" value="<%= airp.getID()%>"/>
            <label class="col-lg-2 control-label">Code</label>
            <input class="form-control" type="text" name="code" value="<%= airp.getCode()%>"/>  </br>
            <label class="col-lg-2 control-label">Owner</label>
            <input class="form-control" type="text" name="owner" value="<%= airp.getOwnerID()%>"/>  </br>
            <label class="col-lg-2 control-label">Status</label>
            <input class="form-control" type="text" name="status" value="<%= airp.getStatus()%>"/>  </br>
            <label class="col-lg-2 control-label">Capacity</label>
            <input class="form-control" type="text" name="capacity" value="<%= airp.getCapacity()%>"/>  </br>
        
        <input type="submit" class="btn btn-default" name="submit" value="Save"/>
        <input type="submit" class="btn btn-default" name="submit" value="Cancel"/>
        </form>
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="Includes/Footer.jsp"/>
