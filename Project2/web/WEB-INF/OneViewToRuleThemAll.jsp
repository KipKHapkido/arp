<%-- 
    Document   : Template:OneViewToRuleThemAll
    Created on : 25-okt-2016, 20:30:00
    Author     : Sven Vervloet
--%>

<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% BaseVM VM = (BaseVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="Includes/Header.jsp"/>
<jsp:include page="Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->

<div class="col-md-2">
    <div class="list-group">
        <a href="#" class="list-group-item">First item</a>
        <a href="#" class="list-group-item">Second item</a>
        <a href="#" class="list-group-item">Third item</a>
    </div>
</div>
<div class="col-md-10">
    <div class="well">
        <p>Some default panel content here. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies
            vehicula ut id elit.</p>
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="Includes/Footer.jsp"/>






