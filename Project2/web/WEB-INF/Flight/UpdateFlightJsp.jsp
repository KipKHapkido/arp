<%-- 
    Document   : UpdateFlight
    Created on : 24-nov-2016, 9:52:36
    Author     : CharlotteHendrik
--%>

<%@page import="Models.Domain.Message"%>
<%@page import="java.util.Date"%>
<%@page import="Models.ViewModel.Flight.CreateFlightVM"%>
<%@page import="Models.Domain.TimeClass"%>
<%@page import="Models.Domain.Airplane"%>
<%@page import="java.util.List"%>
<%@page import="Models.Domain.Airport"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% CreateFlightVM VM = (CreateFlightVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>
<% List<Message> messageList = (List<Message>) VM.getListMessage(); %>
<script src="/Project2/Resources/js/jquery.js" type="text/javascript"></script>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<% List<Airport> Airports = VM.getAirports(); %>
<% List<Airplane> Airplanes = VM.getAirplanes();%>
<% List<TimeClass> Times = VM.getTimes();%>
<% int year = VM.getYear();%>
<% int month = VM.getMonth();%>
<% int day = VM.getDay();%>

<jsp:include page="../Includes/NavBarAirline.jsp"/>
<div class="col-md-6">
    <div class="well">
        
<% for (Message m : messageList) {

         out.println("<p class='bg-"+ m.getBootstrapStatus() + " message'>" + m.getMessage() + "</p>");
        
        }
    %> 
    
        <form action="SaveFlight" method="post">
            
            <label> <%=varMap.get("VluchtNR")%> : </label><br/>
            <input type="text" class="form-control" name="FlightNR" 
                   value="<%= VM.getFlightNR()%>"/><br/>
            
            <label> <%=varMap.get("AirplaneID")%> :</label><br/>
            <select class="selectpicker form-control" data-style="btn-primary" name="AirplaneID">
            <% for(Airplane a1 : Airplanes){%>  
            <option class="form-control" value="<%=a1.getID()%>"
                    <% if (a1.getID() == VM.getAirplaneID()) {%>selected="selected"<%}%>
                    ><%=a1.getBuilder()+ " " + a1.getType() +" (" +a1.getCode()+")"%> 
            </option>
           
            <%}%>   
            </select><br/>
            
            
            <label for="Date"/><%=varMap.get("Date")%> : </label>
                 
                    <div class="input-group date" >
                        <input type='text' class="form-control form-date" name="Date" id="passenger_birthday" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        <script type="text/javascript">

                            $(function () {
                                $('#passenger_birthday').datepicker({ 
                                    changeYear: true,
                                    changeMonth: true,
                                    changeDay: true,
                                    yearRange: "c:+2"
                                }).attr('readonly','readonly');
                                $('#passenger_birthday').datepicker("setDate", new Date(<%=year%>,<%=month%>,<%=day%>) );
                            });
                          
                        </script>
                    </div>
                    </br>
            
            
            
            
            
            <label><%=varMap.get("DepartTimeFrom")%> : </label>
            <select class="selectpicker form-control" data-style="btn-primary" name="DepartFrom">
                <% for(TimeClass t : Times){%>
                <option value="<%=t.getID()%>"
                        <% if (t.getID() == VM.getDepartTimeID()) {%>selected="selected"<%}%>
                        ><%=t.getTimecol() %></option>
              <%}%>   
            </select>
            
            <label><%=varMap.get("DepartTimeUntill")%> : </label>
            <select  class="selectpicker form-control" data-style="btn-primary" name="DepartUntill">
                <% for(TimeClass t : Times){%>
                <option value="<%=t.getID()%>"
                        <% if (t.getID() == VM.getDepartUntillID()) {%>selected="selected"<%}%>
                        ><%=t.getTimecol() %></option>
              <%}%>   
            </select>
            <br/>
            
            <label> <%=varMap.get("DepartureID")%> : </label><br/>
            <select class="selectpicker form-control" data-style="btn-primary" name="DepartureID">
            <% for(Airport a : Airports){%>
            <option value="<%=a.getID()%>"
                    <% if (a.getID() == VM.getDepartureID()) {%>selected="selected"<%}%>
                    ><%=a.getName()%></option>
            <%}%>   
            </select><br/>
            
            
            <label> <%=varMap.get("DestinationID")%> : </label><br/>
            <select class="selectpicker form-control" data-style="btn-primary" name="DestinationID">
            <% for(Airport a : Airports){%>
            <option value="<%=a.getID()%>"
                    <% if (a.getID() == VM.getDestinationID()) {%>selected="selected"<%}%>
                    ><%=a.getName()%></option>
            <%}%>   
            </select><br/>
            <br/>
                
            <input type="hidden" name="ID" value="<%=VM.getID()%>"/>
            <input type="submit" class="btn btn-default" value="<%=varMap.get("save")%>"/>
        </form>
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
