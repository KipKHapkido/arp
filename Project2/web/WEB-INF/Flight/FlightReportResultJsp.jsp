<%-- 
    Document   : FlightReportJsp
    Created on : 28-nov-2016, 13:38:35
    Author     : Jeff
--%>

<%@page import="Models.ViewModel.Flight.FlightReportVM"%>
<%@page import="java.util.List"%>
<%@page import="Models.Domain.Flight"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% FlightReportVM vm = (FlightReportVM) request.getAttribute("VM");%>
<% List<Flight> lf = (List) vm.getFlightList();%>
<% Flight fd = vm.getFlight();%>
<% Map<String, String> varMap = (Map<String, String>) vm.getVarMap(); %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<jsp:include page="../Includes/NavBarAirline.jsp"/>

<div class="col-md-10">
    <div class="well">
        <form action="FlightReport" method="post" class="form-inline">
            <div class="form-group">
                <select class="form-control" name="search" >
                    <% for (Flight f : lf) {%>
                    <option class="form-control" value="<%=f.getID()%>" <% if (fd.getID() == f.getID()) {
                            %>selected<%
                                }
                            %>><%=f.getFlightNR()%></option><%}%>
                </select>
                <button type="submit" name="submit" class="btn btn-default" value="search"><%= varMap.get("Search")%></button>
            </div>

            <!--    <div class="form-group">
                    <input type="text" name="search"/>
                    <button type="submit" name="submit" class="btn btn-default" value="search"><%= varMap.get("Search")%></button>    
                </div>-->
        </form>

        <h2><%=varMap.get("Title")%></h2>
        <form class="form-horizontal">
            <fieldset disabled>
                <div class="form-group">
                    <label for="FlightNR" class="col-sm-3 control-label"><%=varMap.get("FlightNR")%> : </label>
                    <div class="col-sm-9">
                        <input id="report" class="report form-control" placeholder="<%=fd.getFlightNR()%>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="Airplane" class="col-sm-3 control-label"><%=varMap.get("Airplane")%> : </label>
                    <div class="col-sm-9">
                        <input id="report" class="form-control" placeholder="<%=fd.getAirplane().getCode()%>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="DepartDate" class="col-sm-3 control-label"><%=varMap.get("DepartDate")%> : </label>
                    <div class="col-sm-9">
                        <input id="report" class="form-control" placeholder="<%=fd.getDepartDate()%>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="DepartureAirport" class="col-sm-3 control-label"><%=varMap.get("DepartureAirport")%> : </label>
                    <div class="col-sm-9">
                        <input id="report" class="form-control" placeholder="<%=fd.getDepartAirport().getName()%>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="Destination" class="col-sm-3 control-label"><%=varMap.get("Destination")%> : </label>
                    <div class="col-sm-9">
                        <input id="report" class="form-control" placeholder="<%=fd.getDestinationAirport().getName()%>">
                    </div>
                </div>
                <% if (fd.getFlightstate() != null) {%>
                <div class="form-group">
                    <label for="State" class="col-sm-3 control-label"><%=varMap.get("State")%> : </label>
                    <div class="col-sm-9">
                        <input id="report" class="form-control" placeholder="<%=fd.getFlightstate().getState()%>">
                    </div>
                </div>
                <% }%>
                <div class="form-group">
                    <label for="TimeslotStart" class="col-sm-3 control-label"><%=varMap.get("TimeslotStart")%> : </label>
                    <div class="col-sm-3">
                        <input id="report" class="form-control" placeholder="<%=fd.getDepartFrom().getTimecol().toString()%>">
                    </div>
                    <label for="TimeslotEnd" class="col-sm-3 control-label"><%=varMap.get("TimeslotEnd")%> : </label>
                    <div class="col-sm-3">
                        <input id="report" class="form-control" placeholder="<%=fd.getDepartUntil().getTimecol().toString()%>">
                    </div>
                </div>
                <% if (fd.getDepartTime() != null) {%>
                <div class="form-group">
                    <label for="Actual" class="col-sm-3 control-label"><%=varMap.get("Actual")%> : </label>
                    <div class="col-sm-9">
                        <input id="report" class="form-control" placeholder="<%=fd.getDepartTime().getTimecol().toString()%>">
                    </div>
                </div>
                <% }
                %>
            </fieldset>
        </form>
        <a href="/Project2/FlightReport" class="btn btn-default"><%= varMap.get("Link")%></a>
    </div>
</div>
<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>

<!-- Coderead comments
    regels 37-40
        oude code in comment
-->