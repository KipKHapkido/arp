<%-- 
    Document   : Template:OneViewToRuleThemAll
    Created on : 25-okt-2016, 20:30:00
    Author     : Sven Vervloet
--%>


<%@page import="java.util.Date"%>
<%@page import="Models.ViewModel.Flight.SearchFlightsVM"%>
<%@page import="Models.Domain.TimeClass"%>
<%@page import="Models.Domain.Flight"%>
<%@page import="Models.Domain.Airport"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% SearchFlightsVM VM = (SearchFlightsVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>
<% List<Flight> NewFlights = (List<Flight>) VM.getResultList();%>
<% List<Airport> destinationList = (List<Airport>) VM.getDestinationList();%>
<% Date comparisonDate = (Date) VM.getComparisonDate();%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->

<jsp:include page="../Includes/NavBarAirline.jsp"/>
<div class="col-md-10">

    <div class="well">
        <h1><%=varMap.get("Title")%></h1>

        <form method="post" action="SearchFlight" class="form-inline">
            <div class="form-group">
                <label for="DestinationID">Bestemming</label>

                <select class="form-control" id="DestinationID" name="DestinationID">
                    <option value="0"><%=varMap.get("optionMessage")%></option>
                    <% if (destinationList.size() > 0) {%>
                    <%  for (Airport a : destinationList) {%>
                    <option value="<%=a.getID()%>" <%if (a.getID() == VM.getDestinationID()) { %>selected="selected"<%}%>><%=a.getName()%></option>
                    <% } %>
                    <%}%>
                </select>
            </div>  
            <div class="form-group">
                <label for="FlightNR">FlightNR</label>
                <input type="text" class="form-control" id="FlightNR" name="FlightNR" value="<%=VM.getFlightNR()%>"/>
            </div>    

            <div class="form-group">
                <button type="submit" class="btn btn-default glyphicon glyphicon-search" 
                        data-toggle="tooltip" data-placement="left" title="<%= varMap.get("submit")%>"></button>
                <button id="reset" class="btn btn-default">Reset</button>
            </div> 
        </form>
    </div>
    <jsp:include page="../Includes/Messages.jsp"/>
    <div class="col-md-12">


        <% if ((NewFlights != null) && (NewFlights.size() > 0)) {%>
        <div id="resultDiv" class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th><%=varMap.get("FlightNR")%></th>
                        <th><%=varMap.get("Airplane")%></th>
                        <th><%=varMap.get("DepartDate")%></th>
                        <th><%=varMap.get("DepartFrom")%></th>
                        <th><%=varMap.get("DepartUntil")%></th>
                        <th><%=varMap.get("Departure")%></th>
                        <th><%=varMap.get("Destination")%></th>
                        <th><%=varMap.get("Runway")%></th>
                        <th><%=varMap.get("FlightState")%></th>
                        <th><%=varMap.get("Edit")%></th>

                    </tr>
                </thead>
                <% for (Flight f : NewFlights) {%>
                <tr>

                    <td><%=f.getFlightNR()%></td>
                    <td><%=f.getAirplane().getBuilder() + " " + f.getAirplane().getType() + " (" + f.getAirplane().getCode() + ") "%></td>
                    <td  <% if (f.getDepartDate().before(comparisonDate)) {%>class="danger"<%}%>><%=f.getDepartDate()%></td>
                    <td><%=f.getDepartFrom().getTimecol()%></td>
                    <td><%=f.getDepartUntil().getTimecol()%></td>
                    <td>
                        <% if (f.getDepartTime() != null) {%>
                        <%=f.getDepartTime().getTimecol()%>
                        <% } else { %>
                        /
                        <% }%>
                    </td>
                    <td><%=f.getDestinationAirport().getName()%></td>
                    <td>
                        <% if (f.getRunwayID() != 0) {%>
                        <%=f.getRunway().getName()%>
                        <% } else { %>
                        /
                        <% }%>
                    </td>
                    <td
                        <% if (f.getFlightStateID() != 0) {
                                if (f.getFlightStateID() == 1) { %>
                        class="success">
                        <% } else if ((f.getFlightStateID() == 9) || (f.getFlightStateID() == 10)) { %>
                        class="danger">
                        <% }%>
                        <strong><%=f.getFlightstate().getState()%></strong>
                        <% } else { %>
                        >
                        <% }%></td>

                    <td class="col-md-2">
                        <form method="post" action="UpdateFlight" class="col-md-3">
                            <input type="hidden" name="ID" value="<%=f.getID()%>"/> 
                            <button type="submit" name="submit" 
                                <%if (f.getPassengers() > 0) {%>disabled="disabled" title="<%=f.getPassengers()%> <%=varMap.get("Explanation2")%>"<% } else {%> 
                                title="<%=varMap.get("Edit")%>"<% }%> 
                                class="btn btn-default glyphicon glyphicon-pencil" value="Edit"
                                data-toggle="tooltip" data-placement="left"></button>
                        </form>
                        <form method="post" action="CancelFlight"  class="col-md-3">
                            <input type="hidden" name="DestinationID" value="<%=VM.getDestinationID()%>"/>
                            <input type="hidden" name="FlightNR" value="<%=VM.getFlightNR()%>"/>
                            <input type="hidden" name="ID" value="<%=f.getID()%>"/>   

                            <button 
                                type="submit" 
                                onclick="clicked(event)" 
                                <%if (f.getFlightStateID() == 9) { %>disabled="disabled"<% }%> 
                                name="submit" 
                                class="btn btn-default glyphicon glyphicon-remove" 
                                title="<%=varMap.get("Cancel")%>" value="Cancel"
                                data-toggle="tooltip" data-placement="left"></button>
                        </form>
                        <% if (f.getFlightStateID() == 1) {%>
                        
                        <form method="post" action="CrewOverview" class="col-md-6 form-inline">
                            
                            <input type="hidden" name="ID" value="<%=f.getID()%>"/>
                                                        
                            <button 
                                type="submit" name="submit" 
                                class="btn btn-default glyphicon glyphicon-wrench"
                                title="<%=varMap.get("EditCrew")%>" value="EditCrew"
                                data-toggle="tooltip" data-placement="left"></button>
                            
                        </form>
                        <% }%>
                        <form method="post" action="PassengerFlightView" class="col-md-6 form-inline">
                            
                            <input type="hidden" name="ID" value="<%=f.getID()%>"/>
                                                        
                            <button 
                                type="submit" name="submit" 
                                class="btn btn-default glyphicon glyphicon-user"
                                title="<%=varMap.get("EditPassenger")%>" value="EditPassenger"
                                data-toggle="tooltip" data-placement="left"></button>
                            
                        </form>
                    </td>
                </tr>
                <% } %>
            </table>
            <% }%>
            <p><em><%=varMap.get("Explanation")%></em></p>
        </div>
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
<script>
    $("#reset").on("click", function (e) {
        e.preventDefault();
        $("#DestinationID").val(0);
        $("#FlightNR").val("");
        $("#resultDiv").html("");
    });

   

    function clicked(e)
    {
        if (!window.confirm("<%=varMap.get("CancelAsk")%>")) {
            e.preventDefault()
        }
        ;
    }

</script>






