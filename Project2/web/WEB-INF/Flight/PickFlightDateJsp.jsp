<%-- 
    Document   : PickFlightDateJsp
    Created on : 23-Nov-2016, 11:33:11
    Author     : Administrator
--%>

<%@page import="Models.Domain.Airport"%>
<%@page import="Models.Domain.Flight"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="Models.ViewModel.BookFlightVM"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="Models.ViewModel.PassengersVM"%>
<%@page import="Models.Domain.Address"%>
<%@page import="Models.Domain.Passenger"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<%  BookFlightVM VM = (BookFlightVM) request.getAttribute("VM");
    Map<String, String> varMap = (Map<String, String>) VM.getVarMap();
    List<Flight> flights = VM.getFlights();
    List<Airport> airports = VM.getAirports();
    Passenger p = VM.getPassenger();
    int origin = VM.getOrigin();
    int destination = VM.getDestination();
    int seats = VM.getSeats();
    Date date = VM.getDate();

    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    String time = dateFormatter.format(date);
%>

<jsp:include page="../Includes/NavBarAirline.jsp" />

<div class="col-md-10">
    <div class="well">
        <h2><%= varMap.get("SearchFlight")%></h2>
        <form action="FlightsOnDate" method="post">
            <label><%= varMap.get("Origin")%></label> 
            <select name="origin" class="form-control" value="<%= VM.getOrigin()%>">
                <% for (Airport a : airports) {
                %><option value="<%= a.getID()%>" <% if (origin == a.getID()) { %> selected="selected" <% }%>><%= a.getName()%></option>
                <% }%>
            </select>
            <br>
            <label><%= varMap.get("Destination")%></label>
            <select name="destination" class="form-control" value="<%= VM.getDestination()%>">
                <% for (Airport a : airports) {
                %><option value="<%= a.getID()%>" <% if (destination == a.getID()) { %> selected="selected" <% }%> ><%= a.getName()%></option>
                <% }%>
            </select>
            <br>
            <label><%= varMap.get("Seats")%>:</label>
            <input class="form-control" type="number" name="seats" value ="<%= seats%>" min="1" />
            <br>
            <input type="hidden" name="passengerID" value="<%= VM.getPassenger().getID()%>" />
            <input type="hidden" name="action" value="GetFlightsOnDate" />
            <label><%= varMap.get("FlightDate") %></label>
            <div class="input-group date" >
                <input type='text' class="form-control form-date" id="passenger_birthday" name="date" value="<%= time%>" placeholder="<%=varMap.get("FlightDate")%>"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                <script type="text/javascript">
                    $(function () {
                        $('#passenger_birthday').datepicker({
                            changeYear: true,
                            changeMonth: true,
                            changeDay: true,
                            numberOfMonths: 2,
                            yearRange: "c-100:+10"
                        }).attr('readonly', 'readonly');
                    });
                </script>
            </div>
            <input type="submit" class="btn btn-default" name="submit" value="<%= varMap.get("Search")%>"/>
        </form>
    </div>
    <% if (flights != null) { %>
    <div class="well">

        <% if (!flights.isEmpty()) {%>
        <table class="table">
            <thead>
            <th><%= varMap.get("FlightCode")%></th>
            <th><%= varMap.get("DepartureDate")%></th>
            <th><%= varMap.get("DepartureTime")%></th>
            <th><%= varMap.get("ArrivalDate")%></th>
            <th><%= varMap.get("ArrivalTime")%></th>
            </thead>

            <% for (Flight f : flights) {%>
            <tr>
                <td>
                    <% if (f.getFlightNR() != null) {%>
                    <%= f.getFlightNR()%>
                    <%} else {%>
                    <%= varMap.get("Unknown")%>
                    <%}%>
                </td>
                <td>
                    <% if (f.getDepartDate() != null) {%>
                    <%= f.getDepartDate().toString()%>
                    <%} else {%>
                    <%= varMap.get("Unknown")%>
                    <%}%>
                </td>
                <td>
                    <% if (f.getDepartFrom() != null && f.getDepartFrom().getTimecol() != null) {%>
                    <%= f.getDepartFrom().getTimecol().toString()%>
                    <%} else {%>
                    <%= varMap.get("Unknown")%>
                    <%}%>
                </td>
                <td>
                    <% if (f.getArrivalDate() != null) {%>
                    <%= f.getArrivalDate().toString()%>
                    <%} else {%>
                    <%= varMap.get("Unknown")%>
                    <%}%>
                </td>
                <td>
                    <% if (f.getArrivalTime() != null) {
                            if (f.getArrivalTime().getTimecol() != null)%>
                    <%= f.getArrivalTime().getTimecol().toString()%>
                    <%} else {%>
                    <%= varMap.get("Unknown")%>
                    <%}%>
                </td>
                <td>
                    <form action="AddPassenger" method="post">
                        <input type="hidden" name="passengerID" value="<%= p.getID()%>"/>
                        <input type="hidden" name="flightID" value="<%= f.getID()%>"/>
                        <input type="hidden" name="seats" value="<%= VM.getSeats()%>"/>
                        <input type="hidden" name="action" value="AddPassenger" />
                        <input type="submit" value="<%= varMap.get("BookFlight")%>" class="btn btn-default" />
                    </form>
                </td>
            </tr>
            <%}%>
        </table>
        <% } else {%>
        <%= varMap.get("NoFlights")%>
        <% } %>

    </div>
    <%}%>
</div>


<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
