<%-- 
    Document   : BoardingPassJsp
    Created on : 8-nov-2016, 16:03:48
    Author     : Dries
--%>

<%@page import="Models.Domain.Flight"%>
<%@page import="Models.Domain.Passenger"%>
<%@page import="Models.ViewModel.BookFlightVM"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<% BookFlightVM VM = (BookFlightVM) request.getAttribute("VM");
   Map<String, String> varMap = (Map<String,String>) VM.getVarMap();
   Passenger p = VM.getPassenger();
   Flight f = VM.getFlights().get(0);
   String seats = VM.getSeats() + "";
%>

<jsp:include page="../Includes/NavBarAirline.jsp" />

<div class="col-md-10">
    <div class="well">
        <h2><%= varMap.get("Success") %></h2>
        <%= varMap.get("BoardingPass").replace("@pF", p.getFirstName())
                .replace("@pS", p.getSurName())
                .replace("@s", seats)
                .replace("@f", f.getFlightNR())
                .replace("@FO", f.getDepartAirport().getName())
                .replace("@FD", f.getDestinationAirport().getName())
                .replace("@h", f.getDepartFrom().getTimecol().toString().substring(0,5))%>
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
