<%-- 
    Document   : Template:OneViewToRuleThemAll
    Created on : 25-okt-2016, 20:30:00
    Author     : Sven Vervloet
--%>

<%@page import="Models.ViewModel.Flight.IncomingFlightsVM"%>
<%@page import="java.util.Date"%>
<%@page import="Models.Domain.TimeClass"%>
<%@page import="Models.Domain.Flight"%>
<%@page import="Models.Domain.Airport"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>

<% IncomingFlightsVM VM = (IncomingFlightsVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>
<% List<Flight> NewFlights = (List<Flight>) VM.getResultList();%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->


<jsp:include page="../Includes/NavBarAirport.jsp"/>
<div class="col-md-10">

    <div class="well">
        <h1><%=varMap.get("Title")%></h1>
        <jsp:include page="../Includes/Messages.jsp"/>
        <% if (NewFlights != null) {%>
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th><%=varMap.get("FlightNR")%></th>
                        <th><%=varMap.get("Airplane")%></th>
                        <th><%=varMap.get("ArrivalDate")%></th>
                        <th><%=varMap.get("ArrivalTime")%></th>
                        <th><%=varMap.get("DepartAirport")%></th>
                        <th><%=varMap.get("Destination")%></th>
                        <th><%=varMap.get("Runway")%></th>
                        <th><%=varMap.get("FlightState")%></th>
                        <th><%=varMap.get("Approval")%></th>
                    </tr>
                </thead>
                <% for (Flight f : NewFlights) {%>
                <tr>

                    <td><%=f.getFlightNR()%></td>
                    <td><%=f.getAirplane().getBuilder() + " " + f.getAirplane().getType() + " (" + f.getAirplane().getCode() + ") "%></td>
                    <td>
                        <% if (f.getArrivalDate() != null) {%> 
                        <%=f.getArrivalDate()%>
                        <% } else { %>
                        /
                        <% }%> 
                    </td>
                    <td>
                        <% if (f.getArrivalTime() != null) {%>
                        <%=f.getArrivalTime().getTimecol()%>
                        <% } else { %>
                        /
                        <% }%>
                    </td>
                    <td><%=f.getDepartAirport().getName()%></td>
                    <td><%if (f.getDestinationID() == 1) {%><strong><%}%><%=f.getDestinationAirport().getName()%><%if (f.getDestinationID() == 1) {%></strong><%}%></td>
                    
                    <td>
                        <% if (f.getRunwayID() != 0) {%>
                        <%=f.getRunway().getName()%>
                        <% } else { %>
                        /
                        <% }%>
                    </td>
                    <td
                        <% if (f.getFlightStateID() != 0) {
                                if (f.getFlightStateID() == 11) { %>
                        class="warning">
                        <% } else if (f.getFlightStateID() == 12) { %>
                        class="info">
                        <% } else { %>
                        class="success">
                        <% }%>
                        <strong><%=f.getFlightstate().getState()%></strong> 
                        <% } else { %>
                        
                        >/
                    <% }%></td>
                    <td>
                        <form method="post" action="IncomingFlights">
                            <input type="hidden" name="ID" value="<%=f.getID()%>"/>
                            <% if (f.getFlightStateID() == 11) { %>
                            <button type="submit" name="submit" class="btn btn-default" value="Check"><%=varMap.get("check")%></button>
                            <% } %>
                        </form>
                    </td>

                </tr>
                <% } %>
            </table>
        </div>
        <% }%>
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
<script>
    function clicked(e)
    {
        if (!window.confirm("<%=varMap.get("rejectAsk")%>")) {
            e.preventDefault();
        };
    }
</script>






