<%-- 
    Document   : Template:OneViewToRuleThemAll
    Created on : 25-okt-2016, 20:30:00
    Author     : Sven Vervloet
--%>

<%@page import="Models.Domain.Flight"%>
<%@page import="java.util.List"%>
<%@page import="Models.ViewModel.FlightsVM"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.AirlineVM"%>
<% FlightsVM VM = (FlightsVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>
<% List<Flight> flights = VM.getFlights();%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="Includes/Header.jsp"/>
<jsp:include page="Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->

<div class="col-md-2">    
    <div class="dropdown">    
        <a href="#" class="list-group-item" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><%=varMap.get("Passenger")%><span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="SearchPassenger"><%=varMap.get("Search")%></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="CreatePassenger"><%=varMap.get("New")%></a></li>
            <li><a href="UpdatePassenger"><%=varMap.get("Update")%></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="DeletePassenger"><%=varMap.get("Delete")%></a></li>                        
        </ul> 
    </div>
    <div class="dropdown">  
        <a href="#" class="list-group-item" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><%=varMap.get("Flight")%><span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="SearchFlight"><%=varMap.get("Search")%></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#"><%=varMap.get("Approve Flight")%></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="NewFlight"><%=varMap.get("New")%></a></li>
            <li><a href="UpdateFlight"><%=varMap.get("Update")%></a></li>
            <li><a href="AddPassenger"><%=varMap.get("AddPassenger")%></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="DelteFlight"><%=varMap.get("Delete")%></a></li>                        
        </ul>                     
    </div>
    <a href="ManageAirplanes" class="list-group-item"><%=varMap.get("Manage Airplanes")%></a> 
</div>
<div class="col-md-10">
    <form action="AddPassengerFlight" >
        <table class="table table-striped">
            <thead>
                <tr>
                    <td><%= varMap.get("Flight")%></td>
                    <td><%= varMap.get("FreeSeats")%></td>
                    <td><%= varMap.get("Actions")%></td>

                </tr>
            </thead>
            <% for (Flight f : flights) {%>
            <tr>
                <td><%= f.getFlightNR()%></td>
                <td><%= f.getAirplaneID()%></td>
                <td><input type="submit" value=<%= varMap.get("Add") %></td>
            </tr>
            }%>
        </table>
    </form>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="Includes/Footer.jsp"/>






