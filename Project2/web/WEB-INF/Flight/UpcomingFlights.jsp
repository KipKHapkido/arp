<%-- 
    Document   : UpcomingFlights
    Created on : 19-Nov-2016, 14:37:32
    Author     : JurgenVanEynde
--%>


<%@page import="Models.ViewModel.Flight.UpcomingFlightsVM"%>
<%@page import="Models.Datamodels.Status"%>
<%@page import="Models.Domain.Flight"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>

<% UpcomingFlightsVM VM = (UpcomingFlightsVM) request.getAttribute("VM"); %>
<%
    Map<String, String> varMap = (Map<String, String>) VM.getVarMap();
    List<Flight> flights = (List<Flight>) VM.getFlights();
    List<Status> status = (List<Status>) VM.getStatus();
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<jsp:include page="../Includes/NavBarAirline.jsp"/>

<div class="col-md-10">
    <div class="well">
        <h1><%=varMap.get("Upcoming")%></h1>
        <% if (flights != null || flights.size() == 0) {%>
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th><%=varMap.get("FlightNR")%></th>
                    <th><%=varMap.get("Airplane")%></th>
                    <th><%=varMap.get("DepartDate")%></th>
                    <th><%=varMap.get("Departure")%></th>
                    <th><%=varMap.get("Destination")%></th>
                    <th><%=varMap.get("Runway")%></th>
                    <th><%=varMap.get("Status")%></th>
                </tr>
            </thead>
            <% for (Flight f : flights) {%>
            <tr>

                <td><%=f.getFlightNR()%></td>
                <td><%=f.getAirplane().getBuilder() + " " + f.getAirplane().getType() + " (" + f.getAirplane().getCode() + ") "%></td>
                <td><%=f.getDepartDate()%></td>
                <td>
                    <% if (f.getDepartTime() != null) {%>
                    <%=f.getDepartTime().getTimecol()%>
                    <% } else { %>
                    /
                    <% }%>
                </td>
                <td><%=f.getDepartAirport().getName()%></td>
                <td>
                    <% if (f.getRunwayID() != 0) {%>
                    <%=f.getRunway().getName()%>
                    <% } else { %>
                    /
                    <% } %>
                </td>
                <td>
                    <form method="post" action="UpcomingFlight" class="form-inline">
                        <select name="Status" class="form-control">
                            <%for (Status s : status) {%>
                            <option value="<%= s.getID()%>" <% if (s.getID() == f.getFlightStateID()) {%>selected<% }%>><%= varMap.get(s.getState())%></option>
                            <%}%>
                        </select>
                        <input type="hidden" name="flightId" value="<%=f.getID()%>">
                        <input type="submit" value="<%=varMap.get("ConfirmStatus")%>" class="btn btn-default">
                    </form>
                </td>
            </tr>
            <% } %>
        </table>
        <% } else {%><p>No results found for the next 4 hours.</p> <% }%>
    </div>
</div>


<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>