<%-- 
    Document   : CreateFlightJsp
    Created on : 30-okt-2016, 8:40:58
    Author     : CharlotteHendrik
--%>
<%@page import="Models.Domain.Message"%>
<%@page import="Models.Domain.TimeClass"%>
<%@page import="Models.Domain.Airplane"%>
<%@page import="java.util.List"%>
<%@page import="Models.Domain.Airport"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.Flight.CreateFlightVM"%>
<% CreateFlightVM VM = (CreateFlightVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>
<% List<Message> messageList = (List<Message>) VM.getListMessage(); %>
<script src="/Project2/Resources/js/jquery.js" type="text/javascript"></script>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<% List<Airport> Airports = VM.getAirports(); %>
<% List<Airplane> Airplanes = VM.getAirplanes();%>
<% List<TimeClass> Times = VM.getTimes();%>
<% int year = (int) VM.getYear();%>
<% int month = (int) VM.getMonth();%>
<% int day = (int) VM.getDay();%>

<jsp:include page="../Includes/NavBarAirline.jsp"/>
<div class="col-md-6">
    <div class="well">
        <% for (Message m : messageList) {

         out.println("<p class='bg-"+ m.getBootstrapStatus() + " message'>" + m.getMessage() + "</p>");
        
        }
    %> 
        
        <form action="CreateFlight" method="post">
            
            
            <label> <%=varMap.get("VluchtNR")%> : </label><br/>
            <input type="text" class="form-control" name="FlightNR" value="<%= VM.getFlightNR() %>"/><br/>
            
            <label> <%=varMap.get("AirplaneID")%> :</label><br/>
            <select class="selectpicker form-control" data-style="btn-primary" name="AirplaneID">
            <% for(Airplane a1 : Airplanes){%>  
            <option class="form-control" value="<%=a1.getID()%>"
                    <% if (a1.getID() == VM.getAirplaneID()) {%>selected="selected"<%}%>
                    ><%=a1.getBuilder()+ " " + a1.getType() +" (" +a1.getCode()+")"%> 
            </option>
           
            <%}%>   
            </select><br/>

            
            
            <label for="Date"/><%=varMap.get("Date")%> : </label>
                    
                    <div class="input-group date" >
                        <input type='text' class="form-control form-date" name="Date" id="passenger_birthday" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        <script type="text/javascript">
                            $(function () {
                                $('#passenger_birthday').datepicker({ 
                                    changeYear: true,
                                    changeMonth: true,
                                    changeDay: true,
                                    yearRange: "c:+2"
                                }).attr('readonly','readonly');
                                if(<%=year%>!=0){
                                $('#passenger_birthday').datepicker("setDate", new Date(<%=year%>,<%=month%>,<%=day%>) );}
                            });
                        </script>
                    </div>
                    </br>
            
            
            
            
            
            <label><%=varMap.get("DepartTimeFrom")%> : </label>
            <select class="selectpicker form-control" data-style="btn-primary" name="DepartFrom">
                <% for(TimeClass t : Times){%>
                    <option class="form-control" value="<%=t.getID()%>"
                    <% if (t.getID() == VM.getDepartTimeID() ) {%>selected="selected"<%}%>
                    ><%=t.getTimecol()%> 
                </option>
              <%}%>   
            </select>
            
            <label><%=varMap.get("DepartTimeUntill")%> : </label>
            <select  class="selectpicker form-control" data-style="btn-primary" name="DepartUntill">
                <% for(TimeClass t : Times){%>
                    <option class="form-control" value="<%=t.getID()%>"
                    <% if (t.getID() == VM.getDepartUntillID() ) {%>selected="selected"<%}%>
                    ><%=t.getTimecol()%> 
                </option>
              <%}%>   
            </select>
            <br/>
            
            <label> <%=varMap.get("DepartureID")%> : </label><br/>
            <select class="selectpicker form-control" data-style="btn-primary" name="DepartureID">
            <% for(Airport a : Airports){%>
                <option class="form-control" value="<%=a.getID()%>"
                    <% if (a.getID() == VM.getDepartureID()) {%>selected="selected"<%}%>
                    ><%=a.getName()%> 
                </option>
            <%}%>   
            </select><br/>
            
            
            <label> <%=varMap.get("DestinationID")%> : </label><br/>
            <select class="selectpicker form-control" data-style="btn-primary" name="DestinationID">
            <% for(Airport a : Airports){%>
                <option class="form-control" value="<%=a.getID()%>"
                    <% if (a.getID() == VM.getDestinationID() ) {%>selected="selected"<%}%>
                    ><%=a.getName()%> 
                </option>
            <%}%>   
            </select><br/>
            <br/>
                
            
            <input type="submit" class="btn btn-default" value="<%=varMap.get("AddFlight")%>"/>
        </form>
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
