<%-- 
    Document   : Template:OneViewToRuleThemAll
    Created on : 25-okt-2016, 20:30:00
    Author     : Sven Vervloet
--%>

<%@page import="java.util.Date"%>
<%@page import="Models.ViewModel.Flight.NewFlightsVM"%>
<%@page import="Models.Domain.TimeClass"%>
<%@page import="Models.Domain.Flight"%>
<%@page import="Models.Domain.Airport"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% NewFlightsVM VM = (NewFlightsVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>
<% List<Flight> NewFlights = (List<Flight>) VM.getResultList();%>
<% Date comparisonDate = (Date) VM.getComparisonDate(); %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->


<jsp:include page="../Includes/NavBarAirport.jsp"/>
<div class="col-md-10">

    <div class="well">
        <h1><%=varMap.get("Title")%></h1>
        <jsp:include page="../Includes/Messages.jsp"/>
        <% if (NewFlights != null) {%>
        <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th><%=varMap.get("FlightNR")%></th>
                    <th><%=varMap.get("Airplane")%></th>
                    <th><%=varMap.get("DepartDate")%></th>
                    <th><%=varMap.get("Departure")%></th>
                    <th><%=varMap.get("DepartAirport")%></th>
                    <th><%=varMap.get("Destination")%></th>
                    <th><%=varMap.get("Runway")%></th>
                    <th><%=varMap.get("Approval")%></th>
                </tr>
            </thead>
            <% for (Flight f : NewFlights) {%>
            <tr>

                <td><%=f.getFlightNR()%></td>
                <td><%=f.getAirplane().getBuilder() + " " + f.getAirplane().getType() + " (" + f.getAirplane().getCode() + ") "%></td>
                <td <% if (f.getDepartDate().before(comparisonDate)) {%>class="danger"<%}%>><%=f.getDepartDate()%></td>
                
                <td>
                    <% if (f.getDepartTime() != null) {%>
                    <%=f.getDepartTime().getTimecol()%>
                    <% } else { %>
                    /
                    <% }%>
                </td>
                <td><%=f.getDepartAirport().getName()%></td>
                <td <%if (f.getDestinationID()==1) {%>class="warning"<%}%>><%if (f.getDestinationID()==1) {%><strong><%}%><%=f.getDestinationAirport().getName()%><%if (f.getDestinationID()==1) {%></strong><%}%></td>
                <td>
                    <% if (f.getRunwayID() != 0) {%>
                    <%=f.getRunway().getName()%>
                    <% } else { %>
                    /
                    <% }%>
                </td>

                <td>
                    <form method="post" action="NewFlights">
                        <input type="hidden" name="ID" value="<%=f.getID()%>"/>
                        <input type="hidden" name="flight" value="<%=f%>"/>

                        <% if ((f.getDepartureID() == 1) && ((f.getRunwayID() != 0) && (f.getDepartureTimeID() != 0)))  {%>
                            <button type="submit" name="submit" class="btn btn-default glyphicon glyphicon-ok" value="Approve"
                                    data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Approve")%>"></button>
                        <% } else if (f.getDestinationID() == 1) {%>
                            <button type="submit" name="submit" class="btn btn-default glyphicon glyphicon-ok" value="Approve"
                                    data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Approve")%>"></button>
                        <% } else { %>
                            <button type="submit" name="submit" class="btn btn-default glyphicon glyphicon-pencil" value="Complete"
                                    data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Complete")%>"></button>
                        <% } %>
                        <button type="submit" onclick="clicked(event)" name="submit" class="btn btn-default glyphicon glyphicon-remove" value="Reject"
                                data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Reject")%>"></button>
                    </form>
                </td>

            </tr>
            <% } %>
        </table>
        </div>
        <% }%>
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
<script>
function clicked(e)
{
    if(!window.confirm("<%=varMap.get("rejectAsk")%>")){e.preventDefault()};
}
</script>






