<%-- 
    Document   : Template:OneViewToRuleThemAll
    Created on : 25-okt-2016, 20:30:00
    Author     : Sven Vervloet
--%>



<%@page import="Models.Domain.Airport"%>
<%@page import="Models.ViewModel.Flight.CompleteIncomingFlightVM"%>
<%@page import="Models.Domain.Runway"%>
<%@page import="java.util.List"%>
<%@page import="Models.Domain.Flight"%>
<%@page import="java.util.Map"%>

<% CompleteIncomingFlightVM VM = (CompleteIncomingFlightVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>
<% Flight f = (Flight) VM.getFlight();%>
<% List<Airport> destinationList = (List<Airport>) VM.getDestinationList();%>
<% List<Runway> runwayList = (List<Runway>) VM.getRunwayList();%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet"/>
<!-- Ruimte voor eigen content -->

<div class="col-md-4">
    <h1><%=varMap.get("Complete")%></h1>
    <jsp:include page="../Includes/Messages.jsp"/>
    <% if (f != null) {%>
    <form action="CompleteIncomingFlight" method="post" class="form-horizontal">
        <div class="form-group">
            <label for="FlightNR"><%=varMap.get("FlightNR")%></label>
            <input type="text" class="form-control" id="FlightNR" value="<%=f.getFlightNR()%>" disabled="disabled">
        </div>
        <div class="form-group">
            <label for="Airplane"><%=varMap.get("Airplane")%></label>
            <input type="text" class="form-control" id="Airplane" value="<%=f.getAirplane().getBuilder() + " " + f.getAirplane().getType() + " (" + f.getAirplane().getCode() + ") "%>" disabled="disabled">
        </div>
        <div class="form-group">
            <label for="ArrivalDate"><%=varMap.get("ArrivalDate")%></label>
            <input type="text" class="form-control" id="ArrivalDate" value="<%=f.getArrivalDate()%>" disabled="disabled">
            <input type="hidden" name="ArrivalDate" value="<%=f.getArrivalDate()%>">
        </div>
        <div class="form-group">
            <label for="ArrivalTime"><%=varMap.get("ArrivalTime")%></label>
            <input type="text" class="form-control" id="ArrivalTime" value="<%=f.getArrivalTime().getTimecol()%>" disabled="disabled">
            <input type="hidden" id="ArrivalTimeID" name="ArrivalTimeID" value="<%=f.getArrivalTimeID()%>">
        </div>
        
        <div class="form-group">
            <label for="Departure"><%=varMap.get("Departure")%></label>
            <input type="text" class="form-control" id="Departure" value="<%=f.getDepartAirport().getName()%>" disabled="disabled">
        </div>
        <div class="form-group">
            <label for="Destination"><%=varMap.get("Destination")%></label>
            <%if (destinationList.size() == 0) { %>
            <input type="text" class="form-control" id="Destination" value="<%=f.getDestinationAirport().getName()%>" disabled="disabled">
            <% } else {%>
            <select class="form-control" id="DestinationID" name="DestinationID">

                <option value="0"><%=varMap.get("optionMessage")%></option>
                <%  for (Airport a : destinationList) {%>
                <option value="<%=a.getID()%>"><%=a.getName()%></option>
                <% } %>
            </select>
            <% } %>
        </div>
        <div class="form-group">
            <label for="RunwayID"><%=varMap.get("Runway")%></label>
            <% if ((runwayList.size() > 0) && (f.getRunwayID() == 0)) {%>
            <select class="form-control" id="RunwayID" name="RunwayID">

                <option value="0"><%=varMap.get("optionMessage")%></option>
                <%  for (Runway r : runwayList) {%>
                <option value="<%=r.getID()%>"><%=r.getName()%></option>
                <% } %>
            </select>
            <% } else { %>
            <input type="text" disabled="disabled" class="form-control" id="Runway" value="No Runways available"/>
            <% } %>
        </div>
        <input type="hidden" name="ID" value="<%=f.getID()%>"/>
        
        
        <button type="submit" class="btn btn-default " name="submit" value="Save"><%=varMap.get("Save")%></button>
        
        <button type="submit" class="btn btn-default" name="submit" value="Cancel"><%=varMap.get("Cancel")%></button>
    </form>
    <% }%>
</div>


<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>






