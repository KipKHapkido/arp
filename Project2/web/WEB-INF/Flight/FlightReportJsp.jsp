<%-- 
    Document   : FlightReportJsp
    Created on : 28-nov-2016, 13:38:35
    Author     : Jeff
--%>

<%@page import="Models.ViewModel.Flight.FlightReportVM"%>
<%@page import="java.util.List"%>
<%@page import="Models.Domain.Flight"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% FlightReportVM vm = (FlightReportVM) request.getAttribute("VM");%>
<% List<Flight> lf = (List) vm.getFlightList();%>
<% Map<String, String> varMap = (Map<String, String>) vm.getVarMap(); %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<jsp:include page="../Includes/NavBarAirline.jsp"/>

<div class="col-md-10">
    <div class="well">
        <form action="FlightReport" method="post" class="form-inline">
            <div class="form-group">
                <label for="select"><%= varMap.get("Select")%></label><br>
                <select id="select" class="form-control" name="search" >
                    <% for (Flight f : lf) {%>
                    <option class="form-control" value="<%=f.getID()%>"><%=f.getFlightNR()%></option><%}%>
                </select>
                 <button type="submit" name="submit" class="btn btn-default" value="search"><%= varMap.get("Search")%></button>
            </div>

            <!--    <div class="form-group">
                    <input type="text" name="search"/>
                    <button type="submit" name="submit" class="btn btn-default" value="search"><%= varMap.get("Search")%></button>    
                </div>-->
        </form>

        <h2><%=varMap.get("Title")%></h2>
        <table class="table table-responsive table-striped table-bordered">
            <tr>
                <th><%=varMap.get("FlightNR")%></th>                
                <th><%=varMap.get("Airplane")%></th>
                <th><%=varMap.get("DepartDate")%></th>
                <th><%=varMap.get("DepartureAirport")%></th>
                <th><%=varMap.get("Destination")%></th>
                <th><%=varMap.get("State")%></th>
                <th><%= varMap.get("Search")%></th>
            </tr>

            <% for (Flight f : lf) {%>

            <tr>
                <td><%=f.getFlightNR()%> </td>
                <td><%=f.getAirplane().getCode()%></td>
                <td><%=f.getDepartDate()%> </td>
                <td><%=f.getDepartAirport().getName()%></td>
                <td><%=f.getDestinationAirport().getName()%></td>
                <td><%=f.getFlightstate().getState() %></td>
                <td>
                    <form action="FlightReport" method="post">
                        <input type="hidden" value="<%=f.getID()%>" name="search">
                        <Button type="submit" class="btn btn-default glyphicon glyphicon-eye-open"
                                data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Search")%>">
                    </form>
                </td>
            </tr>

            <%}%>      
        </table>

    </div>
</div>



<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>

<!-- Coderead comments
    regels 34-37
        oude code in comment
-->