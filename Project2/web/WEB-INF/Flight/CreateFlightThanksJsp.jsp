<%-- 
    Document   : CreateFlightThanksJsp.jsp
    Created on : 15-nov-2016, 15:48:49
    Author     : CharlotteHendrik
--%>

<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% BaseVM VM = (BaseVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->

<jsp:include page="../Includes/NavBarAirline.jsp"/>
<div class="col-md-10">
    <div class="well">
        <h2><%=varMap.get("ThankYou")%></h2>
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>

