<%-- 
    Document   : Template:OneViewToRuleThemAll
    Created on : 25-okt-2016, 20:30:00
    Author     : Sven Vervloet
--%>



<%@page import="Models.ViewModel.Flight.CompleteFlightVM"%>
<%@page import="Models.Domain.Runway"%>
<%@page import="java.util.List"%>
<%@page import="Models.Domain.TimeClass"%>
<%@page import="Models.Domain.Flight"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% CompleteFlightVM VM = (CompleteFlightVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap(); %>
<% Flight f = (Flight) VM.getFlight();%>
<% List<TimeClass> timeList = (List<TimeClass>) VM.getTimeList();%>
<% List<Runway> runwayList = (List<Runway>) VM.getRunwayList();%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet"/>
<!-- Ruimte voor eigen content -->

<div class="col-md-4">
    <h1><%=varMap.get("Complete")%> - <%if ((f.getDepartureTimeID() == 0) && (f.getRunwayID() == 0)) { %>Step 1<% } else { %>Final step<% } %></h1>
    <jsp:include page="../Includes/Messages.jsp"/>
    <% if (f != null) {%>
    <form action="CompleteFlight" method="post" class="form-horizontal">
        <div class="form-group">
            <label for="FlightNR"><%=varMap.get("FlightNR")%></label>
            <input type="text" class="form-control" id="FlightNR" value="<%=f.getFlightNR()%>" disabled="disabled">
        </div>
        <div class="form-group">
            <label for="Airplane"><%=varMap.get("Airplane")%></label>
            <input type="text" class="form-control" id="Airplane" value="<%=f.getAirplane().getBuilder() + " " + f.getAirplane().getType() + " (" + f.getAirplane().getCode() + ") "%>" disabled="disabled">
        </div>
        <div class="form-group">
            <label for="DepartDate"><%=varMap.get("DepartDate")%></label>
            <input type="text" class="form-control" id="DepartDate" value="<%=f.getDepartDate()%>" disabled="disabled">
        </div>
        <div class="form-group">
            <label for="DepartFrom"><%=varMap.get("DepartFrom")%></label>
            <input type="text" class="form-control" id="DepartFrom" value="<%=f.getDepartFrom().getTimecol()%>" disabled="disabled">
        </div>
        <div class="form-group">
            <label for="DepartUntil"><%=varMap.get("DepartUntil")%></label>
            <input type="text" class="form-control" id="DepartUntil" value="<%=f.getDepartUntil().getTimecol()%>" disabled="disabled">
        </div>
        <div class="form-group">
            <label for="DepartureTimeID"><%=varMap.get("DepartTime")%></label>
            <% if ((timeList.size() > 0) && (f.getDepartureTimeID() == 0)) {%>
            <select class="form-control" id="DepartureTimeID" name="DepartureTimeID">
                <option value="0"><%=varMap.get("optionMessage")%></option>
                <%  for (TimeClass t : timeList) {%>
                <option value="<%=t.getID()%>"><%=t.getTimecol()%></option>
                <% } %>
            </select>
            <% } else{%>
            <input type="text" disabled="disabled" class="form-control" id="DepartureTime" value="<%if (f.getDepartTime() != null) {%><%=f.getDepartTime().getTimecol()%><%}%>"/>
            <% }%>

        </div>
        <div class="form-group">
            <label for="Departure"><%=varMap.get("Departure")%></label>
            <input type="text" class="form-control" id="Departure" value="<%=f.getDepartAirport().getName()%>" disabled="disabled">
        </div>
        <div class="form-group">
            <label for="Destination"><%=varMap.get("Destination")%></label>
            <input type="text" class="form-control" id="Destination" value="<%=f.getDestinationAirport().getName()%>" disabled="disabled">
        </div>
        <div class="form-group">
            <label for="RunwayID"><%=varMap.get("Runway")%></label>
            <% if ((runwayList.size() > 0) && (f.getRunwayID() == 0)) {%>
            <select class="form-control" id="RunwayID" name="RunwayID">

                <option value="0"><%=varMap.get("optionMessage")%></option>
                <%  for (Runway r : runwayList) {%>
                <option value="<%=r.getID()%>"><%=r.getName()%></option>
                <% } %>
            </select>
            <% } else { %>
            <input type="text" disabled="disabled" class="form-control" id="Runway" value="<%if (f.getRunway() != null) {%><%=f.getRunway().getName()%><%}%>"/>
            <% } %>
        </div>
        <input type="hidden" name="ID" value="<%=f.getID()%>"/>
        <%  String saveMessage = "";
            if ((f.getDepartureTimeID() == 0) && (f.getRunwayID() == 0)) {
                saveMessage = varMap.get("SaveContinue");
            } else {
                saveMessage = varMap.get("Save");
            }%>
            <%if ((timeList.size() > 0) || (runwayList.size() > 0)) { %>     
        <button type="submit" class="btn btn-default" name="submit" value="Save"><%=saveMessage%></button>
        <% } %>
        <button type="submit" class="btn btn-default" name="submit" value="Cancel"><%=varMap.get("Cancel")%></button>
    </form>
    <% }%>
</div>


<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>






