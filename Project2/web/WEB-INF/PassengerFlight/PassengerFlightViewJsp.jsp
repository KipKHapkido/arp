<%-- 
    Document   : Template:OneViewToRuleThemAll
    Created on : 25-okt-2016, 20:30:00
    Author     : Jo Brunau
--%>

<%@page import="Models.Domain.Passenger"%>
<%@page import="Models.ViewModel.PassengerFlightVM"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% PassengerFlightVM VM = (PassengerFlightVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap();%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<jsp:include page="/WEB-INF/Includes/NavBarAirline.jsp"/>


<div class="col-md-10">
    <div class="well">
        <% if (VM.getFlightNr() != null) {%>
        <h2><%=varMap.get("TitlePassengerFlight")%><%=VM.getFlightNr()%></h2>

        <div id="resultDiv" class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <tr>
                    <th><%= varMap.get("Passenger ID")%></th>
                    <th><%= varMap.get("First Name")%></th>
                    <th><%= varMap.get("Last Name")%></th>
                    <th><%= varMap.get("CheckedIn")%></th>  
                    <th><%= varMap.get("Actions")%></th>                                                     
                </tr>
                <% for (Passenger p : VM.getPassengerList()) {%>
                <tr>
                    <td >
                        <%= p.getID()%>
                    </td>
                    <td >
                        <%= p.getFirstName()%>
                    </td>
                    <td >
                        <%=  p.getSurName()%>
                    </td>
                    <td <% if (p.getCheckedIn() == 0) { %>
                        class="danger">
                        <% } else if (p.getCheckedIn() == 1) { %>
                        class="success">
                        <% }%>
                        <strong><% if (p.getCheckedIn() == 0 ){ %> Not Checked In <% } else if (p.getCheckedIn() == 1) { %> Checked In <% } %> </strong>
                        </td>
                        
                    </td>
                    <td >
                        <form action="PassengerDelete" method="post">
                            <button type="submit"
                                   name="submit"
                                   class="btn btn-default glyphicon glyphicon-remove"
                                   title="<%= varMap.get("Delete")%>"
                                   data-toggle="tooltip" 
                                   data-placement="left" 
                                   value="Delete Passenger"/>
                            <input type="hidden" name="flightId" value="<%= VM.getId() %>"/>
                            <input type="hidden" name="passengerId" value="<%= p.getID()%>" />
                        </form>
                        <form action="PassengerFlightCheckIn" method="post">
                            <button type="submit"
                                   name="submit"
                                   class="btn btn-default glyphicon glyphicon-user"
                                   title="<%= varMap.get("CheckIn")%>"
                                   data-toggle="tooltip" 
                                   data-placement="left" 
                                   value="Check In"/>
                            <input type="hidden" name="flightId" value="<%= VM.getId() %>"/>
                            <input type="hidden" name="passengerId" value="<%= p.getID()%>" />
                        </form>
                    </td>
                </tr>
                <%}%>               
            </table>
            <% if (VM.getPassengerList().isEmpty()) { %>
                <p><%=varMap.get("NoList")%></p>
            <% }%>
        </div>
            <% } else {%>
        <h2><%=varMap.get("TitlePassengerFlight")%></h2>
        <% }%>
    </div>
</div>

<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>






