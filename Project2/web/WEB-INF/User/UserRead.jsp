<%-- 
    Document   : UserCreate
    Created on : 2-dec-2016, 13:06:14
    Author     : Kathleen
--%>

<%@page import="Models.Domain.Function"%>
<%@page import="java.util.List"%>
<%@page import="Models.ViewModel.UserDetailVM"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% UserDetailVM VM = (UserDetailVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap();%>
<% List<Function> functionList = (List<Function>) VM.getFunctionList();%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<div class="col-md-10 col-md-offset-2">
<div class="well">
    <div>
        <h3><%= varMap.get("Read")%></h3>
    </div>
    <div>
        <table class="table table-striped">
            <tr>
                <th><%= varMap.get("Field")%></th>
                <th><%= varMap.get("Value")%></th>
            </tr>
            <tr>
                <td>
                    <%= varMap.get("Username")%> 
                </td>
                <td>
                    <%= VM.getUsername()%>
                </td>
            </tr>
            <tr>
                <td>
                    <%= varMap.get("Function")%>
                </td>
                <td>
                    <%= VM.getFunctionDescription()%>
                </td>
            </tr>
        </table>
    </div>   
    <a href="User" class="btn btn-default" value="Cancel"><%=varMap.get("Cancel")%></a>
</div>
</div>
<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
