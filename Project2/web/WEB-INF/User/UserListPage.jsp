<%-- 
    Document   : UserListPage
    Created on : 2-dec-2016, 13:06:29
    Author     : Kathleen
--%>

<%@page import="Logic.Helpers.UserObject"%>
<%@page import="Models.Domain.Message"%>
<%@page import="Models.ViewModel.UserListVM"%>
<%@page import="Models.Domain.User"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% UserListVM VM = (UserListVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap();%>
<% UserObject userObject = (UserObject) session.getAttribute("userObject"); %>
<% List<User> userList = (List<User>) VM.getUserList();%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<jsp:include page="../Includes/Messages.jsp"/>
<div class="col-md-10 col-md-offset-2">
<div class="well">
    <div class="row" >
        <div class="well">

            <form class="form-search" Action="User" method="post">
                <label><%= varMap.get("Username")%>:</label>
                <input type="text" name="search"/>
                <button type="submit" name="submit" class="btn btn-default glyphicon glyphicon-search" value="search"
                        data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Search")%>"></button>
                <% if (userObject != null && userObject.isAdmin() == true) {%>
                <a href="UserCreate" class="btn btn-default" value="Create"><%= varMap.get("New")%></a>
                <% }%>                  
            </form>
        </div>
        <div class="gridContent">
            <table  class="table table-striped">
                <tr>
                    <th><%= varMap.get("Username")%></th>
                    <th><%= varMap.get("Function")%></th>
                    <th><%= varMap.get("Actions")%></th>                                                     
                </tr>    
                <% for (User u : userList) {%>
                <tr>
                    <td >
                        <%=  u.getUsername()%>
                    </td>
                    <td>
                        <%= u.getFunction().getDescription()%>
                    </td>
                    <td >

                        <form action="UserDelete" method="post">
                            <% if (userObject != null && userObject.isAdmin() == true) {%>
                            <a href="UserUpdate?Id=<%=  u.getID()%>" class="btn btn-default glyphicon glyphicon-pencil"
                               data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Edit")%>"></a>
                                <% if (userObject != null && !userObject.getUsername().equals(u.getUsername())) {%>
                                <button type="submit" class="btn btn-default glyphicon glyphicon-remove"
                                        data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Delete")%>"></button>
                                <input type='hidden' name='user_Id' value="<%=  u.getID()%>">
                                <% }%>
                            <% }%>
                            <a href="UserRead?Id=<%=  u.getID()%>" class="btn btn-default glyphicon glyphicon-eye-open"
                               data-toggle="tooltip" data-placement="left" title="<%= varMap.get("Details")%>"></a>
                        </form>   

                    </td>
                </tr>
                <%}%>  
            </table>
        </div>        
    </div>
</div>

    <!-- Einde Ruimte voor eigen content -->
    <jsp:include page="../Includes/Footer.jsp"/>

