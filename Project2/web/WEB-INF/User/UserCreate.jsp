<%-- 
    Document   : UserCreate
    Created on : 2-dec-2016, 13:06:14
    Author     : Kathleen
--%>

<%@page import="Models.Domain.Function"%>
<%@page import="java.util.List"%>
<%@page import="Models.ViewModel.UserDetailVM"%>
<%@page import="java.util.Map"%>
<%@page import="Models.ViewModel.BaseVM"%>
<% UserDetailVM VM = (UserDetailVM) request.getAttribute("VM"); %>
<% Map<String, String> varMap = (Map<String, String>) VM.getVarMap();%>
<% List<Function> functionList = (List<Function>) VM.getFunctionList(); %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Includes/Header.jsp"/>
<jsp:include page="../Includes/NavBar.jsp"/>
<!-- Ruimte voor eigen content -->
<div class="col-md-10 col-md-offset-2">
<div class="well">
    <div>
        <h3><%= varMap.get("Create")%></h3>
    </div>
    <div>
        <form action="UserCreate" method="post">
            <div class="form-group">
                <label for="user_username"><%= varMap.get("Username")%></label>
                <input type="text" class="form-control" name="user_username" placeholder="<%= varMap.get("Username")%>">
            </div>
            <div class="form-group">
                <label for="user_password"><%= varMap.get("Password")%></label>
                <input type="Password" class="form-control" name="user_password" placeholder="<%= varMap.get("Password")%>">
            </div>
            <div class="form-group">
                <label for="user_function"><%= varMap.get("Function")%></label>
                <select name="user_function" class="form-control">
                    <% for (Function f : functionList) {%>
                    <option value="<%=  f.getID() %>"><%= f.getDescription() %></option>
                    <%}%>
                </select>
            </div>
                    <a href="User" class="btn btn-default" value="Cancel"><%=varMap.get("Cancel")%></a>
            <button type="submit" class="btn btn-default"><%= varMap.get("Submit")%></button>
        </form> 
    </div>       
</div>
</div>
<!-- Einde Ruimte voor eigen content -->
<jsp:include page="../Includes/Footer.jsp"/>
