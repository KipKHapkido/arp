/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.BaseDAO.DeusDAO;
import Models.Domain.Passenger;
import Models.Domain.PassengerList;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Dries
 */
public class PassengerListDAO extends DeusDAO{

    public PassengerListDAO(PassengerList pl) {
        super(pl);
    }
    
    public List<Passenger> getPassengerListByID(int flightID){
        List<Passenger> passengers = new ArrayList<>();
        
        //Zet de Connectie op
        Connection connect = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.            
            PreparedStatement statement1 = connect.prepareStatement("SELECT * from passenger_flight, " +
                    "(SELECT passenger.Firstname from passenger where PassengerID = passenger.ID) AS Firstname," +
                    "(SELECT passenger.Surname from passenger where PassengerID = passenger.ID) AS Surname," +
                    "(SELECT passenger.Birthday from passenger where PassengerID = passenger.ID) AS Birthday," +
                    "(SELECT passenger.AdresID from passenger where PassengerID = passenger.ID) AS AddressID," +
                    "(SELECT passenger.TypeID from passenger where PassengerID = passenger.ID) AS TypeID," +
                    "FROM " +
                    "   passengers_flight left join " +
                    "   passenger ON (passengerID = passenger.ID) left join " +
                    "WHERE FlightID = ?"
            );
            
            statement1.setInt(1, flightID);
            
            statement1.executeQuery();
            ResultSet result = statement1.getResultSet();
            
            PassengerDAO myPassDAO = new PassengerDAO();

            //Loop over de resultSet en doe je ding.
            while (result.next()) {                
                Passenger p = new Passenger();
                p.setID(result.getInt("PassengerID"));
                p.setAddressID(result.getInt("AddressID"));
                p.setTypeID(result.getInt("TypeID"));
                p.setFirstName(result.getString("Firstname"));
                p.setSurName(result.getString("Surname"));
                p.setBirthday(result.getTimestamp("Birthday"));
                p.setCheckedIn(myPassDAO.isCheckedIn(p.getID(),flightID));
                passengers.add(p);
                
            }
        } catch (ClassNotFoundException | SQLException exc) {
            System.out.println(exc);
        } finally {
            try {
                //connect.close();
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return passengers;
        }
    }
    
    public int getPassengerCountByID(int flightID){
        int count = 0;
        
        //Zet de Connectie op
        Connection connect = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.            
            PreparedStatement statement = connect.prepareStatement("SELECT COUNT(*) AS count FROM passenger_flight"
                    + " WHERE FlightID = ? ;"
            );
            
            statement.setInt(1, flightID);
            
            ResultSet rs = statement.executeQuery();
            
            while(rs.next())
                count = rs.getInt("count");
                
            
        } catch (ClassNotFoundException | SQLException exc) {
            System.out.println(exc);
        } finally {
            try {
                //connect.close();
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return count;
        }
    }
    
    public boolean addPassenger(int flightID, Passenger p){
        int affectedRows;
        int id = -1;
        String tableName;
        
        Connection connect = null;
        
        tableName = "passenger_flight";
        
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            //Zet de Connectie op
            connect = DriverManager.getConnection(super.GetConnectionString());

            //Query beginnen maken alles behalve id wordt opgevuld met waarde van meegegeven object b
            //INSERT INTO table_name (column1,column2,column3,...)
            //VALUES (value1,value2,value3,...);
           
            String query = "INSERT INTO " + tableName + "(";
            String query2 = " VALUES (";
       
            query += "FlightID, PassengerID)";
            
            query2 += flightID + ", " + p.getID() + ")";
            
            query += query2;

            //PreparedStatement klaarmaken
            PreparedStatement st = connect.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            
            affectedRows = st.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating enity failed, no rows affected.");
            }

            try (ResultSet generatedKeys = st.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt(1);
                } else {
                    throw new SQLException("Creating of entity failed, no ID obtained.");
                }
            }

        } catch (ClassNotFoundException | SQLException exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }
        
        return (id > 0);
        
    }
}
