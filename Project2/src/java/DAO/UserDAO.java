/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.BaseDAO.DeusDAO;
import Models.Datamodels.FilterCriteria;
import Models.Domain.Function;
import Models.Domain.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kathleen
 */
public class UserDAO extends DeusDAO<User> {
    public UserDAO() 
    {
        super(new User());
    }
    
    public User getUserByUsername(String username) {
        User user = new User();
        Function function;
        String query;
        Connection connect = null;

        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            //Zet de Connectie op
            connect = DriverManager.getConnection(super.GetConnectionString());
            //query 
            query = "SELECT * FROM user INNER JOIN function ON user.FunctionID = function.ID WHERE Username = ?";
            PreparedStatement pst = connect.prepareStatement(query);
            pst.setString(1, username);

            ResultSet result = pst.executeQuery();

            //User teruggeven
            while (result.next()) {
                user = new User();
                function = new Function();
                user.setID(result.getInt("ID"));
                user.setUsername(result.getString("Username"));
                user.setPassword(result.getBytes("Password"));
                user.setSalt(result.getBytes("Salt"));
                user.setFunctionID(result.getInt("user.FunctionID"));
                
                function.setID(result.getInt("function.ID"));
                function.setDescription(result.getString("function.Description"));

                user.setFunction(function);
            }

            pst.close();

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }

        return user;
    }
    
    public List<User> getAllWithRelations(FilterCriteria filterCriteria) {
        User user;
        Function function;
        String query;
        List<User> userList = new ArrayList<User>();
        PreparedStatement pst;
        ResultSet result;
        Connection connect = null;
        
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            //Zet de Connectie op
            connect = DriverManager.getConnection(super.GetConnectionString());
            //query

            if (filterCriteria != null && filterCriteria.getField() != "" && filterCriteria.getSearchCriteria() != "") {
                query = "SELECT * FROM user INNER JOIN function ON user.FunctionID = function.ID WHERE UPPER(" + filterCriteria.getField() + ") LIKE  UPPER(?) ";
                pst = connect.prepareStatement(query);
                pst.setString(1, "%" + filterCriteria.getSearchCriteria() + "%");
            } else {
                query = "SELECT * FROM user INNER JOIN function ON user.FunctionID = function.ID";
                pst = connect.prepareStatement(query);
            }

            result = pst.executeQuery();

            while (result.next()) {
                user = new User();
                function = new Function();

                user.setID(result.getInt("user.ID"));
                user.setUsername(result.getString("user.Username"));
                user.setPassword(result.getBytes("user.Password"));
                user.setSalt(result.getBytes("user.Salt"));
                user.setFunctionID(result.getInt("user.FunctionID"));
                
                function.setID(result.getInt("function.ID"));
                function.setDescription(result.getString("function.Description"));

                user.setFunction(function);

                userList.add(user);
            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }

        return userList;
    }
    
    public int UpdateWithoutPassword(User user)
    {
        Connection connect = null;
        int returnValue = -1;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            //Zet de Connectie op
            connect = DriverManager.getConnection(super.GetConnectionString());

            String query = "UPDATE USER SET Username = ?, FunctionId = ? where Id = ?";
            
            PreparedStatement st = connect.prepareStatement(query);
            st.setString(1, user.getUsername());
            st.setInt(2, user.getFunctionID());
            st.setInt(3, user.getID());

            returnValue = st.executeUpdate();
        } catch (Exception exc) {
            System.out.println(exc);
        }
        finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            
        }
        return returnValue;
    }
}
