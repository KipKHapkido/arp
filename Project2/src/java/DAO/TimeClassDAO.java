/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.BaseDAO.DeusDAO;
import Models.Domain.TimeClass;

/**
 *
 * @author CharlotteHendrik
 */
public class TimeClassDAO extends DeusDAO<TimeClass> {
    
    public TimeClassDAO (TimeClass t){
        super(t);
    }
    
}
