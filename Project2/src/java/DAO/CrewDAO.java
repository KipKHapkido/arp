/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.BaseDAO.DeusDAO;
import Models.Domain.Crew;
import Models.Domain.Function;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CrewDAO extends DeusDAO<Crew> {

    public CrewDAO(Crew d) {
        super(d);
    }

    @SuppressWarnings({"FinallyDiscardsException", "null", "UseSpecificCatch"})
    public List<Crew> GetCrewMembersByFlight(int FlightID) throws SQLException {
        List<Crew> deLijst = new ArrayList<>();
        //Zet de Connectie op
        Connection connect = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            String query = "SELECT distinct employee.*, Description FROM pr4_luchthaven.flight "
                    + "left join employee_flight on (flight.ID = employee_flight.FlightID) "
                    + "left join employee on (employee_flight.EmployeeID = employee.ID) "
                    + "left join function on (employee.Function = function.ID) "
                    + "where flight.ID = ? order by function, Surname ASC";

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            PreparedStatement st = connect.prepareStatement(query);
            st.setInt(1, FlightID);

            ResultSet result = st.executeQuery();

            //Loop over de resultSet en doe je ding.
            while (result.next()) {
                Crew cr = new Crew();

                cr.setID(result.getInt("ID"));
                cr.setFirstname(result.getString("Firstname"));
                cr.setSurname(result.getString("Surname"));
                cr.setFunction(result.getInt("Function"));

                Function f = new Function();
                f.setDescription(result.getString("Description"));
                cr.setDescription(f);

                deLijst.add(cr);

            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return deLijst;
        }

    }

    public List<Crew> GetAvailableCrew(int FlightID) throws SQLException {

        List<Crew> deLijst = new ArrayList<>();
        //Zet de Connectie op
        Connection connect = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            String query = "SELECT * FROM pr4_luchthaven.employee left join function on (employee.Function = function.ID) where (Function = 3 OR Function = 4) "
                    + "AND employee.ID NOT IN (SELECT EmployeeID FROM employee_flight where FlightID = ?) "
                    + "ORDER BY Function, Surname, Firstname";

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            PreparedStatement st = connect.prepareStatement(query);
            st.setInt(1, FlightID);

            ResultSet result = st.executeQuery();

            //Loop over de resultSet en doe je ding.
            while (result.next()) {
                Crew cr = new Crew();

                cr.setID(result.getInt("ID"));
                cr.setFirstname(result.getString("Firstname"));
                cr.setSurname(result.getString("Surname"));
                cr.setFunction(result.getInt("Function"));

                Function f = new Function();
                f.setDescription(result.getString("Description"));
                cr.setDescription(f);

                deLijst.add(cr);

            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return deLijst;
        }
    }

    public int AddCrewToFlight(int EmployeeID, int FlightID) throws SQLException {
        Connection connect = null;
        PreparedStatement statement = null;
        int affectedRows = 0;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());

            String query = "INSERT INTO employee_flight (EmployeeID, FlightID) VALUES(?, ?) ";

            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            statement = connect.prepareStatement(query);

            statement.setInt(1, EmployeeID);
            statement.setInt(2, FlightID);

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            affectedRows = statement.executeUpdate();

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }
        return affectedRows;
    }

    public int RemoveCrewmemberFromFlight(int EmployeeID, int FlightID) throws SQLException {
        Connection connect = null;
        int affectedRows = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());

            String query = "DELETE FROM employee_flight WHERE EmployeeID = ? AND FlightID = ? ";

            PreparedStatement statement = connect.prepareStatement(query);
            statement.setInt(1, EmployeeID);
            statement.setInt(2, FlightID);

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            affectedRows = statement.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex);
        } finally {
            try {
                connect.close();
            } catch (Exception ex) {
                System.out.println(ex);

            }

        }
         return affectedRows;
    }

}
