/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.BaseDAO;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author JurgenVanEynde
 */
public abstract class DeusDAO<T extends Deus> {

    //T is uw klasseobject
    private T D;
    private String connectionUrl;
    private Class[] arg1 = {Integer.TYPE};
    private Class[] arg2 = {String.class};
    private Class[] arg3 = {Boolean.class};
    private Class[] arg4 = {Date.class};
    private Class[] arg5 = {Double.TYPE};
    private Class[] arg6 = {Timestamp.class};
    private Class[] arg7 = {byte[].class};
    Connection connect = null;

    public DeusDAO(T d) {
        connectionUrl = "jdbc:mysql://149.210.165.55/pr4_luchthaven?user=LuchtUser&password=Wlpihik1617";
       
        D = d;
    }

    public T GetByID(int i) {
        T returnItem = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            //Zet de Connectie op
            connect = DriverManager.getConnection(connectionUrl);
            //mapping van model binnenhalen eerste lijn moet id lijn zijn in dit geval
            List<DataMapping> mapping = D.getDmList();
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            //kan niet dynamisch wegens single quotes
            //enkel binnen programma is string building te gebruiken anders injection !!!!!!
            String query = "SELECT * FROM " + D.getTableName() + " WHERE " + mapping.get(0).getColumnName();
            PreparedStatement st = connect.prepareStatement(query + " = ?");
            st.setInt(1, i);
            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet           
            ResultSet result = st.executeQuery();
            //return item declareren
            returnItem = (T) D.getClass().newInstance();
            //resultset afgaan en per methode bekijken of in mapping van T
            while (result.next()) {
                for (int j = 0; j < mapping.size(); j++) {
                    DataMapping row = mapping.get(j);
                    // eerste mapping moet altijd de ID zijn
                    if (j == 0) {
                        D.getClass().getSuperclass().getDeclaredMethod("set" + row.getProperty(), arg1)
                                .invoke(returnItem, result.getInt(row.getColumnName()));
                        continue;
                    }
                    //switch case voor meerdere types
                    switch (row.getType()) {
                        case "int": //arg1 is int.type
                            D.getClass().getDeclaredMethod("set" + row.getProperty(), arg1)
                                    .invoke(returnItem, result.getInt(row.getColumnName()));
                            break;
                        case "String": //arg2 is string class
                            D.getClass().getDeclaredMethod("set" + row.getProperty(), arg2)
                                    .invoke(returnItem, result.getString(row.getColumnName()));
                            break;
                        case "Boolean": //arg3 is boolean.class
                            D.getClass().getDeclaredMethod("set" + row.getProperty(), arg3)
                                    .invoke(returnItem, result.getBoolean(row.getColumnName()));
                            break;
                        case "Date": //arg4 is date.class
                            D.getClass().getDeclaredMethod("set" + row.getProperty(), arg4)
                                    .invoke(returnItem, result.getDate(row.getColumnName()));
                            break;
                        case "Double": //arg5 is double.type
                            D.getClass().getDeclaredMethod("set" + row.getProperty(), arg5)
                                    .invoke(returnItem, result.getDouble(row.getColumnName()));
                            break;
                        case "Timestamp": //arg6 is timestamp.type
                            D.getClass().getDeclaredMethod("set" + row.getProperty(), arg6)
                                    .invoke(returnItem, result.getTimestamp(row.getColumnName()));
                            break;
                         case "Bytes": //arg7 is byte[].class
                             D.getClass().getDeclaredMethod("set" + row.getProperty(), arg7)
                                    .invoke(returnItem, result.getBytes(row.getColumnName()));
                            break;
                        default:
                            throw new Exception("Unknown type returned by DB");
                    }
                }
            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return returnItem;
        }

    }

    public List<T> GetAll() {
        //lijst om terug te geven        
        List<T> listAll = new ArrayList<>();
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            //Zet de Connectie op
            connect = DriverManager.getConnection(connectionUrl);
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.            
            List<DataMapping> mapping = D.getDmList();
            String s = "SELECT * FROM " + D.getTableName();
            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            PreparedStatement st = connect.prepareStatement(s);
            ResultSet result = st.executeQuery();
            //nodig om lijst op te vullen met unieke instanties
            T temp;
            //resultset afgaan en per methode bekijken of in mapping van T
            while (result.next()) {
                temp = (T) D.getClass().newInstance();
                for (int j = 0; j < mapping.size(); j++) {
                    DataMapping row = mapping.get(j);
                    if (j == 0) {
                        D.getClass().getSuperclass().getDeclaredMethod("set" + row.getProperty(), arg1)
                                .invoke(temp, result.getInt(row.getColumnName()));
                        continue;
                    }
                    //switch case voor meerdere types
                    switch (row.getType()) {
                        case "int":
                            D.getClass().getDeclaredMethod("set" + row.getProperty(), arg1)
                                    .invoke(temp, result.getInt(row.getColumnName()));
                            break;
                        case "String":
                            D.getClass().getDeclaredMethod("set" + row.getProperty(), arg2)
                                    .invoke(temp, result.getString(row.getColumnName()));
                            break;
                        case "Boolean":
                            D.getClass().getDeclaredMethod("set" + row.getProperty(), arg3)
                                    .invoke(temp, result.getBoolean(row.getColumnName()));
                            break;
                        case "Date": //arg4 is date.class
                            D.getClass().getDeclaredMethod("set" + row.getProperty(), arg4)
                                    .invoke(temp, result.getDate(row.getColumnName()));
                            break;
                        case "Double": //arg5 is double.type
                            D.getClass().getDeclaredMethod("set" + row.getProperty(), arg5)
                                    .invoke(temp, result.getDouble(row.getColumnName()));
                            break;
                        case "Timestamp": //arg6 is timestamp.class
                            D.getClass().getDeclaredMethod("set" + row.getProperty(), arg6)
                                    .invoke(temp, result.getTimestamp(row.getColumnName()));
                            break;
                         case "Bytes": //arg7 is byte[].class
                            D.getClass().getDeclaredMethod("set" + row.getProperty(), arg7)
                                    .invoke(temp, result.getBytes(row.getColumnName()));
                            break;
                        default:
                            throw new Exception("Unknown type returned by DB");
                    }
                }
                listAll.add(temp);
            }
        } catch (Exception exc) {
            System.out.println(exc);
            listAll = null;
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return listAll;
        }

    }

    public int Save(T temp) {
        //Deus kan nooit ID null hebben dus ingesteld op -1 voor nieuwe instantie
        if (temp.getID() > 0) {
            return Update(temp);
        } else {
            return Create(temp);
        }
    }

    public int Create(T temp) {
        int returnValue = -1;
        int affectedRows;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            //Zet de Connectie op
            connect = DriverManager.getConnection(connectionUrl);
            //Mapping laden
            List<DataMapping> mapping = D.getDmList();
            //Query beginnen maken alles behalve id wordt opgevuld met waarde van meegegeven object b
            //INSERT INTO table_name (column1,column2,column3,...)
            //VALUES (value1,value2,value3,...);
           
            String query = "INSERT INTO " + temp.getTableName() + " ( ";
            String query2 = " VALUES (";
       
            for (int i = 1; i < mapping.size(); i++) {
                //voor elk rij van datamapping te beginnen op 1 omdat 0 de id is
                DataMapping r = mapping.get(i);
                //voor alle values moet een komma komen behalve de eerste
             
                if (i != 1) {
                    query += ", ";
                    query2 += ", ";
                }
                query += r.getColumnName();
                query2 += "?";
                //als het de laatste rij is dan finaliseren we de query
                if (i == mapping.size() - 1) {
                    query += ")";
                    query2 += ")";
                }
            }
            query += query2;
            //PreparedStatement klaarmaken
            PreparedStatement st = connect.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            //PreparedStatement aanvullen
            for (int i = 1; i < mapping.size(); i++) {
                DataMapping r = mapping.get(i);
                Method m = D.getClass().getDeclaredMethod("get" + r.getProperty());
                //switch case voor meerdere types
                switch (r.getType()) {
                    case "int":
                        Integer test = (int) m.invoke(temp);
                        if (test==0){
                            test = null;
                            st.setNull(i, java.sql.Types.INTEGER);
                        }
                        else{
                        st.setInt(i, test);}
                        break;
                    case "String":
                        //System.out.println(m.getName());
                        st.setString(i, (String) m.invoke(temp));
                        break;
                    case "Boolean":
                        st.setBoolean(i, (Boolean) m.invoke(temp));
                        break;
                    case "Date":
                        Date da = (Date) m.invoke(temp);
                        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                        Date parsed = format.parse(da.toString());
                        java.sql.Date sql = new java.sql.Date(parsed.getTime());
                        st.setDate(i, sql);
                        break;
                    case "Double":
                        st.setDouble(i, (Double) m.invoke(temp));
                        break;
                    case "Timestamp":
                        st.setTimestamp(i, (Timestamp) m.invoke(temp));
                        break;
                    case "Bytes":
                        st.setBytes(i, (byte[]) m.invoke(temp));
                        break;
                    default:
                        throw new Exception("Unknown type");
                }
            }
      
            affectedRows = st.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating enity failed, no rows affected.");
            }
            try (ResultSet generatedKeys = st.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    returnValue = generatedKeys.getInt(1);
                } else {
                    throw new SQLException("Creating of entity failed, no ID obtained.");
                }
            }
        } catch (Exception exc) {
            System.out.println(exc);
            returnValue = -1;
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }
        return returnValue;
    }

    public int Update(T temp) {
        int returnValue = -1;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(connectionUrl);
            List<DataMapping> mapping = D.getDmList();
            String query = "UPDATE " + temp.getTableName() + " SET ";
            for (int i = 1; i < mapping.size(); i++) {
                DataMapping r = mapping.get(i);
                if (i != 1) {
                    query += ", ";
                }
                query += r.getColumnName() + " = ? ";
            }
            query += "WHERE id = ?";
            PreparedStatement st = connect.prepareStatement(query);
            for (int i = 1; i < mapping.size(); i++) {
                DataMapping r = mapping.get(i);
                Method m = D.getClass().getDeclaredMethod("get" + r.getProperty());                
                switch (r.getType()) {
                    case "int":
                        Integer test = (int) m.invoke(temp);
                        if (test==0){
                            test = null;
                            st.setNull(i, java.sql.Types.INTEGER);
                        }
                        else{
                        st.setInt(i, test);}
                        break;
                    case "String":
                        //System.out.println(m.getName());
                        st.setString(i, (String) m.invoke(temp));
                        break;
                    case "Boolean":
                        st.setBoolean(i, (Boolean) m.invoke(temp));
                        break;
                    case "Date":
                        Date da = (Date) m.invoke(temp);
                        st.setDate(i, new java.sql.Date(da.getTime()));
                        break;
                    case "Double":
                        st.setDouble(i, (Double) m.invoke(temp));
                        break;
                    case "Timestamp":
                        st.setTimestamp(i, (Timestamp) m.invoke(temp));
                        break;
                     case "Bytes":
                        st.setBytes(i, (byte[]) m.invoke(temp));
                        break;
                    default:
                        throw new Exception("Unknown type");
                }
            }
            //de id nu instellen
            st.setInt(mapping.size(), temp.getID());

            returnValue = st.executeUpdate();
        } catch (Exception exc) {
            System.out.println(exc);
        }
        finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            
        }
        return returnValue;

    }

    public int Delete(int id) {
        int returnValue = -1;
        try {
            ///Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            //Zet de Connectie op
            connect = DriverManager.getConnection(connectionUrl);

            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.       
            String query = "DELETE FROM " + D.getTableName() + " WHERE " + D.getDmList().get(0).getColumnName() + " = ?";

            PreparedStatement st = connect.prepareStatement(query);
            st.setInt(1, id);
            returnValue = st.executeUpdate();
            //aanhangsels misschien ook nog deleten            
        } catch (Exception exc) {
            System.out.println(exc);
        }finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return returnValue;
        }
    }

    public String GetConnectionString() {
        return connectionUrl;
    }
}
