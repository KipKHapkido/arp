/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.BaseDAO.DeusDAO;
import Models.Domain.Airport;
import Models.Domain.Hangar;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Erik Michielsen
 */
public class HangarDAO extends DeusDAO<Hangar> {

    public HangarDAO(Hangar d) {
        super(d);
    }
    
    public List<Hangar> getAllEager(){
        List<Hangar> listAll = new ArrayList<Hangar>();
        Connection connect = null;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(super.GetConnectionString());
            PreparedStatement st = connect.prepareStatement(
                    "SELECT hangar.ID, hangar.Name, hangar.Capacity, hangar.AirportID, "
                    + "airport.Name AS AirportName, airport.AddressID AS AirportAddress FROM hangar "
                    + "LEFT JOIN airport ON hangar.AirportID = airport.ID");
            ResultSet result = st.executeQuery();
            while(result.next()){
                Hangar temp =  new Hangar();
                temp.setID(result.getInt("ID"));
                temp.setName(result.getString("Name"));
                temp.setCapacity(result.getString("Capacity"));
                temp.setAirportID(result.getInt("AirportID"));
                Airport temp2 = new Airport() ;
                temp2.setID(result.getInt("AirportID"));
                temp2.setName(result.getString("AirportName"));
                temp2.setAddressID(result.getInt("AirportAddress"));
                temp.setAirportObj(temp2);
                listAll.add(temp);
            }
        }catch(Exception exc){
            System.out.println(exc);
            listAll = null;
        }finally{
            try{
                connect.close();
            }
            catch(Exception exc){
                System.out.println(exc);
            }
            return listAll;
        }
    }
    
}
