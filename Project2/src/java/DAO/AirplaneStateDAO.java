/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.BaseDAO.DeusDAO;
import Models.Domain.AirplaneState;

/**
 *
 * @author Jelle
 */
public class AirplaneStateDAO extends DeusDAO<AirplaneState>{

    public AirplaneStateDAO() {
        super(new AirplaneState());
    }    
}
