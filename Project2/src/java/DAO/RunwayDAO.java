/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.BaseDAO.DeusDAO;
import Models.Domain.Airport;
import Models.Domain.Runway;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Jelle
 */
public class RunwayDAO extends DeusDAO<Runway> {

    public RunwayDAO(Runway d) {
        super(d);
    }
    
    public List<Runway> getAllEager(){
        List<Runway> listAll = new ArrayList<Runway>();
        Connection connect = null;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(super.GetConnectionString());
            PreparedStatement st = connect.prepareStatement(
                    "SELECT runway.ID, runway.Name, runway.Length, runway.AirportID, "
                    + "airport.Name AS AirportName, airport.AddressID AS AirportAddress FROM runway "
                    + "LEFT JOIN airport ON runway.AirportID = airport.ID");
            ResultSet result = st.executeQuery();
            while(result.next()){
                Runway temp =  new Runway();
                temp.setID(result.getInt("ID"));
                temp.setName(result.getString("Name"));
                temp.setLength(result.getInt("Length"));
                temp.setAirportID(result.getInt("AirportID"));
                Airport temp2 = new Airport() ;
                temp2.setID(result.getInt("AirportID"));
                temp2.setName(result.getString("AirportName"));
                temp2.setAddressID(result.getInt("AirportAddress"));
                temp.setAirportObj(temp2);
                listAll.add(temp);
            }
        }catch(Exception exc){
            System.out.println(exc);
            listAll = null;
        }finally{
            try{
                connect.close();
            }
            catch(Exception exc){
                System.out.println(exc);
            }
            return listAll;
        }
    }
    
}
