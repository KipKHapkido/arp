/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.BaseDAO.DeusDAO;
import Models.Datamodels.Status;
import Models.Domain.Airplane;
import Models.Domain.Airport;
import Models.Domain.Flight;
import Models.Domain.Runway;
import Models.Domain.FlightState;
import Models.Domain.TimeClass;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sven
 */
public class FlightDAO extends DeusDAO<Flight> {

    public FlightDAO(Flight f) {
        super(f);
    }

    public List<Flight> GetNotApprovedFlights() throws SQLException {
        List<Flight> deLijst = new ArrayList<>();
        //Zet de Connectie op
        Connection connect = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            Statement statement = connect.createStatement();

            String query = FlightQuery();
            query += " where Approved = 0 AND DepartDate >= curdate() AND FlightStateID is null"
                    + " order by DepartDate ASC, DepartureTimeID";

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            ResultSet result = statement.executeQuery(query);

            //Loop over de resultSet en doe je ding.
            while (result.next()) {
                Flight fl = new Flight();
                fl = ReadInFlight(result);
                deLijst.add(fl);

            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return deLijst;
        }

    }

    public Flight GetFlightByID(int FlightID) throws SQLException 
    {
        Flight fl = new Flight();
        //Zet de Connectie op
        Connection connect = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            //connect = DriverManager.getConnection("jdbc:mysql://149.210.165.55/pr4_luchthaven?user=LuchtUser&password=Wlpihik1617");
            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            String query = FlightQuery();
            query += "where flight.ID = ?";
            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet

            PreparedStatement st = connect.prepareStatement(query);
            st.setInt(1, FlightID);

            ResultSet result = st.executeQuery();

            //Loop over de resultSet en doe je ding.
            result.next();
            fl = ReadInFlight(result);

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return fl;
        }
    }

    public List<Flight> GetSearchFlights(int DestinationID, String FlightNR) {
        List<Flight> deLijst = new ArrayList<>();
        //Zet de Connectie op
        Connection connect = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            //connect = DriverManager.getConnection("jdbc:mysql://149.210.165.55/pr4_luchthaven?user=LuchtUser&password=Wlpihik1617");
            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            String query = FlightQuery();
            query += " where flight.DepartDate >= curdate() AND DepartureID = 1 ";

            if (DestinationID > 0) {
                query += "AND DestinationID = ? ";
            }
            if (!FlightNR.equals("")) {
                query += "AND FlightNR Like ? ";
            }

            query += "ORDER BY flight.DepartDate ASC";
            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet

            ResultSet result = null;
            if ((DestinationID > 0) || (!FlightNR.equals(""))) { // zijn er parameters doorgegeven?
                PreparedStatement st = connect.prepareStatement(query);
                if ((DestinationID > 0) && (FlightNR.equals(""))) {
                    st.setInt(1, DestinationID);
                } else if ((DestinationID == 0) && (!FlightNR.equals(""))) {
                    String FlightNRx = "%" + FlightNR + "%";
                    st.setString(1, FlightNRx);
                } else if ((DestinationID > 0) && (!FlightNR.equals(""))) {
                    st.setInt(1, DestinationID);
                    String FlightNRx = "%" + FlightNR + "%";
                    st.setString(2, FlightNRx);
                }
                result = st.executeQuery();
            } else { // het formulier werd leeg verzonden
                Statement statement = connect.createStatement();
                result = statement.executeQuery(query);
            }

            //Loop over de resultSet en doe je ding.
            while (result.next()) {
                Flight fl = new Flight();
                fl = ReadInFlight(result);

                deLijst.add(fl);

            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return deLijst;
        }

    }
    
    public List<Flight> GetIncomingFlights() {
        List<Flight> deLijst = new ArrayList<>();
        //Zet de Connectie op
        Connection connect = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            Statement statement = connect.createStatement();

            String query = FlightQuery();
            query += " where Approved = 1 AND DepartDate >= (curdate() - interval 1 day) AND (FlightStateID Between 11 AND 12 OR FlightStateID = 5) "
                    + " order by FlightStateID, DepartDate ASC";

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            ResultSet result = statement.executeQuery(query);

            //Loop over de resultSet en doe je ding.
            while (result.next()) {
                Flight fl = new Flight();
                fl = ReadInFlight(result);
                deLijst.add(fl);

            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return deLijst;
        }
    }

    public String FlightQuery() {
        String query
                = "SELECT (select count(passenger_flight.ID) from passenger_flight where flight.ID = passenger_flight.FlightID) AS Passengers, flight.*,  "
                + "(SELECT time.Timecol from time where ID = flight.DepartFromID) AS DepartFromTime,"
                + "(SELECT time.Timecol from time where ID = flight.DepartUntilID) AS DepartUntilTime,"
                + "(SELECT time.Timecol from time where ID = flight.DepartureTimeID) AS DepartureTime,"
                + "(SELECT time.Timecol from time where ID = flight.ArrivalTimeID) AS ArrivalTime,"
                + "(select airport.Name from airport where ID = flight.DepartureID) AS DepartAirport,"
                + "(select airport.Name from airport where ID = flight.DestinationID) AS DestinationAirport, "
                + "airplane.*, runway.Name AS RunwayName, flightstate.State AS FlightStateName "
                + "FROM "
                + "   flight left join "
                + "   airplane ON (flight.AirplaneID = airplane.ID) left join"
                + "   runway ON (flight.RunwayID = runway.ID) left join "
                + "    flightstate ON (flight.FlightStateID = flightstate.ID) ";
        return query;
    }

    private Flight ReadInFlight(ResultSet result) throws SQLException {
        Flight fl = new Flight();

        fl.setPassengers(result.getInt("Passengers"));
        fl.setID(result.getInt("ID"));
        fl.setFlightNR(result.getString("FlightNR"));
        fl.setDepartDate((java.sql.Date) result.getDate("DepartDate"));

        //t1 = DepartFrom
        fl.setDepartFromID(result.getInt("DepartFromID"));
        TimeClass t1 = new TimeClass();
        t1.setID(fl.getDepartFromID());
        t1.setTimecol(result.getTime("DepartFromTime"));
        fl.setDepartFrom(t1);

        //t2 = DepartUntil
        fl.setDepartUntilID(result.getInt("DepartUntilID"));
        TimeClass t2 = new TimeClass();
        t2.setID(fl.getDepartUntilID());
        t2.setTimecol(result.getTime("DepartUntilTime"));
        fl.setDepartUntil(t2);

        //t3 = DepartTime
        fl.setDepartureTimeID(result.getInt("DepartureTimeID"));
        if (fl.getDepartureTimeID() != 0) {
            TimeClass t3 = new TimeClass();
            t3.setID(fl.getDepartureTimeID());
            t3.setTimecol(result.getTime("DepartureTime"));
            fl.setDepartTime(t3);
        }

        //t4 = ArrivalTime
        fl.setArrivalDate((java.sql.Date) result.getDate("ArrivalDate"));
        fl.setArrivalTimeID(result.getInt("ArrivalTimeID"));
        if (fl.getArrivalTimeID() != 0) {
            TimeClass t4 = new TimeClass();
            t4.setID(fl.getArrivalTimeID());
            t4.setTimecol(result.getTime("ArrivalTime"));
            fl.setArrivalTime(t4);
        }
        
        fl.setDepartureID(result.getInt("DepartureID"));
        fl.setDestinationID(result.getInt("DestinationID"));

        fl.setApproved(result.getInt("Approved"));

        //DepartAirport
        Airport DepartAirport = new Airport();
        DepartAirport.setName(result.getString("DepartAirport"));
        fl.setDepartAirport(DepartAirport);

        //DestinationAirport
        Airport DestinationAirport = new Airport();
        DestinationAirport.setName(result.getString("DestinationAirport"));
        fl.setDestinationAirport(DestinationAirport);

        //Airplane
        Airplane plane = new Airplane();
        plane.setCode(result.getString("Code"));
        plane.setBuilder(result.getString("Builder"));
        plane.setType(result.getString("Type"));
        plane.setCapacity(result.getString("Capacity"));
        fl.setAirplane(plane);

        //Runway
        fl.setRunwayID(result.getInt("RunwayID"));
        if (fl.getRunwayID() != 0) {
            Runway r = new Runway();
            r.setID(result.getInt("ID"));
            r.setName(result.getString("RunwayName"));
            fl.setRunway(r);
        }

        //FlightState
        fl.setFlightStateID(result.getInt("FlightStateID"));
        if (fl.getFlightStateID() != 0) {
            FlightState fs = new FlightState();
            fs.setState(result.getString("FlightStateName"));
            fl.setFlightstate(fs);
        }
        return fl;
    }
    
    public List<TimeClass> GetAvailableTimes(Flight fl) {
        List<TimeClass> deLijst = new ArrayList<>();
        //Zet de Connectie op
        Connection connect = null;
        PreparedStatement st = null;
        PreparedStatement statement = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            List<Integer> usedTimes = new ArrayList<>();

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date parsed = format.parse(fl.getDepartDate().toString());
            java.sql.Date sql = new java.sql.Date(parsed.getTime());

            String query = "";
            if (fl.getDepartUntilID() < fl.getDepartFromID()) {
                query
                        = "SELECT  distinct DepartureTimeID FROM time, flight "
                        + "where "
                        + "time.ID <> flight.DepartureTimeID "
                        + "AND (time.ID between ? AND 48 AND flight.DepartDate = ?) OR "
                        + "(time.ID between 1 AND ? AND flight.DepartDate = ? + interval 1 day)";
                st = connect.prepareStatement(query);
                st.setInt(1, fl.getDepartFromID());
                st.setDate(2, sql);
                st.setInt(3, fl.getDepartUntilID());
                st.setDate(4, sql);
            } else {
                query
                        = "SELECT  distinct DepartureTimeID FROM time, flight "
                        + "where "
                        + "time.ID <> flight.DepartureTimeID "
                        + "AND time.ID between ? AND ?  "
                        + "AND flight.DepartDate = ?";
                st = connect.prepareStatement(query);
                st.setInt(1, fl.getDepartFromID());
                st.setInt(2, fl.getDepartUntilID());
                st.setDate(3, sql);
            }

            ResultSet result = st.executeQuery();

            //Loop over de resultSet en doe je ding.
            while (result.next()) {
                usedTimes.add(result.getInt("DepartureTimeID"));
            }

            ///
            StringBuilder sqlQuery = new StringBuilder();
            if (fl.getDepartUntilID() < fl.getDepartFromID()) {
                sqlQuery.append("SELECT distinct time.ID, time.Timecol FROM time, flight where (time.ID between ? AND 48 ) OR (time.ID between 1 AND ?) ");
            } else {
                sqlQuery.append("SELECT distinct time.ID, time.Timecol FROM time, flight where (time.ID between ? AND ? ) ");
            }
            if (usedTimes.size() > 0) {
                sqlQuery.append("AND time.ID NOT IN (");
                usedTimes.stream().forEach((timeslot) -> {
                    sqlQuery.append(timeslot.toString()).append(",");
                });
                if (sqlQuery.length() > 0) {
                    sqlQuery.deleteCharAt(sqlQuery.lastIndexOf(","));
                }
                sqlQuery.append(")");
            }

            statement = connect.prepareStatement(sqlQuery.toString());
            statement.setInt(1, fl.getDepartFromID());
            statement.setInt(2, fl.getDepartUntilID());
            result = statement.executeQuery();

            while (result.next()) {
                TimeClass t = new TimeClass();
                t.setID(result.getInt("ID"));
                t.setTimecol(result.getTime("Timecol"));

                deLijst.add(t);
            }

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                //connect.close();
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return deLijst;
        }

    }

    public List<Runway> GetAvailableRunways(Flight fl) {
        List<Runway> deLijst = new ArrayList<>();
        //Zet de Connectie op
        Connection connect = null;
        PreparedStatement st = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            List<Integer> usedRunway = new ArrayList<>();
            String query
                    = "SELECT RunwayID FROM flight where flight.DepartDate = ? AND (DepartureTimeID = ? OR ArrivalTimeID = ?)";

            st = connect.prepareStatement(query);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date parsed = format.parse(fl.getDepartDate().toString());
            java.sql.Date sql = new java.sql.Date(parsed.getTime());

            st.setDate(1, sql);
            st.setInt(2, fl.getDepartureTimeID());
            st.setInt(3, fl.getDepartureTimeID());

            ResultSet result = st.executeQuery();

            //Loop over de resultSet en doe je ding.
            while (result.next()) {
                usedRunway.add(result.getInt("RunwayID"));
            }

            ///
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("SELECT * FROM runway where AirportID = 1 ");
            if (usedRunway.size() > 0) {
                sqlQuery.append("AND ID NOT IN (");
                usedRunway.stream().forEach((runway) -> {
                    sqlQuery.append(runway.toString()).append(",");
                });
                if (sqlQuery.length() > 0) {
                    sqlQuery.deleteCharAt(sqlQuery.lastIndexOf(","));
                }
                sqlQuery.append(")");
            }
            Statement statement = connect.createStatement();
            result = statement.executeQuery(sqlQuery.toString());

            while (result.next()) {
                Runway r = new Runway();
                r.setID(result.getInt("ID"));
                r.setName(result.getString("Name"));
                deLijst.add(r);
            }

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                //connect.close();
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return deLijst;
        }

    }
    
    public int UpdateFlightTime(Flight f) throws SQLException {
        Connection connect = null;
        PreparedStatement statement = null;
        int returnValue = -1;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());

            String query = "UPDATE flight SET DepartureTimeID = ?";
            if (f.getDepartureTimeID() < f.getDepartFromID()) {
                query += ", DepartDate = date_add(DepartDate, interval + 1 DAY) ";
            }

            query += " WHERE id = ?";
            System.out.println(query);
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            statement = connect.prepareStatement(query);

            //Zet de parameters voor de preparedStatement, aan de hand van volgorde van de parameter, te beginnen bij 1
            statement.setInt(1, f.getDepartureTimeID());
            statement.setInt(2, f.getID());

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            returnValue = statement.executeUpdate();

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }
        return returnValue;
    }

    public int UpdateFlightRunway(Flight f) throws SQLException {
        Connection connect = null;
        PreparedStatement statement = null;
        int returnValue = -1;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());

            String query = "UPDATE flight SET RunwayID = ?";
            query += " WHERE id = ?";

            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            statement = connect.prepareStatement(query);

            //Zet de parameters voor de preparedStatement, aan de hand van volgorde van de parameter, te beginnen bij 1
            statement.setInt(1, f.getRunwayID());

            statement.setInt(2, f.getID());

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            returnValue = statement.executeUpdate();

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }
        return returnValue;
    }

    public Flight ApproveFlight(Flight f) throws SQLException {
        Connection connect = null;
        PreparedStatement statement = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());

            String query = "UPDATE flight SET ";
            query += "Approved = ? ";
            query += ", FlightStateID = ? ";
            query += "WHERE id = ?";

            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            statement = connect.prepareStatement(query);

            //Zet de parameters voor de preparedStatement, aan de hand van volgorde van de parameter, te beginnen bij 1
            statement.setInt(1, f.getApproved());
            statement.setInt(2, f.getFlightStateID());
            statement.setInt(3, f.getID());

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            statement.executeUpdate();

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }
        return f = GetFlightByID(f.getID());
    }

    public List<Airport> GetAllDestinations() {
        List<Airport> deLijst = new ArrayList<>();
        //Zet de Connectie op
        Connection connect = null;
        PreparedStatement st = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            String query
                    = "SELECT * FROM airport where ID <> ?";

            st = connect.prepareStatement(query);

            st.setInt(1, 1);

            ResultSet result = st.executeQuery();

            //Loop over de resultSet en doe je ding.
            while (result.next()) {
                Airport a = new Airport();
                a.setID(result.getInt("ID"));
                a.setName(result.getString("Name"));

                deLijst.add(a);
            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                //connect.close();
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return deLijst;
        }

    }

    public List<TimeClass> GetAllTimes() {
        List<TimeClass> lijst = new ArrayList<>();
        //Zet de Connectie op
        Connection connect = null;
        PreparedStatement st = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            String query
                    = "SELECT * FROM time";
            st = connect.prepareStatement(query);
            ResultSet result = st.executeQuery();
            ResultSetMetaData metaData = result.getMetaData();
            int columncount = metaData.getColumnCount();

            while (result.next()) {
                TimeClass t = new TimeClass();
                t.setID(result.getInt("ID"));
                t.setTimecol(result.getTime("Timecol"));

                lijst.add(t);
            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                //connect.close();
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return lijst;
        }
    }

    public void CreateFlight(Flight flight1) {

        Connection connect = null;
        PreparedStatement statement = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());

            String query = "INSERT INTO flight (FlightNR, DepartDate, DepartFromID, DepartUntilID, DepartureID, DestinationID, AirplaneID) VALUES(?, ?, ?, ?, ?, ?, ?) ";

            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            statement = connect.prepareStatement(query);

            //Zet de parameters voor de preparedStatement, aan de hand van volgorde van de parameter, te beginnen bij 1
            //SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            //Date parsed = format.parse(flight1.getDepartDate().toString());
            java.sql.Date sql = new java.sql.Date(flight1.getDepartDate().getTime());

            statement.setString(1, flight1.getFlightNR());
            statement.setDate(2, sql);
            statement.setInt(3, flight1.getDepartFromID());
            statement.setInt(4, flight1.getDepartUntilID());
            statement.setInt(5, flight1.getDepartureID());
            statement.setInt(6, flight1.getDestinationID());
            statement.setInt(7, flight1.getAirplaneID());

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            statement.executeUpdate();

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }

    }

    public int ChangeFlightStatus(int ID, int FlightStateID) {
        Connection connect = null;
        PreparedStatement statement = null;
        int returnValue = -1;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());

            String query = "UPDATE flight SET FlightStateID = ?, RunwayID = null ";
            query += " WHERE id = ?";

            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            statement = connect.prepareStatement(query);

            //Zet de parameters voor de preparedStatement, aan de hand van volgorde van de parameter, te beginnen bij 1
            statement.setInt(1, FlightStateID);
            statement.setInt(2, ID);

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            returnValue = statement.executeUpdate();

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }
        return returnValue;
    }
   
    public List<Flight> GetUpcomingFlights(Date now, int timeID) throws SQLException {
        List<Flight> deLijst = new ArrayList<>();
        //Zet de Connectie op
        Connection connect = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            String query = FlightQuery();
            query += "where Approved = 1 and (DepartureTimeID between ? and ?) and DepartDate = ?";
            
            PreparedStatement ps = connect.prepareStatement(query);

            ps.setInt(1, timeID);
            ps.setInt(2, timeID + 8);
            ps.setDate(3, (java.sql.Date) now);

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            ResultSet result = ps.executeQuery();

            //Loop over de resultSet en doe je ding.
            while (result.next()) {
                Flight fl = new Flight();
                fl = ReadInFlight(result);

                deLijst.add(fl);

            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return deLijst;
        }
    }

    public void UpdateFlightStatus(Flight f) throws SQLException {
        Connection connect = null;
        PreparedStatement statement = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());

            String query = "UPDATE flight SET FlightStateID = ?";
            query += " WHERE id = ?";
            System.out.println(query);
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            statement = connect.prepareStatement(query);

            //Zet de parameters voor de preparedStatement, aan de hand van volgorde van de parameter, te beginnen bij 1
            statement.setInt(1, f.getFlightStateID());
            statement.setInt(2, f.getID());

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            statement.executeUpdate();

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }
    }

        public List<Status> GetAllUpcomingFlightStatus() throws SQLException {
        List<Status> deLijst = new ArrayList<>();
        //Zet de Connectie op
        Connection connect = null;
        PreparedStatement st = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            String query
                    = "SELECT * FROM flightstate WHERE state NOT IN ('Rejected','Inbound for landing','Permission to land','Redirect')";

            st = connect.prepareStatement(query);
            ResultSet result = st.executeQuery();

            //Loop over de resultSet en doe je ding.
            while (result.next()) {
                Status s = new Status();
                s.setID(result.getInt("ID"));
                s.setState(result.getString("State"));

                deLijst.add(s);
            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                //connect.close();
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return deLijst;
        }
    }
    
    public List<Status> GetAllFlightStatus() throws SQLException {
        List<Status> deLijst = new ArrayList<>();
        //Zet de Connectie op
        Connection connect = null;
        PreparedStatement st = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            String query
                    = "SELECT * FROM flightstate";

            st = connect.prepareStatement(query);
            ResultSet result = st.executeQuery();

            //Loop over de resultSet en doe je ding.
            while (result.next()) {
                Status s = new Status();
                s.setID(result.getInt("ID"));
                s.setState(result.getString("State"));

                deLijst.add(s);
            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                //connect.close();
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return deLijst;
        }
    }
    
    public List<Flight> GetFlightReport() throws SQLException {
        List<Flight> deLijst = new ArrayList<Flight>();
        //Zet de Connectie op
        Connection connect = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            PreparedStatement ps = connect.prepareStatement("SELECT flight.*, "
                    + "(SELECT time.Timecol from time where ID = flight.DepartFromID) AS DepartFromTime,"
                    + "(SELECT time.Timecol from time where ID = flight.DepartUntilID) AS DepartUntilTime,"
                    + "(SELECT time.Timecol from time where ID = flight.DepartureTimeID) AS DepartureTime,"
                    + "airport.*, airplane.*, runway.Name AS RunwayName, flightstate.State AS StatusName "
                    + "FROM "
                    + "flight left join "
                    + "airport ON (flight.DestinationID = airport.ID) left join "
                    + "airplane ON (flight.AirplaneID = airplane.ID) left join "
                    + "runway ON (flight.RunwayID = runway.ID) left join "
                    + "flightstate on (flight.FlightStateID = flightstate.ID)"
                    );


            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            ResultSet result = ps.executeQuery();

            //Loop over de resultSet en doe je ding.
            while (result.next()) {
                Flight fl = new Flight();
                fl.setID(result.getInt("ID"));
                fl.setFlightNR(result.getString("FlightNR"));
                fl.setDepartDate((java.sql.Date) result.getDate("DepartDate"));

                //t1 = DepartFrom
                fl.setDepartFromID(result.getInt("DepartFromID"));
                TimeClass t1 = new TimeClass();
                t1.setID(fl.getDepartFromID());
                t1.setTimecol(result.getTime("DepartFromTime"));
                fl.setDepartFrom(t1);

                //t2 = DepartUntil
                fl.setDepartUntilID(result.getInt("DepartUntilID"));
                TimeClass t2 = new TimeClass();
                t2.setID(fl.getDepartUntilID());
                t2.setTimecol(result.getTime("DepartUntilTime"));
                fl.setDepartUntil(t2);

                //t3 = DepartTime
                fl.setDepartureTimeID(result.getInt("DepartureTimeID"));
                if (fl.getDepartureTimeID() != 0) {
                    TimeClass t3 = new TimeClass();
                    t3.setID(fl.getDepartureTimeID());
                    t3.setTimecol(result.getTime("DepartureTime"));
                    fl.setDepartTime(t3);
                }

                //Airport
                Airport port = new Airport();
                port.setName(result.getString("Name"));
                fl.setDepartAirport(port);
                
                //DestinationAirport
                Airport p2 = new Airport();
                p2.setName(result.getString("Name"));
                fl.setDestinationAirport(p2);

                //Airplane
                Airplane plane = new Airplane();
                plane.setCode(result.getString("Code"));
                plane.setBuilder(result.getString("Builder"));
                plane.setType(result.getString("Type"));
                fl.setAirplane(plane);

                //Runway
                fl.setRunwayID(result.getInt("RunwayID"));
                if (fl.getRunwayID() != 0) {
                    Runway r = new Runway();
                    r.setID(result.getInt("ID"));
                    r.setName(result.getString("RunwayName"));
                    //fl.setRunwayID(r.getID());
                    fl.setRunway(r);
                }
                //FlightStatusID
                fl.setFlightStateID(result.getInt("FlightStateID"));
                FlightState s = new FlightState();
                s.setID(result.getInt("ID"));
                s.setState(result.getString("StatusName"));
                fl.setFlightstate(s);
                
                deLijst.add(fl);

            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return deLijst;
        }
    }


    public Flight GetLandingTime(Flight incompleteFlight) {
        
        //Zet de Connectie op
        Connection connect = null;
        PreparedStatement st = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            String query
                    = "SELECT curdate() AS ArrivalDate, ID As ArrivalTimeID, Timecol from time where Timecol between now() AND now() + interval 30 minute";
            st = connect.prepareStatement(query);
            ResultSet result = st.executeQuery();
            ResultSetMetaData metaData = result.getMetaData();
            int columncount = metaData.getColumnCount();
            if (columncount == 0) {
                 query
                    = "SELECT curdate() + interval 1 day AS ArrivalDate, ID As ArrivalTimeID, Timecol from time where time.ID = 1";
                    st = connect.prepareStatement(query);
                    result = st.executeQuery();
            }

            while (result.next()) {
                incompleteFlight.setArrivalDate((java.sql.Date) result.getDate("ArrivalDate"));
                incompleteFlight.setArrivalTimeID(result.getInt("ArrivalTimeID"));
                TimeClass t4 = new TimeClass();
                t4.setID(result.getInt("ArrivalTimeID"));
                t4.setTimecol(result.getTime("Timecol"));
                incompleteFlight.setArrivalTime(t4);
            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                //connect.close();
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return incompleteFlight;
        }
    }
    
    public int UpdateIncomingFlightRunway(Flight f) {
        Connection connect = null;
        PreparedStatement statement = null;
        int returnValue = -1;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());

            String query = "UPDATE flight SET ArrivalDate = ?, ArrivalTimeID = ?, RunwayID = ?, FlightStateID = 12";
            query += " WHERE id = ?";

            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            statement = connect.prepareStatement(query);

            //Zet de parameters voor de preparedStatement, aan de hand van volgorde van de parameter, te beginnen bij 1
            java.sql.Date sql = new java.sql.Date(f.getArrivalDate().getTime());

            statement.setDate(1, sql);
            statement.setInt(2, f.getArrivalTimeID());
            statement.setInt(3, f.getRunwayID());

            statement.setInt(4, f.getID());

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            returnValue = statement.executeUpdate();

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }
        return returnValue;
    }

    public int UpdateIncomingFlightDestination(Flight f) {
        Connection connect = null;
        PreparedStatement statement = null;
        int returnValue = -1;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());

            String query = "UPDATE flight SET DestinationID = ?, FlightStateID = 13";
            query += " WHERE id = ?";

            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            statement = connect.prepareStatement(query);

            //Zet de parameters voor de preparedStatement, aan de hand van volgorde van de parameter, te beginnen bij 1
            statement.setInt(1, f.getDestinationID());

            statement.setInt(2, f.getID());

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            returnValue = statement.executeUpdate();

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }
        return returnValue;
    }

    public void UpdateLandedFlights() {
        Connection connect = null;
        PreparedStatement st = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            String query
                    = "Update flight " +
                      "left join time ON (flight.ArrivalTimeID = time.ID) SET FlightStateID = 5 " +
                      "where ((ArrivalDate = curdate() AND ArrivalTimeID < 48 AND Timecol < time(now())) OR (ArrivalDate = curdate() - interval 1 day)) AND flightStateID = ?";
            st = connect.prepareStatement(query);
            st.setInt(1, 12);
            int returnValue = st.executeUpdate();

            
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                //connect.close();
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            
        }
    }
    
    public void UpdateInboundFlights() {
        Connection connect = null;
        PreparedStatement st = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            String query
                    = "Update flight " +
                      "left join time ON (flight.DepartFromID = time.ID) SET FlightStateID = ? " +
                      "where (DepartDate = curdate() AND DestinationID = 1 AND FlightStateID = 1 AND time.Timecol <= time(now()))";
            st = connect.prepareStatement(query);
            st.setInt(1, 11);
            int returnValue = st.executeUpdate();

            
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                //connect.close();
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            
        }
    }
    
    public void JustUpdateFlight(Flight f) throws SQLException {
        Connection connect = null;
        PreparedStatement statement = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());

            String query = "UPDATE flight SET FlightNR = ?, DepartDate = ?, DepartFromID = ?, DepartUntilID = ?, AirplaneID = ?, DepartureID = ?, DestinationID = ?, DepartureTimeID = null, ArrivalDate = null, ArrivalTimeID = null, RunwayID = null, Approved = 0, FlightStateID=null";

            query += " WHERE ID = ?";
            System.out.println(query);
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            statement = connect.prepareStatement(query);
            java.sql.Date sql = new java.sql.Date(f.getDepartDate().getTime());
            //Zet de parameters voor de preparedStatement, aan de hand van volgorde van de parameter, te beginnen bij 1
            statement.setString(1, f.getFlightNR());
                statement.setDate(2, sql);
                statement.setInt(3, f.getDepartFromID());
                statement.setInt(4, f.getDepartUntilID() );
                statement.setInt(5, f.getAirplaneID());
                statement.setInt(6, f.getDepartureID());
                statement.setInt(7, f.getDestinationID());
                statement.setInt(8, f.getID());
            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            statement.executeUpdate();

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }

    }
    
    public List<Flight> getEligibleFlights(Date date, int AirportID, int DestinationID) {
        List<Flight> deLijst = new ArrayList<>();
        //Zet de Connectie op
        Connection connect = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            
            PreparedStatement statement = connect.prepareStatement(
                    FlightQuery()
                    + "WHERE flight.DepartDate BETWEEN ? AND ? "
                    + "AND flight.DepartureID = ? "
                    + "AND flight.DestinationID = ? "
            );
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(date.getTime());
            c.add(Calendar.WEEK_OF_YEAR, 1);
            statement.setDate(1, new java.sql.Date(date.getTime()));
            statement.setDate(2,new java.sql.Date(c.getTimeInMillis()));
            statement.setInt(3, AirportID);
            statement.setInt(4, DestinationID);
            statement.executeQuery();

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            ResultSet result = statement.getResultSet();

            while(result.next()){
                Flight fl;
                fl = ReadInFlight(result);
                deLijst.add(fl);
            }
        } catch (ClassNotFoundException | SQLException exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return deLijst;
        }
}
    public Airport GetDestinationByID(int DestinationID) {
        Airport destination  = new Airport();
        //Zet de Connectie op
        Connection connect = null;
        PreparedStatement st = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            String query
                    = "SELECT * FROM airport where ID  = ?";

            
            st = connect.prepareStatement(query);

            st.setInt(1, DestinationID);

            ResultSet result = st.executeQuery();

            //Loop over de resultSet en doe je ding.
            result.next();
            destination.setID(result.getInt("ID"));
            destination.setName(result.getString("Name"));
            destination.setEmailAdress(result.getString("EmailAdress"));
            
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                //connect.close();
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return destination;
        }

    }
    
    public Airplane GetAirplaneByID(int AirplaneID) {
        Airplane plane  = new Airplane();
        //Zet de Connectie op
        Connection connect = null;
        PreparedStatement st = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.

            String query
                    = "SELECT * FROM airplane where ID  = ?";

            
            st = connect.prepareStatement(query);

            st.setInt(1, AirplaneID);

            ResultSet result = st.executeQuery();

            //Loop over de resultSet en doe je ding.
            result.next();
            plane.setID(result.getInt("ID"));
            plane.setType(result.getString("Type"));
            plane.setCode(result.getString("Code"));
            plane.setBuilder(result.getString("Builder"));
            

            
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                //connect.close();
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return plane;
        }

    }

    public List<Flight> GetFlightOverview(Airplane p) {
        List<Flight> deLijst = new ArrayList<>();
        Date now = new Date();
        //Zet de Connectie op
        Connection connect = null;
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(super.GetConnectionString());
            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.
            PreparedStatement ps = connect.prepareStatement("SELECT flight.*, "
                    + "(SELECT time.Timecol from time where ID = flight.DepartFromID) AS DepartFromTime,"
                    + "(SELECT time.Timecol from time where ID = flight.DepartUntilID) AS DepartUntilTime,"
                    + "(SELECT time.Timecol from time where ID = flight.DepartureTimeID) AS DepartureTime,"
                    + "(SELECT time.Timecol from time where ID = flight.ArrivalTimeID) AS ArrivalTime,"
                    + "(SELECT airport.Name from airport where ID = flight.DepartureID) AS DepartAirport,"
                    + "(SELECT airport.Name from airport where ID = flight.DestinationID) AS DestinationAirport, "
                    + "(SELECT flightstate.State from flightstate where ID = flight.FlightStateID) AS FlightState, "
                    + "airport.*, airplane.*, runway.Name AS RunwayName "
                    + "FROM "
                    + "flight left join "
                    + "airport ON (flight.DestinationID = airport.ID) left join "
                    + "airplane ON (flight.AirplaneID = airplane.ID) left join "
                    + "runway ON (flight.RunwayID = runway.ID) left join "
                    + "flightstate ON (flight.FlightStateID = flightstate.ID) "
                    + "where Approved = 1 "
                    + "and airplaneID = ? "
                    + "ORDER BY flight.DepartDate, flight.DepartureTimeID");
            ps.setInt(1, p.getID());

            //voer query uit, terugkomende QueryResultaten komt in de vorm van een ResultSet
            ps.executeQuery();
            ResultSet result = ps.getResultSet();

            while (result.next()) {
                Flight fl = new Flight();
                fl.setID(result.getInt("ID"));
                fl.setFlightNR(result.getString("FlightNR"));
                fl.setDepartDate((java.sql.Date) result.getDate("DepartDate"));
                fl.setArrivalDate((java.sql.Date) result.getDate("ArrivalDate"));

                //t1 = DepartFrom
                fl.setDepartFromID(result.getInt("DepartFromID"));
                TimeClass t1 = new TimeClass();
                t1.setID(fl.getDepartFromID());
                t1.setTimecol(result.getTime("DepartFromTime"));
                fl.setDepartFrom(t1);

                //t2 = DepartUntil
                fl.setDepartUntilID(result.getInt("DepartUntilID"));
                TimeClass t2 = new TimeClass();
                t2.setID(fl.getDepartUntilID());
                t2.setTimecol(result.getTime("DepartUntilTime"));
                fl.setDepartUntil(t2);
                
                //t4 = ArrivalTime
                fl.setArrivalTimeID(result.getInt("ArrivalTimeID"));
                TimeClass t4 = new TimeClass();
                t4.setID(fl.getArrivalTimeID());
                t4.setTimecol(result.getTime("ArrivalTime"));
                fl.setArrivalTime(t4);

                //t3 = DepartTime
                fl.setDepartureTimeID(result.getInt("DepartureTimeID"));
                if (fl.getDepartureTimeID() != 0) {
                    TimeClass t3 = new TimeClass();
                    t3.setID(fl.getDepartureTimeID());
                    t3.setTimecol(result.getTime("DepartureTime"));
                    fl.setDepartTime(t3);
                }

                //Airport
                fl.setDepartureID(result.getInt("DepartureID"));
                fl.setDestinationID(result.getInt("DestinationID"));

                fl.setApproved(result.getInt("Approved"));

                //DepartAirport
                Airport DepartAirport = new Airport();
                DepartAirport.setName(result.getString("DepartAirport"));
                fl.setDepartAirport(DepartAirport);

                //DestinationAirport
                Airport DestinationAirport = new Airport();
                DestinationAirport.setName(result.getString("DestinationAirport"));
                fl.setDestinationAirport(DestinationAirport);

                //Airplane
                Airplane plane = new Airplane();
                plane.setCode(result.getString("Code"));
                plane.setBuilder(result.getString("Builder"));
                plane.setType(result.getString("Type"));
                fl.setAirplane(plane);

                //Runway
                fl.setRunwayID(result.getInt("RunwayID"));
                if (fl.getRunwayID() != 0) {
                    Runway r = new Runway();
                    r.setID(result.getInt("ID"));
                    r.setName(result.getString("RunwayName"));
                    fl.setRunwayID(r.getID());
                    fl.setRunway(r);
                }
                //FlightStatusID
                FlightState fs = new FlightState();
                fl.setFlightStateID(result.getInt("FlightStateID"));
                fs.setState(result.getString("FlightState"));
                fl.setFlightstate(fs);
                deLijst.add(fl);
            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }

        List<Flight> nextFlights = new ArrayList<>();

        try {
            nextFlights = GetUpcomingFlights(now, 0);
        } catch (SQLException ex) {
            Logger.getLogger(FlightDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Flight f : nextFlights) {
            deLijst.add(f);
        }

        return deLijst;
    }
}
