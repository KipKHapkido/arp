/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.BaseDAO.DeusDAO;
import Models.Domain.Airplane;
import Models.Domain.Owner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jelle
 */
public class AirplaneDAO extends DeusDAO<Airplane> {

    public AirplaneDAO() {
        super(new Airplane());
    }

    public int countByOwnerID(int ownerID) {
        String query;
        Connection connect = null;
        int numberOfRows = -1;
        
        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            //Zet de Connectie op
            connect = DriverManager.getConnection(super.GetConnectionString());
            //query
            query = "SELECT COUNT(*) FROM airplane WHERE airplane.OwnerID = ?";
            PreparedStatement pst = connect.prepareStatement(query);
            pst.setInt(1, ownerID);
            ResultSet rs = pst.executeQuery();
            
            if (rs.next()) {
                numberOfRows = rs.getInt(1);
            } 
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }

        return numberOfRows;
    }

    public List<Airplane> getAllEager() {
        List<Airplane> listAll = new ArrayList<Airplane>();
        Connection connect = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(super.GetConnectionString());
            PreparedStatement st = connect.prepareStatement(
                    "SELECT airplane.ID, airplane.Code, airplane.OwnerID, airplane.AirplaneStateID, airplane.HangarID,"
                    + " airplane.Capacity, airplane.Builder, airplane.BuilderYear, airplane.Type, airplanestate.State, owner.Name AS OwnerName,"
                    + " owner.AddressID AS OwnerAddressID, airplanestate.State AS State FROM airplane LEFT JOIN"
                    + " owner ON airplane.OwnerID = owner.ID"
                    + " LEFT JOIN airplanestate ON airplane.AirplaneStateID = airplanestate.ID");
            ResultSet result = st.executeQuery();
            while (result.next()) {
                Airplane temp = new Airplane();
                temp.setID(result.getInt("ID"));
                temp.setCode(result.getString("Code"));
                temp.setOwnerID(result.getInt("OwnerID"));
                temp.setStatus(result.getString("State"));
                temp.setCapacity(result.getString("Capacity"));
                temp.setBuilder(result.getString("Builder"));
                temp.setYear(result.getInt("BuilderYear"));
                temp.setType(result.getString("Type"));
                temp.setHangarID(result.getInt("HangarID"));
                temp.setAirplaneState(result.getString("State"));
                Owner o = new Owner();
                o.setID(result.getInt("OwnerID"));
                o.setName(result.getString("OwnerName"));
                o.setAddressID(result.getInt("OwnerAddressID"));
                temp.setOwner(o);
                listAll.add(temp);
            }
        } catch (Exception exc) {
            System.out.println(exc);
            listAll = null;
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return listAll;
        }
    }
    
    public Boolean CheckTail(Airplane a){
        Connection connect = null;
        int b = 1;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(super.GetConnectionString());
            PreparedStatement st = connect.prepareStatement(
                    "SELECT COUNT( Code ) AS rowCount FROM airplane WHERE code = ? AND ID <> ?");                    
            st.setString(1, a.getCode());
            st.setInt(2, a.getID());
            ResultSet result = st.executeQuery();
            result.next();
            b = result.getInt("rowCount");
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            if(b>0)return false;
            else return true;
        }
        
        
    }
    
    public List<Airplane> getSearch(String search) {
        List<Airplane> listAll = new ArrayList<Airplane>();
        Connection connect = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(super.GetConnectionString());
            PreparedStatement st = connect.prepareStatement(
                    "SELECT airplane.ID, airplane.Code, airplane.OwnerID, airplane.Status,"
                    + " airplane.Capacity, airplane.Builder, airplane.BuilderYear, airplane.Type, owner.Name AS OwnerName,"
                    + " owner.AddressID AS OwnerAddressID FROM airplane LEFT JOIN"
                    + " owner ON airplane.OwnerID = owner.ID WHERE airplane.Code LIKE ? ");
            st.setString(1, "%"+search+"%");
            ResultSet result = st.executeQuery();
            while (result.next()) {
                Airplane temp = new Airplane();
                temp.setID(result.getInt("ID"));
                temp.setCode(result.getString("Code"));
                temp.setOwnerID(result.getInt("OwnerID"));
                temp.setStatus(result.getString("Status"));
                temp.setCapacity(result.getString("Capacity"));
                temp.setBuilder(result.getString("Builder"));
                temp.setYear(result.getInt("BuilderYear"));
                temp.setType(result.getString("Type"));
                Owner o = new Owner();
                o.setID(result.getInt("OwnerID"));
                o.setName(result.getString("OwnerName"));
                o.setAddressID(result.getInt("OwnerAddressID"));
                temp.setOwner(o);
                listAll.add(temp);
            }
        } catch (Exception exc) {
            System.out.println(exc);
            listAll = null;
        } finally {
            try {
                connect.close();
            } catch (Exception exc) {
                System.out.println(exc);
            }
            return listAll;
        }

    }
}
