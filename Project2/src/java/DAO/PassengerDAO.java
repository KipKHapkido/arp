/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.BaseDAO.DeusDAO;
import Models.Domain.Address;
import Models.Domain.Flight;
import Models.Domain.Passenger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Stijn
 */
public class PassengerDAO extends DeusDAO<Passenger>
{
    public PassengerDAO(Passenger d) {
        super(d);
    }
    
    String connectUrl = super.GetConnectionString();

    public PassengerDAO() {
        super(new Passenger());
    }
    
    public ArrayList<Passenger> GetAllPass() throws SQLException, ClassNotFoundException
    {
        ArrayList<Passenger> output = new ArrayList<>();
        Class.forName("com.mysql.jdbc.Driver");
        try 
        (Connection conn = DriverManager.getConnection(connectUrl))
        {
            String query = "SELECT * FROM Passenger left join Address on Passenger.AddressID=Address.ID";
            PreparedStatement st = conn.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            
            while(rs.next())
            {
                Passenger newPass = new Passenger();
                newPass.setID(rs.getInt("ID"));
                newPass.setFirstName(rs.getString("Firstname"));
                newPass.setSurName(rs.getString("Surname"));
                newPass.setAddressID(rs.getInt("AddressID"));
                newPass.setBirthday(rs.getDate("Birthday"));
                
                Address newAdd = new Address();
                newAdd.setStreet(rs.getString("Street"));
                newAdd.setNumber(rs.getString("Number"));
                newAdd.setTown(rs.getString("Town"));
                newAdd.setZip_code(rs.getString("Zip_code"));
                newAdd.setCountry(rs.getString("Country"));
                
                newPass.setPassAddress(newAdd);
                
                output.add(newPass);
            }
        }
        catch(Exception exc)
        {
            throw exc;
        }
        return output;
    }
    
    public ArrayList<Passenger> Search(int ID, String FN, String SN) throws Exception
    {
        ArrayList<Passenger> output = new ArrayList<>();
        try
        (Connection conn = DriverManager.getConnection(connectUrl)) {
            Class.forName("com.mysql.jdbc.Driver");
            String query = "";
            PreparedStatement st = null;
            
            if (ID != 0)
            {
                query = "SELECT * FROM Passenger left join Address on Passenger.AddressID=Address.ID WHERE Passenger.ID = ? AND Firstname LIKE ? AND Surname LIKE ?";
                st = conn.prepareStatement(query);
                st.setInt(1, ID);
                st.setString(2, "%" + FN + "%");
                st.setString(3, "%" + SN + "%");
            }
            else
            {
                query = "SELECT * FROM Passenger left join Address on Passenger.AddressID=Address.ID WHERE Firstname LIKE ? AND Surname LIKE ?";
                st = conn.prepareStatement(query);
                st.setString(1, "%" + FN + "%");
                st.setString(2, "%" + SN + "%");
            }


            ResultSet rs = st.executeQuery();
            while (rs.next())
            {
                Passenger newPass = new Passenger();
                newPass.setID(rs.getInt("ID"));
                newPass.setFirstName(rs.getString("Firstname"));
                newPass.setSurName(rs.getString("Surname"));
                newPass.setAddressID(rs.getInt("AddressID"));
                newPass.setBirthday(rs.getDate("Birthday"));
                
                Address newAdd = new Address();
                newAdd.setStreet(rs.getString("Street"));
                newAdd.setNumber(rs.getString("Number"));
                newAdd.setTown(rs.getString("Town"));
                newAdd.setZip_code(rs.getString("Zip_code"));
                newAdd.setCountry(rs.getString("Country"));
                
                newPass.setPassAddress(newAdd);
                
                output.add(newPass);
            }
            
        }
        catch (Exception exc)
        {
            throw exc;
        }
        
        return output;
    }
    
    public void UpdatePassenger(Passenger myPassenger) throws ClassNotFoundException {
        
        Class.forName("com.mysql.jdbc.Driver");
        
        try
        (Connection conn = DriverManager.getConnection(connectUrl)) {
            
            PreparedStatement st = conn.prepareStatement("UPDATE passenger SET Firstname = ?, Surname = ?, Birthday = ?, TypeID = ?, AddressID = ?  WHERE ID = ?");
            
            st.setString(1, myPassenger.getFirstName());
            st.setString(2, myPassenger.getSurName());
            st.setDate(3, new java.sql.Date(myPassenger.getBirthday().getTime()));
            st.setInt(4, myPassenger.getAddressID());
            st.setInt(5, myPassenger.getTypeID());
            st.setInt(6, myPassenger.getID());
            
            st.executeUpdate();
            
        }
        catch (Exception exc)
        {
            
        }
    }
    
    public ArrayList<Passenger> SearchPassengersByFlightId(int flightId) {
        ArrayList<Passenger> listPassengers = new ArrayList<>();
        Connection connect = null;
        Passenger myPassenger;
        
        try {
            
             ///Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            //Zet de Connectie op
            connect = DriverManager.getConnection(connectUrl);

            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.       
            
            String query = "SELECT passenger.ID, passenger.Firstname, passenger.Surname, passenger.Birthday, passenger.AddressID, passenger.TypeID FROM passenger INNER JOIN passenger_flight ON passenger.ID = passenger_flight.PassengerID WHERE passenger_flight.FlightID = ?";

            PreparedStatement st = connect.prepareStatement(query);
            st.setInt(1, flightId);
            
            ResultSet result = st.executeQuery();
           
            while (result.next()) {
                
            myPassenger = new Passenger();
            
            myPassenger.setID(result.getInt("passenger.ID"));
            myPassenger.setFirstName(result.getString("passenger.Firstname"));
            myPassenger.setSurName(result.getString("passenger.Surname"));
            myPassenger.setBirthday(result.getDate("passenger.Birthday"));
            myPassenger.setAddressID(result.getInt("passenger.AddressID"));
            myPassenger.setTypeID(result.getInt("passenger.TypeID"));
            
            listPassengers.add(myPassenger);
            }
          
        } catch (Exception ex) {
            System.out.println(ex);
        }finally {
            try {
                connect.close();
            } catch (Exception ex) {
                System.out.println(ex);
            }
            
        }
        return listPassengers;
        
    }
    
    
     public ArrayList<Flight> SearchFlightByPassengerID(int pasID)
    {
        ArrayList<Flight> listFlights = new ArrayList<>();
        Connection connect = null;
        //Passenger passenger;
        Flight flight;
        try {
            
             ///Laad de driver
            Class.forName("com.mysql.jdbc.Driver");

            //Zet de Connectie op
            connect = DriverManager.getConnection(connectUrl);

            //maak Statement, PreparedStatements indien er variabelen mee zijn gemoeid. Simpele statements indien niet.       
            
            String query = "SELECT flight.ID, flight.FlightNR, flight.DepartDate FROM flight INNER JOIN passenger_flight ON flight.ID = passenger_flight.FlightID WHERE passenger_flight.PassengerID = ?";

            PreparedStatement st = connect.prepareStatement(query);
            st.setInt(1, pasID);
            
            ResultSet result = st.executeQuery();
           
            while (result.next()) {
            //passenger = new Passenger();
            flight = new Flight();
            
            //passenger.setID(result.getInt("passenger.ID"));
            //passenger.setFirstName(result.getString("passenger.Firstname"));
            //passenger.setSurName(result.getString("passenger.Surname"));
            
            flight.setID(result.getInt("flight.ID"));
            flight.setFlightNR(result.getString("flight.FlightNR"));
            flight.setDepartDate(result.getDate("flight.DepartDate"));
                
            
            
            listFlights.add(flight);
            }
          
        } catch (Exception ex) {
            System.out.println(ex);
        }finally {
            try {
                connect.close();
            } catch (Exception ex) {
                System.out.println(ex);
            }
            
        }
        
        return listFlights;
        }
    
    
    
    public void DeleteLinkedFlights(int flightID, int pasID){
       Connection connect = null;
    try{
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager.getConnection(connectUrl);   

            String query = "DELETE from passenger_flight WHERE FlightID = ? AND PassengerID = ?";

            PreparedStatement st = connect.prepareStatement(query);
            st.setInt(1,flightID);
            st.setInt(2,pasID);
            st.executeUpdate();


     } catch (Exception ex) {
            System.out.println(ex);
        }finally {
            try {
                connect.close();
            } catch (Exception ex) {
                System.out.println(ex);
                
                
            }
            
        }    
    
    }
    
    public int CreatePassenger(Passenger passenger) throws ClassNotFoundException, Exception
    {
        int output = -1;
        
        Class.forName("com.mysql.jdbc.Driver");
        
        try (Connection conn = DriverManager.getConnection(connectUrl))
        {
            String sql = "INSERT INTO passenger (Firstname, Surname, Birthday, AddressID, TypeID) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement pst = conn.prepareStatement(sql);
            
            pst.setString(1, passenger.getFirstName());
            pst.setString(2, passenger.getSurName());
            java.sql.Date sqlDate = new java.sql.Date(passenger.getBirthday().getTime());
            pst.setString(3, sqlDate.toString());
            pst.setInt(4, passenger.getAddressID());
            pst.setInt(5, 1);
            
            pst.executeUpdate();
            
            output = 1;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return output;
    }
    
    public void checkInPassenger(String flightID, String passengerID) throws Exception{
        
        try (Connection conn = DriverManager.getConnection(connectUrl))
        {
            String sql = "UPDATE passenger_flight SET CheckedIn = 1 "
                    + "WHERE FlightID = ? "
                    + "AND PassengerID = ?";
            PreparedStatement pst = conn.prepareStatement(sql);
            
            pst.setString(1, flightID);
            pst.setString(2, passengerID);
            
            pst.executeUpdate();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public int isCheckedIn(int passID, int flightID) throws SQLException
    {
        int output;
        
        try 
        (Connection conn = DriverManager.getConnection(super.GetConnectionString()))
        {
            String query = "SELECT CheckedIn FROM passenger_flight where PassengerID=? AND FlightID=?";
            PreparedStatement st = conn.prepareStatement(query);
            st.setInt(1, passID);
            st.setInt(2, flightID);
            ResultSet rs = st.executeQuery();
            
            rs.next();
            output = rs.getInt("CheckedIn");
        }
        return output;
    }

}
