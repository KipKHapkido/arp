/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.BaseDAO.DeusDAO;
import Models.Domain.Address;
import Models.Datamodels.FilterCriteria;
import Models.Domain.Owner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kathleen
 */
public class OwnerDAO extends DeusDAO<Owner> {

    public OwnerDAO() {
        super(new Owner());
    }

    public Owner getOwnerByName(String name) {
        Owner owner = new Owner();
        String query;
        Connection connect = null;

        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            //Zet de Connectie op
            connect = DriverManager.getConnection(super.GetConnectionString());
            //query 
            query = "SELECT * FROM Owner WHERE name = ?";
            PreparedStatement pst = connect.prepareStatement(query);
            pst.setString(1, name);

            ResultSet result = pst.executeQuery();

            //Owner teruggeven
            while (result.next()) {
                owner = new Owner();
                owner.setID(result.getInt("ID"));
                owner.setName(result.getString("Name"));
                owner.setAddressID(result.getInt("AddressID"));
            }

            pst.close();

        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }

        return owner;
    }

    public List<Owner> getAllWithRelations(FilterCriteria filterCriteria) {
        Owner owner;
        Address address;
        String query;
        List<Owner> ownerList = new ArrayList<Owner>();
        PreparedStatement pst;
        ResultSet result;
        Connection connect = null;

        try {
            //Laad de driver
            Class.forName("com.mysql.jdbc.Driver");
            //Zet de Connectie op
            connect = DriverManager.getConnection(super.GetConnectionString());
            //query

            if (filterCriteria != null && filterCriteria.getField() != "" && filterCriteria.getSearchCriteria() != "") {
                query = "SELECT * FROM Owner INNER JOIN address ON owner.addressID = address.ID WHERE UPPER(" + filterCriteria.getField() + ") LIKE  UPPER(?) ";
                pst = connect.prepareStatement(query);
                pst.setString(1, "%" + filterCriteria.getSearchCriteria() + "%");
            } else {
                query = "SELECT * FROM Owner INNER JOIN address ON owner.addressID = address.ID";
                pst = connect.prepareStatement(query);
            }

            result = pst.executeQuery();

            while (result.next()) {
                owner = new Owner();
                address = new Address();

                owner.setID(result.getInt("owner.ID"));
                owner.setName(result.getString("owner.Name"));
                owner.setAddressID(result.getInt("owner.AddressID"));

                address.setID((result.getInt("address.Id")));
                address.setCountry(result.getString("address.Country"));
                address.setStreet(result.getString("address.Street"));
                address.setNumber(result.getString("address.Number"));
                address.setTown(result.getString("address.Town"));
                address.setZip_code(result.getString("address.Zip_code"));

                owner.setAddress(address);

                ownerList.add(owner);
            }
        } catch (Exception exc) {
            System.out.println(exc);
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }

        return ownerList;
    }
}
