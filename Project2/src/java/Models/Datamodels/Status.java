/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Datamodels;

import Models.Domain.BaseModel.Deus;

/**
 *
 * @author JurgenVanEynde
 */
public class Status extends Deus {
    private String State;

    public String getState() {
        return State;
    }

    public void setState(String State) {
        this.State = State;
    }
}
