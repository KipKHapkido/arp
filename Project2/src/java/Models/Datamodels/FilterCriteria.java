/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Datamodels;

/**
 *
 * @author Kathleen
 */
public class FilterCriteria {
    
    private String searchCriteria;
    private String field;
    
    public String getSearchCriteria() {
        return searchCriteria;
    }

    public void setSearchCriteria(String searchCriteria) {
        this.searchCriteria = searchCriteria;
    }
    
    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
    
    public FilterCriteria(String field, String searchCriteria)
    {
        this.searchCriteria = searchCriteria;
        this.field = field;
    }
    
}
