/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Datamodels;

/**
 *
 * @author Jelle
 */
public class Navigation {
    public Navigation(int id, String name, String url, int permission){
        setID(id);
        setName(name);
        setUrl(url);
        setPermission(permission);
    }
    
    private int ID;
    private String Name;
    private String Url;
    private int Permission;
    
    public int getID(){
        return ID;
    }
    
    public void setID(int i){
        ID=i;
    }
    
    public String getName(){
        return Name;
    }
    
    public void setName(String s){
        Name=s;
    }
    
    public String getUrl(){
        return Url;
    }
    
    public void setUrl(String s){
        Url=s;
    }
    
    public int getPermission(){
        return Permission;
    }
    
    public void setPermission(int i){
        Permission = i;
    }
}
