/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kathleen
 */
public class User extends Deus{
    
    private String username;
    private byte[] password;
    private byte[] salt;
    private int functionId;
    private Function function;
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public int getFunctionID() {
        return functionId;
    }

    public void setFunctionID(int functionId) {
        this.functionId = functionId;
    }
    
    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }
    
    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }
    
    public Function getFunction(){
        return function;
    }
    
    public void setFunction(Function function){
        this.function = function;
    }
    
    public User()
    {
        List<DataMapping> tmpDM = new ArrayList<DataMapping>();
        
        tmpDM.add(new DataMapping("ID", "ID", "int"));
        tmpDM.add(new DataMapping("Username", "Username", "String"));
        tmpDM.add(new DataMapping("Password", "Password", "Bytes"));
        tmpDM.add(new DataMapping("Salt", "Salt" , "Bytes"));
        tmpDM.add(new DataMapping("FunctionID", "FunctionID" , "int"));
        
        this.setDmList(tmpDM);
        this.setTableName("user");
        
        this.setUsername("");
    }
    
}
