/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jelle
 */
public class Flight extends Deus {
    private String FlightNR;
    private Date DepartDate;
    private int DepartFromID;
    private int DepartUntilID;
    private int DepartureTimeID;
    private Date ArrivalDate;
    private int ArrivalTimeID;
    private int AirplaneID;
    private int DepartureID;
    private int DestinationID;
    private int RunwayID;
    private int Approved;
    private int FlightStateID;
    private int Passengers;
    
    private List<Passenger> PassengerList;

    public List<Passenger> getPassengerList() {
        return PassengerList;
    }

    public void setPassengerList(List<Passenger> PassengerList) {
        this.PassengerList = PassengerList;
    }
    
    private Airport departAirport;
    private Airport DestinationAirport; 
    private Airplane airplane;
    private Runway runway;
    private FlightState flightstate;
    private TimeClass DepartFrom;//DepartFromID
    private TimeClass DepartUntil;//DepartUntilID
    private TimeClass DepartTime;//DepartureTimeID
    private TimeClass ArrivalTime;//ArrivalTimeID
                      
    private List<DataMapping> list = new ArrayList<>();
    private String tableName;
    
    public Flight(){
        super();
        prepareFlight();
    }
    
    public void prepareFlight(){
          
        tableName = "flight";
        list.add(new DataMapping("ID","ID","int"));
        list.add(new DataMapping("FlightNR","FlightNR","String"));
        list.add(new DataMapping("DepartDate", "DepartDate", "Date"));
        list.add(new DataMapping("DepartFromID", "DepartFromID", "int"));
        list.add(new DataMapping("DepartUntilID", "DepartUntilID", "int"));
        list.add(new DataMapping("DepartureTimeID", "DepartureTimeID", "int"));
        list.add(new DataMapping("ArrivalDate", "ArrivalDate", "Date"));
        list.add(new DataMapping("ArrivalTimeID","ArrivalTimeID","int"));       
        list.add(new DataMapping("AirplaneID","AirplaneID","int"));
        list.add(new DataMapping("DepartureID","DepartureID","int"));
        list.add(new DataMapping("DestinationID","DestinationID","int"));
        list.add(new DataMapping("RunwayID", "RunwayID", "int"));
        list.add(new DataMapping("Approved","Approved","int"));
        super.setTableName(tableName);
        super.setDmList(list);
    }
       
    public String getFlightNR() {
        return FlightNR;
    }

    public void setFlightNR(String FlightNR) {
        this.FlightNR = FlightNR;
    }

    public Date getDepartDate() {
        return DepartDate;
    }

    public void setDepartDate(Date DepartDate) {
        this.DepartDate = DepartDate;
    }

    public int getDepartFromID() {
        return DepartFromID;
    }

    public void setDepartFromID(int DepartFromID) {
        this.DepartFromID = DepartFromID;
    }

    public int getDepartUntilID() {
        return DepartUntilID;
    }

    public void setDepartUntilID(int DepartUntilID) {
        this.DepartUntilID = DepartUntilID;
    }
    
    public int getDepartureTimeID() {
        return DepartureTimeID;
    }

    public void setDepartureTimeID(int DepartureTimeID) {
        this.DepartureTimeID = DepartureTimeID;
    }
  
    public Date getArrivalDate() {
        return ArrivalDate;
    }

    public void setArrivalDate(Date ArrivalDate) {
        this.ArrivalDate = ArrivalDate;
    }

    public int getArrivalTimeID() {
        return ArrivalTimeID;
    }

    public void setArrivalTimeID(int ArrivalTimeID) {
        this.ArrivalTimeID = ArrivalTimeID;
    }

    public int getAirplaneID() {
        return AirplaneID;
    }

    public void setAirplaneID(int AirplaneID) {
        this.AirplaneID = AirplaneID;
    }

    public int getDepartureID() {
        return DepartureID;
    }

    public void setDepartureID(int DepartureID) {
        this.DepartureID = DepartureID;
    }

    public int getDestinationID() {
        return DestinationID;
    }

    public void setDestinationID(int DestinationID) {
        this.DestinationID = DestinationID;
    }

    public int getRunwayID() {
        return RunwayID;
    }

    public void setRunwayID(int RunwayID) {
        this.RunwayID = RunwayID;
    }

    public int getApproved() {
        return Approved;
    }

    public void setApproved(int Approved) {
        this.Approved = Approved;
    }
   
    public Airplane getAirplane() {
        return airplane;
    }

    public void setAirplane(Airplane airplane) {
        this.airplane = airplane;
    }
   
    public Runway getRunway() {
        return runway;
    }

    public void setRunway(Runway runway) {
        this.runway = runway;
    } 
    
    public TimeClass getDepartFrom() {
        return DepartFrom;
    }

    public void setDepartFrom(TimeClass DepartFrom) {
        this.DepartFrom = DepartFrom;
    }

    public TimeClass getDepartUntil() {
        return DepartUntil;
    }

    public void setDepartUntil(TimeClass DepartUntil) {
        this.DepartUntil = DepartUntil;
    }

    public TimeClass getDepartTime() {
        return DepartTime;
    }

    public void setDepartTime(TimeClass DepartTime) {
        this.DepartTime = DepartTime;
    }

    public TimeClass getArrivalTime() {
        return ArrivalTime;
    }

    public void setArrivalTime(TimeClass ArrivalTime) {
        this.ArrivalTime = ArrivalTime;
    }
    
    public int getFlightStateID() {
        return FlightStateID;
    }

    public void setFlightStateID(int FlightStateID) {
        this.FlightStateID = FlightStateID;
    }
    
    public FlightState getFlightstate() {
        return flightstate;
    }

    public void setFlightstate(FlightState flightstate) {
        this.flightstate = flightstate;
    }
    
    public Airport getDepartAirport() {
        return departAirport;
    }

    public void setDepartAirport(Airport departAirport) {
        this.departAirport = departAirport;
    }
    
    public Airport getDestinationAirport() {
        return DestinationAirport;
    }

    public void setDestinationAirport(Airport DestinationAirport) {
        this.DestinationAirport = DestinationAirport;
    }
    
    public int getPassengers() {
        return Passengers;
    }

    public void setPassengers(int Passengers) {
        this.Passengers = Passengers;
    }
}
