/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;

/**
 *
 * @author Jelle
 */
public class Runway extends Deus{
    private String Name;
    private int Length; 
    private int AirportID;
    private Airport AirportObj;

    public Runway() {
        super();
        this.setName("");
        this.setLength(0);
        this.setAirportID(0);
        this.setTableName("runway");
        this.addMapping(new DataMapping("ID","ID","int"));
        this.addMapping(new DataMapping("Name","Name","String"));
        this.addMapping(new DataMapping("Length","Length","int"));
        this.addMapping(new DataMapping("AirportID", "AirportID", "int"));
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getLength() {
        return Length;
    }

    public void setLength(int Length) {
        this.Length = Length;
    }

    public int getAirportID() {
        return AirportID;
    }

    public void setAirportID(int AirportID) {
        this.AirportID = AirportID;
    }

    public Airport getAirportObj() {
        return AirportObj;
    }

    public void setAirportObj(Airport AirportObj) {
        this.AirportObj = AirportObj;
    }
}
