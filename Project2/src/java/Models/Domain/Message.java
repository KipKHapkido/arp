/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import java.util.Objects;

/**
 *
 * @author Kathleen
 */
public class Message {

    private String Message;
    private MessageSeverity Severity;
    
    public enum MessageSeverity {
        SUCCESS,
        ERROR
    }

    public String getBootstrapStatus() {
        if (this.Severity == MessageSeverity.ERROR) {
            return "danger";
        } else {
            return "success";
        }
    }

    public MessageSeverity getSeverity() {
        return Severity;
    }

    public void setSeverity(MessageSeverity Severity) {
        this.Severity = Severity;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public Message(String message, MessageSeverity severity) {
        this.Message = message;
        this.Severity = severity;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.Message);
        hash = 61 * hash + Objects.hashCode(this.Severity);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Message other = (Message) obj;
        return true;
    }

}
