/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;
import java.sql.Time;

/**
 *
 * @author Sven Vervloet
 */
public class TimeClass extends Deus {
    private Time Timecol;
   

    public Time getTimecol() {
        return Timecol;
    }

    public void setTimecol(Time Timecol) {
        this.Timecol = Timecol;
    }

    public TimeClass() {
        super();
        
        super.addMapping(new DataMapping("ID","ID","int"));
        super.addMapping(new DataMapping("Timecol","Timecol","Time"));
        super.setTableName("time");
    }
    
    
}
