/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.Deus;

public class Crew extends Deus {
    private String Firstname;
    private String Surname;
    private int Function;
    private Function Description;
    

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String Firstname) {
        this.Firstname = Firstname;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String Surname) {
        this.Surname = Surname;
    }

    public int getFunction() {
        return Function;
    }

    public void setFunction(int Function) {
        this.Function = Function;
    }

    public Function getDescription() {
        return Description;
    }

    public void setDescription(Function Description) {
        this.Description = Description;
    }
}
