/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kathleen
 */
public class Owner extends Deus {
    
    private String Name;
    private int AddressID;
    private Address address;
    
    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }
    
    public int getAddressID() {
        return AddressID;
    }

    public void setAddressID(int AddressID) {
        this.AddressID = AddressID;
    }
    
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
        
    public Owner()
    {
        List<DataMapping> tmpDM = new ArrayList<DataMapping>();
        
        tmpDM.add(new DataMapping("ID", "ID", "int"));
        tmpDM.add(new DataMapping("Name", "Name", "String"));
        tmpDM.add(new DataMapping("AddressID", "AddressID", "int"));
        
        this.setDmList(tmpDM);
        this.setTableName("owner");
        
        this.Name = "";
    }
    
    
}
