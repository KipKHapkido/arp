/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;

/**
 *
 * @author Jelle
 */
public class Airport extends Deus {
    //deze klasse kan nog volledig wijzige daar opzet nu verkeerd is
    //1runway en 1hangar aan airport ? geen vliegvelden met meerdere runways
    
    private String Name;    
    private int AddressID;
    private String EmailAdress;   
    

    public Airport() {
        super();
        this.setTableName("airport");
        this.addMapping(new DataMapping("ID","ID","int"));
        
        this.addMapping(new DataMapping("Name","Name","String"));        
       
        this.addMapping(new DataMapping("AddressID","AddressID","int"));
        
        this.addMapping(new DataMapping("EmailAdress","EmailAdress","String")); 
        this.setName("");
        this.setEmailAdress("");         
        this.setAddressID(0);
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }
    
        public String getEmailAdress() {
        return EmailAdress;
    }

    public void setEmailAdress(String EmailAdress) {
        this.EmailAdress = EmailAdress;
    }

    public int getAddressID() {
        return AddressID;
    }

    public void setAddressID(int AddressID) {
        this.AddressID = AddressID;
    }
    
}
