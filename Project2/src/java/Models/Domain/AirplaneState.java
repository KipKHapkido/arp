/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;

/**
 *
 * @author Jelle
 */
public class AirplaneState extends Deus{
    private String State;
    
    public AirplaneState(){
        super();
        this.setTableName("airplanestate");
        this.addMapping(new DataMapping("ID","ID","int"));
        this.addMapping(new DataMapping("State","State","String"));
     }

    public String getState() {
        return State;
    }

    public void setState(String State) {
        this.State = State;
    }
}
