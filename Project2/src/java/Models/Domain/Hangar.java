/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;

/**
 *
 * @author Erik Michielsen
 */
public class Hangar extends Deus {
    private String Name;
    //at the moment varchar in sql database
    private String Capacity;
    private int AirportID;
    private Airport AirportObj;

    public Hangar() {
        super();
        this.setName("");
        this.setCapacity("");
        this.setAirportID(0);
        this.setTableName("hangar");
        this.addMapping(new DataMapping("ID","ID","int"));
        this.addMapping(new DataMapping("Name","Name","String"));
        this.addMapping(new DataMapping("Capacity","Capacity","String"));
         this.addMapping(new DataMapping("AirportID", "AirportID", "int"));
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getCapacity() {
        return Capacity;
    }

    public void setCapacity(String Capacity) {
        this.Capacity = Capacity;
    }
    
    public int getAirportID() {
        return AirportID;
    }

    public void setAirportID(int AirportID) {
        this.AirportID = AirportID;
    }

    public Airport getAirportObj() {
        return AirportObj;
    }

    public void setAirportObj(Airport AirportObj) {
        this.AirportObj = AirportObj;
    }
    
    
}
