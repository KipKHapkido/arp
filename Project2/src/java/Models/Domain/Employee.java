/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;

/**
 *
 * @author Jelle
 */
public class Employee extends Deus {
    //kan zijn dat deze nog aangepast moet worden nu function
    //misschien later aparte subklasse hiervoor als ik taiga lees
    private String Firstname;
    private String Surname;
    private int Function;

    public Employee() {
        super();
        this.setTableName("employee");
        this.addMapping(new DataMapping("ID","ID","int"));
        this.addMapping(new DataMapping("Firstname","Firstname","String"));
        this.addMapping(new DataMapping("Surname","Surname","String"));
        this.addMapping(new DataMapping("Function","Function","String"));
        this.setFirstname("");
        this.setSurname("");
        this.setFunction(0);
    }

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String Firstname) {
        this.Firstname = Firstname;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String Surname) {
        this.Surname = Surname;
    }

    public int getFunction() {
        return Function;
    }

    public void setFunction(int Function) {
        this.Function = Function;
    }
    
    
    
    
}
