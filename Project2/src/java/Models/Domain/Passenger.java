/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 *
 * @author Jelle
 */
public class Passenger extends Deus 
{
    
    private Date Birthday;
    private String FirstName;
    private String SurName;
    private int AddressID;
    private int TypeID;
    private Address PassAddress;
    private int CheckedIn;

    public Passenger() {
        List<DataMapping> dm = new ArrayList<DataMapping>();
                
        dm.add(new DataMapping("ID","ID","int"));
        dm.add(new DataMapping("FirstName","Firstname","String"));
        dm.add(new DataMapping("SurName","Surname","String"));
        dm.add(new DataMapping("AddressID","AddressID","int"));
        dm.add(new DataMapping("Birthday","Birthday","Date"));
        dm.add(new DataMapping("TypeID","TypeID","int"));
        
        this.setDmList(dm);
        this.setTableName("passenger");        
    }   
    
    public Date getBirthday() {
        return Birthday;
    }

    public void setBirthday(Date Birthday) {
        this.Birthday = Birthday;
    }
    
    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String SurName) {
        this.SurName = SurName;
    }

    public int getAddressID() {
        return AddressID;
    }

    public void setAddressID(int AddressID) {
        this.AddressID = AddressID;
    }

    public int getTypeID() {
        return TypeID;
    }

    public void setTypeID(int TypeID) {
        this.TypeID = TypeID;
    }
    
    public Address getPassAddress() {
        return PassAddress;
    }

    public void setPassAddress(Address PassAddress) {
        this.PassAddress = PassAddress;
    }
    
    public void setCheckedIn(int checkedIn){
        this.CheckedIn = checkedIn;
    }
    
    public int getCheckedIn(){
        return CheckedIn;
    }
}
