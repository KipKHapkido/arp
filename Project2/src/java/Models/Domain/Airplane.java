/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;

/**
 *
 * @author Jelle
 */
public class Airplane extends Deus {
    private String Code;
    private int OwnerID;
    private int Year;
    private int AirplaneStateID;
    private int HangarID;
    private String Status;
    private String AirplaneState;
    private String Capacity;
    private String Builder;
    private String Type;
    private Owner Owner;
    private Hangar Hangar;
       
    public Airplane(){
        super();
        this.setTableName("airplane");
        this.addMapping(new DataMapping("ID","ID","int"));
        this.addMapping(new DataMapping("OwnerID","OwnerID","int"));
        this.addMapping(new DataMapping("Code","Code","String"));
        this.addMapping(new DataMapping("Status","Status","String"));
        this.addMapping(new DataMapping("Capacity","Capacity","String"));
        this.addMapping(new DataMapping("Builder","Builder","String"));
        this.addMapping(new DataMapping("Year","BuilderYear","int"));
        this.addMapping(new DataMapping("Type","Type","String"));
        this.addMapping(new DataMapping("AirplaneStateID","AirplaneStateID","int"));
        this.addMapping(new DataMapping("HangarID","HangarID","int"));
        this.setID(0);
        this.setCapacity("");
        this.setStatus("");
        this.setCode("");
        this.setOwnerID(0);
        this.setBuilder("");
        this.setYear(1);
        this.setType("");
        this.setHangarID(0);
        this.setAirplaneStateID(0);
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public int getOwnerID() {
        return OwnerID;
    }

    public void setOwnerID(int owner) {
        this.OwnerID = owner;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getCapacity() {
        return Capacity;
    }

    public void setCapacity(String capacity) {
        this.Capacity = capacity;
    }

    public Owner getOwner() {
        return Owner;
    }

    public void setOwner(Owner Owner) {
        this.Owner = Owner;
    }

    public String getBuilder (){
        return Builder;
    }
    
    public void setBuilder(String builder){
        this.Builder = builder;
    }

    public int getYear() {
        return Year;
    }

    public void setYear(int year) {
        this.Year = year;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public int getAirplaneStateID() {
        return AirplaneStateID;
    }

    public void setAirplaneStateID(int AirplaneStateID) {
        this.AirplaneStateID = AirplaneStateID;
    }

    public int getHangarID() {
        return HangarID;
    }

    public void setHangarID(int HangarID) {
        this.HangarID = HangarID;
    }

    public Hangar getHangar() {
        return Hangar;
    }

    public void setHangar(Hangar Hangar) {
        this.Hangar = Hangar;
    }
    public String getAirplaneState() {
        return AirplaneState;
    }

    public void setAirplaneState(String AirplaneState) {
        this.AirplaneState = AirplaneState;
    }
}
