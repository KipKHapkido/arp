/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain.BaseModel;

/**
 *
 * @author JurgenVanEynde
 */
public class DataMapping {
    private String property;
    private String columnName;
    private String type;
    
    public DataMapping(String property, String columnName, String type) {
        this.property = property;
        this.columnName = columnName;
        this.type = type;
    }
    
    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
