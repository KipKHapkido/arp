/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain.BaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Stijn
 */
public abstract class Deus 
{
    private int id;
    private List<DataMapping> dmList = new ArrayList<>();
    private String tableName;
      
    public final void addMapping(DataMapping m){
        this.dmList.add(m);
    }
     
    public Deus(){
        this.id=0;
        this.dmList = new ArrayList<DataMapping>();
        this.tableName="";
    }
     

    public final void setID(int id)

    {
        this.id = id;
        // ID is never nullable. Creating a new object to add to the database? Set the ID to 0.
    }
    
    public final int getID()
    {
        return id;
    }
        
    public final List<DataMapping> getDmList() {
        return dmList;
    }

    public final void setDmList(List<DataMapping> dmList) {
        this.dmList = dmList;
    }
    
    public final String getTableName() {
        return tableName;
    }

    public final void setTableName(String tableName) {
        this.tableName = tableName;
    }
}