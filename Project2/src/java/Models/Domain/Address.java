/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kathleen
 */
public class Address extends Deus{
    
    private String Street;
    private String Town;
    private String Zip_code;
    private String Country;
    private String Number;
    
    public String getStreet() {
        return Street;
    }

    public void setStreet(String Street) {
        this.Street = Street;
    }
    
    public String getTown() {
        return Town;
    }

    public void setTown(String Town) {
        this.Town = Town;
    }
    
    public String getZip_code() {
        return Zip_code;
    }

    public void setZip_code(String Zip_code) {
        this.Zip_code = Zip_code;
    }
    
    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }
    
     public String getNumber() {
        return Number;
    }

    public void setNumber(String Number) {
        this.Number = Number;
    }
    
    public String getFullAddress(){
        return Street + " " + Number + " " + Zip_code + " " + Town + " " + Country;
    }
    
    public Address()
    {
        List<DataMapping> tmpDM = new ArrayList<DataMapping>();
        
        tmpDM.add(new DataMapping("ID", "ID", "int"));
        tmpDM.add(new DataMapping("Street", "Street", "String"));
        tmpDM.add(new DataMapping("Town", "Town", "String"));
        tmpDM.add(new DataMapping("Zip_code", "Zip_code", "String"));
        tmpDM.add(new DataMapping("Country", "Country", "String"));
        tmpDM.add(new DataMapping("Number", "Number", "String"));

        this.setDmList(tmpDM);
        this.setTableName("address");
    }
    
    public boolean equals(Address a){
        return (a.getID() == super.getID());
    }
    
}
