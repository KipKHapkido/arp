/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.Deus;
import Models.Domain.BaseModel.DataMapping;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kathleen
 */
public class Function extends Deus{
    
    private String description;
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Function()
    {
        List<DataMapping> tmpDM = new ArrayList<DataMapping>();
        
        tmpDM.add(new DataMapping("ID", "ID", "int"));
        tmpDM.add(new DataMapping("Description", "Description", "String"));

        
        this.setDmList(tmpDM);
        this.setTableName("function");
        
        this.setDescription("");
    }
}
