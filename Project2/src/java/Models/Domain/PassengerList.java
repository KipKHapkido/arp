/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class PassengerList extends Deus{
    private Flight flight;
    private List<Passenger> passengers;
    
    public PassengerList(Flight flight, List<Passenger> passengers){
        setFlight(flight);
        setPassengers(passengers);
    }
    
    public PassengerList(Flight flight){
        setFlight(flight);
        setPassengers(new ArrayList<>());
    }
    
    public void setFlight(Flight flight){
        this.flight = flight;
    }
    
    public Flight getFlight(){
        return flight;
    }
    
    public void setPassengers(List<Passenger> passengers){
        this.passengers = passengers;
    }
    
    public List<Passenger> getPassengers(){
        return passengers;
    }
    
    public int getPassengerCount(){
        return passengers.size();
    }
}
