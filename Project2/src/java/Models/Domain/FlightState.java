/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Domain;

import Models.Domain.BaseModel.DataMapping;
import Models.Domain.BaseModel.Deus;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sven Vervloet
 */
public class FlightState extends Deus {
    private String State;
     
    private List<DataMapping> list = new ArrayList<>();
    private String tableName;
    
    public FlightState(){
        super();
        prepareFlightState();
    }
    
    public void prepareFlightState(){
          
        tableName = "flight";
        list.add(new DataMapping("ID","ID","int"));
        list.add(new DataMapping("FlightNR","FlightNR","String"));
        super.setTableName(tableName);
        super.setDmList(list);
    }
    
     public String getState() {
        return State;
    }

    public void setState(String State) {
        this.State = State;
    }
}
