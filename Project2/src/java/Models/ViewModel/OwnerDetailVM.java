/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

/**
 *
 * @author Kathleen
 */
public class OwnerDetailVM extends BaseVM {

    private int Id;
    private String Name;
    private String Street;
    private String Town;
    private String Zip_code;
    private String Country;
    private String Number;

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String Street) {
        this.Street = Street;
    }

    public String getTown() {
        return Town;
    }

    public void setTown(String Town) {
        this.Town = Town;
    }

    public String getZip_code() {
        return Zip_code;
    }

    public void setZip_code(String Zip_code) {
        this.Zip_code = Zip_code;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String Number) {
        this.Number = Number;
    }

    public OwnerDetailVM(int language) {
        super(language);
        switch (language) {
            case 1://English
                super.addVar("Name", "Name");
                super.addVar("Country", "Country");
                super.addVar("Street", "Street");
                super.addVar("HouseNumber", "House Number");
                super.addVar("ZipCode", "Zip Code");
                super.addVar("Town", "Town");
                super.addVar("Submit", "Submit");
                super.addVar("Create", "Create new owner");
                super.addVar("Update", "Update owner");
                super.addVar("Read", "Read");
                super.addVar("Field", "Field");
                super.addVar("Value", "Value");
                super.addVar("Cancel", "Cancel");
                break;
            case 2://Nederlands
                super.addVar("Name", "Naam");
                super.addVar("Country", "Land");
                super.addVar("Street", "Straat");
                super.addVar("HouseNumber", "Huisnummer");
                super.addVar("ZipCode", "Postcode");
                super.addVar("Town", "Woonplaats");
                super.addVar("Submit", "Zenden");
                super.addVar("Create", "Maak een nieuwe eigenaar");
                super.addVar("Update", "Wijzig de eigenaar");
                super.addVar("Read", "Lezen");
                super.addVar("Field", "Veld");
                super.addVar("Value", "Waarde");
                super.addVar("Cancel", "Annuleren");
                break;
        }
    }

}
