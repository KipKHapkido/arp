/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

import Logic.AirplaneLogic;
import Models.Domain.Airplane;
import Models.Domain.AirplaneState;
import Models.Domain.Hangar;
import Models.Domain.Flight;
import Models.Domain.Owner;
import java.util.List;

/**
 *
 * @author Jelle
 */
public class AirplaneVM extends BaseVM {
    
    private Airplane Airplane = new Airplane();
    private List<Owner> OwnersList;  
    private List<Airplane> AirplaneList;
    private List<Flight> FlightOverview;
    private AirplaneLogic logic;
    private List<AirplaneState> StateList;
    private LogicAction feedback;
    private List<Hangar> HangarList;
    private int previous, next;
    
    public enum LogicAction{
        INITIAL,
        CANCEL,
        CREATE,
        UPDATE,
        HANGAR,
        DELETE,
        SEARCH,
        CANCELSAVE,
        ERRORSAVE,
        DUPLSAVE,
        SAVED,
        READ
    }
    
    public AirplaneVM() {
        this(1);
    }
    
    public AirplaneVM(int language) {
        super(language);
        logic = new AirplaneLogic();
        this.setOwnersList(logic.getAllOwners());
        previous = 5;
        next = 5;
        switch(language) {
            case 1:
                super.addVar("Code", "Tailcode");
                super.addVar("Capacity", "Capacity");
                super.addVar("Status", "Status");
                super.addVar("Owner", "Owner");
                super.addVar("Builder", "Builder");
                super.addVar("BuildYear", "Build Year");
                super.addVar("Hangar", "Hangar");
                super.addVar("Type", "Type");
                super.addVar("planeCode", "Airplane Code");
                super.addVar("Search", "Search");
                super.addVar("Cancel", "Cancel");
                super.addVar("Create", "Create new");
                super.addVar("New", "New");
                super.addVar("Update", "Update");
                super.addVar("Approve Flight", "Approve Flight");   
                super.addVar("Passenger", "Manage Passengers");     
                super.addVar("Flight", "Manage Flights"); 
                super.addVar("Delete", "Delete");
                super.addVar("Details", "Details");
                super.addVar("ConfirmDelete", "Are you sure you want to delete this airplane");
                super.addVar("Update airplane", "Airplane manager");
                super.addVar("ID", "ID");                
                super.addVar("Action", "Action");
                super.addVar("Edit", "Edit");
                super.addVar("Save", "Save");
                super.addVar("EmptyFields","Please fill in all fields");
                super.addVar("TailCodeFail","The tailcode is not unique");
                super.addVar("DBDelete", "The database deleted your airplane");
                super.addVar("DBDeleteFail", "The database could not deleted your airplane");
                super.addVar("DBSuccess", "The database saved your airplane");
                super.addVar("DBFail", "The database failed to save your airplane");
                super.addVar("PHcode", "Type the tailcode here");
                super.addVar("PHowner", "Please choose an owner");
                super.addVar("PHstatus", "Type the status here");
                super.addVar("PHcapacity", "Type the capacity here");
                super.addVar("PHhangar", "Please choose a hangar");
                super.addVar("planeCode", "Airplane Code");
                super.addVar("buttonSearch", "Search");
                super.addVar("SearchFail", "Your search didn't return anything");
                super.addVar("FlightNR", "FlightNR");
                super.addVar("Airplane", "Airplane");
                super.addVar("DepartDate", "Date of Departure");
                super.addVar("ArrivalDate", "Date of Arival");
                super.addVar("DepartFrom", "Depart (START)");
                super.addVar("DepartUntil", "Depart (UNTIL)");
                super.addVar("DepartTime", "Actual Depart Time");
                super.addVar("ArrivalTime", "Arrival");
                super.addVar("Departure", "Departure");
                super.addVar("Destination", "Destination");
                super.addVar("Runway", "Runway");
                super.addVar("Approval", "Approval");
                super.addVar("Unknown", "Unknown");
                super.addVar("FlightState", "Flight Status");
                super.addVar("Flights", "Flights");
                super.addVar("FlightsPreviousLabel", "Previous flights: ");
                super.addVar("FlightsPrevious", "Showing the @ latest flights.");
                super.addVar("FlightsNoPrevious", "No previous flights found.");
                super.addVar("FlightsNextLabel", "Upcoming flights:");
                super.addVar("FlightsNext1", "Showing ");
                super.addVar("FlightsNext2", " upcoming flights.");
                super.addVar("FlightsNoNext", "No upcoming flights found.");
                super.addVar("All", "all");
                super.addVar("Refresh", "Refresh");
                super.addVar("NoFlights", "No flights have been assigned to this airplane.");
                super.addVar("PlaneDetails", "Airplane Details");
                super.addVar("HangarCode", "Hangar");
                super.addVar("Airport", "Airport");
                super.addVar("State", "Airplane State");
                super.addVar("ToHangar", "To hangar");
                break;
            case 2:
                super.addVar("Code", "Staartcode");
                super.addVar("Capacity", "Capaciteit");
                super.addVar("Status", "Status");
                super.addVar("Owner", "Eigenaar");
                super.addVar("Builder", "Constructeur");
                super.addVar("BuildYear", "Bouwjaar");
                super.addVar("Hangar", "Hangaar");
                super.addVar("Type", "Type");
                super.addVar("planeCode", "Vliegtuig Code");
                super.addVar("Search", "Zoek");
                super.addVar("New", "Nieuw");
                super.addVar("Update", "Updaten");
                
                super.addVar("Cancel", "Annuleer");
                super.addVar("Create", "Maak nieuw");
                super.addVar("Delete", "Verwijder");
                super.addVar("Details", "Details");
                super.addVar("Passenger", "Beheer Passagiers");
                super.addVar("Flight", "Beheer  Vluchten"); 
                super.addVar("ConfirmDelete", "Wilt u dit vliegtuig verwijderen");
                super.addVar("Update airplane","Vliegtuig beheren");
                super.addVar("ID", "ID");
                super.addVar("Action", "Actie");
                super.addVar("Edit", "Bewerken");
                super.addVar("Save", "Bewaar");
                super.addVar("EmptyFields","Gelieve alle velden in te vullen");
                super.addVar("TailCodeFail","De staartcode is niet uniek");
                super.addVar("DBDelete", "De database heeft uw vliegtuig verwijderd");
                super.addVar("DBDeleteFail", "De database kan uw vliegtuig niet verwijderen");
                super.addVar("DBSuccess", "De database heeft uw vliegtuig bewaard");
                super.addVar("DBFail", "De database heeft uw vliegtuig niet kunnen bewaren");
                super.addVar("PHcode", "Type de staartcode hier");
                super.addVar("PHowner", "Kies hier een eigenaar");
                super.addVar("PHstatus", "Type de status hier");
                super.addVar("PHcapacity", "Type de capaciteit hier");
                super.addVar("PHhangar", "Kies hier een hangaar");
                super.addVar("planeCode", "Vliegtuig Code");
                super.addVar("buttonSearch", "Zoek");
                super.addVar("SearchFail", "Uw zoekopdracht leverde niets op");
                super.addVar("FlightNR", "VluchtNR");
                super.addVar("Airplane", "Vliegtuig");
                super.addVar("DepartDate", "Datum vertrek");
                super.addVar("ArrivalDate", "Datum aankomst");
                super.addVar("DepartFrom", "Vertrek (VAN)");
                super.addVar("DepartUntil", "Vertrek (TOT)");
                super.addVar("DepartTime", "Eigenlijk vertrekuur");
                super.addVar("ArrivalTime", "Aankomst");
                super.addVar("Departure", "Vertrek");
                super.addVar("Destination", "Bestemming");
                super.addVar("Runway", "Startbaan");
                super.addVar("Approval", "Goedkeuring");
                super.addVar("Unknown", "Onbekend");
                super.addVar("FlightState", "Vlucht Status");
                super.addVar("Flights", "Vluchten");
                super.addVar("FlightsPreviousLabel", "Vorige Vluchten: ");
                super.addVar("FlightsPrevious", "De @ meest recente vluchten.");
                super.addVar("FlightsNoPrevious", "Geen vorige vluchten gevonden.");
                super.addVar("FlightsNext1", "Toon volgende ");
                super.addVar("FlightsNext2", " vluchten.");
                super.addVar("FlightsNoNext", "Geen volgende vluchten gevonden.");
                super.addVar("FlightsNextLabel", "Volgende vluchten:");
                super.addVar("All", "alle");
                super.addVar("Refresh", "Vernieuwen");
                super.addVar("NoFlights", "Aan dit vliegtuig zijn nog geen vluchten toegekend.");
                super.addVar("PlaneDetails", "Vliegtuiginfo");
                super.addVar("HangarCode", "Hangaar");
                super.addVar("Airport", "Luchthaven");
                super.addVar("State", "Vliegtuig Toestand");
                super.addVar("ToHangar", "Naar hangaar");
                break;
        }
    }
    
    public AirplaneVM(Airplane plane, int lang){
        this(lang);
       
    }
    
    public LogicAction getFeedback() {
        return feedback;
    }

    public void setFeedback(LogicAction feedback) {
        this.feedback = feedback;
    }
    
    public Airplane getAirplane(){        
        return Airplane;        
    }
    
     public void setAirplane(Airplane plane){        
        Airplane = plane;        
    }
    
    public List<Owner> getOwnersList() {
        return this.OwnersList;
    }
    
    public void setOwnersList(List<Owner> owners) {
        this.OwnersList = owners;
    }   

    public List<Airplane> getAirplaneList() {
        return AirplaneList;
    }

    public void setAirplaneList(List<Airplane> AirplaneList) {
        this.AirplaneList = AirplaneList;
    }

    public List<AirplaneState> getStateList() {
        return StateList;
    }

    public void setStateList(List<AirplaneState> StateList) {
        this.StateList = StateList;
    }

    public List<Hangar> getHangarList() {
        return HangarList;
    }

    public void setHangarList(List<Hangar> HangarList) {
        this.HangarList = HangarList;
    }
    public List<Flight> getFlightOverview() {
        return FlightOverview;
    }

    public void setFlightOverview(List<Flight> FlightOverview) {
        this.FlightOverview = FlightOverview;
    }

    public int getPrevious() {
        return previous;
    }

    public void setPrevious(int previous) {
        this.previous = previous;
    }

    public int getNext() {
        return next;
    }

    public void setNext(int next) {
        this.next = next;
    }
}
