package Models.ViewModel;

import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Ruben Veris
 */
public class UpdatePassengerVM extends BaseVM {
    
    private int id;
    private Date Birthday;
    private String FirstName;
    private String SurName;
    private int AddressID;
    private int TypeID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getBirthday() {
        return Birthday;
    }

    public void setBirthday(Date Birthday) {
        this.Birthday = Birthday;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String SurName) {
        this.SurName = SurName;
    }

    public int getAddressID() {
        return AddressID;
    }

    public void setAddressID(int AddressID) {
        this.AddressID = AddressID;
    }

    public int getTypeID() {
        return TypeID;
    }

    public void setTypeID(int TypeID) {
        this.TypeID = TypeID;
    }
    
    public UpdatePassengerVM() {
        this(1);
    }
    
    public UpdatePassengerVM(int language) {
        super(language);
        
        switch(language){
            case 1://English
                super.addVar("Passenger", "Manage Passengers");
                super.addVar("Flight", "Manage Flights");
                super.addVar("Manage Airplanes","Manage Airplanes");
                super.addVar("Search","Search");
                super.addVar("New","New");
                super.addVar("Update","Update");
                super.addVar("Delete","Delete");
                super.addVar("First Name","First Name");
                super.addVar("Last Name", "Last Name");                
                super.addVar("Birthday", "Birthday");
                super.addVar("Street", "Street");
                super.addVar("Number", "Number");
                super.addVar("Location", "Location");
                super.addVar("ZIP", "ZIP");
                super.addVar("Country", "Country");
                super.addVar("Class", "Class"); 
                super.addVar("View passengers", "View Passengers");
                super.addVar("Surname", "Surname");
                super.addVar("Address", "Address");
                super.addVar("Create", "Create");
                super.addVar("Edit", "Edit");
                super.addVar("ThankYou", "Passenger created");   
                super.addVar("Search flight", "Search linked flights");
                super.addVar("CreatePass","Create Passenger");
                
                break;
            case 2://Nederlands
                super.addVar("Passenger", "Beheer Passagiers");
                super.addVar("Flight", "Beheer Vluchten");
                super.addVar("Manage Airplanes","Beheer Vliegtuigen");
                super.addVar("Search", "Opzoeken");
                super.addVar("New","Nieuw");
                super.addVar("Update","Updaten");
                super.addVar("Delete","Verwijderen");
                super.addVar("First Name","Voornaam");
                super.addVar("Last Name", "Achternaam");
                super.addVar("Birthday", "GeboorteDatum");
                super.addVar("Street", "Straat");
                super.addVar("Number", "Nummer");
                super.addVar("Location", "Locatie");
                super.addVar("ZIP", "Postcode");
                super.addVar("Country", "Land"); 
                super.addVar("Class", "Klasse");
                super.addVar("View passengers", "Passengers lijst");
                super.addVar("Surname", "Achternaam");
                super.addVar("Address", "Adres");
                super.addVar("Create", "Aanmaken");
                super.addVar("Edit", "Aanpassen");
                super.addVar("ThankYou", "Passagier aangemaakt"); 
                super.addVar("Search flight", "Gelinkte vluchten");
                super.addVar("CreatePass","Maak passagier aan");
                break;
        }
    }
    
}
