/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

import Models.Datamodels.Language;
import Models.Domain.Message;
import Models.Domain.Message.MessageSeverity;
import Models.Datamodels.Navigation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jelle
 */
public abstract class BaseVM {

    private ArrayList<Navigation> listNav = new ArrayList<>();
    private ArrayList<Language> listLang = new ArrayList<>();
    private Map<String, String> varMap = new HashMap<>();
    private int Language;
    private ArrayList<Message> listMessage = new ArrayList<>();

    public int getLanguage() {
        return Language;
    }

    public Map<String, String> getVarMap() {
        return varMap;
    }

    public final void addVar(String varName, String content) {
        this.varMap.put(varName, content);
    }
    
    public BaseVM(int language)
    {
       this(language, null);
    }

    public BaseVM(int language,  HttpSession session) {
        Language = language;
        //Language Items
        listLang.add(new Language(1, "English"));
        listLang.add(new Language(2, "Nederlands"));

        switch (language) {
            case 1:
                listNav.add(new Navigation(2, "Airline", "Airline", 0));
                listNav.add(new Navigation(3, "Airport", "Airport", 0));
                listNav.add(new Navigation(4, "Owner", "Owner", 0));
                listNav.add(new Navigation(5, "User", "User", 0));
                this.addVar("language", "Choose your language");
                this.addVar("SubmitLanguage", "Submit");
                this.addVar("Passenger", "Manage Passengers");
                this.addVar("Flight", "Manage Flights");
                this.addVar("Manage Airplanes","Manage Airplanes");
                this.addVar("Search","Search");
                this.addVar("Searchandadjust","Manage List");
                this.addVar("New","New");
                this.addVar("Update","Update");
                this.addVar("Delete","Delete");
                this.addVar("Crew","Crew");
                this.addVar("Approve Flights","Approve Flights");
                this.addVar("manage_flights", "Manage flights");
                this.addVar("Approve_Flights", "Manage hangars");
                this.addVar("PassengerList", "View Passengerlist");
                this.addVar("Upcoming", "Upcoming Flights");
                this.addVar("Report", "Flight Reports");
                this.addVar("LogOut", "Log out");
                this.addVar("Incoming Flights", "Incoming Flights");
                this.addVar("Manage runway", "Manage runways");
                this.addVar("Update runway", "Update/Create runway");
                this.addVar("Manage hangar", "Manage hangars");
                this.addVar("Update hangar", "Update/Create hangar");
                break;
            case 2:
                listNav.add(new Navigation(2, "LVM", "Airline", 0));
                listNav.add(new Navigation(3, "Luchthaven", "Airport", 0));
                listNav.add(new Navigation(4, "Eigenaar", "Owner", 0));
                listNav.add(new Navigation(5, "Gebruiker", "User", 0));

                this.addVar("language", "Kies uw taal");
                this.addVar("SubmitLanguage", "Verzenden");
                this.addVar("Passenger", "Beheer Passagiers");
                this.addVar("Flight", "Beheer Vluchten");
                this.addVar("Manage Airplanes", "Beheer Vliegtuigen");
                this.addVar("manage_flights", "Vluchten beheren");
                this.addVar("Search", "Opzoeken");
                this.addVar("Searchandadjust","Beheers Lijst");
                this.addVar("New","Nieuw");
                this.addVar("Update","Updaten");
                this.addVar("Delete","Verwijderen");
                this.addVar("Crew","Bemanning");
                this.addVar("Approve Flights","Vluchten goedkeuren");
                this.addVar("Manage hangar", "Beheer hangars");
                this.addVar("Update hangar", "Update/Nieuwe hangar");
                this.addVar("Manage runway", "Beheer landingsbanen");
                this.addVar("Update runway", "Update/Nieuwe landingsbaan");
                this.addVar("PassengerList", "Passagierslijst opvragen");
                this.addVar("Upcoming", "Komende Vluchten");
                this.addVar("Report", "Vlucht Rapporten");
                this.addVar("LogOut", "Uit loggen");
                addVar("Incoming Flights", "Inkomende Vluchten");
                break;
        }
        
         //Message framework session
        if (session != null && session.getAttribute("MessageList") != null ) {
            ArrayList<Message> messageList  = (ArrayList<Message>)session.getAttribute("MessageList");
            this.setListMessage(messageList);
            
            //Clear message list
            session.setAttribute("MessageList", null);
        }
    }

    public ArrayList<Navigation> getListNav() {
        return listNav;
    }

    public ArrayList<Language> getListLang() {
        return listLang;
    }

    public void addMessage(String message, MessageSeverity severity)
    {
        listMessage.add(new Message(message, severity));
    }
    
    public ArrayList<Message> getListMessage(){
        return listMessage;
    }
    
    public final void setListMessage(ArrayList<Message> listMessage){
        this.listMessage = listMessage;
    }
}