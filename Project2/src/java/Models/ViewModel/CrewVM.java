/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

import Models.Domain.Crew;
import Models.Domain.Flight;
import java.util.List;

/**
 *
 * @author Jelle
 */
public class CrewVM extends BaseVM{
    
    private List<Crew> crewList;
    private List<Crew> listStewards;
    private List<Crew> listPilots;

    public List<Crew> getListPilots() {
        return listPilots;
    }

    public void setListPilots(List<Crew> listPilots) {
        this.listPilots = listPilots;
    }

    public List<Crew> getListStewards() {
        return listStewards;
    }

    public void setListStewards(List<Crew> listStewards) {
        this.listStewards = listStewards;
    }
    
       
    private Flight flight;

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight fl) {
        this.flight = fl;
    }

    public List<Crew> getCrewList() {
        return crewList;
    }

    public void setCrewList(List<Crew> crewList) {
        this.crewList = crewList;
    }
    
    public CrewVM() {
        this(1);
    }
    
    public CrewVM(int language) {
        super(language);
        switch(language) {
            case 1:
                super.addVar("noCrew", "There is no crew selected for this flight");
                super.addVar("noAvailableCrew", "There is no crew available for this flight");
                super.addVar("CrewForFlight", "Crew For Flight");
                super.addVar("Depart", "Depart");
                super.addVar("Function", "Function");
                super.addVar("Surname", "Surname");
                super.addVar("Firstname", "Firstname");
                super.addVar("Back", "Back");
                super.addVar("addCrew", "Add crew to flight");
                super.addVar("deleteCrew", "Remove from flight crew");
                super.addVar("availableCrew", "Available Crew");
                super.addVar("Remove", "Remove");
                super.addVar("RemoveMessage", "Remove crewmember from flight");
                super.addVar("Choose", "Make your choise");
                super.addVar("Save", "Save selection");
                super.addVar("Close", "Close");
                super.addVar("noSelection", "No selection was made. First you have to choose a crew-member");
                super.addVar("addFailed", "The adding of a crew member has failed");
                super.addVar("addSuccess", "The crew member was added to the flight");
                super.addVar("removeFailed", "The removal of the crew member has failed");
                super.addVar("removeSuccess", "The crew member was removed from the flight");
                break;
            case 2:
                super.addVar("noCrew", "Er is nog geen personeel gekoppeld aan deze vlucht");
                super.addVar("noAvailableCrew", "Er is geen personeel beschikbaar voor deze vlucht");
                super.addVar("CrewForFlight", "Bemanning voor vlucht");
                super.addVar("Depart", "Vertrek");
                super.addVar("Function", "Functie");
                super.addVar("Surname", "Familienaam");
                super.addVar("Firstname", "Voornaam");
                super.addVar("Back", "Terug");
                super.addVar("addCrew", "Personeel toevoegen aan de vlucht");
                super.addVar("deleteCrew", "Verwijder personeelslid van vlucht");
                super.addVar("availableCrew", "Beschikbaar personeel");
                super.addVar("Remove", "Verwijderen");
                super.addVar("RemoveMessage", "Personeelslid verwijderen van vlucht");
                super.addVar("Choose", "Maak uw keuze");
                super.addVar("Save", "Keuze bewaren");
                super.addVar("Close", "Sluiten");
                super.addVar("noSelection", "U heeft geen keuze gemaakt. Maak een keuze uit het beschikbaar personeel");
                super.addVar("addFailed", "Het toevoegen van vluchtpersoneel is mislukt");
                super.addVar("addSuccess", "Het personeelslid werd toegevoegd aan de vlucht");
                super.addVar("removeFailed", "Het verwijderen van vluchtpersoneel is mislukt");
                super.addVar("removeSuccess", "Het personeelslid werd verwijderd van de vlucht");
                break;
        }
    }
    
}
