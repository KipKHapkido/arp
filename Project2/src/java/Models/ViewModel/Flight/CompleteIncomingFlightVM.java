/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel.Flight;

import Models.Domain.Airport;
import Models.Domain.Flight;
import Models.Domain.Runway;
import Models.ViewModel.BaseVM;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sven Vervloet
 */
public class CompleteIncomingFlightVM extends BaseVM {
    
    Flight f;
    private List<Airport> destinationList;
    private List<Runway> runwayList;
    
    public List<Airport> getDestinationList() {
        return destinationList;
    }

    public void setDestinationList(List<Airport> destinationList) {
        this.destinationList = destinationList;
    }
       
    public Flight getFlight() {
        return f;
    }

    public void setFlight(Flight f) {
        this.f = f;
    }

    
    public List<Runway> getRunwayList() {
        return runwayList;
    }

    public void setRunwayList(List<Runway> runwayList) {
        this.runwayList = runwayList;
    }
    
    
    public CompleteIncomingFlightVM(int language) {
        super(language);
        
        switch(language) {
            case 1:
                super.addVar("Title", "Flights to approve");
                super.addVar("FlightNR", "FlightNR");
                super.addVar("Airplane", "Airplane");
                super.addVar("ArrivalDate", "Date of Arrival");
                super.addVar("ArrivalTime", "Estimated time of arrival");
                super.addVar("Departure", "Departure");
                super.addVar("Destination", "Destination");
                super.addVar("Runway", "Runway");
                super.addVar("Approval", "Approval");
                super.addVar("Approve", "Approve");
                super.addVar("Complete", "Complete Flight");
                super.addVar("Cancel", "Cancel");
                super.addVar("chooseDestination", "No destination was chosen. The flight can't land here. Choose an other Destination");
                super.addVar("ChooseRunway", "No runway was chosen. You have to choose a runway");
                super.addVar("optionMessage", "Make your choise");
                super.addVar("SaveContinue", "Save and Continue");
                super.addVar("Save", "Complete Flight");
                super.addVar("noFlight", "Something went wrong. We couldn't read in the flight");
                super.addVar("noTimes", "There are no times available for departure");
                super.addVar("noRunways", "There are no runways available for departure");
                super.addVar("updateFlightFailed", "Update failed");
                super.addVar("updateFlightSucces", "Flight updated successfully");
                super.addVar("chooseRunway", "You had to choose a available runway");
                super.addVar("noRunways", "There are no runways available. The flight has to redirect to another airport");
                
                break;
            case 2:
                super.addVar("Title", "Goed te keuren vluchten");
                super.addVar("FlightNR", "VluchtNR");
                super.addVar("Airplane", "Vliegtuig");
                super.addVar("ArrivalDate", "Datum van aankomst");
                super.addVar("ArrivalTime", "Geschat tijdstip van aankomst");
                super.addVar("Departure", "Vertrek");
                super.addVar("Destination", "Bestemming");
                super.addVar("Runway", "Startbaan");
                super.addVar("Approval", "Goedkeuring");
                super.addVar("Approve", "Goedkeuren");
                super.addVar("Complete", "Vlucht vervolledigen");
                super.addVar("Cancel", "Annuleren");
                super.addVar("chooseDestination", "Er werd geen bestemming gekozen. De vlucht kan hier niet landen. Kies een andere bestemming");
                super.addVar("ChooseRunway", "Er werd geen landingsbaan gekozen. Kies een landingsbaan");
                super.addVar("optionMessage", "Maak uw keuze");
                super.addVar("SaveContinue", "Bewaren en Doorgaan");
                super.addVar("Save", "Vlucht vervolledigen");
                super.addVar("noFlight", "Er ging iets mis. De vlucht kon niet gelezen worden");
                super.addVar("noTimes", "Er zijn geen startbanen beschikbaar voor vertrek");
                super.addVar("updateFlightFailed", "Update mislukt");
                super.addVar("updateFlightSucces", "Updaten vlucht gelukt");
                super.addVar("chooseTimes", "U moet een tijdstip van vertrek bepalen");
                super.addVar("chooseRunway", "U moet een beschikbare startbaan kiezen");
                super.addVar("noRunways", "Er zijn geen landingsbanen beschikbaar. De vlucht moet uitwijken naar een andere luchthaven.");
                break;
        }
         f = new Flight();
         destinationList = new ArrayList<>();
         runwayList = new ArrayList<>();
    }
    
}
