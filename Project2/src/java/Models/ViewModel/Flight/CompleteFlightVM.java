/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel.Flight;

import Models.Domain.Flight;
import Models.Domain.Runway;
import Models.Domain.TimeClass;
import Models.ViewModel.BaseVM;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sven Vervloet
 */
public class CompleteFlightVM extends BaseVM {
    
    Flight f;
    List<TimeClass> timeList;
    List<Runway> runwayList;

    public Flight getFlight() {
        return f;
    }

    public void setFlight(Flight f) {
        this.f = f;
    }

    public List<TimeClass> getTimeList() {
        return timeList;
    }

    public void setTimeList(List<TimeClass> timeList) {
        this.timeList = timeList;
    }

    public List<Runway> getRunwayList() {
        return runwayList;
    }

    public void setRunwayList(List<Runway> runwayList) {
        this.runwayList = runwayList;
    }
    
    
    public CompleteFlightVM(int language) {
        super(language);
        
        switch(language) {
            case 1:
                super.addVar("Title", "Flights to approve");
                super.addVar("FlightNR", "FlightNR");
                super.addVar("Airplane", "Airplane");
                super.addVar("DepartDate", "Date of Departure");
                super.addVar("DepartFrom", "Depart (START)");
                super.addVar("DepartUntil", "Depart (UNTIL)");
                super.addVar("DepartTime", "Actual Depart Time");
                super.addVar("Departure", "Departure");
                super.addVar("Destination", "Destination");
                super.addVar("Runway", "Runway");
                super.addVar("Approval", "Approval");
                super.addVar("Approve", "Approve");
                super.addVar("Complete", "Complete Flight");
                super.addVar("Cancel", "Cancel");
                super.addVar("ChooseDeparture", "Choose Time of Departure");
                super.addVar("ChooseRunway", "Choose Runway");
                super.addVar("optionMessage", "Make your choise");
                super.addVar("SaveContinue", "Save and Continue");
                super.addVar("Save", "Complete Flight");
                super.addVar("noFlight", "Something went wrong. We couldn't read in the flight");
                super.addVar("noTimes", "There are no times available for departure");
                super.addVar("noRunways", "There are no runways available for departure");
                super.addVar("updateFlightFailed", "Update failed");
                super.addVar("updateFlightSucces", "Flight updated successfully");
                super.addVar("chooseTimes", "You had to choose a available Time of Departure");
                super.addVar("chooseRunway", "You had to choose a available runway");
                super.addVar("Step1", "Step 1");
                super.addVar("Final", "Final Step");
                break;
            case 2:
                super.addVar("Title", "Goed te keuren vluchten");
                super.addVar("FlightNR", "VluchtNR");
                super.addVar("Airplane", "Vliegtuig");
                super.addVar("DepartDate", "Datum vertrek");
                super.addVar("DepartFrom", "Vertrek (VAN)");
                super.addVar("DepartUntil", "Vertrek (TOT)");
                super.addVar("DepartTime", "Eigenlijk vertrekuur");
                super.addVar("Departure", "Vertrek");
                super.addVar("Destination", "Bestemming");
                super.addVar("Runway", "Startbaan");
                super.addVar("Approval", "Goedkeuring");
                super.addVar("Approve", "Goedkeuren");
                super.addVar("Complete", "Vlucht vervolledigen");
                super.addVar("Cancel", "Annuleren");
                super.addVar("ChooseDeparture", "Kies vertrekuur");
                super.addVar("ChooseRunway", "Kies startbaan");
                super.addVar("optionMessage", "Maak uw keuze");
                super.addVar("SaveContinue", "Bewaren en Doorgaan");
                super.addVar("Save", "Vlucht vervolledigen");
                super.addVar("noFlight", "Er ging iets mis. De vlucht kon niet gelezen worden");
                super.addVar("noTimes", "Er zijn geen startbanen beschikbaar voor vertrek");
                super.addVar("updateFlightFailed", "Update mislukt");
                super.addVar("updateFlightSucces", "Updaten vlucht gelukt");
                super.addVar("chooseTimes", "U moet een tijdstip van vertrek bepalen");
                super.addVar("chooseRunway", "U moet een beschikbare startbaan kiezen");
                super.addVar("Step1", "Stap 1");
                super.addVar("Final", "Laatste Stap");
                break;
        }
         f = new Flight();
         timeList = new ArrayList<>();
         runwayList = new ArrayList<>();
    }
    
}
