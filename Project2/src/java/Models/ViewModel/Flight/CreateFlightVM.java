/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel.Flight;

import Models.Domain.Airplane;
import Models.Domain.Airport;
import Models.Domain.TimeClass;
import Models.ViewModel.BaseVM;
import java.util.Date;
import java.util.List;

/**
 *
 * @author CharlotteHendrik
 */
public class CreateFlightVM extends BaseVM{
   
   public CreateFlightVM() {
   this(1);
   }
   
   public CreateFlightVM(int language) {
       super(language);
       
       switch(language) {
           case 1:
               super.addVar("VluchtNR", "Flight number");
               super.addVar("AirplaneID", "Airplane type");
               super.addVar("DepartureID", "Airport of departure");
               super.addVar("DestinationID", "Airport of Destination");
               super.addVar("AddFlight", "Create flight");
               super.addVar("Date", "Date");
               super.addVar("DepartTimeFrom", "Departure from ");
               super.addVar("DepartTimeUntill", " until ");
               super.addVar("Passenger", "Manage passengers");
                super.addVar("Flight", "Manage flights");
                super.addVar("Manage Airplanes","Manage airplanes");
                super.addVar("Search","Search");
                super.addVar("New","New");
                super.addVar("Update","Update");
                super.addVar("Delete","Delete");
                super.addVar("Approve Flight","Approve flight");
                super.addVar("save","Save");
                super.addVar("FillAll", "Please fill all details");
                super.addVar("FillNumber", "Please fill FlightNumber");
                super.addVar("FillDate", "Please fill Date");
                super.addVar("ThankYou", "Thank you for working with us. We have receive the details of your flight. Soon we will take your request into account!");
                break;
            
           case 2:
               super.addVar("VluchtNR", "Vlucht nummer");
               super.addVar("AirplaneID", "Type vliegtuig");
               super.addVar("DepartureID", "Luchthaven vertrek");
               super.addVar("DestinationID", "Luchthaven bestemming");
               super.addVar("AddFlight", "Vlucht toevoegen");
               super.addVar("Date", "Datum");
               super.addVar("DepartTimeFrom", "Vertrek tussen ");
               super.addVar("DepartTimeUntill", " en ");
               super.addVar("Passenger", "Beheer Passagiers");
                super.addVar("Flight", "Beheer vluchten");
                super.addVar("Manage Airplanes","Beheer vliegtuigen");
                super.addVar("Search", "Opzoeken");
                super.addVar("New","Nieuw");
                super.addVar("Update","Updaten");
                super.addVar("Delete","Verwijderen");
                super.addVar("Approve Flight","Goedkeuren vlucht");
                super.addVar("save", "opslaan");
                super.addVar("FillAll", "Gelieve alle gegevens in te vullen");
                super.addVar("FillNumber", "Gelieve de vluchtnummer in te vullen");
                super.addVar("FillDate", "Gelieve de datum in te vullen");
                super.addVar("ThankYou", "Dank u om voor ons te kiezen. Wij hebben de details van uw vlucht goed ontvangen. Wij zullen uw aanvraag zo snel mogelijk behandelen!");
               break;
       }
   }
   
    private String FlightNR;
    private int AirplaneID;
    private Date DepartDate;
    private int DepartTimeID;
    private int DepartUntillID;
    private int DepartureID;
    private int DestinationID;
    private int ID;
    private List<Airport> Airports;
    private List<Airplane> Airplanes;
    private List<TimeClass> Times;
    private int Year;
    private int Month;
    private int Day;

    
    public int getID(){
        return ID;
    }
    public void setID(int id){
        this.ID=id;
    }
    public int getAirplaneID(){
        return AirplaneID;
    }
    public void setAirplaneID(int airplaneid){
        this.AirplaneID=airplaneid;
    }
    public int getDepartTimeID(){
        return DepartTimeID;
    }
    public void setDepartTimeID(int departtime){
        this.DepartTimeID=departtime;
    }
    
    public int getDepartUntillID(){
        return DepartUntillID;
    }
    public void setDepartUntillID(int departuntill){
        this.DepartUntillID=departuntill;
    }
    public int getDepartureID(){
        return DepartureID;
    }
    public void setDepartureID(int departureid){
        this.DepartureID=departureid;
    }
    public int getDestinationID(){
        return DestinationID;
    }
    public void setDestinationID(int destinationid){
        this.DestinationID=destinationid;
    }

    public Date getDepartDate(){
        return DepartDate;
    }
    public void setDepartDate(Date departdate){
        this.DepartDate=departdate;
    }    
    
    public String getFlightNR() {
        return FlightNR ;
    }

    public void setFlightNR(String flightNR) {
        this.FlightNR = flightNR;
    }
    
        public List<Airport> getAirports(){
        return Airports;
    } 
    
    public void setAirports (List<Airport> airports){
        this.Airports = airports;
    }
    
    public List<Airplane> getAirplanes(){
        return Airplanes;
    }
    
    public void setAirplanes (List<Airplane> airplanes){
        this.Airplanes = airplanes;
    }
       
    public List<TimeClass> getTimes(){
        return Times;
    }
    
    public void setTimes (List<TimeClass> times){
        this.Times = times;
    }
    
    public int getYear() {
        return Year;
    }
    
    public void setYear(int year){
        this.Year=year;
    }
    
    public int getMonth() {
        return Month;
    }
    
    public void setMonth(int month){
        this.Month=month;
    }
    
    public int getDay() {
        return Day;
    }
    
    public void setDay(int day){
        this.Day=day;
    }

   
}


