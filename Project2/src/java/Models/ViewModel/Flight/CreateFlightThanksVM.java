/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel.Flight;

import Models.ViewModel.BaseVM;

/**
 *
 * @author CharlotteHendrik
 */
public class CreateFlightThanksVM extends BaseVM{
   
   public CreateFlightThanksVM() {
   this(1);
   }
   
   public CreateFlightThanksVM(int language) {
       super(language);
       
       switch(language) {
           case 1:
               super.addVar("ThankYou", "Thank you for working with us. We have receive the details of your flight. Soon we will take your request into account!");
                super.addVar("Passenger", "Manage Passengers");
                super.addVar("Flight", "Manage Flights");
                super.addVar("Manage Airplanes","Manage Airplanes");
                super.addVar("Search","Search");
                super.addVar("New","New");
                super.addVar("Update","Update");
                super.addVar("Delete","Delete");
                super.addVar("Approve Flight","Approve Flight");
               break;
           case 2:
               super.addVar("ThankYou", "Dank u om voor ons te kiezen. Wij hebben de details van uw vlucht goed ontvangen. Wij zullen uw aanvraag zo snel mogelijk behandelen!");
               super.addVar("Passenger", "Beheer Passagiers");
                super.addVar("Flight", "Beheer Vluchten");
                super.addVar("Manage Airplanes","Beheer Vliegtuigen");
                super.addVar("Search", "Opzoeken");
                super.addVar("New","Nieuw");
                super.addVar("Update","Updaten");
                super.addVar("Delete","Verwijderen");
                super.addVar("Approve Flight","Goedkeuren vlucht");
               break;
       }
   }
   
}







