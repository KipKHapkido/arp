/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel.Flight;

import Models.Domain.Airport;
import Models.Domain.Flight;
import Models.ViewModel.BaseVM;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Sven Vervloet
 */
public class NewFlightsVM extends BaseVM {
    
    private List<Airport> destinationList;
    private List<Flight> resultList;
    private int DestinationID;
    private String FlightNR;
    private Date comparisonDate;

    public Date getComparisonDate() {
        return comparisonDate;
    }

    public void setComparisonDate(Date comparisonDate) {
        this.comparisonDate = comparisonDate;
    }

    public int getDestinationID() {
        return DestinationID;
    }

    public void setDestinationID(int DestinationID) {
        this.DestinationID = DestinationID;
    }

    public String getFlightNR() {
        return FlightNR;
    }

    public void setFlightNR(String FlightNR) {
        this.FlightNR = FlightNR;
    }
    

    public List<Flight> getResultList() {
        return resultList;
    }

    public void setResultList(List<Flight> resultList) {
        this.resultList = resultList;
    }

    public List<Airport> getDestinationList() {
        return destinationList;
    }

    public void setDestinationList(List<Airport> destinationList) {
        this.destinationList = destinationList;
    }

    public NewFlightsVM() {
        this(1);
    }
    
    public NewFlightsVM(int language) {
        super(language);
        destinationList = new ArrayList<>();
        DestinationID = 0;
        FlightNR = "";
        switch(language) {
            case 1:
                super.addVar("Title", "List of not approved Flights");
                super.addVar("FlightNR", "FlightNR");
                super.addVar("Airplane", "Airplane");
                super.addVar("DepartDate", "Date of Departure");
                super.addVar("DepartFrom", "Depart (START)");
                super.addVar("DepartUntil", "Depart (UNTIL)");
                super.addVar("Departure", "Actual Depart Time");
                super.addVar("DepartAirport", "Departure");
                super.addVar("Destination", "Destination");
                super.addVar("Runway", "Runway");
                super.addVar("Approval", "Approval");
                super.addVar("Approve", "Approve");
                super.addVar("Complete", "Complete Flight");
                super.addVar("Reject", "Reject");
                super.addVar("rejectAsk", "Are you sure you want to reject this flight?");
                super.addVar("ChooseDeparture", "Choose Time of Departure");
                super.addVar("ChooseRunway", "Choose Runway");
                super.addVar("noFlights", "There are no flights to approve");
                super.addVar("rejectFailed", "Rejection of flight failed");
                super.addVar("rejectSucces", "Flight was successfully rejected");
                super.addVar("approveFailed", "Approval of flight failed");
                super.addVar("approveSucces", "Flight was succesfully approved");
                super.addVar("noDestinations", "Something went wrong. We couldn't find any destinations");
                super.addVar("noFlights", "Something went wrong. We couldn't find any flights");
                super.addVar("updateFlightSucces", "Flight updated successfully");
                super.addVar("optionMessage", "Make your choise");
                super.addVar("submit", "Submit");
                super.addVar("Edit", "Edit");
                super.addVar("InfoSend", "Info send to");
                super.addVar("ArrivalDate", "ArrivalDate: ");
                super.addVar("ArrivalHour", "ArrivalHour: ");
                
                break;
            case 2:
                super.addVar("Title", "Lijst van goed te keuren vluchten");
                super.addVar("FlightNR", "VluchtNR");
                super.addVar("Airplane", "Vliegtuig");
                super.addVar("DepartDate", "Datum vertrek");
                super.addVar("DepartFrom", "Vertrek (VAN)");
                super.addVar("DepartUntil", "Vertrek (TOT)");
                super.addVar("Departure", "Eigenlijk vertrekuur");
                super.addVar("DepartAirport", "Vertrek");
                super.addVar("Destination", "Bestemming");
                super.addVar("Runway", "Startbaan");
                super.addVar("Approval", "Goedkeuring");
                super.addVar("Approve", "Goedkeuren");
                super.addVar("Complete", "Vlucht vervolledigen");
                super.addVar("Reject", "Weigeren");
                super.addVar("rejectAsk", "Bent u zeker dat u deze vlucht wil weigeren?");
                super.addVar("ChooseDeparture", "Kies vertrekuur");
                super.addVar("ChooseRunway", "Kies startbaan");
                super.addVar("noFlights", "Er zijn geen vluchten om goed te keuren");
                super.addVar("rejectFailed", "Weigering van vlucht mislukt");
                super.addVar("rejectSucces", "Weigering van vlucht gelukt");
                super.addVar("approveFailed", "Goedkeuring van vlucht mislukt");
                super.addVar("approveSucces", "Goedkeuring van vlucht gelukt");
                super.addVar("noDestinations", "Er ging iets mis. We konden geen bestemmingen vinden");
                super.addVar("noFlights", "Er ging iets mis. We konden geen vluchten vinden");
                super.addVar("updateFlightSucces", "Updaten vlucht gelukt");
                super.addVar("optionMessage", "Maak uw keuze");
                super.addVar("submit", "Zoek");
                super.addVar("Edit", "Wijzigen");
                super.addVar("InfoSend", "Informatie verstuurd naar ");
                super.addVar("ArrivalDate", "Aankomstdatum: ");
                super.addVar("ArrivalHour", "Aankomstuur: ");
                
                break;
        }
    }
    
    
    
    
}
