/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel.Flight;

import Models.Datamodels.Status;
import Models.Domain.Flight;
import Models.ViewModel.BaseVM;
import java.util.List;

/**
 *
 * @author JurgenVanEynde
 */
public class UpcomingFlightsVM extends BaseVM {

    public UpcomingFlightsVM(int language) {
        super(language);
        switch (language) {
            case 1://English
                super.addVar("Upcoming", "Upcoming Flights");
                super.addVar("FlightNR", "FlightNR");
                super.addVar("Airplane", "Airplane");
                super.addVar("DepartDate", "Date of Departure");
                super.addVar("DepartFrom", "Depart (START)");
                super.addVar("DepartUntil", "Depart (UNTIL)");
                super.addVar("Departure", "Actual Depart Time");
                super.addVar("Destination", "Destination");
                super.addVar("Runway", "Runway");
                super.addVar("Status", "Status");
                super.addVar("ConfirmStatus", "Confirm Status");
                super.addVar("Planned", "Planned");
                super.addVar("Check-In", "Check-In");
                super.addVar("Boarding", "Boarding");
                super.addVar("Departed", "Departed");
                super.addVar("Landed", "Landed");
                super.addVar("No Takeoff info", "No Takeoff info");
                super.addVar("Arrived", "Arrived");
                super.addVar("Delayed", "Delayed");
                super.addVar("Canceled", "Canceled");
                break;
            case 2://Nederlands
                super.addVar("Upcoming", "Komende vluchten");
                super.addVar("FlightNR", "VluchtNR");
                super.addVar("Airplane", "Vliegtuig");
                super.addVar("DepartDate", "Datum vertrek");
                super.addVar("DepartFrom", "Vertrek (VAN)");
                super.addVar("DepartUntil", "Vertrek (TOT)");
                super.addVar("Departure", "Eigenlijk vertrekuur");
                super.addVar("Destination", "Bestemming");
                super.addVar("Runway", "Startbaan");
                super.addVar("Status", "Status");
                super.addVar("ConfirmStatus", "Bevestig status");
                super.addVar("Planned", "Gepland");
                super.addVar("Check-In", "Check-In");
                super.addVar("Boarding", "Boarding");
                super.addVar("Departed", "Vertrokken");
                super.addVar("Landed", "Geland");
                super.addVar("No Takeoff info", "Geen info");
                super.addVar("Arrived", "Aangekomen");
                super.addVar("Delayed", "Vertraagd");
                super.addVar("Canceled", "Geannuleerd");
                break;
        }
    }

    private List<Flight> flights;

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    private List<Status> status;

    public List<Status> getStatus() {
        return status;
    }

    public void setStatus(List<Status> status) {
        this.status = status;
    }
}
