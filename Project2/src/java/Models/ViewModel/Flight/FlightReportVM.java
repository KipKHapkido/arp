/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel.Flight;

import Logic.Flight.FlightReportLogic;
import Models.Domain.Flight;
import Models.ViewModel.BaseVM;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Jeff
 */
public class FlightReportVM extends BaseVM {
    
    private List<Flight> flightList;
    private Flight f;
    FlightReportLogic frl = new FlightReportLogic();
    
    public FlightReportVM(int language) throws SQLException{
    super(language);
    
    this.setFlightList(frl.GetAllFlights());
    
    switch(language) {
            case 1:
                super.addVar("Title", "Flight Report");
                super.addVar("Search", "Press here for Flight Details");
                super.addVar("FlightNR", "FlightNR");
                super.addVar("Airplane", "Airplane");
                super.addVar("DepartDate", "Date of departure");
                super.addVar("DepartureAirport", "Departure Airport");
                super.addVar("Destination", "Destination");
                super.addVar("Link", "Back to list");
                super.addVar("Select", "Select a flight");
                super.addVar("State", "Flight status");
                super.addVar("Time", "Departure time");
                super.addVar("TimeslotStart", "Timeslot start");
                super.addVar("TimeslotEnd", "Timeslot end");
                super.addVar("Actual", "Actual Departure");
                break;
            case 2:
                super.addVar("Title", "Vlucht Rapport");
                super.addVar("Search", "Geef Vlucht Details");
                super.addVar("FlightNR", "VluchtNR");
                super.addVar("Airplane", "Vliegtuig");
                super.addVar("DepartDate", "Datum vertrek");
                super.addVar("DepartureAirport", "Vertrek Luchthaven");
                super.addVar("Destination", "Bestemming");
                super.addVar("Link", "Terug naar lijst");
                super.addVar("Select", "Selecteer een vlucht");
                super.addVar("State", "Vlucht status");
                super.addVar("TimeslotStart", "Begin tijdslot");
                super.addVar("TimeslotEnd", "Einde tijdslot");
                super.addVar("Actual", "Vertrekuur");
                break;
        }
    }

    public List<Flight> getFlightList() {
        return flightList;
    }

    public void setFlightList(List<Flight> flightList) {
        this.flightList = flightList;
    }

    public Flight getFlight() {
        return f;
    }

    public void setFlight(Flight f) {
        this.f = f;
    }
    
    
}
