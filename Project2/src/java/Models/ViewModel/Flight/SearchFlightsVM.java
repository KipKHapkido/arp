/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel.Flight;

import Models.Domain.Airport;
import Models.Domain.Flight;
import Models.ViewModel.BaseVM;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Sven Vervloet
 */
public class SearchFlightsVM extends BaseVM {
    
    private List<Airport> destinationList;
    private List<Flight> resultList;
    private int DestinationID;
    private String FlightNR;
    private Date comparisonDate;

    public Date getComparisonDate() {
        return comparisonDate;
    }

    public void setComparisonDate(Date comparisonDate) {
        this.comparisonDate = comparisonDate;
    }
    
    public int getDestinationID() {
        return DestinationID;
    }

    public void setDestinationID(int DestinationID) {
        this.DestinationID = DestinationID;
    }

    public String getFlightNR() {
        return FlightNR;
    }

    public void setFlightNR(String FlightNR) {
        this.FlightNR = FlightNR;
    }
    

    public List<Flight> getResultList() {
        return resultList;
    }

    public void setResultList(List<Flight> resultList) {
        this.resultList = resultList;
    }

    public List<Airport> getDestinationList() {
        return destinationList;
    }

    public void setDestinationList(List<Airport> destinationList) {
        this.destinationList = destinationList;
    }

    public SearchFlightsVM() {
        this(1);
    }
    
    public SearchFlightsVM(int language) {
        super(language);
        destinationList = new ArrayList<>();
        DestinationID = 0;
        FlightNR = "";
        switch(language) {
            case 1:
                super.addVar("Title", "Search Flight");
                super.addVar("FlightNR", "FlightNR");
                super.addVar("Airplane", "Airplane");
                super.addVar("DepartDate", "Date of Departure");
                super.addVar("DepartFrom", "Depart (START)");
                super.addVar("DepartUntil", "Depart (UNTIL)");
                super.addVar("Departure", "Actual Depart Time");
                super.addVar("Destination", "Destination");
                super.addVar("Runway", "Runway");
                super.addVar("Approval", "Approval");
                super.addVar("Approve", "Approve");
                super.addVar("FlightState", "Flight State");
                super.addVar("Delete", "Delete");
                super.addVar("ChooseDeparture", "Choose Time of Departure");
                super.addVar("ChooseRunway", "Choose Runway");
                super.addVar("noFlights", "There are no flights to approve");
                super.addVar("deleteFailed", "Delete of flight failed");
                super.addVar("deleteSucces", "Flight was successfully deleted");
                super.addVar("approveFailed", "Approval of flight failed");
                super.addVar("approveSucces", "Flight was succesfully approved");
                super.addVar("noDestinations", "Something went wrong. We couldn't find any destinations");
                super.addVar("noFlights", "Something went wrong. We couldn't find any flights");
                super.addVar("optionMessage", "Make your choise");
                super.addVar("submit", "Submit");
                super.addVar("Edit", "Edit Flight");
                super.addVar("Cancel", "Cancel Flight");
                super.addVar("CancelAsk", "Are you sure you want to Cancel the Flight?");
                super.addVar("Explanation", "A flight can be edited as long as the flight has no passengers");
                super.addVar("Explanation2", "passenger(s). A flight can only be edited if the flight doesn't have passengers");
                super.addVar("CancelFailed", "Cancellation of the Flight failed");
                super.addVar("CancelSucces", "Cancellation of the Flight is successful");
                super.addVar("ViewCrew", "View the Crew");
                super.addVar("EditCrew", "Manage the Crew");
                super.addVar("ThankYou", "Thank you for working with us. We have receive the details of your flight. Soon we will take your request into account!");

                super.addVar("EditPassenger", "Manage the Passengers");
                break;
            case 2:
                super.addVar("Title", "Vluchten opzoeken");
                super.addVar("FlightNR", "VluchtNR");
                super.addVar("Airplane", "Vliegtuig");
                super.addVar("DepartDate", "Datum vertrek");
                super.addVar("DepartFrom", "Vertrek (VAN)");
                super.addVar("DepartUntil", "Vertrek (TOT)");
                super.addVar("Departure", "Eigenlijk vertrekuur");
                super.addVar("Destination", "Bestemming");
                super.addVar("Runway", "Startbaan");
                super.addVar("Approval", "Goedkeuring");
                super.addVar("Approve", "Goedkeuren");
                super.addVar("FlightState", "Vlucht Status");
                super.addVar("Delete", "Verwijder");
                super.addVar("ChooseDeparture", "Kies vertrekuur");
                super.addVar("ChooseRunway", "Kies startbaan");
                super.addVar("noFlights", "Er zijn geen vluchten om goed te keuren");
                super.addVar("deleteFailed", "Verwijdering van vlucht mislukt");
                super.addVar("deleteSucces", "Verwijdering van vlucht gelukt");
                super.addVar("approveFailed", "Goedkeuring van vlucht mislukt");
                super.addVar("approveSucces", "Goedkeuring van vlucht gelukt");
                super.addVar("noDestinations", "Er ging iets mis. We konden geen bestemmingen vinden");
                super.addVar("noFlights", "Er ging iets mis. We konden geen vluchten vinden");
                super.addVar("optionMessage", "Maak uw keuze");
                super.addVar("submit", "Zoek");
                super.addVar("Edit", "Vlucht aanpassen");
                super.addVar("Cancel", "Vlucht schrappen");
                super.addVar("CancelAsk", "Bent u zeker dat u de vlucht wil schrappen?");
                super.addVar("Explanation", "Een vlucht kan gewijzigd worden zolang de vlucht geen passagiers heeft");
                super.addVar("Explanation2", "passagier(s). Een vlucht kan enkel gewijzigd worden als de vlucht geen passagiers heeft");
                super.addVar("CancelFailed", "Annulatie van de vlucht is mislukt");
                super.addVar("CancelSucces", "Annulatie van de vlucht is gelukt");
                super.addVar("ViewCrew", "Bekijk de bemanning");
                super.addVar("EditCrew", "Beheer de bemanning");
                super.addVar("ThankYou", "Dank u om voor ons te kiezen. Wij hebben de details van uw vlucht goed ontvangen. Wij zullen uw aanvraag zo snel mogelijk behandelen!");

                super.addVar("EditPassenger", "Beheer de passagiers");
                break;
        }
    }
    
    
    
    
}
