/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

/**
 *
 * @author Sven Vervloet
 */
public class AirlineVM extends BaseVM {

    public AirlineVM() {
        this(1);
    }
    
    public AirlineVM(int language) {
        super(language);
        switch(language){
            case 1://English
                super.addVar("Passenger", "Manage Passengers");
                super.addVar("Flight", "Manage Flights");
                super.addVar("Manage Airplanes","Manage Airplanes");
                super.addVar("Search","Search");
                super.addVar("New","New");
                super.addVar("Update","Update");
                super.addVar("AddPassenger", "Add Passenger");
                super.addVar("Delete","Delete");
                super.addVar("Approve Flight","Approve Flight");
                super.addVar("Delete","Delete");
                super.addVar("Approve Flight","Approve Flight");
                super.addVar("ViewAll", "View Passengers");
                super.addVar("Airline","Airline");
                break;
            case 2://Nederlands
                super.addVar("Passenger", "Beheer Passagiers");
                super.addVar("Flight", "Beheer Vluchten");
                super.addVar("Manage Airplanes","Beheer Vliegtuigen");
                super.addVar("Search", "Opzoeken");
                super.addVar("New","Nieuw");
                super.addVar("Update","Updaten");
                super.addVar("AddPassenger", "Passagier Toevoegen");
                super.addVar("Delete","Verwijderen");
                super.addVar("Approve Flight","Goedkeuren vlucht");
                super.addVar("ViewAll", "Alle Passagiers");
                super.addVar("Airline","Luchtvaartmaatschappij");
                break;
        }
    }
    
}
