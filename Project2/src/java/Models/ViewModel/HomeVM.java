/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

/**
 *
 * @author Sven Vervloet
 */
public class HomeVM extends BaseVM {

    public HomeVM() {
        this(1);
    }
    
    public HomeVM(int language) {
        super(language);
        
        switch(language) {
            case 1:
                super.addVar("Welcome", "Welkom to the ARP - System");
                break;
            case 2:
                super.addVar("Welcome", "Welkom op het ARP - System");
                break;
        }
    }
    
}
