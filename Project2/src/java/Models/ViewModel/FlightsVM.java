/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

import Models.Domain.Flight;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class FlightsVM extends BaseVM {
    
    private List<Flight> flights; 
    
    public FlightsVM(){
        this(1);
        flights = new ArrayList<>();
        setFlights(flights);
    }
    
    public FlightsVM(List<Flight> flights){
        this(1);
        setFlights(flights);
    }
    
    public FlightsVM(int language, List<Flight> flights){
        this(language);
        setFlights(flights);
    }
    
    public FlightsVM(int language){
        super(language);
        
        switch(language){
            case 1://English
                super.addVar("Passenger", "Manage Passengers");
                super.addVar("Flight", "Manage Flights");
                super.addVar("Manage Airplanes","Manage Airplanes");
                super.addVar("Search","Search");
                super.addVar("New","New");
                super.addVar("Update","Update");
                super.addVar("AddPassenger", "Add Passenger");
                super.addVar("Delete","Delete");
                super.addVar("Approve Flight","Approve Flight");
                super.addVar("Flight", "Flight");
                super.addVar("FreeSeats", "Free Seats");
                super.addVar("Add", "Add Passenger");
                break;
            case 2://Nederlands
                super.addVar("Passenger", "Beheer Passagiers");
                super.addVar("Flight", "Beheer Vluchten");
                super.addVar("Manage Airplanes","Beheer Vliegtuigen");
                super.addVar("Search", "Opzoeken");
                super.addVar("New","Nieuw");
                super.addVar("Update","Updaten");
                super.addVar("AddPassenger", "Passagier Toevoegen");
                super.addVar("Delete","Verwijderen");
                super.addVar("Approve Flight","Goedkeuren vlucht");
                super.addVar("Flight", "Vlucht");
                super.addVar("FreeSeats", "Vrije Plaatsen");
                super.addVar("Add", "Passagier Toevoegen");
                break;
        }
    }
    
    public List<Flight> getFlights(){
        return flights;
    }
    
    public void setFlights(List<Flight> flights){
        this.flights = flights;
    }
}
