/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;
import java.util.ArrayList;
import Models.Domain.Passenger;

/**
 *
 * @author Jo
 */
public class PassengerFlightVM extends BaseVM {
    
    private int id;
    private String FlightNr;
    private ArrayList<Passenger> passengerList = new ArrayList<>();

    public ArrayList<Passenger> getPassengerList() {
        return passengerList;
    }

    public void setPassengerList(ArrayList<Passenger> passengerList) {
        this.passengerList = passengerList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFlightNr() {
        return FlightNr;
    }

    public void setFlightNr(String FlightNr) {
        this.FlightNr = FlightNr;
    }
    
    
    public PassengerFlightVM(int language) {
        super(language);
        
        switch(language){
            case 1://English
                super.addVar("TitlePassengerFlight", "List of Passengers on Flight ");
                super.addVar("NoList", "No Flight, please use Search Flight to select");
                super.addVar("NoList2", "No passengers found!");
                super.addVar("Flight", "Manage Flights");
                super.addVar("Manage Airplanes","Manage Airplanes");
                super.addVar("Search","Search");
                super.addVar("New","New");
                super.addVar("Update","Update");
                super.addVar("Delete","Delete");
                super.addVar("First Name","First Name");
                super.addVar("Last Name", "Last Name");                
                super.addVar("Birthday", "Birthday");
                super.addVar("Street", "Street");
                super.addVar("Number", "Number");
                super.addVar("Location", "Location");
                super.addVar("ZIP", "ZIP");
                super.addVar("Country", "Country");
                super.addVar("Class", "Class"); 
                super.addVar("View passengers", "View Passengers");
                super.addVar("Surname", "Surname");
                super.addVar("Address", "Address");
                super.addVar("Create", "Create");
                super.addVar("Edit", "Edit");
                super.addVar("ThankYou", "Passenger created");   
                super.addVar("Search flight", "Search linked flights");
                super.addVar("CreatePass","Create Passenger");
                super.addVar("Actions", "Actions");
                super.addVar("Passenger ID", "Passenger ID");
                super.addVar("CheckedIn", "Checked In");
                super.addVar("CheckIn", "Check In");
                
                break;
            case 2://Nederlands
                super.addVar("TitlePassengerFlight", "Passagierslijst van Vlucht ");
                super.addVar("NoList", "Geen vlucht, selecteer een vlucht via Vluchten");
                super.addVar("NoList2", "Geen passagiers gevonden!");
                super.addVar("Flight", "Beheer Vluchten");
                super.addVar("Manage Airplanes","Beheer Vliegtuigen");
                super.addVar("Search", "Opzoeken");
                super.addVar("New","Nieuw");
                super.addVar("Update","Updaten");
                super.addVar("Delete","Verwijderen");
                super.addVar("First Name","Voornaam");
                super.addVar("Last Name", "Achternaam");
                super.addVar("Birthday", "GeboorteDatum");
                super.addVar("Street", "Straat");
                super.addVar("Number", "Nummer");
                super.addVar("Location", "Locatie");
                super.addVar("ZIP", "Postcode");
                super.addVar("Country", "Land"); 
                super.addVar("Class", "Klasse");
                super.addVar("View passengers", "Passengers lijst");
                super.addVar("Surname", "Achternaam");
                super.addVar("Address", "Adres");
                super.addVar("Create", "Aanmaken");
                super.addVar("Edit", "Aanpassen");
                super.addVar("ThankYou", "Passagier aangemaakt"); 
                super.addVar("Search flight", "Gelinkte vluchten");
                super.addVar("CreatePass","Maak passagier aan");
                super.addVar("Actions", "Acties");
                super.addVar("Passenger ID", "Passagier ID");
                super.addVar("CheckedIn", "Checked In");
                super.addVar("CheckIn", "Check In");
                break;
        }
    }
    
}
