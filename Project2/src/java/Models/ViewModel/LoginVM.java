/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

/**
 *
 * @author Sven Vervloet
 */
public class LoginVM extends BaseVM {
    private String username;
    private String password;
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    public LoginVM(int language) {
        super(language);
        
        switch (language) {
            case 1://English
                super.addVar("SignIn", "Please sign in");
                super.addVar("Username", "Username");
                super.addVar("Password", "Password");
                super.addVar("RememberMe", "Remember me");
                super.addVar("SignInButton", "Sign in");
                break;
            case 2://Nederlands
                super.addVar("SignIn", "Gelieve u aan te melden");
                super.addVar("Username", "Gebruikersnaam");
                super.addVar("Password", "Paswoord");
                super.addVar("RememberMe", "Onthoud mij");
                super.addVar("SignInButton", "Aanmelden");
                break;
        }
        
        this.username = "";
        this.password = "";
        
    } 
}
