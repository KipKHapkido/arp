/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

import Models.Domain.User;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kathleen
 */
public class UserListVM extends BaseVM {

    private List<User> userList;

    public List getUserList() {
        return userList;
    }

    public void setUserList(List userList) {
        this.userList = userList;
    }

    public UserListVM(int language, HttpSession session) {
        super(language, session);

        userList = new ArrayList<User>();

        switch (language) {
            case 1://English
                super.addVar("New", "New");
                super.addVar("Search", "Search");
                super.addVar("Edit", "Edit");
                super.addVar("Delete", "Delete");
                super.addVar("Username", "Username");
                super.addVar("Actions", "Actions");
                super.addVar("Function", "Function");
                super.addVar("Cancel", "Cancel");
                super.addVar("Details", "Details");
                break;
            case 2://Nederlands
                super.addVar("New", "Nieuw");
                super.addVar("Search", "Zoek");
                super.addVar("Edit", "Aanpassen");
                super.addVar("Delete", "Verwijder");
                super.addVar("Username", "Gebruikersnaam");
                super.addVar("Actions", "Acties");
                super.addVar("Function", "Functie");
                super.addVar("Cancel", "Annuleren");
                super.addVar("Details", "Details");
                break;
        }
    }

}
