/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

import Models.Domain.Address;
import Models.Domain.Passenger;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class PassengersVM extends BaseVM{
    List<Passenger> Passengers;
    List<Address> Addresses;

    public PassengersVM(int language) {
        super(language);
        
        switch(language) {
            case 1:
                super.addVar("View", "View Passengers");
                super.addVar("ID", "ID");
                super.addVar("Firstname", "First Name");
                super.addVar("Lastname", "Last Name");
                super.addVar("Address", "Address");
                super.addVar("Birthday", "Birthday");
                super.addVar("Search", "Search");
                super.addVar("Edit", "Edit");
                super.addVar("BookFlight", "Book Flight");
                super.addVar("Create", "Create");
                super.addVar("Passenger", "Manage Passengers");
                super.addVar("Flight", "Manage Flights");
                super.addVar("Manage Airplanes","Manage Airplanes");
                super.addVar("Search","Search");
                super.addVar("New","New");
                super.addVar("Update","Update");
                super.addVar("AddPassenger", "Add Passenger");
                super.addVar("Delete","Delete");
                super.addVar("Approve Flight","Approve Flight");
                super.addVar("ViewAll", "View Passengers");
                break;
            case 2:
                super.addVar("View", "Passagiers");
                super.addVar("ID", "ID");
                super.addVar("Firstname", "Voornaam");
                super.addVar("Lastname", "Achternaam");
                super.addVar("Address", "Adres");
                super.addVar("Birthday", "Geboortedatum");
                super.addVar("Search", "Zoek");
                super.addVar("Edit", "Aanpassen");
                super.addVar("BookFlight", "Boek Vlucht");
                super.addVar("Create", "Aanmaken");
                super.addVar("Passenger", "Beheer Passagiers");
                super.addVar("Flight", "Beheer Vluchten");
                super.addVar("Manage Airplanes","Beheer Vliegtuigen");
                super.addVar("Search", "Opzoeken");
                super.addVar("New","Nieuw");
                super.addVar("Update","Updaten");
                super.addVar("AddPassenger", "Passagier Toevoegen");
                super.addVar("Delete","Verwijderen");
                super.addVar("Approve Flight","Goedkeuren vlucht");
                super.addVar("ViewAll", "Alle Passagiers");
                break;
        }
    }
    
    public PassengersVM(List<Passenger> passengers, List<Address> addresses, int lang){
        this(lang);
        Passengers = passengers;
        Addresses = addresses;
    }
    
    public List<Passenger> getPassengers(){
        return Passengers;
    }
    
    public List<Address> getAddresses(){
        return Addresses;
    }
}
