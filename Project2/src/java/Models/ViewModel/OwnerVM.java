/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

import Models.Domain.Owner;
import java.util.List;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kathleen
 */
public class OwnerVM extends BaseVM {

    private List<Owner> ownerList;
    
    public List getownerList() {
        return ownerList;
    }

    public void setOwnerList(List ownerList) {
        this.ownerList = ownerList;
    }
    
    public OwnerVM(int language, HttpSession session) {
        super(language, session);
        ownerList = new ArrayList<Owner>();
        
        switch (language) {
            case 1://English
                super.addVar("New", "New");
                super.addVar("Search", "Search");
                super.addVar("Edit", "Edit");
                super.addVar("Delete", "Delete");
                super.addVar("Name", "Name");
                super.addVar("Address", "Address");
                super.addVar("Actions", "Actions");
                super.addVar("Owner", "Owner");
                super.addVar("Cancel", "Cancel");
                super.addVar("Details", "Details");
                super.addVar("Cancel", "Cancel");
                break;
            case 2://Nederlands
                super.addVar("New", "Nieuw");
                super.addVar("Search", "Zoek");
                super.addVar("Edit", "Aanpassen");
                super.addVar("Delete", "Verwijder");
                super.addVar("Name", "Naam");
                super.addVar("Address", "Adres");
                super.addVar("Actions", "Acties");
                super.addVar("Owner", "Eigenaar");
                super.addVar("Cancel", "Annuleren");
                super.addVar("Details", "Details");
                super.addVar("Cancel", "Annuleren");
                break;
        }
    }
    
}
