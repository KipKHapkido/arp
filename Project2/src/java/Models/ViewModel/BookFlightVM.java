/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

import Models.Domain.Airport;
import Models.Domain.Flight;
import Models.Domain.Passenger;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Dries
 */
public class BookFlightVM extends BaseVM{
    Passenger pass;
    Date date;
    List<Flight> flights;
    List<Airport> airports;
    int origin, destination; //Airport IDs
    int seats;
    boolean addPassSuccess;

    public boolean isAddPassSuccess() {
        return addPassSuccess;
    }

    public void setAddPassSuccess(boolean addPassSuccess) {
        this.addPassSuccess = addPassSuccess;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }
    
    public BookFlightVM(int lang){
        super(lang);
        
        switch(lang){
            case 1://English
                super.addVar("Origin", "Airport of Origin");
                super.addVar("Destination", "Destination Airport");
                super.addVar("Seats", "Nr of Seats");
                super.addVar("Search", "Search");
                super.addVar("FlightCode", "Flight Code");
                super.addVar("DepartureTime", "Departure Time");
                super.addVar("DepartureDate", "Departure Date");
                super.addVar("ArrivalDate", "Arrivale Date");
                super.addVar("ArrivalTime", "Arrival Time");
                super.addVar("BookFlight", "Book Flight");
                super.addVar("SearchFlight", "Search for a Flight");
                super.addVar("Passenger", "Manage Passengers");
                super.addVar("Flight", "Manage Flights");
                super.addVar("Manage Airplanes","Manage Airplanes");
                super.addVar("Search","Search");
                super.addVar("New","New");
                super.addVar("Update","Update");
                super.addVar("AddPassenger", "Add Passenger");
                super.addVar("Delete","Delete");
                super.addVar("Approve Flight","Approve Flight");
                super.addVar("ViewAll", "View Passengers");
                super.addVar("Unknown", "Unknown");
                super.addVar("NoFlights", "No flights found.");
                super.addVar("FlightDate", "Flight date");
                super.addVar("Success", "Success!");
                super.addVar("BoardingPass","@pF @pS has booked @s seat(s) on flight @f from @FO to @FD. This flight is sheduled to leave at @h.");
                break;
            case 2://Nederlands
                super.addVar("Origin", "Luchthaven van Vertrek");
                super.addVar("Destination", "Luchthaven van Aankomst");
                super.addVar("Seats", "Aantal Plaatsen");
                super.addVar("Search", "Zoek");
                super.addVar("FlightCode", "Vlucht Code");
                super.addVar("DepartureTime", "Vertrektijd");
                super.addVar("DepartureDate", "Datum van Vertrek");
                super.addVar("ArrivalDate", "Datum van Aankomst");
                super.addVar("ArrivalTime", "Aankomsttijd");
                super.addVar("BookFlight", "Vlucht Boeken");
                super.addVar("SearchFlight", "Zoek een Vlucht");
                super.addVar("Passenger", "Beheer Passagiers");
                super.addVar("Flight", "Beheer Vluchten");
                super.addVar("Manage Airplanes","Beheer Vliegtuigen");
                super.addVar("Search", "Opzoeken");
                super.addVar("New","Nieuw");
                super.addVar("Update","Updaten");
                super.addVar("AddPassenger", "Passagier Toevoegen");
                super.addVar("Delete","Verwijderen");
                super.addVar("Approve Flight","Goedkeuren vlucht");
                super.addVar("ViewAll", "Alle Passagiers");
                super.addVar("Unknown", "Onbekend");
                super.addVar("NoFlights", "Geen vluchten gevonden.");
                super.addVar("FlightDate", "Vlucht datum");
                super.addVar("Success", "Gelukt!");
                super.addVar("BoardingPass","@pF @pS heeft @s plaats(en) geboekt op vlucht @f van @fO naar @fD. Deze vlucht vertrekt om @h.");
                break;
        }
        
        seats = 1;
        
        date = new Date();
    }
    
    public BookFlightVM(Passenger p){
        this(1);
        pass = p;
    }
    
    public BookFlightVM(int lang, Passenger p){
        this(lang);
        pass = p;
    }
    
    public BookFlightVM(Passenger p, Date d){
        this(1);
        pass = p;
        date = d;
    }
    
    public BookFlightVM(int lang, Passenger p, Date d){
        this(lang);
        pass = p;
        date = d;
    }
    
    public void setPassenger(Passenger p){
        pass = p;
    }
    
    public void setDate(Date d){
        date = d;
    }
    
    public Date getDate(){
        return date;
    }
    
    public Passenger getPassenger(){
        return pass;
    }
    
    public void setFlights(List<Flight> flights){
        this.flights = flights;
    }
    
    public List<Flight> getFlights(){
        return flights;
    }

    public List<Airport> getAirports() {
        return airports;
    }

    public void setAirports(List<Airport> airports) {
        this.airports = airports;
    }
    
    public int getOrigin() {
        return origin;
    }

    public void setOrigin(int origin) {
        this.origin = origin;
    }

    public int getDestination() {
        return destination;
    }

    public void setDestination(int destination) {
        this.destination = destination;
    }
}
