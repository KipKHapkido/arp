/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

/**
 *
 * @author Erik Michielsen
 */
public class AirportVM extends BaseVM {

    public AirportVM() {
        this(1);
    }
    
    public AirportVM(int lang) {
        super(lang);
        switch (lang) {
            //1 = engels
            case 1:

                super.addVar("Manage runway", "Manage runways");
                super.addVar("Update runway", "Update runway");
                super.addVar("Manage hangar", "Manage hangars");
                super.addVar("Update hangar", "Update hangar");
                super.addVar("Name", "Name");
                super.addVar("Airport", "Airport");

                break;
            case 2:

                super.addVar("Manage hangar", "Beheer hangars");
                super.addVar("Update hangar", "Update hangar");
                super.addVar("Manage runway", "Beheer landingsbanen");
                super.addVar("Update runway", "Update landingsbaan");
                super.addVar("Name", "Naam");
                super.addVar("Airport", "Luchthaven");

                break;
        }
    }
	    

    
}
