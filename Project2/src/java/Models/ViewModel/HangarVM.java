/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

import Logic.ManageHangarLogic;
import Models.Domain.Airport;
import Models.Domain.Hangar;
import java.util.List;

/**
 *
 * @author Jelle
 */
public class HangarVM extends BaseVM {

    private LogicAction feedback;
    private ManageHangarLogic logic;
    private List<Airport> airportList;
    private Hangar hangar;

    public enum LogicAction {
        CANCEL, CREATE, UPDATE, DELETE, CANCELSAVE, ERRORSAVE, WRONGSAVE, SAVED, ERROR
    }

    public HangarVM() {
        this(1);
    }

    public HangarVM(int lang) {
        super(lang);
        logic = new ManageHangarLogic();
        this.setAirportList(logic.getAllAirports());
        switch (lang) {
            //1 = engels
            case 1:
                super.addVar("Cancel", "Cancel");
                super.addVar("Edit", "Edit");
                super.addVar("Create", "Create new");
                super.addVar("Save", "Save");
                super.addVar("Delete", "Delete");
                super.addVar("Name", "Name");
                super.addVar("Airport", "Airport");
                super.addVar("Capacity", "Capacity");
                super.addVar("Action", "Action");
                super.addVar("PHname", "Type the name here");
                super.addVar("PHcapacity", "Type the capacity here");
                super.addVar("PHairport", "Select the airport here");
                super.addVar("DelAsk", "Are you sure you want to delete the hangar");
                super.addVar("DelSuccess", "The database succesfully removed the hangar");
                super.addVar("DelFail", "The database could not remove the hangar");
                super.addVar("CreateSucces", "The database succesfully saved the hangar");
                super.addVar("CreateFail", "The database experienced a small malfunction"); 
                super.addVar("CreateFillIn", "Not all fields were filled in"); 
                super.addVar("CreateDataIncorrect", "Some fields contain incorrect datatype"); 
                break;
            case 2:
                super.addVar("Cancel", "Annuleer");
                super.addVar("Edit", "Bewerk");
                super.addVar("Create", "Maak nieuw");
                super.addVar("Save", "Bewaar");
                super.addVar("Delete", "Delete");
                super.addVar("Name", "Naam");
                super.addVar("Airport", "Luchthaven");
                super.addVar("Capacity", "Capaciteit");
                super.addVar("Action", "Actie");
                super.addVar("PHname", "Type de naam hier");
                super.addVar("PHcapacity", "Typ de capaciteit hier");
                super.addVar("PHairport", "Selecteer het vliegveld hier");
                super.addVar("DelAsk", "Wilt u deze landingsbaan verwijderen");
                super.addVar("DelSuccess", "De database heeft succesvol de langdingsbaan verwijderd");
                super.addVar("DelFail", "De database kon de landingsbaan niet verwijderen");
                super.addVar("CreateSucces", "De database heeft succesvol de landingsbaan opgeslagen");
                super.addVar("CreateFail", "De database ondervond een probleem");       
                super.addVar("CreateFillIn", "Niet alle velden zijn ingevuld");
                super.addVar("CreateDataIncorrect", "Sommige velden bevatten een incorrect datatype"); 
                break;
        }
    }

    public List<Airport> getAirportList() {
        return airportList;
    }

    public void setAirportList(List<Airport> airportList) {
        this.airportList = airportList;
    }

    public Hangar getHangar() {
        return hangar;
    }

    public void setHangar(Hangar hangar) {
        this.hangar = hangar;
    }

    public LogicAction getFeedback() {
        return feedback;
    }

    public void setFeedback(LogicAction feedback) {
        this.feedback = feedback;
    }
}