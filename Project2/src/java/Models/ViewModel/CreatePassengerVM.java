/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

import Models.Domain.Passenger;
import java.util.List;
import java.util.ArrayList;
/**
 *
 * @author Ruben Veris
 */
public class CreatePassengerVM extends BaseVM {
    
    private List<Passenger> passengerList;
    
    public List getPassengerList() {
        return passengerList;
    }

    public void setPassengerList(List passengerList) {
        this.passengerList = passengerList;
    }
    
    public CreatePassengerVM() {
        super(1);
        passengerList = new ArrayList<>();
    }
    
    
    public CreatePassengerVM(int language) {
        super(language);
        switch(language){
            case 1://English
                super.addVar("Passenger", "Manage Passengers");
                super.addVar("Flight", "Manage Flights");
                super.addVar("Manage Airplanes","Manage Airplanes");
                super.addVar("Search","Search");
                super.addVar("New","New");
                super.addVar("Update","Update");
                super.addVar("Delete","Delete");
                super.addVar("First Name","First Name");
                super.addVar("Last Name", "Last Name");                
                super.addVar("Birthday", "Birthday");
                super.addVar("Street", "Street");
                super.addVar("Number", "Number");
                super.addVar("Location", "Location");
                super.addVar("ZIP", "ZIP");
                super.addVar("Country", "Country");
                super.addVar("Class", "Class"); 
                super.addVar("View passengers", "View Passengers");
                super.addVar("Surname", "Surname");
                super.addVar("Address", "Address");
                super.addVar("Create", "Create");
                super.addVar("Edit", "Edit");
                super.addVar("ThankYou", "Passenger created");   
                super.addVar("Search flight", "Linked flights");
                super.addVar("CreatePass","Create Passenger");
                super.addVar("BookFlight", "Book Flight");
                super.addVar("Linked Flights","Booked Flights of ");
                super.addVar("FlightNR", "Flight number");
                super.addVar("DepatureDate", "Depature date");
                super.addVar("ConfirmDelete", "Remove the flight from this passenger?");
                super.addVar("NoData", "No passengers found");
                
                
                break;
            case 2://Nederlands
                super.addVar("Passenger", "Beheer Passagiers");
                super.addVar("Flight", "Beheer Vluchten");
                super.addVar("Manage Airplanes","Beheer Vliegtuigen");
                super.addVar("Search", "Opzoeken");
                super.addVar("New","Nieuw");
                super.addVar("Update","Updaten");
                super.addVar("Delete","Verwijderen");
                super.addVar("First Name","Voornaam");
                super.addVar("Last Name", "Achternaam");
                super.addVar("Birthday", "GeboorteDatum");
                super.addVar("Street", "Straat");
                super.addVar("Number", "Nummer");
                super.addVar("Location", "Locatie");
                super.addVar("ZIP", "Postcode");
                super.addVar("Country", "Land"); 
                super.addVar("Class", "Klasse");
                super.addVar("View passengers", "Passengers lijst");
                super.addVar("Surname", "Achternaam");
                super.addVar("Address", "Adres");
                super.addVar("Create", "Aanmaken");
                super.addVar("Edit", "Aanpassen");
                super.addVar("ThankYou", "Passagier aangemaakt"); 
                super.addVar("Search flight", "Gelinkte vluchten");
                super.addVar("CreatePass","Maak passagier aan");
                super.addVar("BookFlight", "Vlucht Boeken");
                super.addVar("Linked Flights","Geboekte vluchten van ");
                super.addVar("FlightNR", "Vluchtnummer");
                super.addVar("DepatureDate", "Vertrek Datum");
                super.addVar("ConfirmDelete", "Wenst u de passagier uit te boeken uit deze vlucht?");
                super.addVar("NoData", "Geen passagiers gevonden");
                break;
        }
    }
    
}
    

