/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.ViewModel;

import Logic.FunctionLogic;
import Models.Domain.Function;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kathleen
 */
public class UserDetailVM extends BaseVM{
    
    private int Id;
    private String username;
    private String password;
    private int functionId;
    private List<Function> functionList;
      
    public List<Function> getFunctionList(){
        return functionList;
    }
    
    public void setFunctionList(List<Function> functionList)
    {
        this.functionList = functionList;
    }
    
    
    public String getFunctionDescription()
    {
        FunctionLogic functionLogic = new FunctionLogic();
        
        Function function = functionLogic.getById(this.functionId);

        return function.getDescription();
    }
    
    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public int getFunctionId() {
        return functionId;
    }

    public void setFunctionId(int functionId) {
        this.functionId = functionId;
    }
    
    public UserDetailVM(int language) {
        super(language);
        
        switch (language) {
            case 1://English
                super.addVar("Create", "Create new user");
                super.addVar("Username", "Username");
                super.addVar("Update", "Update user");
                super.addVar("Password", "Password");
                super.addVar("Submit", "Submit");
                super.addVar("Function", "Function");
                super.addVar("Read", "Read");
                super.addVar("Field", "Field");
                super.addVar("Value", "Value");
                super.addVar("Cancel", "Cancel");
                break;
            case 2://Nederlands
                super.addVar("Create", "Maak een nieuwe gebruiker aan");
                super.addVar("Username", "Gebruikersnaam");
                super.addVar("Update", "Wijzig de gebruiker");
                super.addVar("Password", "Paswoord");
                super.addVar("Submit", "Verzenden");
                super.addVar("Function", "Functie");
                super.addVar("Read", "Lezen");
                super.addVar("Field", "Veld");
                super.addVar("Value", "Waarde");
                super.addVar("Cancel", "Annuleren");
                break;
        }
        
        functionList = new ArrayList<Function>();
    }
    
}
