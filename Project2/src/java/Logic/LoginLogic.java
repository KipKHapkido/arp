/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic;

import DAO.UserDAO;
import Logic.Helpers.LoginHelper;
import Models.Domain.User;
import Models.ViewModel.LoginVM;

/**
 *
 * @author Kathleen
 */
public class LoginLogic {
    public boolean authenticate(LoginVM loginVM)
    {
        LoginHelper loginHelper = new LoginHelper();
        UserDAO userDAO = new UserDAO();
        User user = userDAO.getUserByUsername(loginVM.getUsername());
        Boolean isValid = false;
        
        if(user != null)
        {
            try
            {
                isValid = loginHelper.authenticate(loginVM.getPassword(), user.getPassword(), user.getSalt());
            }
            catch(Exception exc)
            {
            }
        }
        
        return isValid;
    }
}
