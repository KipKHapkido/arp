/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic;

import DAO.AirplaneDAO;
import DAO.AirplaneStateDAO;
import DAO.HangarDAO;
import DAO.AirportDAO;
import DAO.FlightDAO;
import DAO.OwnerDAO;
import Models.Domain.Airplane;
import Models.Domain.Airport;
import Models.Domain.Flight;
import Models.Domain.Hangar;
import Models.Domain.Message;
import Models.Domain.Owner;
import Models.ViewModel.AirplaneVM;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jelle
 */
public class AirplaneLogic {

    private final AirplaneDAO airDao = new AirplaneDAO();
    private final OwnerDAO ownDao = new OwnerDAO();
    private final FlightDAO fliDao = new FlightDAO(new Flight());
    private final HangarDAO hanDao = new HangarDAO(new Hangar());
    private final AirplaneStateDAO staDao = new AirplaneStateDAO();
    private final AirportDAO portDao = new AirportDAO(new Airport());
    private AirplaneVM vm = null;

    //action gedrukt geef gerelateerd object terug
    public AirplaneVM airplaneAction(AirplaneVM tempVM, String submit, String search) {
        vm = tempVM;

        String action = submit.substring(0, 3);
        switch (action) {
            case "Cre":
                vm.setFeedback(AirplaneVM.LogicAction.CREATE);
                vm.setOwnersList(ownDao.GetAll());
                vm.setStateList(staDao.GetAll());
                vm.setAirplane(new Airplane());
                break;
            case "ini":
                vm.setAirplane(null);
                vm.setAirplaneList(airDao.getAllEager());
                vm.setFeedback(AirplaneVM.LogicAction.INITIAL);
                break;
            case "edi":
                vm.setAirplane(airDao.GetByID(Integer.parseInt(submit.substring(3))));
                vm.setOwnersList(ownDao.GetAll());
                vm.setStateList(staDao.GetAll());
                vm.setFeedback(AirplaneVM.LogicAction.UPDATE);
                break;
            case "del":
                if (airDao.Delete(Integer.parseInt(submit.substring(3))) > 0) {
                    vm.addMessage(vm.getVarMap().get("DBDelete"), Message.MessageSeverity.SUCCESS);
                } else {
                    vm.addMessage(vm.getVarMap().get("DBDeleteFail"), Message.MessageSeverity.ERROR);
                }
                vm.setAirplane(null);
                vm.setAirplaneList(airDao.getAllEager());
                vm.setFeedback(AirplaneVM.LogicAction.DELETE);
                break;
            case "det":
                vm.setAirplane(getByCode(submit.substring(3), this.getAllOwners()));
                vm.setFlightOverview(PrepareFlightOverview(vm, fliDao.GetFlightOverview(vm.getAirplane())));
                vm.setPrevious(FlightOverviewCount(vm.getFlightOverview(), new Date()));
                if (vm.getNext() > vm.getFlightOverview().size() - vm.getPrevious()) {
                    vm.setNext(vm.getFlightOverview().size() - vm.getPrevious());
                }
                vm.setFeedback(AirplaneVM.LogicAction.READ);
                break;
            case "sea":
                vm.setAirplaneList(airDao.getSearch(search));
                if (vm.getAirplaneList() == null) {
                    vm.setAirplaneList(airDao.getAllEager());
                    vm.addMessage(vm.getVarMap().get("SearchFail"), Message.MessageSeverity.ERROR);
                }
                vm.setAirplane(null);
                vm.setFeedback(AirplaneVM.LogicAction.SEARCH);
                break;
            default:
                vm.setFeedback(AirplaneVM.LogicAction.CANCEL);
                vm.setAirplane(null);
                break;
        }
        return vm;
    }

    //update of Save doorvoeren
    public AirplaneVM Save(AirplaneVM tempVM, String submit, String id, String code, String ownerID, String state, String capacity) {
        vm = tempVM;
        vm.setOwnersList(ownDao.GetAll());
        vm.setStateList(staDao.GetAll());
        if (submit.equals("-1")) {
            vm.setAirplaneList(airDao.getAllEager());
            vm.setFeedback(AirplaneVM.LogicAction.CANCELSAVE);
            return vm;
        }
        Airplane airp = new Airplane();
        airp.setID(Integer.parseInt(id));
        //geen nieuwe dan opladen bestaande
        if (airp.getID() != 0) {
            airp = airDao.GetByID(airp.getID());
        }
        //zet staart nummer
        airp.setCode(code);
        //zet owner id
        if (ownerID != null) {
            airp.setOwnerID(Integer.parseInt(ownerID));
        }
        //state veranderen
        if (state != null) {
            airp.setHangarID(0);
            airp.setAirplaneStateID(Integer.parseInt(state));
        }
        //capacity veranderen
        airp.setCapacity(capacity);
        if (airp.getOwnerID() < 1 || airp.getCapacity().isEmpty()
                || airp.getCode().isEmpty() || airp.getAirplaneStateID() < 1) {
            vm.setFeedback(AirplaneVM.LogicAction.ERRORSAVE);
            vm.addMessage(vm.getVarMap().get("EmptyFields"), Message.MessageSeverity.ERROR);
            vm.setAirplane(airp);
        } else if (!airDao.CheckTail(airp)) {
            vm.setFeedback(AirplaneVM.LogicAction.DUPLSAVE);
            vm.addMessage(vm.getVarMap().get("TailCodeFail"), Message.MessageSeverity.ERROR);
            airp.setCode("");
            vm.setAirplane(airp);
        } else {
            int r = airDao.Save(airp);
            if (r > 0) {
                vm.addMessage(vm.getVarMap().get("DBSuccess"), Message.MessageSeverity.SUCCESS);
            } else {
                vm.addMessage(vm.getVarMap().get("DBFail"), Message.MessageSeverity.ERROR);
            }
            vm.setFeedback(AirplaneVM.LogicAction.SAVED);
            vm.setAirplaneList(airDao.getAllEager());
        }
        return vm;
    }

    public AirplaneVM HangarSave(AirplaneVM tempVM, String submit, String ID, String Hangar) {
        vm = tempVM;
        if (submit.equals("save") && !Hangar.isEmpty()) {
            Airplane a = airDao.GetByID(Integer.parseInt(ID));
            a.setAirplaneStateID(2);
            a.setHangarID(Integer.parseInt(Hangar));
            if (airDao.Save(a) > 0) {
                vm.addMessage(vm.getVarMap().get("DBSuccess"), Message.MessageSeverity.SUCCESS);
            } else {
                vm.addMessage(vm.getVarMap().get("Fail"), Message.MessageSeverity.ERROR);
            }

        }
        vm.setAirplane(null);
        vm.setAirplaneList(airDao.getAllEager());
        vm.setFeedback(AirplaneVM.LogicAction.INITIAL);
        return vm;
    }

    public AirplaneVM HangarStart(AirplaneVM tempVM, String ID) {
        vm = tempVM;
        int idt = Integer.parseInt(ID);
        vm.setAirplane(airDao.GetByID(idt));
        vm.setHangarList(hanDao.GetAll());
        return vm;
    }

    public List<Airplane> getAll() {
        return airDao.getAllEager();
    }

    public List<Owner> getAllOwners() {
        return ownDao.GetAll();
    }

    public Airplane getByCode(String code, List<Owner> owners) {

        ArrayList<Airplane> planes = (ArrayList<Airplane>) airDao.getAllEager();
        Airplane plane = new Airplane();
        for (Airplane a : planes) {
            if (a.getCode().equals(code)) {
                plane = a;
                if (a.getHangarID() != 0) {
                    a.setHangar(hanDao.GetByID(a.getHangarID()));
                    a.getHangar().setAirportObj(portDao.GetByID(a.getHangar().getAirportID()));
                }

                for (Owner o : owners) {
                    if (o.getID() == a.getOwnerID()) {
                        a.setOwner(o);
                    }
                }
                return plane;
            }
        }
        return null;
    }

    public List<Airplane> getSearch(String s) {
        return airDao.getSearch(s);
    }

    private List<Flight> PrepareFlightOverview(AirplaneVM VM, List<Flight> in) {
        List<Flight> out = new ArrayList<Flight>();
        Date now = new Date();
        int n = -1;      //index of first flight with departdate after now

        for (int i = 0; i < in.size() && n == -1; i++) { //loop stops when n is set
            Flight f = in.get(i);
            if (now.before(f.getDepartDate())) {
                n = in.indexOf(f);
            }
        }
        
        if(n==-1){
            n=in.size();
        }
        
        if (n > 5) {
            for (int i = 5; i > 0; i--) {
                out.add(in.get(n - i));
            }
        }

        for (int i = 0; /*i < next &&*/ (n+i) < in.size() ; i++) {      //get next x flights
            out.add(in.get(n + i));
        }
        
        return out;
    }

    private int FlightOverviewCount(List<Flight> fl, Date now) {

        int prev;
        Date dt; //flight date and time (DepartFrom)
        Calendar c = Calendar.getInstance();

        prev = 0;

        for (Flight f : fl) {
            c.setTimeInMillis(f.getDepartFrom().getTimecol().getTime());
            c.setTimeInMillis(c.getTimeInMillis() + f.getDepartDate().getTime());
            dt = new Date(c.getTimeInMillis());

            if (now.getTime() > dt.getTime()) {
                prev++;
            }
        }

        return prev;
    }
}
