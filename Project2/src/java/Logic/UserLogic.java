/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic;

import DAO.UserDAO;
import Logic.Helpers.LoginHelper;
import Logic.Helpers.ValidationHelper;
import Models.Datamodels.FilterCriteria;
import Models.Domain.Message;
import Models.Domain.User;
import Models.ViewModel.UserDetailVM;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kathleen
 */
public class UserLogic {

    private UserDAO userDAO = new UserDAO();
    private List errorList = new ArrayList<Message>();

    public List getErrorList() {
        return errorList;
    }

    public List<User> getAll() {
        return userDAO.GetAll();
    }
    
    public List<User> getAllWithRelations(){
        return userDAO.getAllWithRelations(null);
    }
    
    public User GetUserByUsername(String username)
    {
        return userDAO.getUserByUsername(username);
    }
    
    public List<User> getAllWithRelations(FilterCriteria filterCriteria){
        return userDAO.getAllWithRelations(filterCriteria);
    }

    public User getByID(int id) {
        return userDAO.GetByID(id);
    }

    public void setErrorList(List errorList) {
        this.errorList = errorList;
    }

    public int Update(UserDetailVM userDetailVM) {
        User user = new User();
        LoginHelper loginHelper = new LoginHelper();

        user.setID(userDetailVM.getId());
        user.setUsername(userDetailVM.getUsername());
        user.setFunctionID(userDetailVM.getFunctionId());        

        if (userDetailVM.getPassword() != "") {
            try {
                user.setSalt(loginHelper.generateSalt());
                user.setPassword(loginHelper.getEncryptedPassword(userDetailVM.getPassword(), user.getSalt()));

            } catch (Exception ex) {

            }
            
            return userDAO.Update(user);
        }
        else    
        {
            return userDAO.UpdateWithoutPassword(user);
        }
    }

    public int Create(UserDetailVM userDetailVM) {
        User user = new User();
        LoginHelper loginHelper = new LoginHelper();
        try {
            user.setUsername(userDetailVM.getUsername());
            user.setSalt(loginHelper.generateSalt());
            user.setPassword(loginHelper.getEncryptedPassword(userDetailVM.getPassword(), user.getSalt()));
            user.setFunctionID(userDetailVM.getFunctionId());

        } catch (Exception ex) {

        }

        return userDAO.Create(user);
    }

    public int Delete(int id) {
        return userDAO.Delete(id);
    }

    public boolean Validate(UserDetailVM userDetailVM) {
        UserDAO userDAO = new UserDAO();
        User originalUser;

        //Check owner name for create
        if (userDetailVM.getId() == 0 && userDAO.getUserByUsername(userDetailVM.getUsername()).getUsername() != "") {
            errorList.add(new Message("User already Exists", Message.MessageSeverity.ERROR));
        }

        //Check name for update
        if (userDetailVM.getId() > 0) {
            originalUser = userDAO.GetByID(userDetailVM.getId());

            if (originalUser != null) {
                if (!originalUser.getUsername().equals(userDetailVM.getUsername()) && !userDAO.getUserByUsername(userDetailVM.getUsername()).getUsername().equals("")) {
                    errorList.add(new Message("User already Exists", Message.MessageSeverity.ERROR));
                }
            } else {
                errorList.add(new Message("User not found", Message.MessageSeverity.ERROR));
            }
        }

        errorList = ValidationHelper.validateEmpty(userDetailVM.getUsername(), "UsernameField", errorList);
        errorList = ValidationHelper.validateMaxNumberOfCharacters(userDetailVM.getUsername(), "UsernameField", 255, errorList);
        errorList = ValidationHelper.validateMinNumberOfCharacters(userDetailVM.getUsername(), "UsernameField", 2, errorList);

        //Password only mandatory for create
        if (userDetailVM.getId() == 0)
        {
            errorList = ValidationHelper.validateEmpty(userDetailVM.getPassword(), "PasswordField", errorList);
        }
        
        return errorList.size() == 0;
    }

}
