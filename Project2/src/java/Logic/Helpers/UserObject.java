/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic.Helpers;

import Logic.UserLogic;
import Models.Domain.Function;
import Models.Domain.User;

/**
 *
 * @author Kathleen
 */
public class UserObject {
    private final User user;
    
    public String getUsername()
    {
        return this.user.getUsername();
    }
    
    public Function getFunction()
    {
        return this.user.getFunction();
    }
    
    public String getFunctionDescription()
    {
        return this.user.getFunction().getDescription();
    }
    
    public Boolean isAdmin()
    {
        Boolean isAdmin = false;
        
        if (this.user.getFunction().getDescription().equals("Admin")) {
            isAdmin = true;
        }
        
        return isAdmin;
    }
    
    public UserObject(String username)
    {
        UserLogic userLogic = new UserLogic();
        this.user = userLogic.GetUserByUsername(username);
    }
}
