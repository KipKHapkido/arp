/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic.Helpers;

import Models.Domain.Message;
import java.util.List;

/**
 *
 * @author Kathleen
 */
public class ValidationHelper {
    
    public static List validateEmpty(String input, String fieldName, List errorList)
    {
        if (input == "") 
        {
            errorList.add(new Message(fieldName + " must not be empty!", Message.MessageSeverity.ERROR));
        }   
        
        return errorList;
    }
    
    public static List validateMinNumberOfCharacters(String input, String fieldName, int amountCharacters, List errorList )
    {
        if (input.length() < amountCharacters) 
        {
            errorList.add(new Message(fieldName + " must have more than " + amountCharacters + " characters!", Message.MessageSeverity.ERROR));
        }    
        
        return errorList;
    }
    
    public static List validateMaxNumberOfCharacters(String input, String fieldName, int amountCharacters, List errorList)
    {
        if (input.length() > amountCharacters) 
        {
            errorList.add(new Message(fieldName + " can't have more than " + amountCharacters + " characters!", Message.MessageSeverity.ERROR));
        }        
        
        return errorList;
    }
}
