/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic;

import DAO.AirportDAO;
import DAO.RunwayDAO;
import Models.Domain.Airport;
import Models.Domain.Message;
import Models.Domain.Runway;
import Models.ViewModel.RunwayVM;
import java.util.List;

/**
 *
 * @author Jelle
 */
public class ManageRunwayLogic {

    private AirportDAO airDao = new AirportDAO(new Airport());
    private RunwayDAO runDao = new RunwayDAO(new Runway());
    private RunwayVM vm = null;

    public RunwayVM OverviewAction(RunwayVM tempVM, String submit) {
        vm = tempVM;
        if (submit.equals("Cancel")) {
            vm.setFeedback(RunwayVM.LogicAction.CANCEL);
            vm.setRunway(null);
        } else if (submit.equals("Create")) {
            vm.setFeedback(RunwayVM.LogicAction.CREATE);
            vm.setRunway(new Runway());
        } else if (submit.substring(0, 3).equals("upd")) {
            vm.setRunway(runDao.GetByID(Integer.parseInt(submit.substring(3))));
            vm.setFeedback(RunwayVM.LogicAction.UPDATE);
        } else if (submit.substring(0, 3).equals("del")) {
            //joption pane is aan serverkant
            int i = runDao.Delete(Integer.parseInt(submit.substring(3)));
            vm.setRunway(null);
            if (i > 0) {
                vm.addMessage(vm.getVarMap().get("DelSuccess"), Message.MessageSeverity.SUCCESS);
            } else {
                vm.addMessage(vm.getVarMap().get("CreateFail"), Message.MessageSeverity.ERROR);
            }
            vm.setFeedback(RunwayVM.LogicAction.DELETE);
        }
        return vm;
    }

    public RunwayVM Save(RunwayVM tempVM, String submit, String ID, String Name,
            String Length, String AirportID) {
        vm = tempVM;
        if (submit.equals("-1")) {
            vm.setFeedback(RunwayVM.LogicAction.CANCELSAVE);
            return vm;
        }
        Runway run = new Runway();
        run.setID(Integer.parseInt(ID));
        run.setName(Name);
        if (AirportID != null) {
            run.setAirportID(Integer.parseInt(AirportID));
        }
        //extra check nodig of 
        if (Length != null && this.tryparse(Length)) {
            run.setLength(Integer.parseInt(Length));
        } else {
            vm.setFeedback(RunwayVM.LogicAction.WRONGSAVE);
            vm.addMessage(vm.getVarMap().get("CreateDataIncorrect"), Message.MessageSeverity.ERROR);
            vm.setRunway(run);
            return vm;
        }
        if (run.getAirportID() < 1 || run.getName().isEmpty()
                || run.getLength() < 1) {
            vm.setFeedback(RunwayVM.LogicAction.ERRORSAVE);
            vm.addMessage(vm.getVarMap().get("CreateFillIn"), Message.MessageSeverity.ERROR);
            vm.setRunway(run);
        } else {
            int r = runDao.Save(run);
            if (r > 0) {
                vm.setFeedback(RunwayVM.LogicAction.SAVED);
                vm.addMessage(vm.getVarMap().get("CreateSucces"), Message.MessageSeverity.SUCCESS);
            } else {
                vm.setFeedback(RunwayVM.LogicAction.ERROR);
                vm.addMessage(vm.getVarMap().get("CreateFail"), Message.MessageSeverity.ERROR);
            }
        }
        return vm;
    }

    private Boolean tryparse(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (Exception exc) {
            return false;
        }
    }

    public List<Runway> getAll() {
        return runDao.getAllEager();
    }

    public List<Airport> getAllAirports() {
        return airDao.GetAll();
    }
}
