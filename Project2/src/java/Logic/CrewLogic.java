/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic;

import DAO.CrewDAO;
import DAO.FlightDAO;
import Models.Domain.Crew;
import Models.Domain.Flight;
import Models.Domain.Message;
import Models.ViewModel.CrewVM;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class CrewLogic {
    
    private final CrewDAO crewDAO = new CrewDAO(new Crew());
    private final FlightDAO flightDAO = new FlightDAO(new Flight());
    private CrewVM cVM = null;

    public CrewVM getcVM() {
        return cVM;
    }

    public void setcVM(CrewVM cVM) {
        this.cVM = cVM;
    }

    public CrewLogic(int language) {
        cVM = new CrewVM(language);
    }
    
    public void CrewData(String FlightStringID, String Submit, String EmployeeStringID) throws SQLException {
        int FlightID = Integer.parseInt(FlightStringID);
        
        Flight fl = flightDAO.GetFlightByID(FlightID); 
        cVM.setFlight(fl);
                        
        if (Submit != null) {      
            int EmployeeID = Integer.parseInt(EmployeeStringID);
            if (Submit.equals("Add")) {
                if (EmployeeID == 0) {
                    cVM.addMessage(cVM.getVarMap().get("noSelection"), Message.MessageSeverity.ERROR);
                } else {
                    int affectedRows = crewDAO.AddCrewToFlight(EmployeeID, FlightID);
                    if (affectedRows == 0) {
                        cVM.addMessage(cVM.getVarMap().get("addFailed"), Message.MessageSeverity.ERROR);
                    } else {
                        cVM.addMessage(cVM.getVarMap().get("addSuccess"), Message.MessageSeverity.SUCCESS);
                    }
                }
            } else {
                int affectedRows = crewDAO.RemoveCrewmemberFromFlight(EmployeeID, FlightID);
                if (affectedRows == 0) {
                        cVM.addMessage(cVM.getVarMap().get("removeFailed"), Message.MessageSeverity.ERROR);
                    } else {
                        cVM.addMessage(cVM.getVarMap().get("removeSuccess"), Message.MessageSeverity.SUCCESS);
                    }
            }
        }
        
        List<Crew> CrewList = crewDAO.GetCrewMembersByFlight(FlightID);
        if (CrewList.isEmpty()) {
            cVM.addMessage(cVM.getVarMap().get("noCrew"), Message.MessageSeverity.ERROR);
        } else if (CrewList.size() == 1) {
            Crew c = CrewList.get(0);
                //Query geeft soms één rij terug, gevuld met NULL-waarden
                if (c.getID() == 0) {
                    cVM.addMessage(cVM.getVarMap().get("noCrew"), Message.MessageSeverity.ERROR);
                } else {
                   cVM.setCrewList(CrewList); 
                }
            
        } else {
            cVM.setCrewList(CrewList);
        }    
        
        List<Crew> availableCrew = crewDAO.GetAvailableCrew(FlightID);
        if (availableCrew.isEmpty()) {
            cVM.addMessage(cVM.getVarMap().get("noAvailableCrew"), Message.MessageSeverity.ERROR);
        } else {
            List<Crew> listPilots = availableCrew.stream().filter(u -> u.getFunction() == 3).collect(Collectors.toList());
            List<Crew> listStewards = availableCrew.stream().filter(u -> u.getFunction() == 4).collect(Collectors.toList());
            
            cVM.setListPilots(listPilots);
            cVM.setListStewards(listStewards);
            //cVM.setAvailableList(availableCrew);
        }
        
    }
}
