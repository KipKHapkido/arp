/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic;

import DAO.AirplaneDAO;
import Models.Domain.Airplane;
import java.util.List;

/**
 *
 * @author Jelle
 */
public class ManageAirplaneLogic {

    private final AirplaneDAO airDao = new AirplaneDAO();

    public Airplane Save(String submit) {
        if (submit.equals("Cancel")) {
            return null;
        } else {
            Airplane air = new Airplane();
            if (!submit.equals("Create")) {

                int id = Integer.parseInt(submit);
                air = airDao.GetByID(id);                
            }
            return air;
        }
    }
    
    public void Save(Airplane airp){
      airDao.Save(airp);
    }
    
    public List<Airplane> getAll(){
        return airDao.GetAll();
    }

}
