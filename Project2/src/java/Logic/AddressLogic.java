/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic;

import DAO.AddressDAO;
import Logic.Helpers.ValidationHelper;
import Models.Domain.Address;
import Models.Domain.Message;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kathleen
 */
public class AddressLogic {

    private AddressDAO addressDAO = new AddressDAO();
    private List errorList = new ArrayList<Message>();

    public List getErrorList() {
        return errorList;
    }

    public void setErrorList(List errorList) {
        this.errorList = errorList;
    }

    public Address getByID(int id) {
        return addressDAO.GetByID(id);
    }

    public int Update(Address address) {
        return addressDAO.Update(address);
    }

    public void Delete(int id) {
        addressDAO.Delete(id);
    }

    public int Create(Address address) {
        return addressDAO.Create(address);
    }
    
    public List<Address> getAll(){
        return addressDAO.GetAll();
    }

    public boolean Validate(Address address) {
        AddressDAO addressDAO = new AddressDAO();

        errorList = ValidationHelper.validateEmpty(address.getCountry(), "CountryField", errorList);        
        errorList = ValidationHelper.validateMaxNumberOfCharacters(address.getCountry(), "CountryField", 255, errorList);       
        errorList = ValidationHelper.validateMinNumberOfCharacters(address.getCountry(), "CountryField", 2, errorList);
        
        errorList = ValidationHelper.validateEmpty(address.getNumber(), "NumberField", errorList);        
        errorList = ValidationHelper.validateMaxNumberOfCharacters(address.getNumber(), "NumberField", 5, errorList);       

        errorList = ValidationHelper.validateEmpty(address.getStreet(), "StreetField", errorList);        
        errorList = ValidationHelper.validateMaxNumberOfCharacters(address.getStreet(), "StreetField", 255, errorList);       
        errorList = ValidationHelper.validateMinNumberOfCharacters(address.getStreet(), "StreetField", 2, errorList);
        
        errorList = ValidationHelper.validateEmpty(address.getTown(), "TownField", errorList);        
        errorList = ValidationHelper.validateMaxNumberOfCharacters(address.getTown(), "TownField", 255, errorList);       
        errorList = ValidationHelper.validateMinNumberOfCharacters(address.getTown(), "TownField", 2, errorList);
        
        errorList = ValidationHelper.validateEmpty(address.getZip_code(), "ZipCodeField", errorList);        
        errorList = ValidationHelper.validateMaxNumberOfCharacters(address.getZip_code(), "ZipCodeField", 255, errorList);       
        errorList = ValidationHelper.validateMinNumberOfCharacters(address.getZip_code(), "ZipCodeField", 2, errorList);

        return errorList.size() == 0;
    }
    
    
}
