/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic.Flight;

import DAO.FlightDAO;
import Models.Domain.Airport;
import Models.Domain.Flight;
import Models.Domain.Message;
import Models.Domain.Runway;
import Models.ViewModel.Flight.CompleteIncomingFlightVM;
import Models.ViewModel.Flight.IncomingFlightsVM;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Sven Vervloet
 */
public class IncomingFlightsLogic {

    private IncomingFlightsVM iVM = null;
    private CompleteIncomingFlightVM cVM = null;
    private final FlightDAO flightDao = new FlightDAO(new Flight());

    //VM's worden geïnitialiseerd bij aanmaken van de Logic
    //Opvulling van de VM vind plaats bij elk van de methodes. Objecten worden doorgegeven aan respectievelijke VM
    public IncomingFlightsVM getiVM() {
        return iVM;
    }

    public void setiVM(IncomingFlightsVM iVM) {
        this.iVM = iVM;
    }

    public CompleteIncomingFlightVM getcVM() {
        return cVM;
    }

    public void setcVM(CompleteIncomingFlightVM cVM) {
        this.cVM = cVM;
    }

    public IncomingFlightsLogic(int language) {
        super();

        iVM = new IncomingFlightsVM(language);
        cVM = new CompleteIncomingFlightVM(language);
    }

    public void getIncomingFlights() throws SQLException {
        flightDao.UpdateInboundFlights(); //Alle vluchten die al vertrokken zijn, van status veranderen
        flightDao.UpdateLandedFlights(); //Alle vluchten die al geland zijn
        List<Flight> notApprovedList = flightDao.GetIncomingFlights(); //Lijst opvragen van alle vluchten die willen landen. 
        if (notApprovedList.isEmpty()) {
            iVM.addMessage(iVM.getVarMap().get("noFlights"), Message.MessageSeverity.ERROR);
        }
        iVM.setResultList(notApprovedList);
    }

    public void getFlightByID(int FlightID) throws SQLException {
        Flight inCompleteFlight = flightDao.GetFlightByID(FlightID);

        if (inCompleteFlight == null) {
            cVM.addMessage(cVM.getVarMap().get("noFlight"), Message.MessageSeverity.ERROR);
        } else {
            inCompleteFlight = flightDao.GetLandingTime(inCompleteFlight);
            Flight tempFlight = flightDao.GetFlightByID(FlightID);//TempFlight om te gebruiken in de query
            tempFlight.setDepartureTimeID(inCompleteFlight.getArrivalTimeID());//Ipv ArrivalTime te gebruiken, gebruiken we DepartureTime. Maakt de query om te zoeken makkelijker
            getAvailableRunways(tempFlight);
        }
        cVM.setFlight(inCompleteFlight);
    }

    public void getAvailableRunways(Flight fl) throws SQLException {
        List<Runway> runwayList = flightDao.GetAvailableRunways(fl);
        if (runwayList.isEmpty()) {
            cVM.addMessage(cVM.getVarMap().get("noRunways"), Message.MessageSeverity.ERROR);
            getAllDestinations();
        } else {
            cVM.setRunwayList(runwayList);
        }
    }

    public void getAllDestinations() throws SQLException {
        List<Airport> destinationList = flightDao.GetAllDestinations();
        if (destinationList.isEmpty()) {
            cVM.addMessage(cVM.getVarMap().get("noDestinations"), Message.MessageSeverity.ERROR);
        }
        cVM.setDestinationList(destinationList); 
    }
    
    public int save(int ID, String DestinationString, String RunwayString, String ArrivalDateString, String ArrivalTimeString) throws SQLException, ParseException {
        Flight f = flightDao.GetFlightByID(ID);
        Boolean updateValues = false;
        int returnValue = 0; //StartValue == Zelfde waarde dan update Failed

        if (DestinationString != null) { //Is de waarde doorgegeven vanuit Jsp? Indien niet, bestond ze niet
            int DestinationID = Integer.parseInt(DestinationString); //Omzetting naar int
            if (DestinationID == 0) { //Als waarde == 0, dan werd er geen waarde gekozen. Lijst moet terug opgehaald om door te geven aan Jsp (via vm)
                cVM.addMessage(cVM.getVarMap().get("chooseDestination"), Message.MessageSeverity.ERROR);     
            } else { // Anders mogen we proberen updaten
                f.setDestinationID(DestinationID);
                updateValues = true;
                returnValue = flightDao.UpdateIncomingFlightDestination(f);
            }
        }

        if (RunwayString != null) { //Is de waarde doorgegeven vanuit Jsp? Indien niet, bestond ze niet
            int RunwayID = Integer.parseInt(RunwayString);//Omzetting naar int
            if (RunwayID == 0) {//Als waarde == 0, dan werd er geen waarde gekozen. Lijst moet terug opgehaald om door te geven aan Jsp (via vm)
                cVM.addMessage(cVM.getVarMap().get("chooseRunway"), Message.MessageSeverity.ERROR);
                getAvailableRunways(f);
            } else { // Anders mogen we proberen updaten
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                //surround below line with try catch block as below code throws checked exception
                Date ArrivalDate = sdf.parse(ArrivalDateString);
                f.setArrivalDate(ArrivalDate);
                f.setArrivalTimeID(Integer.parseInt(ArrivalTimeString));
                f.setRunwayID(RunwayID);
                updateValues = true;
                returnValue = flightDao.UpdateIncomingFlightRunway(f);//Doet een Update
            }
        }

        if (updateValues) {
            if (returnValue < 1) { //Update UnSuccesFull
                cVM.addMessage(cVM.getVarMap().get("updateFlightFailed"), Message.MessageSeverity.ERROR);
            } else {
               iVM.addMessage(cVM.getVarMap().get("updateFlightSucces"), Message.MessageSeverity.SUCCESS);
               getIncomingFlights(); //Ophalen en doorgeven aan VM 

            }
        }
        getFlightByID(ID); //Terug ophalen en doorgeven aan VM
        return returnValue;
    }
    
    public Date TransformDate(String d) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed = format.parse(d);
        return parsed;
    }
}
