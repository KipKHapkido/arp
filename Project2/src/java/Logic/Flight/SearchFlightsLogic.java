/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic.Flight;

import DAO.FlightDAO;
import Models.Domain.Airport;
import Models.Domain.Flight;
import Models.Domain.Message;
import Models.ViewModel.Flight.SearchFlightsVM;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Sven Vervloet
 */
public class SearchFlightsLogic {

    private SearchFlightsVM sVM = null;
    
    public SearchFlightsVM getsVM() {
        return sVM;
    }
  
    public SearchFlightsLogic(int language) {
        super();
        sVM = new SearchFlightsVM(language);
        sVM.setComparisonDate(comparisonDate());
    }

    private final FlightDAO flightDao = new FlightDAO(new Flight());

    //NewFlights
    
    
    public void getAllDestinations() throws SQLException {
        List<Airport> destinationList = flightDao.GetAllDestinations();
        if (destinationList.isEmpty()) {
            sVM.addMessage(sVM.getVarMap().get("noDestinations"), Message.MessageSeverity.ERROR);
        }
        sVM.setDestinationList(destinationList); 
    }
   
    public void getSearchFlights(String DestinationString, String FlightNR) {
        int DestinationID = 0;
        if (DestinationString != null) {
            DestinationID = Integer.parseInt(DestinationString);
        }
                
        if (FlightNR == null) {
            FlightNR = "";
        }
        
        List<Flight> resultList = flightDao.GetSearchFlights(DestinationID, FlightNR);
        if (DestinationID > 0) {
            sVM.setDestinationID(DestinationID); //Waarden voor het zoekformulier -> Terug doorgeven, zodat zoekwaarden en resultaat teruggaan naar Jsp
        }
        if (!FlightNR.equals("")) {
            sVM.setFlightNR(FlightNR);//Waarden voor het zoekformulier -> Terug doorgeven, zodat zoekwaarden en resultaat teruggaan naar Jsp
        }
        
        if (resultList.isEmpty()) {
            sVM.addMessage(sVM.getVarMap().get("noFlights"), Message.MessageSeverity.ERROR);
        }
        
        sVM.setResultList(resultList);
    }
    
    public void CancelFlight(int ID) throws SQLException {
        int result = flightDao.ChangeFlightStatus(ID, 9); //Canceled
        if (result < 1) {
            sVM.addMessage(sVM.getVarMap().get("CancelFailed"), Message.MessageSeverity.ERROR);
        } else {
            sVM.addMessage(sVM.getVarMap().get("CancelSucces"), Message.MessageSeverity.SUCCESS);
        }
    }
    
    public final Date comparisonDate() {
        int noOfDays = 2; //i.e two days           
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_YEAR, noOfDays);
        return calendar.getTime();
    }

    
}
