/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic.Flight;

import DAO.AirplaneDAO;
import DAO.AirportDAO;
import DAO.FlightDAO;
import Models.Domain.Airport;
import Models.Domain.Flight;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Jeff
 */
public class FlightReportLogic {
    
    Flight f = new Flight();
    FlightDAO fdao = new FlightDAO(f);
    AirplaneDAO adao = new AirplaneDAO();
    Airport a = new Airport();
    AirportDAO airdao = new AirportDAO(a);
    
    public List<Flight> GetAllFlights() throws SQLException
    {
        List<Flight> l;
        l = fdao.GetFlightReport();
//        for(Flight f : l){
//        f.setAirplane(adao.GetByID(f.getAirplaneID()));
//        f.setDepartAirport(airdao.GetByID(f.getDepartureID()));
//        f.setDestinationAirport(airdao.GetByID(f.getDestinationID()));
//        }
        return l;
    }
    
    public Flight GetFlightByID(int ID) throws SQLException{
        f=fdao.GetFlightByID(ID);
        return f;
    }
}


/* Coderead comments
    32-36   oude code 
*/