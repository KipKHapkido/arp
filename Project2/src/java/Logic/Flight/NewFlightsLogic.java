/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic.Flight;

import DAO.FlightDAO;
import Models.Domain.Airplane;
import Models.Domain.Airport;
import Models.Domain.Flight;
import Models.Domain.Message;
import Models.Domain.Runway;
import Models.Domain.TimeClass;
import Models.ViewModel.Flight.CompleteFlightVM;
import Models.ViewModel.Flight.CreateFlightVM;
import Models.ViewModel.Flight.NewFlightsVM;
import Models.ViewModel.Flight.SearchFlightsVM;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Sven Vervloet
 */
public class NewFlightsLogic {

    private CompleteFlightVM cVM = null;
    //VM's worden geïnitialiseerd bij aanmaken van de Logic
    //Opvulling van de VM vind plaats bij elk van de methodes. Objecten worden doorgegeven aan VM
    private NewFlightsVM nVM = null;
    //VM's worden geïnitialiseerd bij aanmaken van de Logic
    //Opvulling van de VM vind plaats bij elk van de methodes. Objecten worden doorgegeven aan respectievelijke VM
    private CreateFlightVM createVM = null;
    private SearchFlightsVM searchVM = null;
    public NewFlightsVM getnVM() {
        return nVM;
    }

    public void setnVM(NewFlightsVM nVM) {
        this.nVM = nVM;
    }

    public CompleteFlightVM getcVM() {
        return cVM;
    }

    public NewFlightsLogic(int language) {
        super();
        cVM = new CompleteFlightVM(language);
        nVM = new NewFlightsVM(language);
        nVM.setComparisonDate(comparisonDate());//Huidige datum + 2 dagen
        createVM = new CreateFlightVM(language);
        searchVM = new SearchFlightsVM(language);
    }

    private final FlightDAO flightDao = new FlightDAO(new Flight());

    //NewFlights
    public void getNotApprovedFlights() throws SQLException {
        List<Flight> notApprovedList = flightDao.GetNotApprovedFlights();
        if (notApprovedList.isEmpty()) {
            nVM.addMessage(nVM.getVarMap().get("noFlights"), Message.MessageSeverity.ERROR);
        }
        nVM.setResultList(notApprovedList);
    }

    public void ProcesFlight(int ID, String submit) throws SQLException {
        if (submit.equals("Reject")) {
            int result = flightDao.ChangeFlightStatus(ID, 10); //Rejected
            if (result < 1) {
                nVM.addMessage(nVM.getVarMap().get("rejectFailed"), Message.MessageSeverity.ERROR);
            } else {
                nVM.addMessage(nVM.getVarMap().get("rejectSucces"), Message.MessageSeverity.SUCCESS);
            }
        } else {
            ApproveFlight(ID);
        }
    }

    public void ApproveFlight(int ID) throws SQLException {
        Flight f = flightDao.GetByID(ID);
        f.setApproved(1);
        f.setFlightStateID(1);
        Flight f2 = flightDao.ApproveFlight(f);
        Airport d = flightDao.GetDestinationByID(f.getDestinationID());
        Airplane p = flightDao.GetAirplaneByID(f.getAirplaneID());

        if (f2.getApproved() != 0) 
        {
            StringBuilder strMessage = new StringBuilder();
            strMessage.append((nVM.getVarMap().get("approveSucces")));
            strMessage.append(". ");
            strMessage.append((nVM.getVarMap().get("InfoSend")));
            strMessage.append(" ");
            strMessage.append(d.getEmailAdress());
            strMessage.append(": ");
            strMessage.append((nVM.getVarMap().get("FlightNR")));
            strMessage.append(": ");
            strMessage.append(f.getFlightNR());
            strMessage.append(" - ");
            strMessage.append((nVM.getVarMap().get("Airplane")));
            strMessage.append(": ");
            strMessage.append(p.getID());
            strMessage.append(" - ");
            strMessage.append(p.getCode());
            strMessage.append(" - ");
            strMessage.append(p.getType());

        
            nVM.addMessage((strMessage.toString()), Message.MessageSeverity.SUCCESS);
                        
        } else {
            nVM.addMessage(nVM.getVarMap().get("approveFailed"), Message.MessageSeverity.ERROR);
        }
    }

    //CompleteFlights
    public void getFlightByID(int FlightID) throws SQLException {
        Flight inCompleteFlight = null;
        inCompleteFlight = flightDao.GetFlightByID(FlightID);

        if (inCompleteFlight == null) {
            cVM.addMessage(cVM.getVarMap().get("noFlight"), Message.MessageSeverity.ERROR);
        } else {
            if (inCompleteFlight.getDepartureTimeID() == 0) {
                getAvailableTimes(inCompleteFlight);
            }
            if ((inCompleteFlight.getDepartureTimeID() != 0) && (inCompleteFlight.getRunwayID() == 0)) {
                getAvailableRunways(inCompleteFlight);
            }
        }
        cVM.setFlight(inCompleteFlight);
    }

    public void getAvailableTimes(Flight fl) {
        List<TimeClass> timeList = flightDao.GetAvailableTimes(fl);
        if (timeList.isEmpty()) {
            cVM.addMessage(cVM.getVarMap().get("noTimes"), Message.MessageSeverity.ERROR);
        }
        cVM.setTimeList(timeList);
    }

    public void getAvailableRunways(Flight fl) {
        List<Runway> runwayList = flightDao.GetAvailableRunways(fl);
        if (runwayList.isEmpty()) {
            cVM.addMessage(cVM.getVarMap().get("noRunways"), Message.MessageSeverity.ERROR);
        }
        cVM.setRunwayList(runwayList);
    }

    public int save(int ID, String DepartureTimeString, String RunwayString) throws SQLException {
        Flight f = flightDao.GetFlightByID(ID);
        Boolean updateValues = false;
        int returnValue = 0; //StartValue == Zelfde waarde dan update Failed

        if (DepartureTimeString != null) { //Is de waarde doorgegeven vanuit Jsp? Indien niet, bestond ze niet
            int DepartureTimeID = Integer.parseInt(DepartureTimeString); //Omzetting naar int
            if (DepartureTimeID == 0) { //Als waarde == 0, dan werd er geen waarde gekozen. Lijst moet terug opgehaald om door te geven aan Jsp (via vm)
                cVM.addMessage(cVM.getVarMap().get("chooseTimes"), Message.MessageSeverity.ERROR);
                getAvailableTimes(f);
            } else { // Anders mogen we proberen updaten
                f.setDepartureTimeID(DepartureTimeID);
                getAvailableRunways(f);
                updateValues = true;
                returnValue = flightDao.UpdateFlightTime(f);//Doet een Update van de Time (+Date)
            }
        }

        if (RunwayString != null) { //Is de waarde doorgegeven vanuit Jsp? Indien niet, bestond ze niet
            int RunwayID = Integer.parseInt(RunwayString);//Omzetting naar int
            if (RunwayID == 0) {//Als waarde == 0, dan werd er geen waarde gekozen. Lijst moet terug opgehaald om door te geven aan Jsp (via vm)
                cVM.addMessage(cVM.getVarMap().get("chooseRunway"), Message.MessageSeverity.ERROR);
                getAvailableRunways(f);
            } else { // Anders mogen we proberen updaten
                f.setRunwayID(RunwayID);
                updateValues = true;
                returnValue = flightDao.UpdateFlightRunway(f);//Doet een Update
            }
        }

        if (updateValues) {
            if (returnValue == 1) { //Update SuccesFull
                if ((f.getDepartureTimeID() == 0) || (f.getRunwayID() == 0)) { //Boodschap op VM van CompleteFlights (=vlucht onvolledig)
                    cVM.addMessage(cVM.getVarMap().get("updateFlightSucces"), Message.MessageSeverity.SUCCESS);
                    getFlightByID(ID); //Ophalen en doorgeven aan VM
                } else { // Boodschap op VM van NewFlights (=overzichtIk )
                    nVM.addMessage(cVM.getVarMap().get("updateFlightSucces"), Message.MessageSeverity.SUCCESS);
                    getNotApprovedFlights(); //Ophalen en doorgeven aan VM
                    returnValue = 2; //Update Succesfull & Object Flight complete
                }

            } else {
                cVM.addMessage(cVM.getVarMap().get("updateFlightFailed"), Message.MessageSeverity.ERROR);

            }
        }
        getFlightByID(ID); //Terug ophalen en doorgeven aan VM
        return returnValue;
    }

public boolean CreateFlightLogic(String flightNR, String stringdate, int airplaneID, int departFrom, int DepartUntill, int departureID, int destinationID) throws ParseException {
        boolean IsValid=true;
        
        if (flightNR.isEmpty() || stringdate.isEmpty()) {
            if (flightNR.isEmpty()){
            errorList.add(new Message(createVM.getVarMap().get("FillNumber"), Message.MessageSeverity.ERROR));
            IsValid = false;}
            if(stringdate.isEmpty()) {errorList.add(new Message(createVM.getVarMap().get("FillDate"), Message.MessageSeverity.ERROR));
        IsValid = false;}
        }
        
        else {
                Date departdate = TransformDate(stringdate);
                Flight flight1;
                
                flight1 = new Flight();
                flight1.setFlightNR(flightNR);
                flight1.setDepartDate(departdate);
                flight1.setDepartFromID(departFrom);
                flight1.setDepartUntilID(DepartUntill);
                flight1.setDepartureID(departureID);
                flight1.setDestinationID(destinationID);
                flight1.setAirplaneID(airplaneID);
                
                flightDao.CreateFlight(flight1);
                errorList.add(new Message(createVM.getVarMap().get("ThankYou"), Message.MessageSeverity.SUCCESS));
        }
        return IsValid;
    }

    public List<TimeClass> getAllTimes() {
        List<TimeClass> Times = flightDao.GetAllTimes();
        return Times;
    }

    public Date TransformDate(String d) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        Date parsed = format.parse(d);
        return parsed;
    }

    public final Date comparisonDate() {
        int noOfDays = 2; //i.e two days           
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_YEAR, noOfDays);
        return calendar.getTime();
    }

    public CreateFlightVM getcreateVM() {
        return createVM;
    }

    public Flight getById(int id) {
        return flightDao.GetByID(id);

    }

    private List<Message> errorList = new ArrayList<>();

    public List<Message> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<Message> errorList) {
        this.errorList = errorList;
    }

    public boolean JustUpdateFlightLogic(Flight flight1, String flightNR, Date departdate, int airplaneID, int departFrom, int DepartUntill, int departureID, int destinationID) throws SQLException {
        boolean isValid = true;

        if (flightNR.isEmpty()) {
            errorList.add(new Message(createVM.getVarMap().get("FillAll"), Message.MessageSeverity.ERROR));
            isValid = false;

        } else {
            flight1.setFlightNR(flightNR);
            flight1.setDepartDate(departdate);
            flight1.setDepartFromID(departFrom);
            flight1.setDepartUntilID(DepartUntill);
            flight1.setDepartureID(departureID);
            flight1.setDestinationID(destinationID);
            flight1.setAirplaneID(airplaneID);

            flightDao.JustUpdateFlight(flight1);
            searchVM.addMessage(searchVM.getVarMap().get("ThankYou"), Message.MessageSeverity.SUCCESS);
        }

        return isValid;

    }
    
        public SearchFlightsVM getsearchVM() {
        return searchVM;
    }

}
