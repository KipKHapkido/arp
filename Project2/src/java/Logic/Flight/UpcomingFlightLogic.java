/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic.Flight;

import DAO.FlightDAO;
import Models.Datamodels.Status;
import Models.Domain.Flight;
import Models.Domain.TimeClass;
import Models.ViewModel.Flight.UpcomingFlightsVM;
import java.sql.SQLException;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author JurgenVanEynde
 */
public class UpcomingFlightLogic {

    private final FlightDAO _flDAO = new FlightDAO(new Flight());

    public List<Flight> GetUpcomingFlights() throws SQLException {
        
        Calendar now = Calendar.getInstance();
        int hour =  now.get(Calendar.HOUR_OF_DAY);
        int minutes = now.get(Calendar.MINUTE);
        int timeID = GetTimeId(hour, minutes);

        List<Flight> list = _flDAO.GetUpcomingFlights(new Date(now.getTimeInMillis()), timeID);

        return list;
    }

    private int GetTimeId(int hour, int minutes) {
        List<TimeClass> l = _flDAO.GetAllTimes();
        int id = 0;
        for (int i = 1; i <= l.size(); i = i + 2) {
            Calendar tmpBrol = Calendar.getInstance();
            tmpBrol.setTime(l.get(i).getTimecol());
            if (tmpBrol.get(Calendar.HOUR_OF_DAY) == hour) {
                if (minutes < 30) {
                    id = i;
                    break;
                } else {
                    id = i + 1;
                    break;
                }
            }
        }
        return id;
    }

    public List<Status> getAllStatus() throws SQLException {
        List<Status> l = _flDAO.GetAllUpcomingFlightStatus();        
        return l;
    }

    public UpcomingFlightsVM GetViewModel(int language) throws SQLException {
        UpcomingFlightsVM vm = new UpcomingFlightsVM(language);
        vm.setFlights(GetUpcomingFlights());
        vm.setStatus(getAllStatus());
        return vm;
    }
    
    public void SetStatus(int flightId, int statusId) throws SQLException
    {
        Flight f = _flDAO.GetFlightByID(flightId);
        f.setFlightStateID(statusId);
       _flDAO.UpdateFlightStatus(f);
    }
}
