/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic.Flight;

import DAO.AirplaneDAO;
import DAO.AirportDAO;
import DAO.FlightDAO;
import DAO.PassengerListDAO;
import Logic.Passenger.PassengerRead;
import Models.Domain.Airport;
import Models.Domain.Flight;
import Models.Domain.Passenger;
import Models.Domain.PassengerList;
import Models.ViewModel.BookFlightVM;
import Servlets.Flight.FlightsOnDateServlet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dries
 */
public class FlightLogic {

    FlightDAO flightDao = new FlightDAO(new Flight());
    AirplaneDAO airplaneDao = new AirplaneDAO();
    AirportDAO airportDao = new AirportDAO(new Airport());
    PassengerRead passLogic = new PassengerRead();
    PassengerListDAO passListDao;

    public List<Passenger> getPassengers(int flightID) {
        passListDao = new PassengerListDAO(new PassengerList(new Flight()));

        return passListDao.getPassengerListByID(flightID);
    }

    public List<Flight> getEligibleFlights(Date date, int origin, int destination, int seats) {
        List<Flight> temp = flightDao.getEligibleFlights(date, origin, destination);
        List<Flight> flights = new ArrayList<>();
        passListDao = new PassengerListDAO(new PassengerList(new Flight()));

        for (Flight f : temp) {
            try {
                if ((Integer.parseInt(f.getAirplane().getCapacity()) - passListDao.getPassengerCountByID(f.getID())) > seats) {
                    flights.add(f);
                }
            } catch (Exception exc) {
                
            }
        }

        return flights;
    }

    public boolean addPassengerToFlight(int flightID, Passenger p) {

        passListDao = new PassengerListDAO(new PassengerList(new Flight()));

        return passListDao.addPassenger(flightID, p);

    }

    public Flight getByID(int flightID) {

        return flightDao.GetByID(flightID);

    }

    public BookFlightVM prepareBookFlightVM(BookFlightVM vm, String strDate, String passID, String originID, String destinationID, String seats) {

        Date date = new Date();

        //binnenkomende data gaat via gecontroleerde invoer => pareseable
        SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yyyy");
        //date string omzetten naar een parseable string

        //strDate = strDate.replace('T', ' ');

        try {
            date = dt.parse(strDate);
        } catch (ParseException ex) {
            Logger.getLogger(FlightsOnDateServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        vm.setDate(date);
        vm.setPassenger(passLogic.SearchPassById(Integer.parseInt(passID)));
        vm.setOrigin(Integer.parseInt(originID));
        vm.setDestination(Integer.parseInt(destinationID));
        vm.setAirports(airportDao.GetAll());
        vm.setSeats(Integer.parseInt(seats));

        //null als er geen vluchten zijn, wordt opgevangen in jsp
        List<Flight> flights = getEligibleFlights(vm.getDate(), vm.getOrigin(), vm.getDestination(), vm.getSeats());
        vm.setFlights(flights);

        return vm;
    }

    public BookFlightVM addPassenger(BookFlightVM vm, String fID, String pID) {

        int flightID = Integer.parseInt(fID);
        Passenger p = passLogic.SearchPassById(Integer.parseInt(pID));
        boolean status = addPassengerToFlight(flightID, p);

        List<Flight> flights;

        //was addPassenger successvol?
        vm.setAddPassSuccess(status);

        if (status) {
            vm.setPassenger(p);

            flights = new ArrayList<>();

            flights.add(getByID(flightID));
            vm.setFlights(flights);
        }

        return vm;
    }
    
    public BookFlightVM prepareBoardingPass(BookFlightVM vm, String flightID) {
        
        List<Flight> fl = new ArrayList<>();
        
        try {
            fl.add(flightDao.GetFlightByID(Integer.parseInt(flightID)));
        } catch (NumberFormatException | SQLException ex) {
            Logger.getLogger(FlightLogic.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        vm.setFlights(fl);
        
        return vm;
    }

}
