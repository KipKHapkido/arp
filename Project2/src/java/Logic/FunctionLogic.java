/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic;

import DAO.FunctionDAO;
import Models.Domain.Function;
import java.util.List;

/**
 *
 * @author Kathleen
 */
public class FunctionLogic {
    
    FunctionDAO functionDAO =  new FunctionDAO();
    
     public List<Function> getAll() {
        return functionDAO.GetAll();
    }
     
    public Function getById(int id)
    {
        return functionDAO.GetByID(id);
    }
    
}
