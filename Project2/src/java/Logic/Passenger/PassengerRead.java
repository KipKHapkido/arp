/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic.Passenger;

import DAO.AddressDAO;
import DAO.PassengerDAO;
import Models.Domain.Flight;
import Models.Domain.Passenger;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Stijn
 */
public class PassengerRead {

    PassengerDAO passDao = new PassengerDAO(new Passenger());
    AddressDAO addDao = new AddressDAO();

    public ArrayList<Passenger> GetAll() throws SQLException, ClassNotFoundException 
    {
        ArrayList<Passenger> output = new ArrayList<>();

        try 
        {
            output = (ArrayList) passDao.GetAllPass();
        } 
        catch (SQLException | ClassNotFoundException exc) 
        {
            throw exc;
        }

        return output;
    }

    public ArrayList<Passenger> SearchPass(String ID, String FN, String SN) throws Exception 
    {
        
        int IDint = 0;
        if (!ID.equals("")) 
        {
            IDint = Integer.parseInt(ID);
        }
        
        ArrayList<Passenger> output = passDao.Search(IDint, FN, SN);
        
        return output;
    }
    
    public Passenger SearchPassById (int id)
    {
       Passenger output = passDao.GetByID(id);
       return output;
    }
    
    public ArrayList<Flight> SearchFlightByPassengerID(int pasID)
    {
        ArrayList<Flight> output = new ArrayList<>();
        
        try 
        {
            output = (ArrayList) passDao.SearchFlightByPassengerID(pasID);
        } 
        catch (Exception ex) 
        {
            throw ex;
        }
        
        return output;
    }
    
    public void DeleteLinkedFlights(int flightID, int pasID){
    
    try 
        {
            passDao.DeleteLinkedFlights(flightID, pasID);
        } 
        catch (Exception ex) 
            
        {
            
        }
    }
}
