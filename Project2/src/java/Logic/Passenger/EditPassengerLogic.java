package Logic.Passenger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Jo
 */

import DAO.PassengerDAO;
import Models.Domain.Passenger;
import java.util.Date;

public class EditPassengerLogic {
   
    private PassengerDAO _passengerDAO = new PassengerDAO();
    
    public void PassengerUpdate(int id, String firstName, String surName, Date birthDay, int addressId, int typeId) throws ClassNotFoundException {
        
        Passenger myPassenger = new Passenger();
        myPassenger.setID(id);
        myPassenger.setFirstName(firstName);
        myPassenger.setSurName(surName);
        myPassenger.setTypeID(1);
        myPassenger.setAddressID(addressId);
        myPassenger.setBirthday(birthDay);
        
       
        _passengerDAO.Update(myPassenger);
       
        
    }
    
    
    
}
