/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic.Passenger;

import DAO.FlightDAO;
import DAO.PassengerDAO;
import Models.Domain.Flight;
import Models.Domain.Passenger;
import Models.ViewModel.PassengerFlightVM;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jo
 */
public class PassengerFlightLogic {

    public PassengerFlightLogic() {
        
    }
    
    public void DeletePassengerFlight (String flightId, String passengerId) {
        
        PassengerDAO myPassengerDAO = new PassengerDAO();
        myPassengerDAO.DeleteLinkedFlights(Integer.parseInt(flightId), Integer.parseInt(passengerId));
    }
    
    public PassengerFlightVM GetVM (String id, int language) throws SQLException {
        
        PassengerFlightVM VM = new PassengerFlightVM(language);
        Flight myFlight = new Flight();
        FlightDAO myFlightDAO = new FlightDAO(myFlight);
        PassengerDAO myPassengerDAO = new PassengerDAO();
        ArrayList<Passenger> listPassenger = new ArrayList<>();

        int flightId = Integer.parseInt(id);

        try {
            myFlight = myFlightDAO.GetFlightByID(flightId);
        } catch (SQLException ex) {
            Logger.getLogger(PassengerFlightLogic.class.getName()).log(Level.SEVERE, null, ex);
        }

        listPassenger = myPassengerDAO.SearchPassengersByFlightId(flightId);
        for (Passenger x: listPassenger) 
        {
            int i = myPassengerDAO.isCheckedIn(x.getID(), flightId);
            x.setCheckedIn(i);
        }

        VM.setId(flightId);
        VM.setFlightNr(myFlight.getFlightNR());
        VM.setPassengerList(listPassenger);
        
        return VM;
    }
    
    public void CheckInPassengerFlight(String fID, String pID) throws Exception{
        
        PassengerDAO myPassengerDAO = new PassengerDAO();
        myPassengerDAO.checkInPassenger(fID, pID);
        
    }

}
