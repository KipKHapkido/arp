/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic.Passenger;

/**
 *
 * @author Sam
 */
import DAO.PassengerDAO;
import Models.Domain.Passenger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

public class PassengerCreate {
    
    private PassengerDAO passengerDao = new PassengerDAO();
    
    public void PassengerCreate(String firstName,String surName,String birthDay,int addressId) throws Exception
    {   
        Passenger passenger = new Passenger();        
        passenger.setFirstName(firstName);
        passenger.setSurName(surName);        
        passenger.setTypeID(1);        
        passenger.setAddressID(addressId);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        
        Date date = df.parse(birthDay);
        passenger.setBirthday(date);
        
        passengerDao.CreatePassenger(passenger);
    }
    
    public int Create(Passenger passenger) throws Exception
    {
        return passengerDao.CreatePassenger(passenger);
        
    }
    private List errorList = new ArrayList<>();
    public List getErrorList() {
        return errorList;
    }

    public void setErrorList(List errorList) {
        this.errorList = errorList;
    }
    
    
}
