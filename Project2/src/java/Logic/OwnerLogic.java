/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic;

import DAO.AirplaneDAO;
import DAO.OwnerDAO;
import Logic.Helpers.ValidationHelper;
import Models.Datamodels.FilterCriteria;
import Models.Domain.Message;
import Models.Domain.Owner;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kathleen
 */
public class OwnerLogic {
    private OwnerDAO ownerDAO = new OwnerDAO();
    private List errorList = new ArrayList<Message>(); 
    
    public List getErrorList() {
        return errorList;
    }

    public void setErrorList(List errorList) {
        this.errorList = errorList;
    }
    
    public List<Owner> getAll(){
        return ownerDAO.GetAll();
    }
    
    public List<Owner> getAllWithRelations(){
        return ownerDAO.getAllWithRelations(null);
    }
    
    public List<Owner> getAllWithRelations(FilterCriteria filterCriteria){
        return ownerDAO.getAllWithRelations(filterCriteria);
    }
    
    public Owner getByID(int id)
    {
        return ownerDAO.GetByID(id);
    }
    
    public int Update(Owner owner)
    {
        return ownerDAO.Update(owner);
    }
    
    public List Delete(int id)
    {
        AirplaneDAO airplaneDAO = new AirplaneDAO();
        int numberOfAirplanes = airplaneDAO.countByOwnerID(id);
        
        if (numberOfAirplanes == 0) 
        {
            ownerDAO.Delete(id);
        }
        else
        {
            errorList.add(new Message("Delete failed: This owner still has " + numberOfAirplanes + " Airplane(s)", Message.MessageSeverity.ERROR));
        }
        
        return errorList;
    }
    
    public int Create(Owner owner)
    {
        return ownerDAO.Create(owner);
    }
    
    public boolean Validate(Owner owner)
    {     
        OwnerDAO ownerDAO = new OwnerDAO();
        Owner originalOwner;

        //Check owner name for create
        if (owner.getID() == 0 && ownerDAO.getOwnerByName(owner.getName()).getName() != "" ) {
            errorList.add(new Message("Owner already Exists", Message.MessageSeverity.ERROR));
        }
        
        //Check name for update
        if(owner.getID() > 0)
        {
            originalOwner = ownerDAO.GetByID(owner.getID());
            
            if(originalOwner != null)
            {
                if(originalOwner.getName() != owner.getName() && ownerDAO.getOwnerByName(owner.getName()).getName() == "" )
                {
                    errorList.add(new Message("Owner already Exists", Message.MessageSeverity.ERROR));
                }
            }
            else
            {
                errorList.add(new Message("Owner not found", Message.MessageSeverity.ERROR));
            }            
        }
        
        errorList = ValidationHelper.validateEmpty(owner.getName(), "NameField", errorList);        
        errorList = ValidationHelper.validateMaxNumberOfCharacters(owner.getName(), "NameField", 255, errorList);       
        errorList = ValidationHelper.validateMinNumberOfCharacters(owner.getName(), "NameField", 2, errorList);
                
       return errorList.size() == 0;
    }
}
