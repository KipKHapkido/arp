/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic;

import DAO.AirportDAO;
import DAO.HangarDAO;
import Models.Domain.Airport;
import Models.Domain.Message;
import Models.Domain.Hangar;
import Models.ViewModel.HangarVM;
import java.util.List;

/**
 *
 * @author Erik Michielsen
 */
public class ManageHangarLogic {

    private AirportDAO airDao = new AirportDAO(new Airport());
    private HangarDAO hangDao = new HangarDAO(new Hangar());
    private HangarVM vm = null;

    public HangarVM OverviewAction(HangarVM tempVM, String submit) {
        vm = tempVM;
        if (submit.equals("Cancel")) {
            vm.setFeedback(HangarVM.LogicAction.CANCEL);
            vm.setHangar(null);
        } else if (submit.equals("Create")) {
            vm.setFeedback(HangarVM.LogicAction.CREATE);
            vm.setHangar(new Hangar());
        } else if (submit.substring(0, 3).equals("upd")) {
            vm.setHangar(hangDao.GetByID(Integer.parseInt(submit.substring(3))));
            vm.setFeedback(HangarVM.LogicAction.UPDATE);
        } else if (submit.substring(0, 3).equals("del")) {
            //joption pane is aan serverkant
            int i = hangDao.Delete(Integer.parseInt(submit.substring(3)));
            vm.setHangar(null);
            if (i > 0) {
                vm.addMessage(vm.getVarMap().get("DelSuccess"), Message.MessageSeverity.SUCCESS);
            } else {
                vm.addMessage(vm.getVarMap().get("CreateFail"), Message.MessageSeverity.ERROR);
            }
            vm.setFeedback(HangarVM.LogicAction.DELETE);
        }
        return vm;
    }

    public HangarVM Save(HangarVM tempVM, String submit, String ID, String Name, 
            String Capacity, String AirportID) {
        vm = tempVM;
        if (submit.equals("-1")) {
            vm.setFeedback(HangarVM.LogicAction.CANCELSAVE);
            return vm;
        }
        Hangar hang = new Hangar();
        hang.setID(Integer.parseInt(ID));
        hang.setName(Name);
        hang.setCapacity(Capacity);
        if (AirportID != null) {
            hang.setAirportID(Integer.parseInt(AirportID));
        }

        if (hang.getAirportID() < 1 || hang.getName().isEmpty())
        {
            vm.setFeedback(HangarVM.LogicAction.ERRORSAVE);
            vm.addMessage(vm.getVarMap().get("CreateFillIn"), Message.MessageSeverity.ERROR);
            vm.setHangar(hang);
        } else {
            int r = hangDao.Save(hang);
            if (r > 0) {
                vm.setFeedback(HangarVM.LogicAction.SAVED);
                vm.addMessage(vm.getVarMap().get("CreateSucces"), Message.MessageSeverity.SUCCESS);
            } else {
                vm.setFeedback(HangarVM.LogicAction.ERROR);
                vm.addMessage(vm.getVarMap().get("CreateFail"), Message.MessageSeverity.ERROR);
            }
        }
        return vm;
    }

    private Boolean tryparse(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (Exception exc) {
            return false;
        }
    }

    public List<Hangar> getAll() {
        return hangDao.getAllEager();
    }

    public List<Airport> getAllAirports() {
        return airDao.GetAll();
    }
}
