/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic;

import DAO.AirportDAO;
import Models.Domain.Airport;
import java.util.List;

/**
 *
 * @author CharlotteHendrik
 */
public class AirportLogic {
    
        AirportDAO _airportDAO= new AirportDAO(new Airport());
    
        public List<Airport> getAll(){
            List<Airport> Airports = _airportDAO.GetAll();
        return Airports;
    }

    
}
