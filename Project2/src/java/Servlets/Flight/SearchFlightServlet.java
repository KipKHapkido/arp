/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Flight;

import Logic.Flight.SearchFlightsLogic;
import Servlets.BaseServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sven Vervloet
 */
public class SearchFlightServlet extends BaseServlet {

    SearchFlightsLogic logic = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int language = super.CheckLanguage(request);
        logic = new SearchFlightsLogic(language);
        //VM wordt aangemaakt en ingevuld in Logic

        try {
            logic.getAllDestinations();
            //Destinations worden opgehaald en in Logic doorgegeven aan VM
        } catch (SQLException ex) {
            Logger.getLogger(SearchFlightServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.setAttribute("VM", logic.getsVM());
        //VM wordt opgehaald vanuit Logic en doorgegeven aan Jsp
        
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Flight/SearchFlightsJsp.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
               
        int language = super.CheckLanguage(request);
        logic = new SearchFlightsLogic(language);
        //VM wordt aangemaakt en ingevuld in Logic

        try {
            logic.getAllDestinations();
            //Destinations worden in Logic opgehaald en daar doorgegeven aan VM
        } catch (SQLException ex) {
            Logger.getLogger(SearchFlightServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        logic.getSearchFlights(request.getParameter("DestinationID"), request.getParameter("FlightNR"));
        //FormValues en Flights worden in Logic doorgegeven aan VM
        
        request.setAttribute("VM", logic.getsVM());
        //VM wordt opgehaald vanuit Logic en doorgegeven aan Jsp
              
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Flight/SearchFlightsJsp.jsp");
        rd.forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
