/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Flight;

import Logic.AirplaneLogic;
import Logic.AirportLogic;
import Logic.Flight.NewFlightsLogic;
import Logic.TimeLogic;
import Models.Domain.Flight;
import Models.Domain.Message;
import Models.ViewModel.Flight.CreateFlightVM;
import Servlets.BaseServlet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author CharlotteHendrik
 */
public class UpdateFlightServlet extends BaseServlet {
    Flight flightn = new Flight();
    NewFlightsLogic fl = null;
    AirportLogic al = new AirportLogic();
    AirplaneLogic logicAirplane= new AirplaneLogic();
    TimeLogic tl = new TimeLogic();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int language= super.CheckLanguage(request);
        CreateFlightVM createFlightVM = new CreateFlightVM(language);
        fl = new NewFlightsLogic(language);
        createFlightVM.setAirports(al.getAll());
        createFlightVM.setAirplanes(logicAirplane.getAll());
        createFlightVM.setTimes(fl.getAllTimes());

        int Id = Integer.parseInt(request.getParameter("ID"));

            flightn = fl.getById(Id);
        createFlightVM.setFlightNR("");
        createFlightVM.setAirplaneID(flightn.getAirplaneID());
        createFlightVM.setDepartTimeID(flightn.getDepartFromID());
        createFlightVM.setDepartUntillID(flightn.getDepartUntilID());
        createFlightVM.setDepartureID(flightn.getDepartureID());
        createFlightVM.setDestinationID(flightn.getDestinationID());
        createFlightVM.setDepartDate(flightn.getDepartDate());
        createFlightVM.setID(Id);
        
        Date newdate = flightn.getDepartDate();
        Calendar Cal = Calendar.getInstance();
        Cal.setTime(newdate);
        int year=Cal.get(Calendar.YEAR);
        int month = Cal.get(Calendar.MONTH);
        int day = Cal.get(Calendar.DAY_OF_MONTH);
        
        createFlightVM.setYear(year);
        createFlightVM.setDay(day);
        createFlightVM.setMonth(month);

        request.setAttribute("VM", createFlightVM);
        
        HttpSession session = request.getSession();
        if (session.getAttribute("MessageList") != null ) {
            ArrayList<Message> messageList  = (ArrayList<Message>)session.getAttribute("MessageList");
            createFlightVM.setListMessage(messageList);
            session.setAttribute("ResultMessage", null);
        }
        
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Flight/UpdateFlightJsp.jsp");
        rd.forward(request, response);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int language= super.CheckLanguage(request);
        CreateFlightVM createFlightVM = new CreateFlightVM(language);
        fl = new NewFlightsLogic(language);
        createFlightVM.setAirports(al.getAll());
        createFlightVM.setAirplanes(logicAirplane.getAll());
        createFlightVM.setTimes(fl.getAllTimes());

        

        int Id = Integer.parseInt(request.getParameter("ID"));

            flightn = fl.getById(Id);

        createFlightVM.setFlightNR(flightn.getFlightNR());
        createFlightVM.setAirplaneID(flightn.getAirplaneID());
        createFlightVM.setDepartTimeID(flightn.getDepartFromID());
        createFlightVM.setDepartUntillID(flightn.getDepartUntilID());
        createFlightVM.setDepartureID(flightn.getDepartureID());
        createFlightVM.setDestinationID(flightn.getDestinationID());
        createFlightVM.setDepartDate(flightn.getDepartDate());
        createFlightVM.setID(Id);
        
        Date newdate = flightn.getDepartDate();
        Calendar Cal = Calendar.getInstance();
        Cal.setTime(newdate);
        int year=Cal.get(Calendar.YEAR);
        int month = Cal.get(Calendar.MONTH);
        int day = Cal.get(Calendar.DAY_OF_MONTH);

        createFlightVM.setYear(year);
        createFlightVM.setDay(day);
        createFlightVM.setMonth(month);
        request.setAttribute("VM", createFlightVM);
        
        
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Flight/UpdateFlightJsp.jsp");
        rd.forward(request, response);
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
