/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Flight;

import Logic.Flight.SearchFlightsLogic;
import Servlets.BaseServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sven Vervloet
 */
public class CancelFlightServlet extends BaseServlet {

    SearchFlightsLogic logic = null;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int language = super.CheckLanguage(request);
        logic = new SearchFlightsLogic(language);
        //VM wordt aangemaakt en ingevuld in Logic

        int ID = Integer.parseInt(request.getParameter("ID"));
        try {
            logic.CancelFlight(ID);
            logic.getAllDestinations();
            //Destinations worden opgehaald en in Logic doorgegeven aan VM
        } catch (SQLException ex) {
            Logger.getLogger(SearchFlightServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        logic.getSearchFlights(request.getParameter("DestinationID"), request.getParameter("FlightNR"));
        //FormValues en Flights worden in Logic doorgegeven aan VM
        
        request.setAttribute("VM", logic.getsVM());
        //VM wordt opgehaald vanuit Logic en doorgegeven aan Jsp
              
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Flight/SearchFlightsJsp.jsp");
        rd.forward(request, response);
        
    }
   
}
