/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Flight;

import Logic.Flight.IncomingFlightsLogic;
import Servlets.BaseServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sven Vervloet
 */
public class IncomingFlightsServlet extends BaseServlet {

    IncomingFlightsLogic logic = null;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int language = super.CheckLanguage(request);
        logic = new IncomingFlightsLogic(language);
        //een object van VM bestaat in de logic     
        try {
            logic.getIncomingFlights();
            //lijst van niet goedgekeurde vluchten wordt in logic doorgegeven aan VM
        } catch (SQLException ex) {
            Logger.getLogger(NewFlightsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.setAttribute("VM", logic.getiVM());
        //VM wordt vanuit logic opgevraagd en doorgegeven
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Flight/IncomingFlightsJsp.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int ID = Integer.parseInt(request.getParameter("ID"));
        int language = super.CheckLanguage(request);
        logic = new IncomingFlightsLogic(language);
        //een Object VM bestaat binnen logic en wordt daar opgevuld.

        try {
            logic.getFlightByID(ID);
            //Flight wordt opgehaald (later nodig) en in Logic ook doorgegeven aan VM 
        } catch (SQLException ex) {
            Logger.getLogger(NewFlightsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.setAttribute("VM", logic.getcVM());
        //De opgevulde VM wordt opgehaald en doorgegeven aan JSP
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Flight/CompleteIncomingFlightJsp.jsp");
        rd.forward(request, response);
    }

}
