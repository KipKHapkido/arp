/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Flight;


import Logic.Flight.NewFlightsLogic;
import Models.Domain.Flight;
import Models.Domain.Message;
import Servlets.BaseServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author CharlotteHendrik
 */
public class SaveFlightServlet extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int language= super.CheckLanguage(request);
        NewFlightsLogic fl= new NewFlightsLogic(language);
        List<Message> messageList = new ArrayList<Message>();
        Flight flight;
        Boolean validFlight;
        
        try {
            int id = Integer.parseInt(request.getParameter("ID"));
            flight=fl.getById(id);
            String stringdate = request.getParameter("Date");
            Date newdate = fl.TransformDate(stringdate);
            String flightnumber = request.getParameter("FlightNR");
            int airplaneid = Integer.parseInt(request.getParameter("AirplaneID"));
            int departfrom = Integer.parseInt(request.getParameter("DepartFrom"));
            int departuntill= Integer.parseInt(request.getParameter("DepartUntill"));
            int departureid= Integer.parseInt(request.getParameter("DepartureID"));
            int arrivalid = Integer.parseInt(request.getParameter("DestinationID"));
            validFlight = fl.JustUpdateFlightLogic(flight, flightnumber, newdate, airplaneid, departfrom, departuntill, departureid, arrivalid);
            //Sessie aanmaken        
            HttpSession session = request.getSession();
            if (validFlight) {
                request.setAttribute("VM", fl.getsearchVM());
                RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Flight/SearchFlightsJsp.jsp");
                rd.forward(request, response); 
            }
            else{
                messageList.addAll(fl.getErrorList());
                session.setAttribute("MessageList", messageList); 
                 //terugzenden naar UpdateServlet
                response.sendRedirect(request.getContextPath() + "/UpdateFlight?ID="+id);
                }
            } catch (ParseException ex) {
            Logger.getLogger(CreateFlightServlet.class.getName()).log(Level.SEVERE, null, ex);
           
        } catch (SQLException ex) {
            Logger.getLogger(SaveFlightServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
