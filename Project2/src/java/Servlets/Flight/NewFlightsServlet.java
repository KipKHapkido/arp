/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Flight;

import Logic.Flight.NewFlightsLogic;
import Servlets.BaseServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sven Vervloet
 */
public class NewFlightsServlet extends BaseServlet {

    NewFlightsLogic logic = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int language = super.CheckLanguage(request);
        logic = new NewFlightsLogic(language);
        //een object van VM bestaat in de logic     
        try {
            logic.getNotApprovedFlights();
            //lijst van niet goedgekeurde vluchten wordt in logic doorgegeven aan VM
        } catch (SQLException ex) {
            Logger.getLogger(NewFlightsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        request.setAttribute("VM", logic.getnVM());
        //VM wordt vanuit logic opgevraagd en doorgegeven
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Flight/NewFlightsJsp.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int ID = Integer.parseInt(request.getParameter("ID"));
        String submit = request.getParameter("submit");
        int language = super.CheckLanguage(request);
        logic = new NewFlightsLogic(language);
        //een Object VM bestaat binnen logic en wordt daar opgevuld.

        if (submit.equals("Complete")) {

            try {
                logic.getFlightByID(ID);
                //Flight wordt opgehaald (later nodig) en in Logic ook doorgegeven aan VM 
            } catch (SQLException ex) {
                Logger.getLogger(NewFlightsServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            request.setAttribute("VM", logic.getcVM());
            //De opgevulde VM wordt opgehaald en doorgegeven aan JSP
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Flight/CompleteFlightJsp.jsp");
            rd.forward(request, response);
        } else {
            try {
                logic.ProcesFlight(ID, submit);
                //Delete of Goedkeuren
                logic.getNotApprovedFlights();
            } catch (SQLException ex) {
                Logger.getLogger(NewFlightsServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            request.setAttribute("VM", logic.getnVM());
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Flight/NewFlightsJsp.jsp");
            //De opgevulde VM wordt opgehaald en doorgegeven aan JSP
            rd.forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
