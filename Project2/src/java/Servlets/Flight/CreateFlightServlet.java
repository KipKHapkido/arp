/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Flight;

import Logic.AirportLogic;
import Logic.AirplaneLogic;
import Logic.Flight.NewFlightsLogic;
import Logic.TimeLogic;
import Models.Domain.Flight;
import Models.Domain.Message;
import Models.ViewModel.Flight.CreateFlightVM;
import Servlets.BaseServlet;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author CharlotteHendrik
 */
public class CreateFlightServlet extends BaseServlet {
    
    Flight flightn = new Flight();
    NewFlightsLogic fl = null;
    AirportLogic al = new AirportLogic();
    AirplaneLogic logicAirplane= new AirplaneLogic();
    TimeLogic tl = new TimeLogic();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int language= super.CheckLanguage(request);
        CreateFlightVM createFlightVM = new CreateFlightVM(language);
        fl = new NewFlightsLogic(language);
        createFlightVM.setAirports(al.getAll());
        createFlightVM.setAirplanes(logicAirplane.getAll());
        createFlightVM.setTimes(fl.getAllTimes());
        
        HttpSession session = request.getSession();
        if (session.getAttribute("MessageList") != null ) {
            int airplaneid = (int) session.getAttribute("AirplaneID");
            int departfrom = (int) session.getAttribute("DepartFrom");
            int departuntill= (int) session.getAttribute("DepartUntill");
            int departureid= (int) session.getAttribute("DepartID");
            int arrivalid = (int) session.getAttribute("DestinationID");
            String FlightNR = (String)session.getAttribute("FlightNR");
            String stringdate = (String)session.getAttribute("Stringdate");

            ArrayList<Message> messageList  = (ArrayList<Message>)session.getAttribute("MessageList");
            createFlightVM.setListMessage(messageList);
            session.setAttribute("ResultMessage", null);
            
            if (FlightNR != null) {
                createFlightVM.setFlightNR(FlightNR);
            }
            if (!stringdate.isEmpty()) {
                    Date newdate;
                try {
                    newdate = fl.TransformDate(stringdate);
                    Calendar Cal = Calendar.getInstance();
                    Cal.setTime(newdate);
                    int year=Cal.get(Calendar.YEAR);
                    int month = Cal.get(Calendar.MONTH);
                    int day = Cal.get(Calendar.DAY_OF_MONTH);
                    createFlightVM.setYear(year);
                    createFlightVM.setMonth(month);
                    createFlightVM.setDay(day);
                } catch (ParseException ex) {
                    Logger.getLogger(CreateFlightServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                    
            }

            createFlightVM.setAirplaneID(airplaneid);
            createFlightVM.setDepartTimeID(departfrom);
            createFlightVM.setDepartUntillID(departuntill);
            createFlightVM.setDepartureID(departureid);
            createFlightVM.setDestinationID(arrivalid);
            request.setAttribute("VM", createFlightVM);
        }
        else{
            createFlightVM.setFlightNR("");
            request.setAttribute("VM", createFlightVM);
            
        }
        
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Flight/CreateFlightJsp.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int language= super.CheckLanguage(request);
        CreateFlightVM createFlightVM = new CreateFlightVM(language);
        fl = new NewFlightsLogic(language);
        createFlightVM.setAirports(al.getAll());
        createFlightVM.setAirplanes(logicAirplane.getAll());
        createFlightVM.setTimes(fl.getAllTimes());
        Boolean validFlight;
        List<Message> messageList = new ArrayList<Message>();
        
        String stringdate = request.getParameter("Date");
        String flightnumber = request.getParameter("FlightNR");
        int airplaneid = Integer.parseInt(request.getParameter("AirplaneID"));
        int departfrom = Integer.parseInt(request.getParameter("DepartFrom"));
        int departuntill= Integer.parseInt(request.getParameter("DepartUntill"));
        int departureid= Integer.parseInt(request.getParameter("DepartureID"));
        int arrivalid = Integer.parseInt(request.getParameter("DestinationID"));
        try {
            validFlight = fl.CreateFlightLogic(flightnumber,stringdate, airplaneid,departfrom, departuntill, departureid, arrivalid);
                    HttpSession session = request.getSession();
        if (validFlight) {
            
            request.setAttribute("VM", new CreateFlightVM(language));
            messageList.addAll(fl.getErrorList());
            session.setAttribute("MessageList", messageList);
            session.setAttribute("FlightNR", "");
            session.setAttribute("AirplaneID", 1);
            session.setAttribute("DepartFrom", 1);
            session.setAttribute("DepartUntill", 1);
            session.setAttribute("DepartID", 1);
            session.setAttribute("DestinationID", 1);
            session.setAttribute("Stringdate", "");
           response.sendRedirect(request.getContextPath() + "/CreateFlight");

        }
        else{ 
            messageList.addAll(fl.getErrorList());
            session.setAttribute("MessageList", messageList);
            session.setAttribute("FlightNR", flightnumber);
            session.setAttribute("AirplaneID", airplaneid);
            session.setAttribute("DepartFrom", departfrom);
            session.setAttribute("DepartUntill", departuntill);
            session.setAttribute("DepartID", departureid);
            session.setAttribute("DestinationID", arrivalid);
            session.setAttribute("Stringdate", stringdate);
            response.sendRedirect(request.getContextPath() + "/CreateFlight");
        }
        } catch (ParseException ex) {
            Logger.getLogger(CreateFlightServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
            
        
        
        
        


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
    }// </editor-fold>


