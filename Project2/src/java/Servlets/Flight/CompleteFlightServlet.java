/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Flight;

import Logic.Flight.NewFlightsLogic;
import Servlets.BaseServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sven Vervloet
 */
public class CompleteFlightServlet extends BaseServlet {

    NewFlightsLogic logic = null;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + "/NewFlights");
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String submit = request.getParameter("submit");
        int language = super.CheckLanguage(request);
        
        logic = new NewFlightsLogic(language);
        //een Object VM bestaat binnen logic en wordt daar opgevuld.
               
        if (submit.equals("Save")) {
            int returnValue = 0;           
            int ID = Integer.parseInt(request.getParameter("ID"));
            try {
                returnValue = logic.save(ID, request.getParameter("DepartureTimeID"), request.getParameter("RunwayID")); //Save and return Flight
            } catch (SQLException ex) {
                Logger.getLogger(CompleteFlightServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
           
            if (returnValue < 2) { // < 2. 0: No update or 1: Update but not complete
                BackToCompletion(request, response);
            } else {
                BackToOverview(request, response);
            }
        } else if (submit.equals("Cancel")) {
            try {
                logic.getNotApprovedFlights();
            } catch (SQLException ex) {
                Logger.getLogger(CompleteFlightServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            BackToOverview(request, response);
        }

    }

    private void BackToOverview(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("VM", logic.getnVM());
        
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Flight/NewFlightsJsp.jsp");
        rd.forward(request, response);
    }

    private void BackToCompletion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        request.setAttribute("VM", logic.getcVM());

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Flight/CompleteFlightJsp.jsp"); // Back to Form (Complete)
        rd.forward(request, response);
    }

}
