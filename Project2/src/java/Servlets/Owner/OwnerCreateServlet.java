/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Owner;

import Logic.AddressLogic;
import Logic.OwnerLogic;
import Models.Domain.Address;
import Models.Domain.Message;
import Models.Domain.Owner;
import Models.ViewModel.OwnerDetailVM;
import Servlets.BaseServlet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author Kathleen
 */
public class OwnerCreateServlet extends BaseServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int language = super.CheckLanguage(request);
        
        request.setAttribute("VM", new OwnerDetailVM(language));
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Owner/OwnerCreate.jsp");
        rd.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Address address = new Address();
        Owner owner = new Owner();
        OwnerLogic ownerLogic = new OwnerLogic();
        AddressLogic addressLogic = new AddressLogic(); 
        List<Message> messageList = new ArrayList<>();
        int addressId;
        boolean validOwner;
        boolean validAddress;

        //Fill addresss
        address.setCountry(request.getParameter("owner_country"));
        address.setTown(request.getParameter("owner_town"));
        address.setZip_code(request.getParameter("owner_zipcode"));
        address.setStreet(request.getParameter("owner_street"));
        address.setNumber(request.getParameter("owner_number"));
        
        //Fill owner
        owner.setName(request.getParameter("owner_name"));       
        
        //Sessie aanmaken        
        HttpSession session = request.getSession();
        
        validOwner = ownerLogic.Validate(owner);
        validAddress = addressLogic.Validate(address);
        
        if (validOwner && validAddress) {             
            addressId = addressLogic.Create(address);
            owner.setAddressID(addressId);           
            ownerLogic.Create(owner);
            
            messageList.add(new Message("Owner Create Success. ", Message.MessageSeverity.SUCCESS));
                        
        }
        else
        {
            messageList.add(new Message("Owner Create Failed. ", Message.MessageSeverity.ERROR));
            messageList.addAll(ownerLogic.getErrorList());
            messageList.addAll(addressLogic.getErrorList());
        }
        
        session.setAttribute("MessageList", messageList);   
        response.sendRedirect(request.getContextPath() + "/Owner");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
