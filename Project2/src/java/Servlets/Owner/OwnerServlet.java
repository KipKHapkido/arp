/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Owner;


import Logic.OwnerLogic;
import Models.Datamodels.FilterCriteria;
import Models.ViewModel.OwnerVM;
import Servlets.BaseServlet;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kathleen
 */
public class OwnerServlet extends BaseServlet {
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int language = super.CheckLanguage(request);
        OwnerVM ownerVM = new OwnerVM(language, request.getSession());
        OwnerLogic ownerLogic = new OwnerLogic();
        
        ownerVM.setOwnerList(ownerLogic.getAllWithRelations());
        
        request.setAttribute("VM", ownerVM);
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Owner/OwnerListPageJsp.jsp");
        rd.forward(request, response);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         int language = super.CheckLanguage(request);
        OwnerVM ownerVM = new OwnerVM(language, request.getSession());
        OwnerLogic ownerLogic = new OwnerLogic();
        
        String search = request.getParameter("search");
        FilterCriteria filterCriteria = new FilterCriteria("owner.Name" , search);
        
        ownerVM.setOwnerList(ownerLogic.getAllWithRelations(filterCriteria));

        request.setAttribute("VM", ownerVM);
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Owner/OwnerListPageJsp.jsp");
        rd.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
