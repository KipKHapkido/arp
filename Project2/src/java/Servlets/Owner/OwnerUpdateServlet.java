/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Owner;

import DAO.AddressDAO;
import DAO.OwnerDAO;
import Logic.AddressLogic;
import Logic.OwnerLogic;
import Models.Domain.Address;
import Models.Domain.Message;
import Models.Domain.Owner;
import Models.ViewModel.OwnerDetailVM;
import Servlets.BaseServlet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kathleen
 */
public class OwnerUpdateServlet extends BaseServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int language = super.CheckLanguage(request);
        OwnerDetailVM ownerDetailVM = new OwnerDetailVM(language);
        OwnerDAO ownerDAO = new OwnerDAO();
        AddressDAO addressDAO = new AddressDAO();

        Owner owner;
        Address address;

        int Id = Integer.parseInt(request.getParameter("Id"));

        owner = ownerDAO.GetByID(Id);
        address = addressDAO.GetByID(owner.getAddressID());
        ownerDetailVM.setName(owner.getName());
        ownerDetailVM.setCountry(address.getCountry());
        ownerDetailVM.setStreet(address.getStreet());
        ownerDetailVM.setTown(address.getTown());
        ownerDetailVM.setNumber(address.getNumber());
        ownerDetailVM.setZip_code(address.getZip_code());
        ownerDetailVM.setId(owner.getID());

        request.setAttribute("VM", ownerDetailVM);
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Owner/OwnerUpdate.jsp");
        rd.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        OwnerLogic ownerLogic = new OwnerLogic();
        AddressLogic addressLogic = new AddressLogic();
        List<Message> messageList = new ArrayList<>();
        Owner owner;
        Address address;
        boolean validOwner;
        boolean validAddress;

        int Id = Integer.parseInt(request.getParameter("owner_Id"));

        owner = ownerLogic.getByID(Id);
        address = addressLogic.getByID(owner.getAddressID());

        //Fill addresss
        address.setCountry(request.getParameter("owner_country"));
        address.setTown(request.getParameter("owner_town"));
        address.setZip_code(request.getParameter("owner_zipcode"));
        address.setStreet(request.getParameter("owner_street"));
        address.setNumber(request.getParameter("owner_number"));

        validOwner = ownerLogic.Validate(owner);
        validAddress = addressLogic.Validate(address);

        //Sessie aanmaken        
        HttpSession session = request.getSession();

        if (validOwner && validAddress) {
            //Update address
            addressLogic.Update(address);
            //Fill owner
            owner.setName(request.getParameter("owner_name"));
            //Update owner
            ownerLogic.Update(owner);
            //Session opvullen
            messageList.add(new Message("User Update Success. ", Message.MessageSeverity.SUCCESS));

            session.setAttribute("MessageList", messageList);
        } else {
            messageList.add(new Message("User Update Failed. ", Message.MessageSeverity.ERROR));
            messageList.addAll(ownerLogic.getErrorList());
            messageList.addAll(addressLogic.getErrorList());
            
            session.setAttribute("MessageList", messageList);   
        }
        //terugzenden naar OwnerServlet
        response.sendRedirect(request.getContextPath() + "/Owner");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
