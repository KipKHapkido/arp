/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Owner;

import DAO.AddressDAO;
import DAO.OwnerDAO;
import Models.Domain.Address;
import Models.Domain.Owner;
import Models.ViewModel.OwnerDetailVM;
import Servlets.BaseServlet;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kathleen
 */
public class OwnerReadServlet extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         int language = super.CheckLanguage(request);
        OwnerDetailVM ownerDetailVM = new OwnerDetailVM(language);
        OwnerDAO ownerDAO = new OwnerDAO();
        AddressDAO addressDAO = new AddressDAO();

        Owner owner;
        Address address;

        int Id = Integer.parseInt(request.getParameter("Id"));

        owner = ownerDAO.GetByID(Id);
        address = addressDAO.GetByID(owner.getAddressID());
        ownerDetailVM.setName(owner.getName());
        ownerDetailVM.setCountry(address.getCountry());
        ownerDetailVM.setStreet(address.getStreet());
        ownerDetailVM.setTown(address.getTown());
        ownerDetailVM.setNumber(address.getNumber());
        ownerDetailVM.setZip_code(address.getZip_code());
        ownerDetailVM.setId(owner.getID());

        request.setAttribute("VM", ownerDetailVM);
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Owner/OwnerRead.jsp");
        rd.forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
