/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Airplane;

import Logic.AirplaneLogic;
import Models.ViewModel.AirplaneVM;
import Servlets.BaseServlet;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jelle
 */
public class AirplaneSaveServlet extends BaseServlet {

    AirplaneLogic logic = new AirplaneLogic();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AirplaneVM vm = new AirplaneVM(super.CheckLanguage(request));
        vm = logic.Save(vm, request.getParameter("submit"), request.getParameter("id"), request.getParameter("code"),
                request.getParameter("owner"), request.getParameter("state"), request.getParameter("capacity"));
        if (vm.getFeedback() == AirplaneVM.LogicAction.DUPLSAVE
                || vm.getFeedback() == AirplaneVM.LogicAction.ERRORSAVE) {
            request.setAttribute("VM", vm);
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Airplane/AirplaneSaveJsp.jsp");
            rd.forward(request, response);
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("VM", vm);
            response.sendRedirect(request.getContextPath() + "/AirplaneOverview");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
