/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Airplane;

import Logic.AirplaneLogic;
import Models.ViewModel.AirplaneVM;
import Servlets.BaseServlet;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jelle
 */
public class AirplaneHangarServlet extends BaseServlet {

    AirplaneLogic logic = new AirplaneLogic();

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AirplaneVM vm = new AirplaneVM(super.CheckLanguage(request));
        vm = logic.HangarStart(vm, request.getParameter("ID"));
        request.setAttribute("VM", vm);
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Airplane/AirplaneHangarJsp.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AirplaneVM vm = new AirplaneVM(super.CheckLanguage(request));
        vm = logic.HangarSave(vm, request.getParameter("submit"), request.getParameter("id"), request.getParameter("hangar"));
        HttpSession session = request.getSession();
        session.setAttribute("VM", vm);
        response.sendRedirect(request.getContextPath() + "/AirplaneOverview");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
