/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Airplane;

import Logic.AirplaneLogic;
import Models.ViewModel.AirplaneVM;
import Servlets.BaseServlet;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jelle
 */
public class AirplaneOverviewServlet extends BaseServlet {

    AirplaneLogic logic = new AirplaneLogic();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        AirplaneVM vm = (AirplaneVM) session.getAttribute("VM");
        if (vm == null) {
            vm = logic.airplaneAction(new AirplaneVM(super.CheckLanguage(request)), "initial", "");
        } else {
            session.removeAttribute("VM");
        }
        request.setAttribute("VM", vm);
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Airplane/AirplaneOverviewJsp.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AirplaneVM vm = new AirplaneVM(super.CheckLanguage(request));
        String search = request.getParameter("search");
        try {
            vm.setNext(Integer.parseInt(request.getParameter("next")));
        } catch (NumberFormatException ex) {

        }
        vm = logic.airplaneAction(vm, request.getParameter("submit"), search);
        switch (vm.getFeedback()) {
            case CANCEL:
                response.sendRedirect(request.getContextPath() + "/Airline");
                return;
            case DELETE: {
                HttpSession session = request.getSession();
                session.setAttribute("VM", vm);
                response.sendRedirect(request.getContextPath() + "/AirplaneOverview");
                break;
            }
            case READ: {
                request.setAttribute("VM", vm);
                RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Airplane/AirplaneReadJsp.jsp");
                rd.forward(request, response);
                break;
            }
            case SEARCH: {
                request.setAttribute("VM", vm);
                RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Airplane/AirplaneOverviewJsp.jsp");
                rd.forward(request, response);
                break;
            }
            default: {
                request.setAttribute("VM", vm);
                RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Airplane/AirplaneSaveJsp.jsp");
                rd.forward(request, response);
                break;
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
