/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.User;

import Logic.FunctionLogic;
import Logic.UserLogic;
import Models.Domain.Message;
import Models.Domain.User;
import Models.ViewModel.UserDetailVM;
import Servlets.BaseServlet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kathleen
 */
public class UserUpdateServlet extends BaseServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int language = super.CheckLanguage(request);
        UserDetailVM userDetailVM = new UserDetailVM(language);
        UserLogic userLogic = new UserLogic();
        FunctionLogic functionLogic = new FunctionLogic();
        User user;

        int Id = Integer.parseInt(request.getParameter("Id"));
        user = userLogic.getByID(Id);
        userDetailVM.setFunctionList(functionLogic.getAll());
        userDetailVM.setUsername(user.getUsername());
        userDetailVM.setFunctionId(user.getFunctionID());
        userDetailVM.setId(Id);

        request.setAttribute("VM", userDetailVM);
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/User/UserUpdate.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserLogic userLogic = new UserLogic();
        int language = super.CheckLanguage(request);
        List<Message> messageList = new ArrayList<>();
        UserDetailVM userDetailVM = new UserDetailVM(language);
        boolean validUser;

        //Sessie aanmaken        
        HttpSession session = request.getSession();

        int Id = Integer.parseInt(request.getParameter("user_Id"));

        //Fill user detail
        userDetailVM.setUsername(request.getParameter("user_username"));
        userDetailVM.setPassword(request.getParameter("user_password"));
        userDetailVM.setFunctionId(Integer.parseInt(request.getParameter("user_function")));
        userDetailVM.setId(Id);

        validUser = userLogic.Validate(userDetailVM);

        if (validUser) {
            userLogic.Update(userDetailVM);
            messageList.add(new Message("User Update Success. ", Message.MessageSeverity.SUCCESS));
        } else {
            messageList.add(new Message("User Update Failed. ", Message.MessageSeverity.ERROR));
            messageList.addAll(userLogic.getErrorList());
        }

        session.setAttribute("MessageList", messageList);
        //terugzenden naar OwnerServlet
        response.sendRedirect(request.getContextPath() + "/User");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
