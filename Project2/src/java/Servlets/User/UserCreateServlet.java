/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.User;

import Logic.FunctionLogic;
import Logic.UserLogic;
import Models.Domain.Message;
import Models.ViewModel.UserDetailVM;
import Servlets.BaseServlet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kathleen
 */
public class UserCreateServlet extends BaseServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int language = super.CheckLanguage(request);
        FunctionLogic functionLogic = new FunctionLogic();
        UserDetailVM userDetailVM = new UserDetailVM(language);
        
        userDetailVM.setFunctionList(functionLogic.getAll());
        
        request.setAttribute("VM", userDetailVM);

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/User/UserCreate.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int language = super.CheckLanguage(request);
        UserDetailVM userDetailVM = new UserDetailVM(language);
        UserLogic userLogic = new UserLogic();
        boolean validUser;
        List<Message> messageList = new ArrayList<>();
        
        //fill user
        userDetailVM.setUsername(request.getParameter("user_username"));
        userDetailVM.setPassword(request.getParameter("user_password"));
        userDetailVM.setFunctionId(Integer.parseInt(request.getParameter("user_function")));
        
        //Sessie aanmaken        
        HttpSession session = request.getSession();
        
        //validUser?
        validUser = userLogic.Validate(userDetailVM);
        
        if (validUser) {
            userLogic.Create(userDetailVM);
            messageList.add(new Message("User Create Success. ", Message.MessageSeverity.SUCCESS));           
        }
        else
        {
            messageList.addAll(userLogic.getErrorList());
            messageList.add(new Message("User Create Failed. ", Message.MessageSeverity.ERROR));
        }
        
        session.setAttribute("MessageList", messageList);   
        response.sendRedirect(request.getContextPath() + "/User");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
