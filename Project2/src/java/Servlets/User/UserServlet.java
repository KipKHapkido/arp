/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.User;

import Logic.UserLogic;
import Models.Datamodels.FilterCriteria;
import Servlets.BaseServlet;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Models.ViewModel.UserListVM;



/**
 *
 * @author Kathleen
 */
public class UserServlet extends BaseServlet {

    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int language = super.CheckLanguage(request);
        UserLogic userLogic = new UserLogic();
        UserListVM userListVM = new UserListVM(language, request.getSession());
        userListVM.setUserList(userLogic.getAllWithRelations());

        request.setAttribute("VM", userListVM);
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/User/UserListPage.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int language = super.CheckLanguage(request);
        String search = request.getParameter("search");
        UserLogic userLogic = new UserLogic();
        UserListVM userListVM = new UserListVM(language, request.getSession());
        
        FilterCriteria filterCriteria = new FilterCriteria("user.Username" , search);
        userListVM.setUserList(userLogic.getAllWithRelations(filterCriteria));
        
        request.setAttribute("VM", userListVM);
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/User/UserListPage.jsp");
        rd.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
