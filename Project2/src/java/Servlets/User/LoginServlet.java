/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.User;

import Logic.Helpers.UserObject;
import Logic.LoginLogic;
import Logic.UserLogic;
import Models.Domain.Message;
import Models.Domain.User;
import Models.ViewModel.LoginVM;
import Servlets.BaseServlet;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kathleen
 */
public class LoginServlet extends BaseServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int language = super.CheckLanguage(request);
        LoginVM loginVM = new LoginVM(language); 
         Cookie ck[] = request.getCookies();  
         Cookie currentCookie;
         
         for(int i = 0;i < ck.length;i++)
         {
             currentCookie = ck[i];
                
             switch(currentCookie.getName())
             {
                case "username":
                    loginVM.setUsername(currentCookie.getValue());
                    break;
                case "password":
                    loginVM.setPassword(currentCookie.getValue());
                    break;
             }
            
         }

        request.setAttribute("VM", loginVM);

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/LoginJsp.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int language = super.CheckLanguage(request);
        LoginVM loginVM = new LoginVM(language);
        LoginLogic loginLogic = new LoginLogic();
        UserLogic userLogic = new UserLogic();
        Cookie userNameCookie, passwordCookie;
        User user;
        String[] rememberMe;

        loginVM.setUsername(request.getParameter("username"));
        loginVM.setPassword(request.getParameter("password"));

        if (loginLogic.authenticate(loginVM)) {
            HttpSession x = request.getSession();
            x.setAttribute("userObject", new UserObject(loginVM.getUsername()));

            rememberMe = request.getParameterValues("remember_me");
            
            if (rememberMe != null && rememberMe.length > 0) {
                //Create cookie 
                userNameCookie = new Cookie("username", loginVM.getUsername()); 
                response.addCookie(userNameCookie);
                passwordCookie = new Cookie("password", loginVM.getPassword());
                response.addCookie(passwordCookie);
            }
        } 

        response.sendRedirect(request.getContextPath() + "/Home");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
