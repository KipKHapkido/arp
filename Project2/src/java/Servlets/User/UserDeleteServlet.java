/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.User;

import Logic.UserLogic;
import Models.Domain.Message;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kathleen
 */
public class UserDeleteServlet extends HttpServlet {

    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        UserLogic userLogic = new UserLogic();
        List<Message> messageList = new ArrayList<>();
        
        int Id = Integer.parseInt(request.getParameter("user_Id"));             
        
        //Sessie aanmaken        
        HttpSession session = request.getSession();
        
        try
        {
            userLogic.Delete(Id);
            messageList.add(new Message("User Delete Success. ", Message.MessageSeverity.SUCCESS));
        }
        catch(Exception ex)
        {
            messageList.add(new Message("User Delete Failed. ", Message.MessageSeverity.ERROR)); 
        }
        
        session.setAttribute("MessageList", messageList);
        response.sendRedirect(request.getContextPath() + "/User");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
