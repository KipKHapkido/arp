/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Runway;

import Logic.ManageRunwayLogic;
import Models.ViewModel.RunwayVM;
import Servlets.BaseServlet;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jelle
 */
public class RunwaySaveServlet extends BaseServlet {
    
    ManageRunwayLogic logic = new ManageRunwayLogic();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {    
       
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RunwayVM vm = new RunwayVM(super.CheckLanguage(request));
        vm = logic.Save(vm, request.getParameter("submit"), request.getParameter("id"), 
                request.getParameter("name"), request.getParameter("length"), request.getParameter("airport"));
        if(vm.getFeedback() == RunwayVM.LogicAction.ERRORSAVE||
                vm.getFeedback()==RunwayVM.LogicAction.WRONGSAVE){
            request.setAttribute("VM", vm);
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Runway/RunwaySaveJsp.jsp");
            rd.forward(request, response);
        }else{
            HttpSession session=request.getSession();  
            session.setAttribute("VM",vm);  
            //request dispatcher geen oplossing dan is refresh altijd opnieuw doen 
            response.sendRedirect(request.getContextPath()+"/RunwayOverview");  
            
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
