/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Runway;

import Logic.ManageRunwayLogic;
import Models.ViewModel.RunwayVM;
import Servlets.BaseServlet;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jelle
 */
public class RunwayOverviewServlet extends BaseServlet {

    ManageRunwayLogic logic = new ManageRunwayLogic();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();     
        RunwayVM vm = (RunwayVM)session.getAttribute("VM");
        if(vm==null){            
            vm = new RunwayVM(super.CheckLanguage(request));
        }
        else{
            session.removeAttribute("VM");
        }
        request.setAttribute("VM", vm );
        request.setAttribute("Runways", logic.getAll());        
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Runway/RunwayOverviewJsp.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RunwayVM vm = new RunwayVM(super.CheckLanguage(request));
        vm = logic.OverviewAction(vm, request.getParameter("submit"));
        if (vm.getFeedback() == RunwayVM.LogicAction.CANCEL) {
            response.sendRedirect(request.getContextPath() + "/Airport");
            return;
        } else if (vm.getFeedback() == RunwayVM.LogicAction.DELETE) {
            HttpSession session=request.getSession();  
            session.setAttribute("VM",vm);  
            response.sendRedirect(request.getContextPath()+"/RunwayOverview");
            //request.setAttribute("Runways", logic.getAll());
            //request.setAttribute("VM", vm);
            //RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/ManageRunway/RunwayOverviewJsp.jsp");
            //rd.forward(request, response);
        } else {
            request.setAttribute("VM", vm);
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Runway/RunwaySaveJsp.jsp");
            rd.forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
