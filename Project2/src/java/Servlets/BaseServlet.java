/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sven Vervloet
 */
@WebServlet(name = "BaseServlet", urlPatterns = {"/BaseServlet"})
public abstract class BaseServlet extends HttpServlet {
      
    public int CheckLanguage(HttpServletRequest request) {
        String language = "1";
        Cookie cookies[] = request.getCookies();

        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals("language")) {
                    language = c.getValue();
                }
            }
        } 
        return Integer.parseInt(language);
    }
    
    public void setLanguageCookie(HttpServletRequest request, HttpServletResponse response, String language) throws IOException {
        Cookie cookie = new Cookie("language", language);
        cookie.setMaxAge(365 * 24 * 60 * 60); // one year
        
        response.addCookie(cookie);
      
        response.sendRedirect(request.getHeader("referer"));
    }

}
