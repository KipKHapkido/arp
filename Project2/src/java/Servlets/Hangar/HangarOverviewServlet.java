/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Hangar;

import Logic.ManageHangarLogic;
import Models.ViewModel.HangarVM;
import Servlets.BaseServlet;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jelle
 */
public class HangarOverviewServlet extends BaseServlet {

    ManageHangarLogic logic = new ManageHangarLogic();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();     
        HangarVM vm = (HangarVM)session.getAttribute("VM");
        if(vm==null){            
            vm = new HangarVM(super.CheckLanguage(request));
        }
        else{
            session.removeAttribute("VM");
        }
        request.setAttribute("VM", vm );
        request.setAttribute("Hangars", logic.getAll());        
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Hangar/HangarOverviewJsp.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HangarVM vm = new HangarVM(super.CheckLanguage(request));
        vm = logic.OverviewAction(vm, request.getParameter("submit"));
        if (vm.getFeedback() == HangarVM.LogicAction.CANCEL) {
            response.sendRedirect(request.getContextPath() + "/Airport");
            return;
        } else if (vm.getFeedback() == HangarVM.LogicAction.DELETE) {
            HttpSession session=request.getSession();  
            session.setAttribute("VM",vm);  
            response.sendRedirect(request.getContextPath()+"/HangarOverview");
            //request.setAttribute("Hangars", logic.getAll());
            //request.setAttribute("VM", vm);
            //RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/ManageHangar/HangarOverviewJsp.jsp");
            //rd.forward(request, response);
        } else {
            request.setAttribute("VM", vm);
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Hangar/HangarSaveJsp.jsp");
            rd.forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
