/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Hangar;

import Logic.ManageHangarLogic;
import Models.ViewModel.HangarVM;
import Servlets.BaseServlet;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jelle
 */
public class HangarSaveServlet extends BaseServlet {
    
    ManageHangarLogic logic = new ManageHangarLogic();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {    
       
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HangarVM vm = new HangarVM(super.CheckLanguage(request));
        vm = logic.Save(vm, request.getParameter("submit"), request.getParameter("id"), 
                request.getParameter("name"), request.getParameter("capacity"), request.getParameter("airport"));
        if(vm.getFeedback() == HangarVM.LogicAction.ERRORSAVE||
                vm.getFeedback()==HangarVM.LogicAction.WRONGSAVE){
            request.setAttribute("VM", vm);
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Hangar/HangarSaveJsp.jsp");
            rd.forward(request, response);
        }else{
            HttpSession session=request.getSession();  
            session.setAttribute("VM",vm);  
            //request dispatcher geen oplossing dan is refresh altijd opnieuw doen 
            response.sendRedirect(request.getContextPath()+"/HangarOverview");  
            
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
