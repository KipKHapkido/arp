/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Passenger;

import Models.Domain.Passenger;
import Models.Domain.Flight;
import Models.ViewModel.CreatePassengerVM;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Logic.Passenger.PassengerRead;
import javax.servlet.RequestDispatcher;
import Servlets.BaseServlet;

/**
 *
 * @author Kristof
 */
public class DeleteFlightByPassengerServlet extends BaseServlet {

    Passenger tempPass = new Passenger();

    Flight linkedFlights = new Flight();
    PassengerRead passRead = new PassengerRead();

    int passId = 0;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //int id = Integer.parseInt(request.getParameter("submit2"));
        int id = 0;
        String x = request.getParameter("PasId");
        if (x != null) {
            id = Integer.parseInt(request.getParameter("PasId"));
        } else {
            id = passId;
        }


        tempPass = passRead.SearchPassById(id);

        request.setAttribute("Flights", passRead.SearchFlightByPassengerID(id));
        request.setAttribute("passenger_id", tempPass.getID());
        request.setAttribute("passenger_firstname", tempPass.getFirstName());
        request.setAttribute("passenger_lastname", tempPass.getSurName());

        int language = super.CheckLanguage(request);
        request.setAttribute("VM", new CreatePassengerVM(language));

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Passenger/SearchFlightsByPassenger.jsp");
        rd.forward(request, response);
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int flightId = Integer.parseInt(request.getParameter("flight_id"));
        int passengerId = Integer.parseInt(request.getParameter("passenger_id"));
        passId = passengerId;

        passRead.DeleteLinkedFlights(flightId, passengerId);

        doGet(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
