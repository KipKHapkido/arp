package Servlets.Passenger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import DAO.PassengerDAO;
import DAO.AddressDAO;
import Logic.AddressLogic;
import Models.Domain.Address;
import Models.Domain.Passenger;
import Models.ViewModel.UpdatePassengerVM;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Logic.Passenger.EditPassengerLogic;
import Servlets.BaseServlet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.text.DateFormat;
import java.text.ParseException;
//import Models.Datamodels.Address;

/**
 *
 * @author Jo
 */
public class UpdatePassengerServlet extends BaseServlet {
    
    Passenger myPassenger = new Passenger();
    PassengerDAO _myPassengerDAO = new PassengerDAO(myPassenger);
    Address myAddress = new Address();
    AddressDAO _myAddressDAO = new AddressDAO();
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int language = super.CheckLanguage(request);
        
        request.setAttribute("Action", "UpdatePassenger");
        
        String passengerId = request.getParameter("submit"); 
        int id = Integer.parseInt(passengerId);
        
        myPassenger = _myPassengerDAO.GetByID(id);
        myAddress = _myAddressDAO.GetByID(myPassenger.getAddressID());
        
        UpdatePassengerVM _updatePassengerVM = new UpdatePassengerVM(language);
        _updatePassengerVM.setId(myPassenger.getID());
        _updatePassengerVM.setFirstName(myPassenger.getFirstName());
        _updatePassengerVM.setSurName(myPassenger.getSurName());
        _updatePassengerVM.setBirthday(myPassenger.getBirthday());
        request.setAttribute("VM", _updatePassengerVM);
        
        request.setAttribute("address_id", Integer.toString(myAddress.getID()));
        request.setAttribute("address_street", myAddress.getStreet());
        request.setAttribute("passenger_number", myAddress.getNumber());
        request.setAttribute("address_location", myAddress.getTown());
        request.setAttribute("address_country", myAddress.getCountry());
        request.setAttribute("address_zipCode", myAddress.getZip_code());
        
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Passenger/UpdatePassengerJsp.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int language = super.CheckLanguage(request);
        request.setAttribute("VM", new UpdatePassengerVM(language));
        
        Address myAddress = new Address();
        AddressLogic addressLogic = new AddressLogic();
        int addressId;
        
        EditPassengerLogic passengerLogic = new EditPassengerLogic();
        
        //Address
        myAddress.setID(Integer.parseInt(request.getParameter("address_id")));
        myAddress.setStreet(request.getParameter("passenger_street"));
        myAddress.setTown(request.getParameter("passenger_location"));
        myAddress.setZip_code(request.getParameter("passenger_zip"));
        myAddress.setNumber(request.getParameter("passenger_number"));
        myAddress.setCountry(request.getParameter("passenger_country"));
        addressId = Integer.parseInt(request.getParameter("address_id"));
        
        //BirthDay
        Date mybirthDay = null;
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
//        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
        String dateTest = request.getParameter("passenger_birthday");
        
        try {
            mybirthDay = df.parse(request.getParameter("passenger_birthday"));
        } catch (ParseException ex) {
            Logger.getLogger(UpdatePassengerServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
//        try {
//            mybirthDay = df2.parse(request.getParameter("passenger_birthday"));
//        } catch (ParseException ex) {
//            Logger.getLogger(UpdatePassengerServlet.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        try {
            passengerLogic.PassengerUpdate(
                    Integer.parseInt(request.getParameter("passenger_id")),
                    request.getParameter("passenger_firstName"),
                    request.getParameter("passenger_surName"),
                    mybirthDay,
                    addressId,
                    1); //typeId hard coded
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UpdatePassengerServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        addressLogic.Update(myAddress);
        
        PassengerReadServlet passRead = new PassengerReadServlet();
        passRead.doGet(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
