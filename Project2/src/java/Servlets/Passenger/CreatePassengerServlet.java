/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Passenger;

import Logic.AddressLogic;
import Logic.Passenger.PassengerCreate;
import Models.Domain.Address;
import Models.Domain.Message;
import Models.ViewModel.CreatePassengerVM;
import Servlets.BaseServlet;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ruben Veris && SamD
 */
public class CreatePassengerServlet extends BaseServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int language = super.CheckLanguage(request);
        request.setAttribute("VM", new CreatePassengerVM(language));

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Passenger/CreatePassengerJsp.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Address address = new Address();
        AddressLogic addressLogic = new AddressLogic();
        PassengerCreate passengerCL = new PassengerCreate();
        boolean validAddress;
        int addressId;

        int language = super.CheckLanguage(request);

        CreatePassengerVM VM = new CreatePassengerVM(language);

        //Address
        address.setStreet(request.getParameter("passenger_street"));
        address.setTown(request.getParameter("passenger_location"));
        address.setZip_code(request.getParameter("passenger_zip"));
        address.setNumber(request.getParameter("passenger_number"));
        address.setCountry(request.getParameter("passenger_country"));
        validAddress = addressLogic.Validate(address);
        addressId = addressLogic.Create(address);

        if (validAddress) {

            try {
                //passenger
                passengerCL.PassengerCreate(
                        request.getParameter("passenger_firstName"),
                        request.getParameter("passenger_surName"),
                        request.getParameter("passenger_birthday"),
                        addressId
                );
                VM.addMessage("Succes", Message.MessageSeverity.SUCCESS);
            } catch (Exception ex) {
                Logger.getLogger(CreatePassengerServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
        {
            VM.addMessage("Error", Message.MessageSeverity.ERROR);
        }
        request.setAttribute("VM", VM);        
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Passenger/CreatePassengerJsp.jsp");
        rd.forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
