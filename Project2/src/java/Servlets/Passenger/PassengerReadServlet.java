/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Passenger;

import Logic.AddressLogic;
import Logic.Passenger.PassengerRead;
import Models.Domain.Passenger;
import Models.ViewModel.BookFlightVM;
import Models.ViewModel.CreatePassengerVM;
import Servlets.BaseServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Stijn
 */
//@WebServlet(name = "PassengerReadServlet", urlPatterns = {"/PassengerRead"})
public class PassengerReadServlet extends BaseServlet {

    

    // <editor-fold defaultstate="" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    PassengerRead passRead = new PassengerRead();
    AddressLogic addrLogic = new AddressLogic();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        ArrayList<Passenger> allPass = new ArrayList<>();
        try 
        {
            allPass = passRead.GetAll();
        } 
        catch (SQLException | ClassNotFoundException ex) 
        {
            Logger.getLogger(PassengerReadServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.setAttribute("PassList", allPass);
        
        int language = super.CheckLanguage(request);
        request.setAttribute("VM", new CreatePassengerVM(language));
        
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Passenger/PassengersViewJsp.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {        
        String result = request.getParameter("submit");
        
        switch (result)
        {
            case "Create": CreatePassengerServlet pass = new CreatePassengerServlet();
                pass.doGet(request, response);
                break;
            case "BookFlight": 
                Passenger p = passRead.SearchPassById(Integer.parseInt(request.getParameter("pid")));
                
                BookFlightVM vm = new BookFlightVM(this.CheckLanguage(request), p);
                vm.setDate(new Date());
                request.setAttribute("VM", vm);
                request.setAttribute("action", "PickFlightDate");
                RequestDispatcher rd = request.getRequestDispatcher("/PickFlightDate");
                rd.forward(request, response);
                break;
            case "Search": 
                String ID = request.getParameter("ID");
                String FN = request.getParameter("FirstName");
                String SN = request.getParameter("Surname");
                
                ArrayList<Passenger> SearchPass = new ArrayList<>();
                Map<Integer, String> AddList = new HashMap<>();
                
                try 
                {
                    SearchPass = passRead.SearchPass(ID, FN, SN);
                    
                } 
                catch (Exception ex) 
                {
                    Logger.getLogger(PassengerReadServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("PassList", SearchPass);
                request.setAttribute("AddList", AddList);
                
                int language = super.CheckLanguage(request);
                request.setAttribute("VM", new CreatePassengerVM(language));

                rd = request.getRequestDispatcher("/WEB-INF/Passenger/PassengersViewJsp.jsp");
                rd.forward(request, response);
                break;
                
            case "SFlights":
                DeleteFlightByPassengerServlet flightDell = new DeleteFlightByPassengerServlet();
                flightDell.doGet(request, response);
                break;
                
            default:
                UpdatePassengerServlet passUpdate = new UpdatePassengerServlet();
                passUpdate.doGet(request, response);
                break;      
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "This servlet makes sure the application can find a passenger.";
    }// </editor-fold>

}
