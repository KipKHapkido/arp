/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.Crew;

import Logic.CrewLogic;
import Servlets.BaseServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Jelle
 */
@WebServlet(name = "CrewOverviewServlet", urlPatterns = {"/CrewOverview"})
public class CrewOverviewServlet extends BaseServlet {

    CrewLogic CL = null;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + "/SearchFlight");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CL = new CrewLogic(super.CheckLanguage(request));
        
        try {
            CL.CrewData(request.getParameter("ID"), request.getParameter("Submit"), request.getParameter("EmployeeID"));
        } catch (SQLException ex) {
            Logger.getLogger(CrewOverviewServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        request.setAttribute("VM", CL.getcVM());
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Crew/CrewOverviewJsp.jsp");
        rd.forward(request, response);

    }

}
